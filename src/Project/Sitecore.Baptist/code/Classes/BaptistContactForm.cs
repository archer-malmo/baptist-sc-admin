using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data;
using Sitecore.Diagnostics;
using Sitecore.WFFM.Abstractions.Actions;
using Sitecore.WFFM.Abstractions.Mail;
using System.Text.RegularExpressions;

namespace Sitecore.Baptist.Website.Classes
{
    public class BaptistContactForm
    {
        public void Process(ProcessMessageArgs args) {

            if(args.FormID == ID.Parse("{342D3BC6-0E53-401E-BCE0-2084D3203110}"))
            {

                if(args.Fields != null)
                {
                    string fields = Newtonsoft.Json.JsonConvert.SerializeObject(args.Fields);
                    Log.Info("WCContactForm message " + fields, this);
                }

                if(args.Host != null)
                {
                    string hostinfo = Newtonsoft.Json.JsonConvert.SerializeObject(args.Host);
                    Log.Info("WCContactForm host " + hostinfo, this);
                }

                /*if(args.Properties != null)
                {
                    string properties = Newtonsoft.Json.JsonConvert.SerializeObject(args.Properties);
                    Log.Info("WCContactForm properties " + properties, this);
                }*/

                //var locat = args.Fields.GetEntryByName("Location department");
                var locat = args.Fields.GetEntryByID(ID.Parse("{65E81D70-824E-4729-BDEE-6E4C1476781C}"));

                //var email = locat.Value.Split('|')[0];
                var email = locat.Value;

                var subject = args.Fields.GetEntryByID(ID.Parse("{265D323E-CEE0-4AF7-988C-32427FA54EBB}"));

                var selsubject = subject.Value;

                args.To.Clear().Append(email);

                /*if (email != "helpdesk@bmhcc.org" && selsubject != "Patient Complaint" && selsubject != "Baptist Medical Group (doctors)")
                {
                    args.To.Append("; helpdesk@bmhcc.org");
                }*/

                args.CC.Clear();
                args.BCC.Clear();

                /*if (selsubject != "Patient Complaint")
                {
                    args.BCC.Clear().Append("Darnell.Settles@bmhcc.org");
                }*/

                switch(selsubject) {

                    case "Baptist One Care / Medical Record":
						args.Subject.Clear().Append("BOL Contact Us – Baptist OneCare");
                        if(email.ToLower() == "webmaster@bmg.md"){
                            args.To.Clear().Append("helpdesk@bmhcc.org");
                        }
                        break;
                    case "Baptist One Care/My Chart Technical Issue":
                        args.Subject.Clear().Append("BOL Contact Us – Baptist OneCare");
                        if(email.ToLower() == "webmaster@bmg.md"){
                            args.To.Clear().Append("helpdesk@bmhcc.org");
                        }
                        break;
                    case "Hospital Pre-registration":
                        args.BCC.Append("Tommy.Cloar@bmhcc.org;");
						args.Subject.Clear().Append("BOL Contact Us – Hospital Pre-registration");
                        break;
                    case "Baptist Medical Group (doctors)":
                        args.To.Clear();
                        if (email != "helpdesk@bmhcc.org")
                        {
                            args.To.Append(email);
                        }
                        args.To.Append("; webmaster@bmg.md");
						args.Subject.Clear().Append("BOL Contact Us – Baptist Medical Group");
                        break;
                    case "Patient Complaint":
                        //See Patient Complaint section below
                        break;
                    case "Patient Care":
                        //See Patient Complaint section below
                        break;
                    case "Jobs":
                        //See Location HR eamil section
                        string hremailj = selectHRRecipient(email);
                        args.To.Append("; " + hremailj);
                        break;
                    case "Careers":
                        //See Location HR eamil section
                        string hremail = selectHRRecipient(email);
                        args.To.Append("; " + hremail);
                        break;
                    case "Employee Discount Program":
                        args.To.Append("; webmaster@bmhcc.org");
                        break;
                    case "Website":
                        args.To.Append("; webmaster@bmhcc.org");
                        break;
                    case "Billing Issue":
                        if(email == "infoneajonesboro@bmhcc.org"){
                            args.To.Clear().Append("customerservicenea@bmhcc.org");
                        }else{
                            args.To.Clear().Append("customerservice@bmhcc.org");
                        }
                        break;
                    case "Other":
                        args.To.Append("; webmaster@bmhcc.org");
                        args.CC.Clear().Append("helpdesk@bmhcc.org");
                        args.BCC.Clear().Append("Darnell.Settles@bmhcc.org");
                        break;
                }

                if (selsubject == "Patient Complaint" || selsubject == "Patient Care")
                {
                    string patientComplaintSubj = "BOL Contact Us – Patient Complaint at ";
                    args.To.Clear().Append(email);
                    args.CC.Clear();
                    args.BCC.Clear();
                    args.Subject.Clear();

                    string locationNameSubj = "";
                    switch (email)
                    {
                        case "info.collierville@bmhcc.org":
                            locationNameSubj = "Collierville";
                            break;
                        case "info.childrens@bmhcc.org":
                            locationNameSubj = "Children’s";
                            break;
                        case "info.womens@bmhcc.org":
                            locationNameSubj = "Women’s";
                            break;
                        case "info.restorativecare@bmhcc.org":
                            locationNameSubj = "Restorative Care";
                            break;
                        case "info.memphis@bmhcc.org":
                            locationNameSubj = "Memphis";
                            break;
                        case "info.desoto@bmhcc.org":
                            locationNameSubj = "DeSoto";
                            break;
                        case "info.crittenden@bmhcc.org":
                            locationNameSubj = "Crittenden";
                            break;
                        case "info.tipton@bmhcc.org":
                            locationNameSubj = "Tipton";
                            break;
                        case "info.northmiss@bmhcc.org":
                            locationNameSubj = "North Mississippi";
                            break;
                        case "info.unioncounty@bmhcc.org":
                            locationNameSubj = "Union County";
                            break;
                        case "info.booneville@bmhcc.org":
                            locationNameSubj = "Booneville";
                            break;
                        case "infoneajonesboro@bmhcc.org":
                            locationNameSubj = "NEA";
                            break;
                        case "info.calhoun@bmhcc.org":
                            locationNameSubj = "Calhoun";
                            break;
                        case "info.huntingdon@bmhcc.org":
                            locationNameSubj = "Carroll County";
                            break;
                        case "info.unioncity@bmhcc.org":
                            locationNameSubj = "Union City";
                            break;
                        case "info.goldentriangle@bmhcc.org":
                            locationNameSubj = "Golden Triangle";
                            break;
                        case "baptisthealthline+attala@bmhcc.org":
                            args.To.Clear().Append("baptisthealthline@bmhcc.org");
                            locationNameSubj = "Attala";
                            break;
                        case "baptisthealthline+yazoo@bmhcc.org":
                            args.To.Clear().Append("baptisthealthline@bmhcc.org");
                            locationNameSubj = "Yazoo";
                            break;
                        case "baptisthealthline+leake@bmhcc.org":
                            args.To.Clear().Append("baptisthealthline@bmhcc.org");
                            locationNameSubj = "Leake";
                            break;
                        case "baptisthealthline+jackson@bmhcc.org":
                            args.To.Clear().Append("baptisthealthline@bmhcc.org");
                            locationNameSubj = "Mississippi Baptist";
                            break;
                        case "webmaster+corporate@bmhcc.org":
                        	args.To.Clear().Append("webmaster@bmhcc.org");
                        	locationNameSubj = "Baptist Memorial Health Care Corporation (Corporate Office)";
                        	break;
                        default:
                            args.To.Clear().Append("WebMarketing@bmhcc.org");
                            locationNameSubj = "";
                            break;
                    }

                    args.Subject.Append(patientComplaintSubj + locationNameSubj);
                }

                    //Add Diane on Mississippi
                    /* if (Regex.IsMatch(email,"^(attala|yazoo|jackson|leake)$", RegexOptions.IgnoreCase)) {
                    args.BCC.Append("; baptisthealthline@bmhcc.org");
                }*/

                //args.BCC.Clear().Append("dtaylor@archermalmo.com");
                //args.BCC.Append("; mbrandt@archermalmo.com");

                if(HttpContext.Current != null && !Regex.IsMatch(HttpContext.Current.Request.ServerVariables["HTTP_HOST"], @"/^(www\.baptistonline\.org|www\.baptistdoctors\.org|www\.baptistcancercenter\.com)$/"))
                {
					Log.Info("***** Debugging contact form submission*****", this);
					Log.Info("To: " + args.To, this);
					Log.Info("CC: " + args.CC, this);
					Log.Info("BCC: " + args.BCC, this);
					Log.Info("Subject: " + args.Subject, this);
					Log.Info("********************", this);
                    string torcpt = args.To.ToString();
                    string bccrcpt = args.BCC.ToString();
                    string ccrcpt = args.CC.ToString();
                    args.To.Clear().Append("dtaylor@archermalmo.com; jjanovetz@archermalmo.com;");
                    args.CC.Clear();
                    args.BCC.Clear();
                    string format = "<p>To RCPT: " + torcpt + "</p>";
                    format += "<p>CC RCPT: " + ccrcpt + "</p>";
                    format += "<p>BCC RCPT: " + bccrcpt + "</p>";
                    args.Mail.Append(format);
                }

            }
        }

        public string selectHRRecipient( string email )
        {
            string hrrecipient = "";
            switch (email.ToLower())
            {
                case "info.collierville@bmhcc.org":
                    hrrecipient = "Debby.Michael@bmhcc.org";
                    break;
                case "info.childrens@bmhcc.org":
                    hrrecipient = "Gail.Henderson@bmhcc.org";
                    break;
                case "info.womens@bmhcc.org":
                    hrrecipient = "Gail.Henderson@bmhcc.org";
                    break;
                case "info.restorativecare@bmhcc.org":
                    hrrecipient = "Leslie.Gordon@bmhcc.org";
                    break;
                case "info.memphis@bmhcc.org":
                    hrrecipient = "Dennis.Fisher@bmhcc.org; Anne.Thompson@bmhcc.org";
                    break;
                case "info.desoto@bmhcc.org":
                    hrrecipient = "Karen.Ingram@bmhcc.org; Melissa.Monhollon@bmhcc.org";
                    break;
                case "info.crittenden@bmhcc.org":
                    hrrecipient = "Stacie.Schroeppel@BMHCC.org";
                    break;
                case "info.tipton@bmhcc.org":
                    hrrecipient = "Myra.Cousar@bmhcc.org";
                    break;
                case "info.northmiss@bmhcc.org":
                    hrrecipient = "Josh.Lowery@bmhcc.org";
                    break;
                case "info.unioncounty@bmhcc.org":
                    hrrecipient = "Lori.Goode@bmhcc.org";
                    break;
                case "info.booneville@bmhcc.org":
                    hrrecipient = "LeeAnn.Ross@bmhcc.org";
                    break;
                case "infoneajonesboro@bmhcc.org":
                    hrrecipient = "James.Keller@bmhcc.org; Lanae.Word@bmhcc.org";
                    break;
                case "info.calhoun@bmhcc.org":
                    hrrecipient = "Margaret.Dye@bmhcc.org";
                    break;
                case "info.huntingdon@bmhcc.org":
                    hrrecipient = "Melody.Heyduck@bmhcc.org";
                    break;
                case "info.unioncity@bmhcc.org":
                    hrrecipient = "Nicky.Thomas@bmhcc.org";
                    break;
                case "info.goldentriangle@bmhcc.org":
                    hrrecipient = "Bob.McCallister@bmhcc.org; Bobbie.Robinson@bmhcc.org";
                    break;
                case "baptisthealthline+attala@bmhcc.org":
                    hrrecipient = "Stephanie.Washington@bmhcc.org";
                    break;
                case "baptisthealthline+yazoo@bmhcc.org":
                    hrrecipient = "Stephanie.Washington@bmhcc.org";
                    break;
                case "baptisthealthline+leake@bmhcc.org":
                    hrrecipient = "Stephanie.Washington@bmhcc.org";
                    break;
                case "baptisthealthline+jackson@bmhcc.org":
                    hrrecipient = "Greg.Baldwin@bmhcc.org; Amy.Lemmons@bmhcc.org";
                    break;
                //bmg in this case
                case "webmaster@bmg.md":
                    hrrecipient = "Amy.Pettit@bmg.md";
                    break;
                default:
                    hrrecipient = "";
                    break;
            }
            return hrrecipient;
        }
    }
}
