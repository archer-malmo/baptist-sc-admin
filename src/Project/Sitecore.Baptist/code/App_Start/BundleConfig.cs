﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace Sitecore.Baptist.Website.App_Start
{
    public class BundleConfig
    {
          public static void RegisterBundles(BundleCollection bundles) {

            //StyleBundle styles = new StyleBundle("~/Content/css/maincombined");

            //styles.Include("~/Content/bootstrap.css");

            ScriptBundle scripts = new ScriptBundle("~/Content/js/opticombined");

            scripts.Include("~/Scripts/bootstrap.js",
                "~/Content/javascript/vendor/jquery.cookie.js",
                "~/Content/javascript/locations-listing/hover-intent.js",
                "~/Content/javascript/erwait.js",
                "~/Content/javascript/erwait/er-wait-anywhere.js",
                "~/Content/javascript/locations/detail-map.js",
                "~/Content/javascript/locations-listing/change-location-button.js",
                "~/Content/javascript/locations-listing/location-listing.js",
                "~/Content/javascript/locations/clinical-trials-filter.js");
            scripts.Include("~/Content/javascript/physician/physician-profile-map.js",
                "~/Content/javascript/physician/physician-search.js");
            scripts.Include("~/Content/javascript/event/event-listing.js");
            scripts.Include("~/Content/javascript/epic/mychart-login-box.js");
            scripts.Include("~/Content/javascript/jsel.js",
                "~/Content/javascript/handlebars.js", 
                "~/Content/javascript/careers/career-search.js");
            scripts.Include("~/Content/javascript/vendor/infinite-scroll.pkgd.js");

            //bundles.Add(styles);
            bundles.Add(scripts);
            string bundleopts = Sitecore.Configuration.Settings.GetSetting("Sitecore.Baptist.Website.BundleEnableOptimizations");

            bool enableOpts = System.Convert.ToBoolean(bundleopts);

            BundleTable.EnableOptimizations = enableOpts;

          }        
    }
}