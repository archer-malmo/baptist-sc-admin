﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sitecore.Baptist.Feature.GeneralComponents.Areas.Feature_GeneralComponents.Models
{
    public class BaptistEventCalendarRenderingModel : BaptistEventListRenderingModel
    {
        public string JsonDataProperties { get; set; }
    }
}