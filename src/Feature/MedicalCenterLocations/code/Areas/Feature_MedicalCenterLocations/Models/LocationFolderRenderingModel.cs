﻿using Sitecore.Data.Items;
using Sitecore.XA.Foundation.Mvc.Models;
using System.Collections.Generic;

namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Models
{
    public class LocationFolderRenderingModel : RenderingModelBase
    {
        public virtual IEnumerable<Item> Locations { get; set; }
    }
}