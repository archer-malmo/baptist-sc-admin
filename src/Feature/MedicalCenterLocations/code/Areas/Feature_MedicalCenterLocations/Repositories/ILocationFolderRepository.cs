﻿using Sitecore.XA.Foundation.Mvc.Repositories.Base;

namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Repositories
{
    public interface ILocationFolderRepository : IModelRepository, IAbstractRepository<IRenderingModelBase>
    {

    }
}