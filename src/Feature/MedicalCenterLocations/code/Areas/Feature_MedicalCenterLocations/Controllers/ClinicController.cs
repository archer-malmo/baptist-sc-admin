﻿using Fortis.Model;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Models;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Repositories;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom;
using Sitecore.XA.Foundation.Mvc.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Controllers
{
    public class ClinicController : StandardController
    {
      protected readonly ILocationItemRepository LocationRepository;

      public ClinicController(ILocationItemRepository locationRepository)
      {
        this.LocationRepository = locationRepository;
      }

      public ActionResult ClinicIndex()
      {
        return (ActionResult)this.PartialView("ClinicIndex", this.GetModel());
      }

      protected override object GetModel()
      {
        return (object)this.LocationRepository.GetModel();
      }
  }
}