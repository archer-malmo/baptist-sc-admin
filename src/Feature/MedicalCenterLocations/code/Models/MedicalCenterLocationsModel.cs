﻿using Sitecore.XA.Foundation.RenderingVariants.Models;
using Sitecore.XA.Foundation.Variants.Abstractions.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Models
{
  public class MedicalCenterLocationsModel : VariantsRenderingModel
  {
    public virtual string StyleDisplay { get; set; }
  }
}