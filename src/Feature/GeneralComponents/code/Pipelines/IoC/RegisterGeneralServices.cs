﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Baptist.Feature.GeneralComponents.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.XA.Foundation.IOC.Pipelines.IOC;
using Sitecore.Baptist.Feature.GeneralComponents.Areas.Feature_GeneralComponents.Repositories;
using Sitecore.DependencyInjection;

namespace Sitecore.Baptist.Feature.GeneralComponents.Pipelines.IoC
{
  public class RegisterGeneralServices : IocProcessor
  {
    public override void Process(IocArgs args)
    {
      //args.ServiceCollection.AddTransient<IGeneralComponentsRepository, GeneralComponentsRepository>();
      args.ServiceCollection.AddTransient<IBaptistEventListRepository, BaptistEventListRepository>();
    }
  } 
}
