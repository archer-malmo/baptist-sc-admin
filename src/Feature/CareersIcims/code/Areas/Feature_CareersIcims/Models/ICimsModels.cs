﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace Sitecore.Baptist.Feature.CareersIcims.Areas.Feature_CareersIcims.Models
{

    public class PortalFilters {
        public string portalname { get; set; }
        public List<PortalFilter> filters { get; set; }
    }

    public class PortalFilter {
        public string name { get; set; }
        public List<FilterOption> options { get; set; }
        public string label { get; set; }
        public FilterType type { get; set; }
    }

    public class FilterOption {
        public string id { get; set; }
        public string value { get; set; }
    }

    public class FilterType {
        public string display { get; set; }
        public string id { get; set; }
    }


    public class PortalSearchResult {

        public string portalUrl { get; set; }
        public string self { get; set; }
        public int id { get; set; }
        public string updatedDate { get; set; }
    }

    public class PortalSearchResults {
        public List<PortalSearchResult> searchResults { get; set; }
        public PortalFilters portalfilters { get; set; }
        public NameValueCollection originalsearch { get; set; }
        public string portalname { get; set; }
    }

    public class ICimsJobPosting {
        public string portalname { get; set; }
        public string jobid { get; set; }
        public List<ICimsDataValue> header { get; set; }
        public List<ICimsDataValue> description { get; set; }
    }


    public class ICimsDataValue {
        public string label { get; set; }
        public string value { get; set; }
    }
    
}