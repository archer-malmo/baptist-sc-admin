﻿$(function () {
  if ($('#toggle-filters').length > 0) {
    $('#toggle-filters').click(function (event) {
      event.preventDefault();
    });
    
  }
  if ($('#view-all-job-postings').length > 0) {
    $('#view-all-job-postings').click(function (event) {
      event.preventDefault();
      $('form#job-search')
        .not(':button, :submit, :reset, :hidden')
        .val('')
        .removeAttr('checked')
        .removeAttr('selected');      
      $('form#job-search').submit();
    });
  }

  $(window).resize(function () {

    $("[data-collapse-group='save-filters']").each(function () {

      if ($(this).hasClass("collapse") == false && $(this).data("target") != "#manage-agents") {
        $(this).addClass('collapsed');
        $($(this).data("target")).removeClass("in").addClass('collapse');
      }
    });

    if ($(window).width() > 992) {
      $("#manage-agents").removeClass("collapse");
      $("#manage-agents").css('height', 'auto');
    }
  });
});