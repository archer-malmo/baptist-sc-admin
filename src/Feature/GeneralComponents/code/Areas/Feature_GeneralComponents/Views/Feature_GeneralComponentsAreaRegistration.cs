﻿using System.Web.Mvc;

namespace Sitecore.Baptist.Feature.GeneralComponents.Areas.Feature_GeneralComponents
{
    public class Feature_GeneralComponentsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Feature_GeneralComponents";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Feature_GeneralComponents_default",
                "Feature_GeneralComponents/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}