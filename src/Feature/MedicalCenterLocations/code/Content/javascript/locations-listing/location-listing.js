﻿function setmap(lat, lng, mapid) {
  var uluru = { lat: parseFloat(lat), lng: parseFloat(lng) };
  //console.log(uluru);
  //console.log(mapid);

  var mapdiv = document.getElementById(mapid);
  //console.log(mapdiv);
  var map = new google.maps.Map(document.getElementById(mapid), {
    zoom: 17,
    center: uluru
  });
  var marker = new google.maps.Marker({
    position: uluru,
    map: map
  });

  google.maps.event.addListener(map, 'tilesloaded', function () {
    $(this.getDiv()).find('img').each(function (i, eimg) {
      if (!eimg.alt || eimg.alt === '') {
        eimg.alt = 'Click to see this area on Google Maps';
      }
    });
  });
}

function setstaticmap(lat, lng, mapid, label) {

  var loc = lat + "," + lng;
  var size = "609x325";
  var center = decodeURIComponent(label);

  var marker = "&markers=color:red%7C" + center.replace('#', '');
  var apikey = "&key=AIzaSyDEnRKFd3wvn2deWnClojgqd2fcePl-Ujk";
  var mapurl = "https://maps.googleapis.com/maps/api/staticmap?zoom=17&size=" + size + marker + apikey;
  //var mapurl = "https://www.google.com/maps/embed/v1/place?q=" + center + apikey;

  var mapidsel = "img#" + mapid;

  var expandsel = 'a.location-map-expand[data-loc-id="' + mapid + '"]';
  var dirlink = llgetdirectionslink(lat, lng, label);
  console.log(dirlink);

  $(expandsel).attr('href', dirlink);

  $(mapidsel).attr('src', mapurl);
  $(mapidsel).attr('alt', 'Location Map of ' + decodeURIComponent(label));
  $(mapidsel).click(function (event) {
    window.open(llgetdirectionslink(lat, lng, label));
  });

}

function initMap() {
  var uluru = { lat: -25.363, lng: 131.044 };
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 17,
    center: uluru
  });
  var marker = new google.maps.Marker({
    position: uluru,
    map: map
  });
}

function updatemaplocation(location, staticmap) {

  //add in static maps
  if (staticmap === undefined) {
    staticmap = false;
  }

  if (location === undefined) {
    return;
  }

  var lat, lng, locname, mapid, clientid;

  lat = location.find('input[name=location-lat]').val();
  lng = location.find('input[name=location-lng]').val();
  locname = location.find('input[name="location-name"]').val();
  mapid = location.data('mapid');
  clientid = location.data('clientid');

  //console.log(mapid);
  //console.log(lat);
  //console.log(lng);

  if (mapid === undefined || lat === undefined || lng === undefined) {
    return;
  }

  if (staticmap == false) {
    setmap(lat, lng, mapid);
  } else if (staticmap == true) {
    setstaticmap(lat, lng, mapid, locname);
  }

  //$('a.location-directions-link').attr('href', getdirectionslink(lat, lng, locname));
  var expandsel = 'a.location-map-expand[data-loc-id="' + mapid + '"]';
  $(expandsel).attr('href', llgetdirectionslink(lat, lng, locname));

  /*$('html, body').animate({
    scrollTop: $('#' + mapid).offset().top
  }, 2000);*/
}



function llgetdirectionslink(lat, lng, locname) {
  var lllinkpath = "https://www.google.com/maps/place/" + locname;
  return lllinkpath;
  //return "https://www.google.com/maps/place/" + locname + "/@" + lat + "," + lng + ",17z";
}

$('li.row[data-clientid]').hoverIntent(function (event) {
  updatemaplocation($(this), true);
});

$(function () {
  var listings = $('div[data-rendering-id]');

  listings.each(function () {
    var startmap = $(this).find('ul li.row[data-clientid]').first();
    setTimeout(function () {
      updatemaplocation(startmap, true)
    }, 600);
    
    var ctxtSel = 'input[name=schedule-filter][data-tab-context="' + $(this).attr('data-rendering-id') + '"]';
    var schdFilt =  $(ctxtSel);
    //console.log(schdFilt);

    if (schdFilt.length > 0) {
      console.log("bound", ctxtSel);
      bindschedulefilter( schdFilt );
    }

    if (schdFilt.prop('checked') == 1) {
      schdFilt.change();
    }

  });
});

function bindschedulefilter(element) {
  var schdFilter = $(element);
  schdFilter.on('change', function () {
    var dsc = $(this).attr('data-tab-context');

    var dsel = 'div[data-rendering-id="' + dsc + '"] ul li:not([data-scheduling-available])';
    //console.log(dsel);
    //console.log($(this).prop('checked'));
    //console.log($(dsel));
    if ($(this).prop('checked') == 1) {
      $(dsel).hide();
      //console.log('hide');
    } else {
      $(dsel).show();
      //console.log('show');
    }
  });
}