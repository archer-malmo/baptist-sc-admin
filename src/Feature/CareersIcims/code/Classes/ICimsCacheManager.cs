﻿using Sitecore.Baptist.Foundation.Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sitecore.Baptist.Feature.CareersIcims.Classes
{
    public static class ICimsCacheManager
    {
        private static readonly BaptistCustomCache Cache;

        static ICimsCacheManager() {
            Cache = new BaptistCustomCache("ICims_Job_Post_Response_Cache", StringUtil.ParseSizeString("100MB"));
        }

        public static string GetCache( string key )
        {
            return Cache.GetString(key);
        }

        public static void SetCache( string key, string value )
        {
            Cache.SetString(key, value);
        }        
    }
    
}