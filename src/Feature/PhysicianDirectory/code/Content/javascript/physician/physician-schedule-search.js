﻿var totalresults, loadcount, rows;

var nextURL = $('.pagination__next').attr('href');

var scrollopts = {
  debug: false,
  path: function () {
    loadcount = this.loadCount;
    //console.log('load count', loadcount);
    return nextURL;
  },
  append: '.searchable',
  history: false,
  scrollThreshold: false,
  status: '.page-load-status',
  button: '.pagination__next'
};


var $iscroll = $('.search-results').infiniteScroll(scrollopts);
$iscroll.on('load.infiniteScroll', function (event, response) {
  updateNextURL(response, this);
});

var searchbox = $('input.search-box-input-baptist.has-search-icon');
searchbox.off('keydown');

var specialfilt = $('select#filter-by-specialty');
var sboxbutton = $('.search-box-button');

sboxbutton.off('click');
sboxbutton.on('click', function (e) {
  e.preventDefault();
  //console.log("Specialty Filter:", specialfilt.val());
  //console.log("Search Box:", searchbox.val());

  $('.search-results *, .pagination__next').fadeOut().remove();
  $('.page-load-status *').css('display', 'none');

  var response = dorequest(searchbox, specialfilt, $iscroll, scrollopts);

});

specialfilt.off('change');
specialfilt.on('change', function () {
  sboxbutton.click();
});

function updateNextURL(doc, instance) {
  totalresults = $(doc).find('input[type=hidden]#total_results').val();
  rows = $(doc).find('input[type=hidden]#rows').val();

  lc = loadcount;
  if ((loadcount == 0)) {
    lc = 1;
  }

  if ((lc * rows) >= (totalresults - 1)) {
    $(instance).trigger("last");
    $('.pagination__next').hide();
    $('.infinite-scroll-last').show();
  }

  nextURL = $(doc).find('.pagination__next').attr('href');
}

function dorequest(searchbox, specialfilt, appendto, isoptions) {
  var query = "";

  if (searchbox.val() != "" && searchbox.val() != undefined) {
    query = searchbox.val();
  } else {
    query = "False";
  }

  searchurl = "/api/sitecore/Physician/SearchPhysicians?q=" + query + "&radius=3000&listed_providers=True&featured_providers=True&paging=True&rows=12&start=0";
  //console.log(specialfilt);
  if (specialfilt.val() != "" && specialfilt != undefined) {
    searchurl += "&scheduling_specialties=" + specialfilt.val();
  }


  //console.log(searchurl);
  var resp = false;
  return $.get(searchurl,
    function (data) {
      //console.log('success', data);
      var hold = $.parseHTML("<div>" + data + "</div>");

      items = $(hold).find('.searchable');
      afteritems = $(hold).find('.pagination__next');
      nextURL = afteritems.attr('href');

      totalresults = $(hold).find('input[type=hidden]#total_results').val();

      rows = $(hold).find('input[type=hidden]#rows').val();

      //kill iscroll
      appendto.infiniteScroll('destroy');

      appendto.append(items);
      appendto.after(afteritems);
     
      //console.log('nextULR', nextURL);      

      resp = true;
    },
    'html')
      .fail(function (response) {
        console.log('fail', response);
        resp = false;
      })
        .done(function () {

          appendto.infiniteScroll(isoptions);
          appendto.on('load.infiniteScroll', function (event, response) {
            updateNextURL(response, this);
          });

          lc = loadcount;
          if ((loadcount == 0)) {
            lc = 1;
          }

          //console.log('total results', totalresults);
          //console.log('rows', (lc * rows));

          if ((lc * rows) >= (totalresults - 1)) {
            $(appendto).trigger("last");
            $('.pagination__next').hide();
            $('.infinite-scroll-last').show();
          }

          return resp
        });

}