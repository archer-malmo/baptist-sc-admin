﻿using Sitecore.XA.Foundation.Variants.Abstractions.Pipelines.GetVelocityTemplateRenderers;
using Sitecore.Baptist.Foundation.Common.Variants.NVelocityExtensions;

namespace Sitecore.Baptist.Foundation.Common.Variants.Pipelines.GetVelocityTemplateRenderers
{
    public class AddTemplateRenderers : IGetTemplateRenderersPipelineProcessor
    {
        public void Process( GetTemplateRenderersPipelineArgs args )
        {
            args.Context.Put("linkTool", new LinkTool());
        }
    }
}