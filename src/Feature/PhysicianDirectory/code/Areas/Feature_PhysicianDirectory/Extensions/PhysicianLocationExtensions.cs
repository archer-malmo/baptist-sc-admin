﻿using Sitecore.Baptist.Feature.PhysicianDirectory.Areas.Feature_PhysicianDirectory.Models;
using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Baptist.Foundation.Common.Utils;

namespace Sitecore.Baptist.Feature.PhysicianDirectory.Areas.Feature_PhysicianDirectory.Extensions
{
    public static class PhysicianLocationExtensions
    {
        public static string getPhysicianLocationUrl(this PhysicianLocation pl, string locationPath = "/sitecore/content//home/location/", string urlPrefix = "/locations/", string databaseName = "web") {

            string locurl = urlPrefix;
                        
            string query = "/sitecore/content//home/locations//*[@Location='" + pl.ItemID + "']";
            Database db = Database.GetDatabase(databaseName);
            Item locationpage = db.SelectSingleItem(query);

            if(locationpage == null)
            {
                locurl = "false";
            }
            else
            {
                locurl = Sitecore.Links.LinkManager.GetItemUrl(locationpage);
            }

            return locurl;
        }

        public static string getOtherLocationUrl(this PhysicianLocation pl, string databaseName = "web") {

            Database db = Database.GetDatabase(databaseName);
            Item locationitem = db.GetItem(pl.ItemID);

            string locationurl = "";
            string mapurl = "https://www.google.com/maps/place/" + pl.LocationName +  "/@" + pl.Latitude + "," + pl.Longitude + ",17z";

            if(locationitem["Override With Maps Link"] == "1") {
                locationurl = mapurl;
                return locationurl;
            }

            if(locationitem["Enable Alternate Link"] == "1"  && locationitem["Alternate Location Listing Page"].HasValue()) {
                locationurl = locationitem["Alternate Location Listing Page"];                
            }

            if(locationitem["Location Type"] == "Minor Med" || locationitem["Location Type"] == "Specialty") {
                string query = "/sitecore/content//home/locations//*[@Location='" + pl.ItemID + "']";
                Item locationpage = db.SelectSingleItem(query);

                if(locationpage == null) {
                    locationurl = mapurl;
                }else {
                    locationurl = Sitecore.Links.LinkManager.GetItemUrl(locationpage);
                }
            }
            else {
                locationurl = mapurl;                
            }
            return locationurl;
        }
    }
}