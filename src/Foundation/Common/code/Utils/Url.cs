﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Sitecore.Baptist.Foundation.Common.Utils
{
    public static class Url
    {
        public static string GenerateSlug( this string phrase )
        {            
            string str = phrase.RemoveAccent().ToLower();
            // invalid chars           
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            // convert multiple spaces into one space   
            str = Regex.Replace(str, @"\s+", " ").Trim();
            // cut and trim 
            str = str.Substring(0, str.Length <= 45 ? str.Length : 45).Trim();
            str = Regex.Replace(str, @"\s", "-"); // hyphens   
            return str;
        }

        public static string RemoveAccent( this string txt )
        {
            byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(txt);
            return System.Text.Encoding.ASCII.GetString(bytes);
        }

        public static String toSlug( this string text )
        {
            String value = text.Normalize(NormalizationForm.FormD).Trim();
            StringBuilder builder = new StringBuilder();

            foreach(char c in text.ToCharArray())
                if(CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
                    builder.Append(c);

            value = builder.ToString();

            byte[] bytes = Encoding.GetEncoding("Cyrillic").GetBytes(text);

            value = Regex.Replace(Regex.Replace(Encoding.ASCII.GetString(bytes), @"\s{2,}|[^\w]", " ", RegexOptions.ECMAScript).Trim(), @"\s+", "-");

            return value.ToLowerInvariant();
        }        

        public static String poplast( this string text)
        {
            if(text.hasQuery())
            {
                text = text.Replace(text.popquery(), "");
            }

            int max = text.Length;

            int last = text.LastIndexOf('/') + 1;            
            int leng = max - last;

            return text.Substring(last, leng);
        }        

        public static String popquery( this string text)
        {
            int max = text.Length;

            int last = text.LastIndexOf('?');
            int leng = max - last;

            return text.Substring(last, leng);
        }        

        public static bool hasQuery( this string text)
        {
            if(text.Contains("?")) {
                return true;
            }else {
                return false;
            }
        }
    }
}