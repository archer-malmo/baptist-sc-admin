﻿using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Models;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using Sitecore.XA.Foundation.RenderingVariants.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Fields;
using Fortis.Model;
using static Sitecore.Configuration.Settings;
using Sitecore.XA.Foundation.Multisite;
using System.Collections.Specialized;
using Sitecore.Data.Items;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.XA.Foundation.Multisite.LinkManagers;
using Sitecore.ContentSearch.Spatial.DataTypes;
using Sitecore.XA.Foundation.Search.Models;

namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Repositories
{

    public class LocationItemRepository : ModelRepository, ILocationItemRepository, IModelRepository, IAbstractRepository<IRenderingModelBase>
    {

        public Item LocationItem { get; set; }

        public override IRenderingModelBase GetModel()
        {
            Models.LocationItemRenderingModel locationRenderingModel = new Models.LocationItemRenderingModel();
            this.FillBaseProperties((object)locationRenderingModel);

            //set location section fields
            locationRenderingModel.ItemID = this.GetItemID();
            locationRenderingModel.LocationName = this.GetLocationName();
            locationRenderingModel.TermName = this.GetTermName();
            locationRenderingModel.ShortName = this.GetShortName();
            locationRenderingModel.AddressLine1 = this.GetAddressLine1();
            locationRenderingModel.AddressLine2 = this.GetAddressLine2();
            locationRenderingModel.City = this.GetCity();
            locationRenderingModel.State = this.GetState();
            locationRenderingModel.PostalCode = this.GetPostalCode();
            locationRenderingModel.Latitude = this.GetLatitude();
            locationRenderingModel.Longitude = this.GetLongitude();
            locationRenderingModel.Coordinates = this.GetCoordinates();

            //set location meta section
            locationRenderingModel.ClientID = this.GetClientID();
            locationRenderingModel.IsPubliclySearchable = this.GetIsPubliclySearchable();
            locationRenderingModel.IsActive = this.GetIsActive();
            locationRenderingModel.IsBMG = this.GetIsBMG();
            locationRenderingModel.ImportTaxonomy = this.GetImportTaxonomy();
            locationRenderingModel.ImportProperties = this.GetImportProperties();
            locationRenderingModel.LocationType = this.GetLocationType();

            //set location contact section
            locationRenderingModel.MainPhoneNumber = this.GetMainPhoneNumber();
            locationRenderingModel.MainEmailAddress = this.GetMainEmailAddress();
            locationRenderingModel.AdditionalContactInformation = this.GetAdditionalContactInformation();
            locationRenderingModel.EmergencyRoomPhoneNumber = this.GetEmergencyRoomPhoneNumber();

            //set location content section
            locationRenderingModel.Title = this.GetTitle();
            locationRenderingModel.MainDescription = this.GetMainDescription();

            //set location hours 
            locationRenderingModel.HoursSectionTitle = this.GetHoursSectionTitle();
            locationRenderingModel.HoursOfOperation = this.GetHoursOfOperation();
            locationRenderingModel.HoursInfoMessage = this.GetHoursInfoMessage();
            locationRenderingModel.ShowLocationHours = this.GetShowLocationHours();

            //set location list overrides
            locationRenderingModel.AlternateLocationListingPage = this.GetAlternateLocationListingPage();
            locationRenderingModel.EnableAlternateLink = this.GetEnableAlternateLink();
            locationRenderingModel.OverrideWithMapsLink = this.GetOverrideWithMapsLink();

            //ER Settings
            locationRenderingModel.ERWaitURLKey = this.GetERWaitURLKey();

            //Baptist Cancer Centers
            locationRenderingModel.ServicesProvidedJson = this.GetServicesProvidedJson();
            locationRenderingModel.CancerTypesJson = this.GetCancerTypesJson();

            //Scheduling
            locationRenderingModel.SchedulingType = this.GetSchedulingType();
            locationRenderingModel.SchedulingLabel = this.GetSchedulingLabel();
            locationRenderingModel.SchedulingCode = this.GetSchedulingCode();
                    
            return (IRenderingModelBase)locationRenderingModel;
        }

        protected virtual ID GetItemID()
        {
            if(this.LocationItem != null)
                return this.LocationItem.ID;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem.ID;
            return this.Rendering.DataSourceItem.ID;
        }

        protected virtual string GetLocationName()
        {
            if (this.LocationItem != null)
                return this.LocationItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.LocationName] ?? string.Empty;
            else if (this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.LocationName] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetTermName()
        {
            if (this.LocationItem != null)
                return this.LocationItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.TermName] ?? string.Empty;
            else if (this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.TermName] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetShortName()
        {
            if(this.LocationItem != null)
                return this.LocationItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.ShortName] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.ShortName] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetAddressLine1()
        {
            if (this.LocationItem != null)
                return this.LocationItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.AddressLine1] ?? string.Empty;
            else if (this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.AddressLine1] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetAddressLine2()
        {
            if (this.LocationItem != null)
                return this.LocationItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.AddressLine2] ?? string.Empty;
            else if (this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.AddressLine2] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetCity()
        {
            if (this.LocationItem != null)
                return this.LocationItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.City] ?? string.Empty;
            else if (this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.City] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetState()
        {
            if (this.LocationItem != null)
                return this.LocationItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.State] ?? string.Empty;
            else if (this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.State] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetPostalCode()
        {
            if (this.LocationItem != null)
                return this.LocationItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.PostalCode] ?? string.Empty;
            else if (this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.PostalCode] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetLatitude()
        {
            if (this.LocationItem != null)
                return this.LocationItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.Latitude] ?? string.Empty;
            else if (this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.Latitude] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetLongitude()
        {
            if (this.LocationItem != null)
                return this.LocationItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.Longitude] ?? string.Empty;
            else if (this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.Longitude] ?? string.Empty;
            return string.Empty;
        }

        protected virtual Coordinates GetCoordinates()
        {
            if(this.LocationItem != null) {                
                return Coordinates.Parse(this.LocationItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.Coordinates] ?? string.Empty);
            }else if(this.Rendering.DataSourceItem != null) {
                return Coordinates.Parse(this.LocationItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.Coordinates] ?? string.Empty);
            }
            return Coordinates.Parse(string.Empty);
        }

        protected virtual string GetClientID()
        {
            if (this.LocationItem != null)
                return this.LocationItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.ClientID] ?? string.Empty;
            else if (this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.ClientID] ?? string.Empty;
            return string.Empty;
        }        

        protected virtual bool GetIsPubliclySearchable()
        {
            if(this.LocationItem != null)
                return this.LocationItem != null && this.LocationItem.Fields[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.IsPubliclySearchable] != null && this.LocationItem.Fields[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.IsPubliclySearchable].Value != string.Empty && this.LocationItem.Fields[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.IsPubliclySearchable].Value == "1";
            else
                return this.Rendering.DataSourceItem != null && this.Rendering.DataSourceItem.Fields[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.IsPubliclySearchable] != null && this.Rendering.DataSourceItem.Fields[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.IsPubliclySearchable].Value != string.Empty && this.LocationItem.Fields[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.IsPubliclySearchable].Value == "1";
        }

        protected virtual bool GetIsActive()
        {
            if(this.LocationItem != null)
                return this.LocationItem != null && this.LocationItem.Fields[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.IsActive] != null && this.LocationItem.Fields[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.IsActive].Value != string.Empty && this.LocationItem.Fields[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.IsActive].Value == "1";
            else
                return this.Rendering.DataSourceItem != null && this.Rendering.DataSourceItem.Fields[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.IsActive] != null && this.Rendering.DataSourceItem.Fields[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.IsActive].Value != string.Empty && this.Rendering.DataSourceItem.Fields[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.IsActive].Value == "1" ;
        }

        protected virtual bool GetIsBMG()
        {
            if(this.LocationItem != null)
                return this.LocationItem != null && this.LocationItem.Fields[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.IsBMG] != null && this.LocationItem.Fields[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.IsBMG].Value != string.Empty && this.LocationItem.Fields[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.IsBMG].Value == "1";
            else
                return this.Rendering.DataSourceItem != null && this.Rendering.DataSourceItem.Fields[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.IsBMG] != null && this.Rendering.DataSourceItem.Fields[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.IsBMG].Value != string.Empty && this.Rendering.DataSourceItem.Fields[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.IsBMG].Value == "1";
        }

        protected virtual string GetImportTaxonomy()
        {
            if (this.LocationItem != null)
                return this.LocationItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.ImportTaxonomy] ?? string.Empty;
            else if (this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.ImportTaxonomy] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetImportProperties()
        {
            if (this.LocationItem != null)
                return this.LocationItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.ImportProperties] ?? string.Empty;
            else if (this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.ImportProperties] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetLocationType()
        {
            if(this.LocationItem != null)
                return this.LocationItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.LocationType] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.LocationType] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetMainPhoneNumber()
        {
            if (this.LocationItem != null)
                return this.LocationItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.MainPhoneNumber] ?? string.Empty;
            else if (this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.MainPhoneNumber] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetMainEmailAddress()
        {
            if (this.LocationItem != null)
                return this.LocationItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.MainEmailAddress] ?? string.Empty;
            else if (this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.MainEmailAddress] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetAdditionalContactInformation()
        {
            if (this.LocationItem != null)
                return this.LocationItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.AdditionalContactInformation] ?? string.Empty;
            else if (this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.AdditionalContactInformation] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetEmergencyRoomPhoneNumber()
        {
            if (this.LocationItem != null)
                return this.LocationItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.EmergencyRoomPhoneNumber] ?? string.Empty;
            else if (this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.EmergencyRoomPhoneNumber] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetTitle()
        {
            if (this.LocationItem != null)
                return this.LocationItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.Title] ?? string.Empty;
            else if (this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.Title] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetMainDescription()
        {
            if (this.LocationItem != null)
                return this.LocationItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.MainDescription] ?? string.Empty;
            else if (this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.MainDescription] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetHoursSectionTitle()
        {
            if (this.LocationItem != null)
                return this.LocationItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.HoursSectionTitle] ?? string.Empty;
            else if (this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.HoursSectionTitle] ?? string.Empty;
            return string.Empty;
        }

        protected virtual NameValueCollection GetHoursOfOperation()
        {
            if (this.LocationItem != null)
                return new NameValueCollection(((NameValueListField)this.LocationItem.Fields[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.HoursOfOperation]).NameValues);
            else if (this.Rendering.DataSourceItem != null)
                return new NameValueCollection(((NameValueListField)this.Rendering.DataSourceItem.Fields[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.HoursOfOperation]).NameValues);
            return (NameValueCollection)null;
        }

        protected virtual string GetHoursInfoMessage()
        {
            if (this.LocationItem != null)
                return this.LocationItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.HoursInfoMessage] ?? string.Empty;
            else if (this.Rendering.DataSource != null)
                return this.Rendering.DataSourceItem[Feature_MedicalCenterLocations.Templates.LocationItem.Fields.HoursInfoMessage] ?? string.Empty;
            return string.Empty;
        }

        protected virtual bool GetShowLocationHours()
        {
            if(this.LocationItem != null)
                return this.LocationItem != null && this.LocationItem.Fields[Templates.LocationItem.Fields.ShowLocationHours] != null && this.LocationItem.Fields[Templates.LocationItem.Fields.ShowLocationHours].Value != string.Empty && this.LocationItem.Fields[Templates.LocationItem.Fields.ShowLocationHours].Value == "1";
            else
                return this.Rendering.DataSourceItem != null && this.Rendering.DataSourceItem.Fields[Templates.LocationItem.Fields.ShowLocationHours] != null && this.Rendering.DataSourceItem.Fields[Templates.LocationItem.Fields.ShowLocationHours].Value != string.Empty && this.LocationItem.Fields[Templates.LocationItem.Fields.ShowLocationHours].Value == "1";
        }

        protected virtual string GetERWaitURLKey()
        {
            if(this.LocationItem != null)
                return this.LocationItem[MedicalCenterLocations.Templates.Custom.LocationItemTemplate.LocationItem.Fields.ERWaitURLKey] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[MedicalCenterLocations.Templates.Custom.LocationItemTemplate.LocationItem.Fields.ERWaitURLKey] ?? string.Empty;
            return string.Empty;
        }

        /*protected virtual LinkItem GetAlternateLocationListingPage()
        {
            if(this.LocationItem != null) {
                return new LinkItem((LinkField)this.LocationItem.Fields[LocationItemTemplate.LocationItem.Fields.AlternateLocationListingPage]);
            }
            else if (this.Rendering.DataSourceItem != null) {
                return new LinkItem((LinkField)this.Rendering.DataSourceItem.Fields[LocationItemTemplate.LocationItem.Fields.AlternateLocationListingPage]);
            }
            return (LinkItem)null;
        }*/

        protected virtual string GetAlternateLocationListingPage()
        {
            if(this.LocationItem != null)
                return this.LocationItem[LocationItemTemplate.LocationItem.Fields.AlternateLocationListingPage] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[LocationItemTemplate.LocationItem.Fields.AlternateLocationListingPage] ?? string.Empty;
            return string.Empty;
        }



        protected virtual bool GetEnableAlternateLink()
        {
            if(this.LocationItem != null)
                return this.LocationItem != null && this.LocationItem.Fields[LocationItemTemplate.LocationItem.Fields.EnableAlternateLink] != null && this.LocationItem.Fields[LocationItemTemplate.LocationItem.Fields.EnableAlternateLink].Value != string.Empty && this.LocationItem.Fields[LocationItemTemplate.LocationItem.Fields.EnableAlternateLink].Value == "1";
            else
                return this.Rendering.DataSourceItem != null && this.Rendering.DataSourceItem.Fields[LocationItemTemplate.LocationItem.Fields.EnableAlternateLink] != null && this.Rendering.DataSourceItem.Fields[LocationItemTemplate.LocationItem.Fields.EnableAlternateLink].Value != string.Empty && this.LocationItem.Fields[LocationItemTemplate.LocationItem.Fields.EnableAlternateLink].Value == "1";
        }


        protected virtual bool GetOverrideWithMapsLink()
        {
            if(this.LocationItem != null)
                return this.LocationItem != null && this.LocationItem.Fields[LocationItemTemplate.LocationItem.Fields.OverrideWithMapsLink] != null && this.LocationItem.Fields[LocationItemTemplate.LocationItem.Fields.OverrideWithMapsLink].Value != string.Empty && this.LocationItem.Fields[LocationItemTemplate.LocationItem.Fields.OverrideWithMapsLink].Value == "1";
            else
                return this.Rendering.DataSourceItem != null && this.Rendering.DataSourceItem.Fields[LocationItemTemplate.LocationItem.Fields.OverrideWithMapsLink] != null && this.Rendering.DataSourceItem.Fields[LocationItemTemplate.LocationItem.Fields.OverrideWithMapsLink].Value != string.Empty && this.LocationItem.Fields[LocationItemTemplate.LocationItem.Fields.OverrideWithMapsLink].Value == "1";
        }

        //Baptist Cancer Centers

        protected virtual string GetServicesProvidedJson()
        {
            if(this.LocationItem != null)
                return this.LocationItem[LocationItemTemplate.LocationItem.Fields.ServicesProvidedJson] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[LocationItemTemplate.LocationItem.Fields.ServicesProvidedJson] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetCancerTypesJson()
        {
            if(this.LocationItem != null)
                return this.LocationItem[LocationItemTemplate.LocationItem.Fields.CancerTypesJson] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[LocationItemTemplate.LocationItem.Fields.CancerTypesJson] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetSchedulingType()
        {
            if(this.LocationItem != null)
                return this.LocationItem[LocationItemTemplate.LocationItem.Fields.SchedulingType] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[LocationItemTemplate.LocationItem.Fields.SchedulingType] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetSchedulingLabel()
        {
            if(this.LocationItem != null)
                return this.LocationItem[LocationItemTemplate.LocationItem.Fields.SchedulingLabel] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[LocationItemTemplate.LocationItem.Fields.SchedulingLabel] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetSchedulingCode()
        {
            if(this.LocationItem != null)
                return this.LocationItem[LocationItemTemplate.LocationItem.Fields.SchedulingCode] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[LocationItemTemplate.LocationItem.Fields.SchedulingCode] ?? string.Empty;
            return string.Empty;
        }
    }

}