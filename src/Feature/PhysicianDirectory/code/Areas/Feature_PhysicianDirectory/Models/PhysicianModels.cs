﻿using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Baptist.Feature.PhysicianDirectory.Templates.Custom;
using Sitecore.Mvc.Presentation;
using Sitecore.Data;
using Sitecore.ContentSearch.SearchTypes;

namespace Sitecore.Baptist.Feature.PhysicianDirectory.Areas.Feature_PhysicianDirectory.Models
{
    public class PhysicianListing
    {
        public List<Specialty> Specialties { get; set; }
        //public List<PhysicianRenderingModel> doctors { get; set; }
    }

    public class PhysicianListingBySpecialty
    {
        public string Specialty { get; set; }
        public string specialtyurl { get; set; }
        public List<PhysicianRenderingModel> doctors { get; set; }        
        public RenderingContext renderingContext { get; set; }
    }

    public class PhysicianListingBySpecialtyCached
    {
        public string Specialty { get; set; }
        public string specialtyurl { get; set; }
        public List<PhysicianListingRow> doctors { get; set; }
        public RenderingContext renderingContext { get; set; }
        public string bmglogourl { get; set; }
    }

    public class SchedulePhysicianAppointment
    {
        public List<PhysicianRenderingModel> doctors { get; set; }
        public string bmglogourl { get; set; }
    }

    public class SchedulePhysicianAppointmentDistance
    {
        public List<PhysicianWithDistance> doctors { get; set; }
        public string bmglogourl { get; set; }
    }

    public class SchedulePhysicianAppointmentDistanceQuery
    {
        public List<PhysicianWithDistance> doctors { get; set; }
        public string bmglogourl { get; set; }
        public bool bmgonly { get; set; }
        public bool bcconly { get; set; }
        public Dictionary<string, string> parameters { get; set; }
    }

    public class PhysicianWithDistance
    {
        public PhysicianRenderingModel physician { get; set; }
        public double distance { get; set; }
        public double score { get; set; }
    }

    public class PhysicianListingRow {
        public string ImageImportedPath { get; set; }
        public string PhysicianName { get; set; }
        public bool IsBMG { get; set; }
        public bool ScheduleAppt { get; set; }
        public string LastName { get; set; }
        public string specialties { get; set; }
        public PhysicianLocation location { get; set; }
    }

    public class PhysicianLocation {
        public ID ItemID { get; set; }
        public string ClientID { get; set; }
        public string LocationName { get; set; }
        public string Address1 { get; set; }        
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string MainPhoneNumber { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string PostalCode { get; set; }
        public string LocationType { get; set; }
    }

    public class PhysicianRegionListing 
    {
        public List<PhysicianRenderingModel> doctors { get; set; }
        public Region region { get; set; }
    }

    public class Specialty
    {
        public string specialtyName { get; set; }
        public string specialtyUrl { get; set; }
        public int count { get; set; }
        public bool bmgonly { get; set; }
    }

    public class Region
    {
        public string city { get; set; }
        public string state { get; set; }
        public string postal { get; set; }
    }    

    public class PhysicianSearchResultSet {
        public List<SearchResultItem> results { get; set; }
        public bool bmgonly { get; set; }
        public bool bcconly { get; set; }
        public Dictionary<string, string> parameters { get; set; }
    }

    public class MediaResource {
        public string Type { get; set; }
        public string Label { get; set; }
        public string Value { get; set; }
    }
}