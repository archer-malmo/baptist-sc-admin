﻿using Sitecore.Data.Items;
using Sitecore.XA.Feature.Events.Models;
using Sitecore.XA.Foundation.Mvc.Models;
using Sitecore.XA.Foundation.SitecoreExtensions.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sitecore.Baptist.Feature.GeneralComponents.Areas.Feature_GeneralComponents.Models
{
    public class BaptistEventRenderingModel : RenderingModelBase
    {
        public Item InnterItem { get; set; }

        public bool IsFromComposite { get; set; }

        public string eventListCategory { get; set; }

        public BaptistEventRenderingModel( Item item, RenderingWebEditingParams renderingParams )
        {
            this.InnterItem = item;            
            this.RenderingWebEditingParams = renderingParams;
            this.IsControlEditable = !renderingParams.DisableWebEditing;
            this.IsFromComposite = renderingParams.SkipCommonButtons;            
        }
    }
}