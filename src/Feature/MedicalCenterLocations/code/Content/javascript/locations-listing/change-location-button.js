﻿var ajaxinprogress = false;
var formindexes;
var finishedindexes = [];

var bindformfunction = function () {

  $('a.location-button[rel="set-geo"]').click(function (event) {
    event.preventDefault();
    $(this).toggleClass('expanded');
    $('body').toggleClass('location-box-open');
    var locationForm = $(this).siblings('.location-form');
    locationForm.toggleClass('expanded');
    if (locationForm.attr('aria-expanded') === 'true') {
      locationForm.attr('aria-expanded', false);
    } else {
      locationForm.attr('aria-expanded', true);
      locationForm.find('#zip').focus();
    }
  });

  $('button.formlocationsubmit').click(function (event) {
    event.preventDefault();
    $(this).parent().submit();
  });

  $('body').click(function (event) {
    if ($(this).hasClass('location-box-open')) {
      if (!$.contains($('.location-form')[0], event.originalEvent.target)) {
        if (!$(event.originalEvent.target).hasClass('location-form')) {
          if (!$(event.originalEvent.target).hasClass('location-button')) {
            $('a.location-button[rel="set-geo"]').removeClass('expanded');
            $('.location-form').removeClass('expanded');
            $('.location-form').attr('aria-expanded', false);
            $('body').removeClass('location-box-open');
          }
        }
      }
    }
  });

  usegpsbuttons();
};

var rebindformfunction = function (dsi, query) {

  $('input[name=query]').val(query);

  $('div[data-rendering-id="' + dsi + '"] a.location-button[rel="set-geo"]').click(function (event) {
    event.preventDefault();
    $(this).toggleClass('expanded');
    $('body').toggleClass('location-box-open');
    var locationForm = $(this).siblings('.location-form');
    locationForm.toggleClass('expanded');
    if (locationForm.attr('aria-expanded') === 'true') {
      locationForm.attr('aria-expanded', false);
    } else {
      locationForm.attr('aria-expanded', true);
      locationForm.find('#zip').focus();
    }
  });

  $('div[data-rendering-id="' + dsi + '"] button.formlocationsubmit').click(function (event) {
    event.preventDefault();
    $(this).parent().submit();
  });

  $('body').click(function (event) {
    if ($(this).hasClass('location-box-open')) {
      if (!$.contains($('.location-form')[0], event.originalEvent.target)) {
        if (!$(event.originalEvent.target).hasClass('location-form')) {
          if (!$(event.originalEvent.target).hasClass('location-button')) {
            $('a.location-button[rel="set-geo"]').removeClass('expanded');
            $('.location-form').removeClass('expanded');
            $('.location-form').attr('aria-expanded', false);
            $('body').removeClass('location-box-open');
          }
        }
      }
    }
  });

  var listings = $('div[data-rendering-id]');
  listings.each(function () {
    
    var ctxtSel = 'input[name=schedule-filter][data-tab-context="' + $(this).attr('data-rendering-id') + '"]';
    var schdFilt = $(ctxtSel);
    console.log(schdFilt);

    if (schdFilt.length > 0) {
      console.log("bound", ctxtSel);
      bindschedulefilter(schdFilt);
    }
  });

  usegpsbuttons();
};

var rebindloclist = function (dsi) {
  $('li.row[data-clientid]').hoverIntent(function (event) {
    updatemaplocation($(this), true);
  });
  //console.log(dsi);
  var listing = $('div[data-rendering-id="' + dsi + '"]');
  //console.log(listing);
  var startmap = listing.find('ul li.row[data-clientid]').first();
  //console.log(startmap);
  updatemaplocation(startmap, true);
};

var submitlocationsearch = function (event) {

  event.preventDefault();
  var q = $(this).find('input[name=query]').val();
  $('input[name=query]').val(q);

  formindexes = $('input[name=query]').length;
  //console.log(formindexes);
  ajaxinprogress = true;


  $('div.css-loader').css('display', 'block');

  setTimeout(function () {
    $('div.css-loader').css('display', 'none');
  }, 25000);

  $('input[name=query]').each(function (index) {
    submitrequest($(this).parent(), index);
  });

};

var submitrequest = function (targetform, index) {

  //console.log('click');

  var q = targetform.find('input[name=query]').val();

  //console.log(q);

  var dsi = targetform.find('input[name=dsiID]').val();
  //console.log(dsi);

  var unid = targetform.find('input[name=unqID]').val();

  var bmgquery = "";
  if (targetform.find('input[name=bmgonly]').length > 0) {
    bmgquery = "&bmgonly=true";
  }

  var schdquery = "";
  if (targetform.find('input[name=scheduling_only]').length > 0) {
    schdquery = "&scheduling_only=true";
  }

  var locationType;

  var div = 'div[data-rendering-id="' + dsi + '"]';

  var chradius = $(div).attr('data-location-radius');

  var ep = "/api/sitecore/LocationsFolder/LocationsListingSearch?query=" + q + "&dsiID=" + dsi + "&uniqueID=" + unid + "&radius= " + chradius + bmgquery + schdquery;

  if ($(div).attr('data-location-type')) {
    var locationType = $('div[data-rendering-id="' + dsi + '"]').attr('data-location-type');
    ep = "/api/sitecore/LocationsFolder/LocationsListingSearchByType?query=" + q + "&dsiID=" + dsi + "&locationType=" + locationType + "&uniqueID=" + unid + "&radius=" + chradius + bmgquery + schdquery;
  }

  $('a.location-button[rel="set-geo"]').removeClass('expanded');
  $('.location-form').removeClass('expanded');
  $('.location-form').attr('aria-expanded', false);

  $.ajax({
    url: ep,
    cache: false,
    method: 'GET',
    success: function (data) {
      $('div[data-map-rendering-id="' + dsi + '"]').remove();
      $('div[data-rendering-id="' + dsi + '"]').replaceWith(data);

      if (locationType) {
        $('div[data-rendering-id="' + dsi + '"]').attr('data-location-type', locationType);
      }

      $('form.location-form').bind('submit', submitlocationsearch);

      rebindloclist(dsi);

      rebindformfunction(dsi, q);
      //ajaxinprogress = false;
      finishedindexes.push(index);
      //console.log(finishedindexes.length);

      if (formindexes == finishedindexes.length) {
        $('div.css-loader').css('display', 'none');
        ajaxinprogress = false;
      }
    },
    error: function (data) {
      $('div.css-loader').css('display', 'none');
      //console.log(data);
      //ajaxinprogress = false;
      //$('.option-list').toggleClass('active');
    }
  });
  //ajaxinprogress = false;
};

var gpschangelistener = function (event) {
  var startPosition;
  var latitude;
  var longitude;

  var geoSuccess = function (position) {
    startPosition = position;
    latitude = startPosition.coords.latitude;
    longitude = startPosition.coords.longitude;

    var usergeoupdateurl = '/api/sitecore/ERWait/UpdateUserGeoFromBrowser?lat=' + latitude + '&lon=' + longitude;

    $.ajax({
      url: usergeoupdateurl,
      cache: false,
      method: 'GET',
      success: function (data) {
        var clientid = data;
        updateErWaitTime(clientid);
        $('#header-er-wait').removeClass('hidden');
        //location.reload();
        /* var clientidurl = '/api/sitecore/ERWait/GetDefaultClientId';
          var clientID;

          $.ajax({
            url: clientidurl,
            cache: false,
            method: 'GET',
            success: function (data) {
              clientID = data;              
              updateErWaitTime(clientID);
            },
            error: function (data) {
              console.log(data);
            }
          });
          */
      },
      error: function (data) {
        console.log(data);
        //$('.option-list').toggleClass('active');
        $('#header-er-wait').addClass('hidden');
      }
    });



  };

  var geoError = function (error) {
    switch (error.code) {
      case error.TIMEOUT:
        // The user didn't accept the callout
        console.log(error);
        break;
    }
  };
  navigator.geolocation.getCurrentPosition(geoSuccess, geoError);
}

var locationcheckgpschangelocationlistener = function (event) {

  gpschangelistener();

  var pname = window.location.pathname;
  if (pname == '/locations' || pname == '/locations/') {
    location.reload();
  }
}

var changelocationgps = function (event) {
  event.preventDefault();
  $('div.css-loader').css('display', 'block');

  setTimeout(function () {
    $('div.css-loader').css('display', 'none');
  }, 25000);

  locationcheckgpschangelocationlistener();
}

var usegpsbuttons = function () {

  if (navigator.geolocation) {
    $('button[rel="gps-change-location-listing"]').bind('click', changelocationgps);
  } else {
    $('button[rel="gps-change-location-listing"]').css('display', 'none');
  }
};


$(function () {

  $('form.location-form').bind('submit', submitlocationsearch);
  bindformfunction();

});

