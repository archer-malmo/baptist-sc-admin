﻿using Sitecore.ContentSearch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Models
{
    public class HospitalItemRenderingModel : LocationItemRenderingModel
    {

        /// <summary>
        /// <para>Template: Hospital Item</para><para>Field: CheerCardEmailAddress</para><para>Data type: Single-Line Text</para>
        /// </summary>
        [IndexField("cheer_card_email_address")]
        public virtual string CheerCardEmailAddress { get; set; }
        [IndexField("pre_registration_url")]
        public virtual string PreRegistrationURL { get; set; }       

    }
}