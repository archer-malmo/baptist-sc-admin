﻿using Sitecore.Data;
using System.Runtime.InteropServices;

namespace Sitecore.Baptist.Feature.PhysicianDirectory.Areas.Feature_PhysicianDirectory
{
    public class Templates
    {
        [StructLayout(LayoutKind.Sequential, Size = 1)]
        public struct Physician
        {
            public static ID ID = ID.Parse("{C1F76E27-A48F-4850-9475-76A5E0392ED4}");
        
            [StructLayout(LayoutKind.Sequential, Size = 1)]    
            public struct Fields
            {
                //Physician Section
                public static readonly ID FirstName = new ID("{B11F045C-86AC-4CA4-BF57-1D4533731DD1}");
                public static readonly ID LastName = new ID("{A38A4998-9D0E-4DA5-B946-1B29E68AEC8A}");
                public static readonly ID MiddleName = new ID("{04CCCAE2-86C7-4D51-A572-EF65AEE6D4F5}");
                public static readonly ID Suffix = new ID("{EDB3FE77-32BD-44A1-8970-44EE3E08EB73}");
                public static readonly ID ProfessionalTitles = new ID("{5A90D6A9-C6D8-4277-A4A6-80132FCC9F78}");
                public static readonly ID SpecialDesignations = new ID("{6AED044A-C9F4-46CC-BA0D-2A61ED620F09}");
                public static readonly ID Specialties = new ID("{B5CBE89F-80F5-470D-9053-5D6A0BFB9654}");
                public static readonly ID Languages = new ID("{5339B476-15E8-4B19-8527-9C057A6FA53A}");
                public static readonly ID TreatmentsAndServices = new ID("{6F5B3834-82EF-4E7E-B999-9C363A14A9F9}");
                public static readonly ID DiseasesAndConditions = new ID("{D6AAA425-685F-4075-BE44-6D9053EF23F8}");
                public static readonly ID SpecialtyInterests = new ID("{15E38A39-9CDE-445E-9621-7A9866DAA5B5}");

                //Physician Meta
                public static readonly ID ClientID = new ID("{11E35CBC-7CBA-4869-A540-7B03D040B96A}");
                public static readonly ID IsActive = new ID("{1F3B6CDC-56C5-49BB-A364-C538EDC555AC}");
                public static readonly ID IsBMG = new ID("{9B004B79-B3E5-45BB-AB37-B777CC8BBE00}");
                public static readonly ID ImportTaxonomy = new ID("{8D20FD80-94C8-4430-BFEC-3A6DFF62F4EA}");
                public static readonly ID ImportProperties = new ID("{4D694006-5FA3-4FC8-9671-FBE673B0295A}");
                public static readonly ID ImageImportedPath = new ID("{BA7B4E5D-EEF3-440E-99E9-9E98B57EFD40}");
                public static readonly ID Regions = new ID("{BA7B4E5D-EEF3-440E-99E9-9E98B57EFD40}");

                //Physician Contact
                public static readonly ID PrimaryLocationID = new ID("{33C3A1AC-332F-4F01-840A-BDB2D0AEBF9C}");
                public static readonly ID PrimaryPhoneNumber = new ID("{6298D4F2-1BBF-499E-A383-A8F4976E9BA2}");
                public static readonly ID PrimaryPhoneIsPublic = new ID("{34A43178-AA5D-43E4-A367-4E0A3EC9B36B}");
                public static readonly ID Locations = new ID("{62A3D38C-EFD1-452C-8AA3-3BF1A687C8E6}");
                public static readonly ID HospitalAffiliations = new ID("{CD65FBD5-992C-4C87-AD08-00CEBDE5D09A}");

                //Physician Biographical 
                public static readonly ID Bio = new ID("{713133EC-8021-4CB9-BD70-C2C04F6D9EE5}");
                public static readonly ID Education = new ID("{A4555ABA-EFEE-49FD-BEEC-08C960497C25}");
                public static readonly ID ProfessionalRecognition = new ID("{4B747256-3580-4C5A-AB01-88152614DF3C}");
                public static readonly ID MeetTheDoctor = new ID("{E021707F-110F-4587-9578-00C8B63C39EF}");
            }
        }
    }
}