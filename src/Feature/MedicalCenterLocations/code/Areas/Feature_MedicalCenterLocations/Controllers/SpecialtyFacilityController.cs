﻿using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Models;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Repositories;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Utils;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom;
using Sitecore.XA.Foundation.Mvc.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Controllers
{
    public class SpecialitesFolderController : StandardController
    {
        // GET: Feature_MedicalCenterLocations/SpecialtyFacility
        protected readonly ISpecialtiesFolderRepository SpecialtiesFolderRepository;

        public SpecialitesFolderController( ISpecialtiesFolderRepository specialtiesfolderRepository )
        {
            this.SpecialtiesFolderRepository = specialtiesfolderRepository;
        }

        public ActionResult ServiceGallery()
        {
            LocationUtils lu = new LocationUtils();
            lu.req = Request;
            lu.respnse = Response;

            LocationWithDistance defaultLocation = lu.getDefaultLocation();

            List<Models.SpecialtyRenderingModel> folder = this.SpecialtiesFolderRepository.GetSpecialties();

            List<Models.SpecialtyRenderingModel> locSpecs = new List<Models.SpecialtyRenderingModel>();

            if(defaultLocation != null) {
                locSpecs = folder.Where(x => x.Locations.Contains(defaultLocation.location.ItemID.Guid)).ToList<Models.SpecialtyRenderingModel>();
                //locSpecs = folder.Where(x => x.Locations.Contains(defaultLocation.location.ItemId.Guid)).ToList<Models.SpecialtyRenderingModel>();
            }
            else {
                locSpecs = folder.Where(x => x.Locations.Contains(lu.getFallBackLocation().location.ItemID.Guid)).ToList<Models.SpecialtyRenderingModel>();
                //locSpecs = folder.Where(x => x.Locations.Contains(lu.getFallBackLocation().location.ItemId.Guid)).ToList<Models.SpecialtyRenderingModel>();
            }            

            return (ActionResult)this.PartialView("~/Areas/Feature_MedicalCenterLocations/Views/SpecialtyFacility/ServiceGallery.cshtml", locSpecs);
        }
    }
}