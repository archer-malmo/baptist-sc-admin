﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Sitecore.XA.Foundation.Mvc.Controllers;
using Sitecore.Baptist.Feature.DemoRendering.Repositories;
using Sitecore.XA.Feature.Media.Controllers;
using Sitecore.XA.Feature.Media.Repositories;

namespace Sitecore.Baptist.Feature.DemoRendering.Areas.Feature_DemoRendering.Controllers
{
    public class DemoRenderingController : StandardController
    {
        private readonly IDemoRenderingRepository _repository;
     
        public DemoRenderingController(IDemoRenderingRepository repository) {
          this._repository = repository;
        }

        protected override object GetModel()
        {
          return this._repository.GetModel();
        }
    }   
}