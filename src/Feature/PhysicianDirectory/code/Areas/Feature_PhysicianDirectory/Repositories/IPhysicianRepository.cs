﻿using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sitecore.Baptist.Feature.PhysicianDirectory.Areas.Feature_PhysicianDirectory.Repositories
{
    public interface IPhysicianRepository : IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
    }
}  