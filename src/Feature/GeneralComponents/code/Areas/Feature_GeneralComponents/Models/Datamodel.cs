using Sitecore.Data.Items;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Baptist.Feature.GeneralComponents.Templates.Custom;
using Sitecore.XA.Foundation.Mvc.Models;
using Sitecore.Data;
using System.Runtime.InteropServices;
using Fortis.Model.Fields;
#region Baptist Event (Custom)
namespace Sitecore.Baptist.Feature.GeneralComponents.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Baptist Event</para>
	/// <para>ID: {1EA93005-C59E-40BD-AD6A-0CB8FE39C4A1}</para>
	/// <para>/sitecore/templates/Feature/Experience Accelerator/Events/Datasource/Baptist Event</para>
	/// </summary>
	[TemplateMapping("{1EA93005-C59E-40BD-AD6A-0CB8FE39C4A1}", "InterfaceMap")]
	public partial interface IBaptistEventItem : IItemWrapper , Sitecore.Baptist.Feature.GeneralComponents.Templates.Custom.ICalendarEventItem
	{		
		/// <summary>
		/// <para>Template: Baptist Event</para><para>Field: Fequency</para><para>Data type: Droplist</para>
		/// </summary>
		ITextFieldWrapper Fequency { get; }
		string FequencyValue { get; }
		/// <summary>
		/// <para>Template: Baptist Event</para><para>Field: IsRecurring</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("is_recurring")]
		IBooleanFieldWrapper IsRecurring { get; }

		/// <summary>
		/// <para>Template: Baptist Event</para><para>Field: IsRecurring</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("is_recurring")]
		bool IsRecurringValue { get; }
		/// <summary>
		/// <para>Template: Baptist Event</para><para>Field: RecurByWeekday</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("recur_by_weekday")]
		IBooleanFieldWrapper RecurByWeekday { get; }

		/// <summary>
		/// <para>Template: Baptist Event</para><para>Field: RecurByWeekday</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("recur_by_weekday")]
		bool RecurByWeekdayValue { get; }
		/// <summary>
		/// <para>Template: Baptist Event</para><para>Field: BaptistEventLocation</para><para>Data type: Droptree</para>
		/// </summary>
		[IndexField("baptist_event_location")]
		ILinkFieldWrapper BaptistEventLocation { get; }

		/// <summary>
		/// <para>Template: Baptist Event</para><para>Field: BaptistEventLocation</para><para>Data type: Droptree</para>
		/// </summary>
		[IndexField("baptist_event_location")]
		Guid BaptistEventLocationValue { get; }
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Feature/Experience Accelerator/Events/Datasource/Baptist Event</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{1EA93005-C59E-40BD-AD6A-0CB8FE39C4A1}", typeof(Guid))]
	[TemplateMapping("{1EA93005-C59E-40BD-AD6A-0CB8FE39C4A1}", "")]
	public partial class BaptistEventItem : ItemWrapper, IBaptistEventItem
	{
		private Item _item = null;

		public BaptistEventItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public BaptistEventItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public BaptistEventItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public BaptistEventItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

		/// <summary><para>Template: Baptist Event</para><para>Field: EndLabel</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("endlabel")]
		public virtual ITextFieldWrapper EndLabel
		{
			get { return GetField<TextFieldWrapper>("EndLabel", "endlabel"); }
		}

		/// <summary><para>Template: Baptist Event</para><para>Field: EndLabel</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("endlabel")]
		public string EndLabelValue
		{
			get { return EndLabel.Value; }
		}
		/// <summary><para>Template: Baptist Event</para><para>Field: EventDescription</para><para>Data type: Rich Text</para></summary>
		[IndexField("eventdescription")]
		public virtual IRichTextFieldWrapper EventDescription
		{
			get { return GetField<RichTextFieldWrapper>("EventDescription", "eventdescription"); }
		}

		/// <summary><para>Template: Baptist Event</para><para>Field: EventDescription</para><para>Data type: Rich Text</para></summary>
		[IndexField("eventdescription")]
		public string EventDescriptionValue
		{
			get { return EventDescription.Value; }
		}
		/// <summary><para>Template: Baptist Event</para><para>Field: EventEnd</para><para>Data type: Datetime</para></summary>
		[IndexField("eventend")]
		public virtual IDateTimeFieldWrapper EventEnd
		{
			get { return GetField<DateTimeFieldWrapper>("EventEnd", "eventend"); }
		}

		/// <summary><para>Template: Baptist Event</para><para>Field: EventEnd</para><para>Data type: Datetime</para></summary>
		[IndexField("eventend")]
		public DateTime EventEndValue
		{
			get { return EventEnd.Value; }
		}
		/// <summary><para>Template: Baptist Event</para><para>Field: EventLink</para><para>Data type: General Link</para></summary>
		[IndexField("eventlink")]
		public virtual IGeneralLinkFieldWrapper EventLink
		{
			get { return GetField<GeneralLinkFieldWrapper>("EventLink", "eventlink"); }
		}

		/// <summary><para>Template: Baptist Event</para><para>Field: EventLink</para><para>Data type: General Link</para></summary>
		[IndexField("eventlink")]
		public string EventLinkValue
		{
			get { return EventLink.Value; }
		}
		/// <summary><para>Template: Baptist Event</para><para>Field: EventName</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("eventname")]
		public virtual ITextFieldWrapper EventName
		{
			get { return GetField<TextFieldWrapper>("EventName", "eventname"); }
		}

		/// <summary><para>Template: Baptist Event</para><para>Field: EventName</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("eventname")]
		public string EventNameValue
		{
			get { return EventName.Value; }
		}
		/// <summary><para>Template: Baptist Event</para><para>Field: EventPlace</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("eventplace")]
		public virtual ITextFieldWrapper EventPlace
		{
			get { return GetField<TextFieldWrapper>("EventPlace", "eventplace"); }
		}

		/// <summary><para>Template: Baptist Event</para><para>Field: EventPlace</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("eventplace")]
		public string EventPlaceValue
		{
			get { return EventPlace.Value; }
		}
		/// <summary><para>Template: Baptist Event</para><para>Field: EventStart</para><para>Data type: Datetime</para></summary>
		[IndexField("eventstart")]
		public virtual IDateTimeFieldWrapper EventStart
		{
			get { return GetField<DateTimeFieldWrapper>("EventStart", "eventstart"); }
		}

		/// <summary><para>Template: Baptist Event</para><para>Field: EventStart</para><para>Data type: Datetime</para></summary>
		[IndexField("eventstart")]
		public DateTime EventStartValue
		{
			get { return EventStart.Value; }
		}
		/// <summary><para>Template: Baptist Event</para><para>Field: EventType</para><para>Data type: Multilist</para></summary>
		[IndexField("eventtype")]
		public virtual IListFieldWrapper EventType
		{
			get { return GetField<ListFieldWrapper>("EventType", "eventtype"); }
		}

		/// <summary><para>Template: Baptist Event</para><para>Field: EventType</para><para>Data type: Multilist</para></summary>
		[IndexField("eventtype")]
		public IEnumerable<Guid> EventTypeValue
		{
			get { return EventType.Value; }
		}
		/// <summary><para>Template: Baptist Event</para><para>Field: StartLabel</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("startlabel")]
		public virtual ITextFieldWrapper StartLabel
		{
			get { return GetField<TextFieldWrapper>("StartLabel", "startlabel"); }
		}

		/// <summary><para>Template: Baptist Event</para><para>Field: StartLabel</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("startlabel")]
		public string StartLabelValue
		{
			get { return StartLabel.Value; }
		}
		/// <summary><para>Template: Baptist Event</para><para>Field: Fequency</para><para>Data type: Droplist</para></summary>
		public virtual ITextFieldWrapper Fequency
		{
			get { return GetField<TextFieldWrapper>("Fequency"); }
		}

		/// <summary><para>Template: Baptist Event</para><para>Field: Fequency</para><para>Data type: Droplist</para></summary>
		public string FequencyValue
		{
			get { return Fequency.Value; }
		}
		/// <summary><para>Template: Baptist Event</para><para>Field: IsRecurring</para><para>Data type: Checkbox</para></summary>
		[IndexField("is_recurring")]
		public virtual IBooleanFieldWrapper IsRecurring
		{
			get { return GetField<BooleanFieldWrapper>("Is Recurring", "is_recurring"); }
		}

		/// <summary><para>Template: Baptist Event</para><para>Field: IsRecurring</para><para>Data type: Checkbox</para></summary>
		[IndexField("is_recurring")]
		public bool IsRecurringValue
		{
			get { return IsRecurring.Value; }
		}
		/// <summary><para>Template: Baptist Event</para><para>Field: RecurByWeekday</para><para>Data type: Checkbox</para></summary>
		[IndexField("recur_by_weekday")]
		public virtual IBooleanFieldWrapper RecurByWeekday
		{
			get { return GetField<BooleanFieldWrapper>("Recur by weekday", "recur_by_weekday"); }
		}

		/// <summary><para>Template: Baptist Event</para><para>Field: RecurByWeekday</para><para>Data type: Checkbox</para></summary>
		[IndexField("recur_by_weekday")]
		public bool RecurByWeekdayValue
		{
			get { return RecurByWeekday.Value; }
		}
		/// <summary><para>Template: Baptist Event</para><para>Field: BaptistEventLocation</para><para>Data type: Droptree</para></summary>
		[IndexField("baptist_event_location")]
		public virtual ILinkFieldWrapper BaptistEventLocation
		{
			get { return GetField<LinkFieldWrapper>("Baptist Event Location", "baptist_event_location"); }
		}

		/// <summary><para>Template: Baptist Event</para><para>Field: BaptistEventLocation</para><para>Data type: Droptree</para></summary>
		[IndexField("baptist_event_location")]
		public Guid BaptistEventLocationValue
		{
			get { return BaptistEventLocation.Value; }
		}
	
	}
	public class BaptistEventTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct BaptistEvent
	    {
	    	public static ID ID = ID.Parse("{1EA93005-C59E-40BD-AD6A-0CB8FE39C4A1}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
						public static readonly ID Fequency = new ID("{D7C2B2C8-97BC-407C-BBDF-27D622013587}");
						public static readonly ID IsRecurring = new ID("{3C6D2D17-A716-4CC1-8C31-1FDE1CCC4904}");
						public static readonly ID Recurbyweekday = new ID("{D89906D1-0A27-483D-8C9A-44B02A0704B9}");
						public static readonly ID BaptistEventLocation = new ID("{C8F5D34D-57AE-4D57-B659-864F904AB37C}");
				
	        }
	    }
	}

	public class BaptistEventRenderingModel : RenderingModelBase
	{
				
				 public virtual string Fequency { get; set; }
			
				/// <summary>
			/// <para>Template: Baptist Event</para><para>Field: IsRecurring</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("is_recurring")]
				 public virtual bool IsRecurring { get; set; }
			
				/// <summary>
			/// <para>Template: Baptist Event</para><para>Field: RecurByWeekday</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("recur_by_weekday")]
				 public virtual bool RecurByWeekday { get; set; }
			
				/// <summary>
			/// <para>Template: Baptist Event</para><para>Field: BaptistEventLocation</para><para>Data type: Droptree</para>
			/// </summary>
			[IndexField("baptist_event_location")]
				 public virtual Guid BaptistEventLocation { get; set; }
		
	}

}


#endregion
#region Event List (Custom)
namespace Sitecore.Baptist.Feature.GeneralComponents.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Event List</para>
	/// <para>ID: {311E5F53-CF27-4A85-B887-B84BEE5D3BA3}</para>
	/// <para>/sitecore/templates/Feature/Experience Accelerator/Events/Datasource/Event List</para>
	/// </summary>
	[TemplateMapping("{311E5F53-CF27-4A85-B887-B84BEE5D3BA3}", "InterfaceMap")]
	public partial interface IEventListItem : IItemWrapper 
	{		
		/// <summary>
		/// <para>Template: Event List</para><para>Field: EventTypes</para><para>Data type: Droplink</para>
		/// </summary>
		[IndexField("eventtypes")]
		ILinkFieldWrapper EventTypes { get; }

		/// <summary>
		/// <para>Template: Event List</para><para>Field: EventTypes</para><para>Data type: Droplink</para>
		/// </summary>
		[IndexField("eventtypes")]
		Guid EventTypesValue { get; }
		/// <summary>
		/// <para>Template: Event List</para><para>Field: GoogleCalendarAPIKey</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("googlecalendarapikey")]
		ITextFieldWrapper GoogleCalendarAPIKey { get; }

		/// <summary>
		/// <para>Template: Event List</para><para>Field: GoogleCalendarAPIKey</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("googlecalendarapikey")]
		string GoogleCalendarAPIKeyValue { get; }
		/// <summary>
		/// <para>Template: Event List</para><para>Field: GoogleCalendarID</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("googlecalendarid")]
		ITextFieldWrapper GoogleCalendarID { get; }

		/// <summary>
		/// <para>Template: Event List</para><para>Field: GoogleCalendarID</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("googlecalendarid")]
		string GoogleCalendarIDValue { get; }
		/// <summary>
		/// <para>Template: Event List</para><para>Field: Title</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("title")]
		ITextFieldWrapper Title { get; }

		/// <summary>
		/// <para>Template: Event List</para><para>Field: Title</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("title")]
		string TitleValue { get; }
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Feature/Experience Accelerator/Events/Datasource/Event List</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{311E5F53-CF27-4A85-B887-B84BEE5D3BA3}", typeof(Guid))]
	[TemplateMapping("{311E5F53-CF27-4A85-B887-B84BEE5D3BA3}", "")]
	public partial class EventListItem : ItemWrapper, IEventListItem
	{
		private Item _item = null;

		public EventListItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public EventListItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public EventListItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public EventListItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

		/// <summary><para>Template: Event List</para><para>Field: EventTypes</para><para>Data type: Droplink</para></summary>
		[IndexField("eventtypes")]
		public virtual ILinkFieldWrapper EventTypes
		{
			get { return GetField<LinkFieldWrapper>("EventTypes", "eventtypes"); }
		}

		/// <summary><para>Template: Event List</para><para>Field: EventTypes</para><para>Data type: Droplink</para></summary>
		[IndexField("eventtypes")]
		public Guid EventTypesValue
		{
			get { return EventTypes.Value; }
		}
		/// <summary><para>Template: Event List</para><para>Field: GoogleCalendarAPIKey</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("googlecalendarapikey")]
		public virtual ITextFieldWrapper GoogleCalendarAPIKey
		{
			get { return GetField<TextFieldWrapper>("GoogleCalendarAPIKey", "googlecalendarapikey"); }
		}

		/// <summary><para>Template: Event List</para><para>Field: GoogleCalendarAPIKey</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("googlecalendarapikey")]
		public string GoogleCalendarAPIKeyValue
		{
			get { return GoogleCalendarAPIKey.Value; }
		}
		/// <summary><para>Template: Event List</para><para>Field: GoogleCalendarID</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("googlecalendarid")]
		public virtual ITextFieldWrapper GoogleCalendarID
		{
			get { return GetField<TextFieldWrapper>("GoogleCalendarID", "googlecalendarid"); }
		}

		/// <summary><para>Template: Event List</para><para>Field: GoogleCalendarID</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("googlecalendarid")]
		public string GoogleCalendarIDValue
		{
			get { return GoogleCalendarID.Value; }
		}
		/// <summary><para>Template: Event List</para><para>Field: Title</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("title")]
		public virtual ITextFieldWrapper Title
		{
			get { return GetField<TextFieldWrapper>("Title", "title"); }
		}

		/// <summary><para>Template: Event List</para><para>Field: Title</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("title")]
		public string TitleValue
		{
			get { return Title.Value; }
		}
	
	}
	public class EventListTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct EventList
	    {
	    	public static ID ID = ID.Parse("{311E5F53-CF27-4A85-B887-B84BEE5D3BA3}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
						public static readonly ID EventTypes = new ID("{19DE610D-9714-4B5D-A17E-A2ACEA64769C}");
						public static readonly ID GoogleCalendarAPIKey = new ID("{7CFFA839-9AB6-4242-9B50-65B6B1E42691}");
						public static readonly ID GoogleCalendarID = new ID("{7B27442B-03C0-47DE-8C93-4C93B8493604}");
						public static readonly ID Title = new ID("{C08B42E9-C147-47D1-B50C-6274EB3A8BAA}");
				
	        }
	    }
	}

	public class EventListRenderingModel : RenderingModelBase
	{
				
				/// <summary>
			/// <para>Template: Event List</para><para>Field: EventTypes</para><para>Data type: Droplink</para>
			/// </summary>
			[IndexField("eventtypes")]
				 public virtual Guid EventTypes { get; set; }
			
				/// <summary>
			/// <para>Template: Event List</para><para>Field: GoogleCalendarAPIKey</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("googlecalendarapikey")]
				 public virtual string GoogleCalendarAPIKey { get; set; }
			
				/// <summary>
			/// <para>Template: Event List</para><para>Field: GoogleCalendarID</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("googlecalendarid")]
				 public virtual string GoogleCalendarID { get; set; }
			
				/// <summary>
			/// <para>Template: Event List</para><para>Field: Title</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("title")]
				 public virtual string Title { get; set; }
		
	}

}


#endregion
#region Event Calendar Parameters (Custom)
namespace Sitecore.Baptist.Feature.GeneralComponents.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Event Calendar Parameters</para>
	/// <para>ID: {43E4F676-DA99-4859-9F7F-FA82F9D52277}</para>
	/// <para>/sitecore/templates/Feature/Experience Accelerator/Events/Rendering Parameters/Event Calendar Parameters</para>
	/// </summary>
	[TemplateMapping("{43E4F676-DA99-4859-9F7F-FA82F9D52277}", "InterfaceMap")]
	public partial interface IEventCalendarParametersItem : IItemWrapper 
	{		
		/// <summary>
		/// <para>Template: Event Calendar Parameters</para><para>Field: CalendarType</para><para>Data type: Multilist</para>
		/// </summary>
		[IndexField("calendartype")]
		IListFieldWrapper CalendarType { get; }

		/// <summary>
		/// <para>Template: Event Calendar Parameters</para><para>Field: CalendarType</para><para>Data type: Multilist</para>
		/// </summary>
		[IndexField("calendartype")]
		IEnumerable<Guid> CalendarTypeValue { get; }
		/// <summary>
		/// <para>Template: Event Calendar Parameters</para><para>Field: CompactView</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("compactview")]
		IBooleanFieldWrapper CompactView { get; }

		/// <summary>
		/// <para>Template: Event Calendar Parameters</para><para>Field: CompactView</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("compactview")]
		bool CompactViewValue { get; }
		/// <summary>
		/// <para>Template: Event Calendar Parameters</para><para>Field: ShowMonthCaptions</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("showmonthcaptions")]
		IBooleanFieldWrapper ShowMonthCaptions { get; }

		/// <summary>
		/// <para>Template: Event Calendar Parameters</para><para>Field: ShowMonthCaptions</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("showmonthcaptions")]
		bool ShowMonthCaptionsValue { get; }
		/// <summary>
		/// <para>Template: Event Calendar Parameters</para><para>Field: ShowPreviousNext</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("showpreviousnext")]
		IBooleanFieldWrapper ShowPreviousNext { get; }

		/// <summary>
		/// <para>Template: Event Calendar Parameters</para><para>Field: ShowPreviousNext</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("showpreviousnext")]
		bool ShowPreviousNextValue { get; }
		/// <summary>
		/// <para>Template: Event Calendar Parameters</para><para>Field: TitleHeadingLevel</para><para>Data type: Droplink</para>
		/// </summary>
		[IndexField("titleheadinglevel")]
		ILinkFieldWrapper TitleHeadingLevel { get; }

		/// <summary>
		/// <para>Template: Event Calendar Parameters</para><para>Field: TitleHeadingLevel</para><para>Data type: Droplink</para>
		/// </summary>
		[IndexField("titleheadinglevel")]
		Guid TitleHeadingLevelValue { get; }
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Feature/Experience Accelerator/Events/Rendering Parameters/Event Calendar Parameters</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{43E4F676-DA99-4859-9F7F-FA82F9D52277}", typeof(Guid))]
	[TemplateMapping("{43E4F676-DA99-4859-9F7F-FA82F9D52277}", "")]
	public partial class EventCalendarParametersItem : ItemWrapper, IEventCalendarParametersItem
	{
		private Item _item = null;

		public EventCalendarParametersItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public EventCalendarParametersItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public EventCalendarParametersItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public EventCalendarParametersItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

		/// <summary><para>Template: Event Calendar Parameters</para><para>Field: CalendarType</para><para>Data type: Multilist</para></summary>
		[IndexField("calendartype")]
		public virtual IListFieldWrapper CalendarType
		{
			get { return GetField<ListFieldWrapper>("CalendarType", "calendartype"); }
		}

		/// <summary><para>Template: Event Calendar Parameters</para><para>Field: CalendarType</para><para>Data type: Multilist</para></summary>
		[IndexField("calendartype")]
		public IEnumerable<Guid> CalendarTypeValue
		{
			get { return CalendarType.Value; }
		}
		/// <summary><para>Template: Event Calendar Parameters</para><para>Field: CompactView</para><para>Data type: Checkbox</para></summary>
		[IndexField("compactview")]
		public virtual IBooleanFieldWrapper CompactView
		{
			get { return GetField<BooleanFieldWrapper>("CompactView", "compactview"); }
		}

		/// <summary><para>Template: Event Calendar Parameters</para><para>Field: CompactView</para><para>Data type: Checkbox</para></summary>
		[IndexField("compactview")]
		public bool CompactViewValue
		{
			get { return CompactView.Value; }
		}
		/// <summary><para>Template: Event Calendar Parameters</para><para>Field: ShowMonthCaptions</para><para>Data type: Checkbox</para></summary>
		[IndexField("showmonthcaptions")]
		public virtual IBooleanFieldWrapper ShowMonthCaptions
		{
			get { return GetField<BooleanFieldWrapper>("ShowMonthCaptions", "showmonthcaptions"); }
		}

		/// <summary><para>Template: Event Calendar Parameters</para><para>Field: ShowMonthCaptions</para><para>Data type: Checkbox</para></summary>
		[IndexField("showmonthcaptions")]
		public bool ShowMonthCaptionsValue
		{
			get { return ShowMonthCaptions.Value; }
		}
		/// <summary><para>Template: Event Calendar Parameters</para><para>Field: ShowPreviousNext</para><para>Data type: Checkbox</para></summary>
		[IndexField("showpreviousnext")]
		public virtual IBooleanFieldWrapper ShowPreviousNext
		{
			get { return GetField<BooleanFieldWrapper>("ShowPreviousNext", "showpreviousnext"); }
		}

		/// <summary><para>Template: Event Calendar Parameters</para><para>Field: ShowPreviousNext</para><para>Data type: Checkbox</para></summary>
		[IndexField("showpreviousnext")]
		public bool ShowPreviousNextValue
		{
			get { return ShowPreviousNext.Value; }
		}
		/// <summary><para>Template: Event Calendar Parameters</para><para>Field: TitleHeadingLevel</para><para>Data type: Droplink</para></summary>
		[IndexField("titleheadinglevel")]
		public virtual ILinkFieldWrapper TitleHeadingLevel
		{
			get { return GetField<LinkFieldWrapper>("TitleHeadingLevel", "titleheadinglevel"); }
		}

		/// <summary><para>Template: Event Calendar Parameters</para><para>Field: TitleHeadingLevel</para><para>Data type: Droplink</para></summary>
		[IndexField("titleheadinglevel")]
		public Guid TitleHeadingLevelValue
		{
			get { return TitleHeadingLevel.Value; }
		}
	
	}
	public class EventCalendarParametersTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct EventCalendarParameters
	    {
	    	public static ID ID = ID.Parse("{43E4F676-DA99-4859-9F7F-FA82F9D52277}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
						public static readonly ID CalendarType = new ID("{606078CC-C134-486D-84E7-1E5266973B5F}");
						public static readonly ID CompactView = new ID("{F7265871-664D-4F5B-91D4-C07427782ABE}");
						public static readonly ID ShowMonthCaptions = new ID("{849E1A6B-E39E-43F6-8019-B810487C8C25}");
						public static readonly ID ShowPreviousNext = new ID("{5758F3A6-31F5-441D-93ED-37BC9700D21F}");
						public static readonly ID TitleHeadingLevel = new ID("{44C2597F-790B-460E-AD68-89ADEB11AA7D}");
				
	        }
	    }
	}

	public class EventCalendarParametersRenderingModel : RenderingModelBase
	{
				
				/// <summary>
			/// <para>Template: Event Calendar Parameters</para><para>Field: CalendarType</para><para>Data type: Multilist</para>
			/// </summary>
			[IndexField("calendartype")]
				 public virtual IEnumerable<Guid> CalendarType { get; set; }
			
				/// <summary>
			/// <para>Template: Event Calendar Parameters</para><para>Field: CompactView</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("compactview")]
				 public virtual bool CompactView { get; set; }
			
				/// <summary>
			/// <para>Template: Event Calendar Parameters</para><para>Field: ShowMonthCaptions</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("showmonthcaptions")]
				 public virtual bool ShowMonthCaptions { get; set; }
			
				/// <summary>
			/// <para>Template: Event Calendar Parameters</para><para>Field: ShowPreviousNext</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("showpreviousnext")]
				 public virtual bool ShowPreviousNext { get; set; }
			
				/// <summary>
			/// <para>Template: Event Calendar Parameters</para><para>Field: TitleHeadingLevel</para><para>Data type: Droplink</para>
			/// </summary>
			[IndexField("titleheadinglevel")]
				 public virtual Guid TitleHeadingLevel { get; set; }
		
	}

}


#endregion
#region Calendar Event (Custom)
namespace Sitecore.Baptist.Feature.GeneralComponents.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Calendar Event</para>
	/// <para>ID: {62C2B7D0-1C34-4AA6-B3D3-5D612C0891BD}</para>
	/// <para>/sitecore/templates/Feature/Experience Accelerator/Events/Datasource/Calendar Event</para>
	/// </summary>
	[TemplateMapping("{62C2B7D0-1C34-4AA6-B3D3-5D612C0891BD}", "InterfaceMap")]
	public partial interface ICalendarEventItem : IItemWrapper 
	{		
		/// <summary>
		/// <para>Template: Calendar Event</para><para>Field: EndLabel</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("endlabel")]
		ITextFieldWrapper EndLabel { get; }

		/// <summary>
		/// <para>Template: Calendar Event</para><para>Field: EndLabel</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("endlabel")]
		string EndLabelValue { get; }
		/// <summary>
		/// <para>Template: Calendar Event</para><para>Field: EventDescription</para><para>Data type: Rich Text</para>
		/// </summary>
		[IndexField("eventdescription")]
		IRichTextFieldWrapper EventDescription { get; }

		/// <summary>
		/// <para>Template: Calendar Event</para><para>Field: EventDescription</para><para>Data type: Rich Text</para>
		/// </summary>
		[IndexField("eventdescription")]
		string EventDescriptionValue { get; }
		/// <summary>
		/// <para>Template: Calendar Event</para><para>Field: EventEnd</para><para>Data type: Datetime</para>
		/// </summary>
		[IndexField("eventend")]
		IDateTimeFieldWrapper EventEnd { get; }

		/// <summary>
		/// <para>Template: Calendar Event</para><para>Field: EventEnd</para><para>Data type: Datetime</para>
		/// </summary>
		[IndexField("eventend")]
		DateTime EventEndValue { get; }
		/// <summary>
		/// <para>Template: Calendar Event</para><para>Field: EventLink</para><para>Data type: General Link</para>
		/// </summary>
		[IndexField("eventlink")]
		IGeneralLinkFieldWrapper EventLink { get; }

		/// <summary>
		/// <para>Template: Calendar Event</para><para>Field: EventLink</para><para>Data type: General Link</para>
		/// </summary>
		[IndexField("eventlink")]
		string EventLinkValue { get; }
		/// <summary>
		/// <para>Template: Calendar Event</para><para>Field: EventName</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("eventname")]
		ITextFieldWrapper EventName { get; }

		/// <summary>
		/// <para>Template: Calendar Event</para><para>Field: EventName</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("eventname")]
		string EventNameValue { get; }
		/// <summary>
		/// <para>Template: Calendar Event</para><para>Field: EventPlace</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("eventplace")]
		ITextFieldWrapper EventPlace { get; }

		/// <summary>
		/// <para>Template: Calendar Event</para><para>Field: EventPlace</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("eventplace")]
		string EventPlaceValue { get; }
		/// <summary>
		/// <para>Template: Calendar Event</para><para>Field: EventStart</para><para>Data type: Datetime</para>
		/// </summary>
		[IndexField("eventstart")]
		IDateTimeFieldWrapper EventStart { get; }

		/// <summary>
		/// <para>Template: Calendar Event</para><para>Field: EventStart</para><para>Data type: Datetime</para>
		/// </summary>
		[IndexField("eventstart")]
		DateTime EventStartValue { get; }
		/// <summary>
		/// <para>Template: Calendar Event</para><para>Field: EventType</para><para>Data type: Multilist</para>
		/// </summary>
		[IndexField("eventtype")]
		IListFieldWrapper EventType { get; }

		/// <summary>
		/// <para>Template: Calendar Event</para><para>Field: EventType</para><para>Data type: Multilist</para>
		/// </summary>
		[IndexField("eventtype")]
		IEnumerable<Guid> EventTypeValue { get; }
		/// <summary>
		/// <para>Template: Calendar Event</para><para>Field: StartLabel</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("startlabel")]
		ITextFieldWrapper StartLabel { get; }

		/// <summary>
		/// <para>Template: Calendar Event</para><para>Field: StartLabel</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("startlabel")]
		string StartLabelValue { get; }
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Feature/Experience Accelerator/Events/Datasource/Calendar Event</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{62C2B7D0-1C34-4AA6-B3D3-5D612C0891BD}", typeof(Guid))]
	[TemplateMapping("{62C2B7D0-1C34-4AA6-B3D3-5D612C0891BD}", "")]
	public partial class CalendarEventItem : ItemWrapper, ICalendarEventItem
	{
		private Item _item = null;

		public CalendarEventItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public CalendarEventItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public CalendarEventItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public CalendarEventItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

		/// <summary><para>Template: Calendar Event</para><para>Field: EndLabel</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("endlabel")]
		public virtual ITextFieldWrapper EndLabel
		{
			get { return GetField<TextFieldWrapper>("EndLabel", "endlabel"); }
		}

		/// <summary><para>Template: Calendar Event</para><para>Field: EndLabel</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("endlabel")]
		public string EndLabelValue
		{
			get { return EndLabel.Value; }
		}
		/// <summary><para>Template: Calendar Event</para><para>Field: EventDescription</para><para>Data type: Rich Text</para></summary>
		[IndexField("eventdescription")]
		public virtual IRichTextFieldWrapper EventDescription
		{
			get { return GetField<RichTextFieldWrapper>("EventDescription", "eventdescription"); }
		}

		/// <summary><para>Template: Calendar Event</para><para>Field: EventDescription</para><para>Data type: Rich Text</para></summary>
		[IndexField("eventdescription")]
		public string EventDescriptionValue
		{
			get { return EventDescription.Value; }
		}
		/// <summary><para>Template: Calendar Event</para><para>Field: EventEnd</para><para>Data type: Datetime</para></summary>
		[IndexField("eventend")]
		public virtual IDateTimeFieldWrapper EventEnd
		{
			get { return GetField<DateTimeFieldWrapper>("EventEnd", "eventend"); }
		}

		/// <summary><para>Template: Calendar Event</para><para>Field: EventEnd</para><para>Data type: Datetime</para></summary>
		[IndexField("eventend")]
		public DateTime EventEndValue
		{
			get { return EventEnd.Value; }
		}
		/// <summary><para>Template: Calendar Event</para><para>Field: EventLink</para><para>Data type: General Link</para></summary>
		[IndexField("eventlink")]
		public virtual IGeneralLinkFieldWrapper EventLink
		{
			get { return GetField<GeneralLinkFieldWrapper>("EventLink", "eventlink"); }
		}

		/// <summary><para>Template: Calendar Event</para><para>Field: EventLink</para><para>Data type: General Link</para></summary>
		[IndexField("eventlink")]
		public string EventLinkValue
		{
			get { return EventLink.Value; }
		}
		/// <summary><para>Template: Calendar Event</para><para>Field: EventName</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("eventname")]
		public virtual ITextFieldWrapper EventName
		{
			get { return GetField<TextFieldWrapper>("EventName", "eventname"); }
		}

		/// <summary><para>Template: Calendar Event</para><para>Field: EventName</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("eventname")]
		public string EventNameValue
		{
			get { return EventName.Value; }
		}
		/// <summary><para>Template: Calendar Event</para><para>Field: EventPlace</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("eventplace")]
		public virtual ITextFieldWrapper EventPlace
		{
			get { return GetField<TextFieldWrapper>("EventPlace", "eventplace"); }
		}

		/// <summary><para>Template: Calendar Event</para><para>Field: EventPlace</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("eventplace")]
		public string EventPlaceValue
		{
			get { return EventPlace.Value; }
		}
		/// <summary><para>Template: Calendar Event</para><para>Field: EventStart</para><para>Data type: Datetime</para></summary>
		[IndexField("eventstart")]
		public virtual IDateTimeFieldWrapper EventStart
		{
			get { return GetField<DateTimeFieldWrapper>("EventStart", "eventstart"); }
		}

		/// <summary><para>Template: Calendar Event</para><para>Field: EventStart</para><para>Data type: Datetime</para></summary>
		[IndexField("eventstart")]
		public DateTime EventStartValue
		{
			get { return EventStart.Value; }
		}
		/// <summary><para>Template: Calendar Event</para><para>Field: EventType</para><para>Data type: Multilist</para></summary>
		[IndexField("eventtype")]
		public virtual IListFieldWrapper EventType
		{
			get { return GetField<ListFieldWrapper>("EventType", "eventtype"); }
		}

		/// <summary><para>Template: Calendar Event</para><para>Field: EventType</para><para>Data type: Multilist</para></summary>
		[IndexField("eventtype")]
		public IEnumerable<Guid> EventTypeValue
		{
			get { return EventType.Value; }
		}
		/// <summary><para>Template: Calendar Event</para><para>Field: StartLabel</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("startlabel")]
		public virtual ITextFieldWrapper StartLabel
		{
			get { return GetField<TextFieldWrapper>("StartLabel", "startlabel"); }
		}

		/// <summary><para>Template: Calendar Event</para><para>Field: StartLabel</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("startlabel")]
		public string StartLabelValue
		{
			get { return StartLabel.Value; }
		}
	
	}
	public class CalendarEventTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct CalendarEvent
	    {
	    	public static ID ID = ID.Parse("{62C2B7D0-1C34-4AA6-B3D3-5D612C0891BD}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
						public static readonly ID EndLabel = new ID("{A746E8B0-B3A9-4918-A3C6-DB8977DD0DA5}");
						public static readonly ID EventDescription = new ID("{79825619-C640-4892-98A8-9C4B112673D5}");
						public static readonly ID EventEnd = new ID("{C9999F64-A453-46A6-AE47-C423599986A7}");
						public static readonly ID EventLink = new ID("{84BFA68D-1D6A-4187-86D4-D7EC6294D5F0}");
						public static readonly ID EventName = new ID("{1FB9F55D-2C43-49C2-A61D-DF8552C88C20}");
						public static readonly ID EventPlace = new ID("{62BAFE3F-B288-429A-928E-B6A516A198B6}");
						public static readonly ID EventStart = new ID("{9E47D308-2E9C-413A-AC93-3C51C52E74D2}");
						public static readonly ID EventType = new ID("{2C51A553-BB18-4DB2-B887-11AE200F96A9}");
						public static readonly ID StartLabel = new ID("{B2ACD4DA-A306-460B-81DC-FE40D3461A43}");
				
	        }
	    }
	}

	public class CalendarEventRenderingModel : RenderingModelBase
	{
				
				/// <summary>
			/// <para>Template: Calendar Event</para><para>Field: EndLabel</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("endlabel")]
				 public virtual string EndLabel { get; set; }
			
				/// <summary>
			/// <para>Template: Calendar Event</para><para>Field: EventDescription</para><para>Data type: Rich Text</para>
			/// </summary>
			[IndexField("eventdescription")]
				 public virtual string EventDescription { get; set; }
			
				/// <summary>
			/// <para>Template: Calendar Event</para><para>Field: EventEnd</para><para>Data type: Datetime</para>
			/// </summary>
			[IndexField("eventend")]
				 public virtual DateTime EventEnd { get; set; }
			
				/// <summary>
			/// <para>Template: Calendar Event</para><para>Field: EventLink</para><para>Data type: General Link</para>
			/// </summary>
			[IndexField("eventlink")]
				 public virtual string EventLink { get; set; }
			
				/// <summary>
			/// <para>Template: Calendar Event</para><para>Field: EventName</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("eventname")]
				 public virtual string EventName { get; set; }
			
				/// <summary>
			/// <para>Template: Calendar Event</para><para>Field: EventPlace</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("eventplace")]
				 public virtual string EventPlace { get; set; }
			
				/// <summary>
			/// <para>Template: Calendar Event</para><para>Field: EventStart</para><para>Data type: Datetime</para>
			/// </summary>
			[IndexField("eventstart")]
				 public virtual DateTime EventStart { get; set; }
			
				/// <summary>
			/// <para>Template: Calendar Event</para><para>Field: EventType</para><para>Data type: Multilist</para>
			/// </summary>
			[IndexField("eventtype")]
				 public virtual IEnumerable<Guid> EventType { get; set; }
			
				/// <summary>
			/// <para>Template: Calendar Event</para><para>Field: StartLabel</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("startlabel")]
				 public virtual string StartLabel { get; set; }
		
	}

}


#endregion
#region Baptist Combined Event List Parameters (Custom)
namespace Sitecore.Baptist.Feature.GeneralComponents.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Baptist Combined Event List Parameters</para>
	/// <para>ID: {7F6B2F13-F70E-43C7-A4A8-719FF8222846}</para>
	/// <para>/sitecore/templates/Feature/Experience Accelerator/Events/Rendering Parameters/Baptist Combined Event List Parameters</para>
	/// </summary>
	[TemplateMapping("{7F6B2F13-F70E-43C7-A4A8-719FF8222846}", "InterfaceMap")]
	public partial interface IBaptistCombinedEventListParametersItem : IItemWrapper 
	{		
		/// <summary>
		/// <para>Template: Baptist Combined Event List Parameters</para><para>Field: EventLists</para><para>Data type: Multilist</para>
		/// </summary>
		[IndexField("event_lists")]
		IListFieldWrapper EventLists { get; }

		/// <summary>
		/// <para>Template: Baptist Combined Event List Parameters</para><para>Field: EventLists</para><para>Data type: Multilist</para>
		/// </summary>
		[IndexField("event_lists")]
		IEnumerable<Guid> EventListsValue { get; }
		/// <summary>
		/// <para>Template: Baptist Combined Event List Parameters</para><para>Field: HideLocationFilter</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("hide_location_filter")]
		IBooleanFieldWrapper HideLocationFilter { get; }

		/// <summary>
		/// <para>Template: Baptist Combined Event List Parameters</para><para>Field: HideLocationFilter</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("hide_location_filter")]
		bool HideLocationFilterValue { get; }
		/// <summary>
		/// <para>Template: Baptist Combined Event List Parameters</para><para>Field: HideTopicFilter</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("hide_topic_filter")]
		IBooleanFieldWrapper HideTopicFilter { get; }

		/// <summary>
		/// <para>Template: Baptist Combined Event List Parameters</para><para>Field: HideTopicFilter</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("hide_topic_filter")]
		bool HideTopicFilterValue { get; }
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Feature/Experience Accelerator/Events/Rendering Parameters/Baptist Combined Event List Parameters</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{7F6B2F13-F70E-43C7-A4A8-719FF8222846}", typeof(Guid))]
	[TemplateMapping("{7F6B2F13-F70E-43C7-A4A8-719FF8222846}", "")]
	public partial class BaptistCombinedEventListParametersItem : ItemWrapper, IBaptistCombinedEventListParametersItem
	{
		private Item _item = null;

		public BaptistCombinedEventListParametersItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public BaptistCombinedEventListParametersItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public BaptistCombinedEventListParametersItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public BaptistCombinedEventListParametersItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

		/// <summary><para>Template: Baptist Combined Event List Parameters</para><para>Field: EventLists</para><para>Data type: Multilist</para></summary>
		[IndexField("event_lists")]
		public virtual IListFieldWrapper EventLists
		{
			get { return GetField<ListFieldWrapper>("Event Lists", "event_lists"); }
		}

		/// <summary><para>Template: Baptist Combined Event List Parameters</para><para>Field: EventLists</para><para>Data type: Multilist</para></summary>
		[IndexField("event_lists")]
		public IEnumerable<Guid> EventListsValue
		{
			get { return EventLists.Value; }
		}
		/// <summary><para>Template: Baptist Combined Event List Parameters</para><para>Field: HideLocationFilter</para><para>Data type: Checkbox</para></summary>
		[IndexField("hide_location_filter")]
		public virtual IBooleanFieldWrapper HideLocationFilter
		{
			get { return GetField<BooleanFieldWrapper>("Hide Location Filter", "hide_location_filter"); }
		}

		/// <summary><para>Template: Baptist Combined Event List Parameters</para><para>Field: HideLocationFilter</para><para>Data type: Checkbox</para></summary>
		[IndexField("hide_location_filter")]
		public bool HideLocationFilterValue
		{
			get { return HideLocationFilter.Value; }
		}
		/// <summary><para>Template: Baptist Combined Event List Parameters</para><para>Field: HideTopicFilter</para><para>Data type: Checkbox</para></summary>
		[IndexField("hide_topic_filter")]
		public virtual IBooleanFieldWrapper HideTopicFilter
		{
			get { return GetField<BooleanFieldWrapper>("Hide Topic Filter", "hide_topic_filter"); }
		}

		/// <summary><para>Template: Baptist Combined Event List Parameters</para><para>Field: HideTopicFilter</para><para>Data type: Checkbox</para></summary>
		[IndexField("hide_topic_filter")]
		public bool HideTopicFilterValue
		{
			get { return HideTopicFilter.Value; }
		}
	
	}
	public class BaptistCombinedEventListParametersTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct BaptistCombinedEventListParameters
	    {
	    	public static ID ID = ID.Parse("{7F6B2F13-F70E-43C7-A4A8-719FF8222846}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
						public static readonly ID EventLists = new ID("{27AAD94D-BDC8-4F55-A314-0A600DDA259A}");
						public static readonly ID HideLocationFilter = new ID("{17DC6B12-74E7-4F67-B3FA-7B4B43DF7818}");
						public static readonly ID HideTopicFilter = new ID("{DCA0E7E3-DD6A-4ADA-B210-F776854E792D}");
				
	        }
	    }
	}

	public class BaptistCombinedEventListParametersRenderingModel : RenderingModelBase
	{
				
				/// <summary>
			/// <para>Template: Baptist Combined Event List Parameters</para><para>Field: EventLists</para><para>Data type: Multilist</para>
			/// </summary>
			[IndexField("event_lists")]
				 public virtual IEnumerable<Guid> EventLists { get; set; }
			
				/// <summary>
			/// <para>Template: Baptist Combined Event List Parameters</para><para>Field: HideLocationFilter</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("hide_location_filter")]
				 public virtual bool HideLocationFilter { get; set; }
			
				/// <summary>
			/// <para>Template: Baptist Combined Event List Parameters</para><para>Field: HideTopicFilter</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("hide_topic_filter")]
				 public virtual bool HideTopicFilter { get; set; }
		
	}

}


#endregion
#region Events Folder (Custom)
namespace Sitecore.Baptist.Feature.GeneralComponents.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Events Folder</para>
	/// <para>ID: {AF1CC1AF-99CC-4E3C-9A26-3EAD62890FCE}</para>
	/// <para>/sitecore/templates/Feature/Experience Accelerator/Events/Datasource/Events Folder</para>
	/// </summary>
	[TemplateMapping("{AF1CC1AF-99CC-4E3C-9A26-3EAD62890FCE}", "InterfaceMap")]
	public partial interface IEventsFolderItem : IItemWrapper 
	{		
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Feature/Experience Accelerator/Events/Datasource/Events Folder</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{AF1CC1AF-99CC-4E3C-9A26-3EAD62890FCE}", typeof(Guid))]
	[TemplateMapping("{AF1CC1AF-99CC-4E3C-9A26-3EAD62890FCE}", "")]
	public partial class EventsFolderItem : ItemWrapper, IEventsFolderItem
	{
		private Item _item = null;

		public EventsFolderItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public EventsFolderItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public EventsFolderItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public EventsFolderItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

	
	}
	public class EventsFolderTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct EventsFolder
	    {
	    	public static ID ID = ID.Parse("{AF1CC1AF-99CC-4E3C-9A26-3EAD62890FCE}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
				
	        }
	    }
	}

	public class EventsFolderRenderingModel : RenderingModelBase
	{
			
	}

}


#endregion
#region Baptist Event List (Custom)
namespace Sitecore.Baptist.Feature.GeneralComponents.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Baptist Event List</para>
	/// <para>ID: {B7F7355A-E858-4D9F-A36A-FDC4C061DB44}</para>
	/// <para>/sitecore/templates/Feature/Experience Accelerator/Events/Datasource/Baptist Event List</para>
	/// </summary>
	[TemplateMapping("{B7F7355A-E858-4D9F-A36A-FDC4C061DB44}", "InterfaceMap")]
	public partial interface IBaptistEventListItem : IItemWrapper , Sitecore.Baptist.Feature.GeneralComponents.Templates.Custom.IEventListItem
	{		
		/// <summary>
		/// <para>Template: Baptist Event List</para><para>Field: EventListCategory</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("event_list_category")]
		ITextFieldWrapper EventListCategory { get; }

		/// <summary>
		/// <para>Template: Baptist Event List</para><para>Field: EventListCategory</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("event_list_category")]
		string EventListCategoryValue { get; }
		/// <summary>
		/// <para>Template: Baptist Event List</para><para>Field: Location</para><para>Data type: Droptree</para>
		/// </summary>
		[IndexField("location")]
		ILinkFieldWrapper Location { get; }

		/// <summary>
		/// <para>Template: Baptist Event List</para><para>Field: Location</para><para>Data type: Droptree</para>
		/// </summary>
		[IndexField("location")]
		Guid LocationValue { get; }
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Feature/Experience Accelerator/Events/Datasource/Baptist Event List</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{B7F7355A-E858-4D9F-A36A-FDC4C061DB44}", typeof(Guid))]
	[TemplateMapping("{B7F7355A-E858-4D9F-A36A-FDC4C061DB44}", "")]
	public partial class BaptistEventListItem : ItemWrapper, IBaptistEventListItem
	{
		private Item _item = null;

		public BaptistEventListItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public BaptistEventListItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public BaptistEventListItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public BaptistEventListItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

		/// <summary><para>Template: Baptist Event List</para><para>Field: EventTypes</para><para>Data type: Droplink</para></summary>
		[IndexField("eventtypes")]
		public virtual ILinkFieldWrapper EventTypes
		{
			get { return GetField<LinkFieldWrapper>("EventTypes", "eventtypes"); }
		}

		/// <summary><para>Template: Baptist Event List</para><para>Field: EventTypes</para><para>Data type: Droplink</para></summary>
		[IndexField("eventtypes")]
		public Guid EventTypesValue
		{
			get { return EventTypes.Value; }
		}
		/// <summary><para>Template: Baptist Event List</para><para>Field: GoogleCalendarAPIKey</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("googlecalendarapikey")]
		public virtual ITextFieldWrapper GoogleCalendarAPIKey
		{
			get { return GetField<TextFieldWrapper>("GoogleCalendarAPIKey", "googlecalendarapikey"); }
		}

		/// <summary><para>Template: Baptist Event List</para><para>Field: GoogleCalendarAPIKey</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("googlecalendarapikey")]
		public string GoogleCalendarAPIKeyValue
		{
			get { return GoogleCalendarAPIKey.Value; }
		}
		/// <summary><para>Template: Baptist Event List</para><para>Field: GoogleCalendarID</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("googlecalendarid")]
		public virtual ITextFieldWrapper GoogleCalendarID
		{
			get { return GetField<TextFieldWrapper>("GoogleCalendarID", "googlecalendarid"); }
		}

		/// <summary><para>Template: Baptist Event List</para><para>Field: GoogleCalendarID</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("googlecalendarid")]
		public string GoogleCalendarIDValue
		{
			get { return GoogleCalendarID.Value; }
		}
		/// <summary><para>Template: Baptist Event List</para><para>Field: Title</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("title")]
		public virtual ITextFieldWrapper Title
		{
			get { return GetField<TextFieldWrapper>("Title", "title"); }
		}

		/// <summary><para>Template: Baptist Event List</para><para>Field: Title</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("title")]
		public string TitleValue
		{
			get { return Title.Value; }
		}
		/// <summary><para>Template: Baptist Event List</para><para>Field: EventListCategory</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("event_list_category")]
		public virtual ITextFieldWrapper EventListCategory
		{
			get { return GetField<TextFieldWrapper>("Event List Category", "event_list_category"); }
		}

		/// <summary><para>Template: Baptist Event List</para><para>Field: EventListCategory</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("event_list_category")]
		public string EventListCategoryValue
		{
			get { return EventListCategory.Value; }
		}
		/// <summary><para>Template: Baptist Event List</para><para>Field: Location</para><para>Data type: Droptree</para></summary>
		[IndexField("location")]
		public virtual ILinkFieldWrapper Location
		{
			get { return GetField<LinkFieldWrapper>("Location", "location"); }
		}

		/// <summary><para>Template: Baptist Event List</para><para>Field: Location</para><para>Data type: Droptree</para></summary>
		[IndexField("location")]
		public Guid LocationValue
		{
			get { return Location.Value; }
		}
	
	}
	public class BaptistEventListTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct BaptistEventList
	    {
	    	public static ID ID = ID.Parse("{B7F7355A-E858-4D9F-A36A-FDC4C061DB44}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
						public static readonly ID EventListCategory = new ID("{5A61C238-65CC-4C6C-9304-4F752C1907CC}");
						public static readonly ID Location = new ID("{49230889-C346-4236-A91C-77E7BFB72446}");
				
	        }
	    }
	}

	public class BaptistEventListRenderingModel : RenderingModelBase
	{
				
				/// <summary>
			/// <para>Template: Baptist Event List</para><para>Field: EventListCategory</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("event_list_category")]
				 public virtual string EventListCategory { get; set; }
			
				/// <summary>
			/// <para>Template: Baptist Event List</para><para>Field: Location</para><para>Data type: Droptree</para>
			/// </summary>
			[IndexField("location")]
				 public virtual Guid Location { get; set; }
		
	}

}


#endregion
#region Event List Parameters (Custom)
namespace Sitecore.Baptist.Feature.GeneralComponents.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Event List Parameters</para>
	/// <para>ID: {CA514E36-37FB-4F43-B024-F7F365B91B9E}</para>
	/// <para>/sitecore/templates/Feature/Experience Accelerator/Events/Rendering Parameters/Event List Parameters</para>
	/// </summary>
	[TemplateMapping("{CA514E36-37FB-4F43-B024-F7F365B91B9E}", "InterfaceMap")]
	public partial interface IEventListParametersItem : IItemWrapper 
	{		
		/// <summary>
		/// <para>Template: Event List Parameters</para><para>Field: TitleHeadingLevel</para><para>Data type: Droplink</para>
		/// </summary>
		[IndexField("titleheadinglevel")]
		ILinkFieldWrapper TitleHeadingLevel { get; }

		/// <summary>
		/// <para>Template: Event List Parameters</para><para>Field: TitleHeadingLevel</para><para>Data type: Droplink</para>
		/// </summary>
		[IndexField("titleheadinglevel")]
		Guid TitleHeadingLevelValue { get; }
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Feature/Experience Accelerator/Events/Rendering Parameters/Event List Parameters</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{CA514E36-37FB-4F43-B024-F7F365B91B9E}", typeof(Guid))]
	[TemplateMapping("{CA514E36-37FB-4F43-B024-F7F365B91B9E}", "")]
	public partial class EventListParametersItem : ItemWrapper, IEventListParametersItem
	{
		private Item _item = null;

		public EventListParametersItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public EventListParametersItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public EventListParametersItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public EventListParametersItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

		/// <summary><para>Template: Event List Parameters</para><para>Field: TitleHeadingLevel</para><para>Data type: Droplink</para></summary>
		[IndexField("titleheadinglevel")]
		public virtual ILinkFieldWrapper TitleHeadingLevel
		{
			get { return GetField<LinkFieldWrapper>("TitleHeadingLevel", "titleheadinglevel"); }
		}

		/// <summary><para>Template: Event List Parameters</para><para>Field: TitleHeadingLevel</para><para>Data type: Droplink</para></summary>
		[IndexField("titleheadinglevel")]
		public Guid TitleHeadingLevelValue
		{
			get { return TitleHeadingLevel.Value; }
		}
	
	}
	public class EventListParametersTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct EventListParameters
	    {
	    	public static ID ID = ID.Parse("{CA514E36-37FB-4F43-B024-F7F365B91B9E}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
						public static readonly ID TitleHeadingLevel = new ID("{31EA642D-5757-4CF3-863B-DCFBD73B277A}");
				
	        }
	    }
	}

	public class EventListParametersRenderingModel : RenderingModelBase
	{
				
				/// <summary>
			/// <para>Template: Event List Parameters</para><para>Field: TitleHeadingLevel</para><para>Data type: Droplink</para>
			/// </summary>
			[IndexField("titleheadinglevel")]
				 public virtual Guid TitleHeadingLevel { get; set; }
		
	}

}


#endregion
#region Event Types (Custom)
namespace Sitecore.Baptist.Feature.GeneralComponents.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Event Types</para>
	/// <para>ID: {FD612519-1C44-48B7-A44A-D582E22C38DD}</para>
	/// <para>/sitecore/templates/Feature/Experience Accelerator/Events/Event Types</para>
	/// </summary>
	[TemplateMapping("{FD612519-1C44-48B7-A44A-D582E22C38DD}", "InterfaceMap")]
	public partial interface IEventTypesItem : IItemWrapper 
	{		
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Feature/Experience Accelerator/Events/Event Types</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{FD612519-1C44-48B7-A44A-D582E22C38DD}", typeof(Guid))]
	[TemplateMapping("{FD612519-1C44-48B7-A44A-D582E22C38DD}", "")]
	public partial class EventTypesItem : ItemWrapper, IEventTypesItem
	{
		private Item _item = null;

		public EventTypesItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public EventTypesItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public EventTypesItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public EventTypesItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

	
	}
	public class EventTypesTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct EventTypes
	    {
	    	public static ID ID = ID.Parse("{FD612519-1C44-48B7-A44A-D582E22C38DD}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
				
	        }
	    }
	}

	public class EventTypesRenderingModel : RenderingModelBase
	{
			
	}

}


#endregion
