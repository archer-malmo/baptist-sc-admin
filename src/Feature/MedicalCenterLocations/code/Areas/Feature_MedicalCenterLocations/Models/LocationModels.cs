﻿using Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Models
{
    public class UserGeo
    {
        public string ip { get; set; }
        public float latitude { get; set; }
        public float longitude { get; set; }
        public string country_code { get; set; }
        public string country_name { get; set; }
        public string region_code { get; set; }
        public string region_name { get; set; }
        public string city { get; set; }
        public string ziP_code { get; set; }
        public string time_zone { get; set; }
        public string metro_code { get; set; }
    }

    public class LocationListing 
    {
        public List<LocationWithDistance> locations { get; set; }                
        public bool hospitallistings { get; set; }
        public string locationtype { get; set; }
    }

    public class LocationListingSearch
    {
        public List<LocationWithDistanceSearch> locations { get; set; }
        public int radius { get; set; }
        public bool hospitallistings { get; set; }
        public string locationtype { get; set; }
        public bool scheduling_only { get; set; }
    }

    public class LocationWithDistance
    {
        public LocationItemRenderingModel location { get; set; }
        public double? distance { get; set; }
        public ERWaitModel er_wait { get; set; }
    }  

    public class LocationWithDistanceSearch
    {
        public LocationSearchResultItem location { get; set; }
        public double? distance { get; set; }
        public ERWaitModel er_wait { get; set; }      
        public int radius { get; set; }  
    }
    
    public class ERWaitModel
    {     
       public string CV_ED_Wait { get; set; }        
    }

    public class LocationDetailModel
    {
        public Item pageitem { get; set; }
        public LocationItemRenderingModel location { get; set; }
    }

    public class ERWaitListings
    {
        public LocationWithDistance def { get; set; }
        public List<LocationWithDistance> locations { get; set; }
        //public List<LocationWithDistance> locations { get; set; }
    }

    public class ERWaitListingsSearch {

        public LocationWithDistanceSearch def { get; set; }
        public List<LocationWithDistanceSearch> locations { get; set; }
    }

    public class LocationListingResSearch
    {
        public LocationListingSearch locationlisting { get; set; }
        public RenderingContext renderingcontext { get; set; }
        public string originalquery { get; set; }
        public bool viewall { get; set; }
        public string locationtype { get; set; }
    }


    public class LocationListingResult 
    {
        public LocationListing locationlisting { get; set; }
        public RenderingContext renderingcontext { get; set; }        
        public string originalquery { get; set; }
        public bool viewall { get; set; }
    }

  
    public class LocationHoursResult
    {
        public string days { get; set; }
        public string hours { get; set; }
    }

    public class ServicesProvidedContentModel
    {
        public ServicesProvidedContentRenderingModel servicesprovidedcontent { get; set; }
        public LocationItemRenderingModel location { get; set; }
    }

    [DataContract]
    public class ServiceTerm
    {
        [DataMember(Name = "@TermName")]
        public string TermName { get; set; }
        [DataMember(Name = "@TermURL")]
        public string TermUrl { get; set; }
    }
}