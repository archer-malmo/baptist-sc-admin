﻿using Sitecore.Baptist.Feature.PhysicianDirectory.Templates.Custom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Models;
using Sitecore.Data.Items;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Utils;
using Sitecore.Diagnostics;
using Sitecore.Baptist.Feature.PhysicianDirectory.Areas.Feature_PhysicianDirectory.Models;
using Sitecore.Baptist.Foundation.Common.Utils;
using Sitecore.Baptist.Foundation.Common.Extensions;
using Sitecore.ContentSearch.SearchTypes;
using Newtonsoft.Json;

namespace Sitecore.Baptist.Feature.PhysicianDirectory.Areas.Feature_PhysicianDirectory.Utils
{
    public class PhysicianHelper
    {
        //get formatted doctors name
        public static string formatPhysicianName(PhysicianRenderingModel physician, bool abbreviatemiddle = false) {

            string physicianname = "";

            List<string> ltitles = new List<string>();
            string titles = "";

            if(physician.ProfessionalTitles != null && physician.ProfessionalTitles.HasKeys() && physician.ProfessionalTitles.Count > 0) {
                ltitles = physician.ProfessionalTitles.Cast<string>().Select(e => HttpUtility.UrlDecode(physician.ProfessionalTitles[e])).ToList<string>();
                foreach(string title in ltitles)
                {
                    titles += title + ",";
                }
                titles = titles.TrimEnd(' ', ',');
            }                                                
            

            string middle = "";
            if(abbreviatemiddle)
            {
                middle = ((!string.IsNullOrEmpty(physician.MIddleName) ? " " + physician.MIddleName.Substring(0, 1) + "." : ""));
                
            }else {
                middle = ((!string.IsNullOrEmpty(physician.MIddleName) ? " " + physician.MIddleName : ""));
            }

            if(physician.PreferredName.HasValue())
            {
                physicianname = string.Format("{0}{1}{2}", physician.PreferredName, " " + physician.LastName, ((!string.IsNullOrEmpty(titles)) ? ", " + titles : ""));
            }
            else
            {
                physicianname = string.Format("{0}{1}{2}{3}", physician.FirstName, middle, " " + physician.LastName, ((!string.IsNullOrEmpty(titles)) ? ", " + titles : ""));
            }

            return physicianname;
        }     
        
        public static string formatSearchPhysicianName(SearchResultItem physician, bool abbreviatemiddle = false) {
            string physicianname;

            string titles = "";

            List<string> ltitles = ((string)physician.Fields["professional_titles"]).Split(',').ToList<string>();

            foreach(string title in ltitles)
            {
                titles += title + ",";
            }
            titles = titles.TrimEnd(' ', ',');


            string middle = "";

            string middlenamefield = "middle_name";

            if(abbreviatemiddle)
            {
                middle = ((physician.Fields.Keys.Contains("middle_name") && !string.IsNullOrEmpty((string)physician.Fields[middlenamefield]) ? " " + ((string)physician.Fields[middlenamefield]).Substring(0, 1) + "." : ""));
            }
            else
            {
                middle = ((physician.Fields.Keys.Contains("middle_name") && !string.IsNullOrEmpty((string)physician.Fields[middlenamefield]) ? " " + physician.Fields[middlenamefield] : ""));
            }

            string preferrednamefield = "preferred_name";

            if(physician.Fields.Keys.Contains("preferred_name") && !string.IsNullOrEmpty((string)physician.Fields[preferrednamefield]) )
            {
                physicianname = string.Format("{0}{1}{2}", physician.Fields["preferred_name"], " " + physician.Fields["last_name"], ((!string.IsNullOrEmpty(titles)) ? ", " + titles : ""));
            }
            else
            {
                physicianname = string.Format("{0}{1}{2}{3}", physician.Fields["first_name"], middle, " " + physician.Fields["last_name"], ((!string.IsNullOrEmpty(titles)) ? ", " + titles : ""));
            }

            return physicianname;
        }

        //get concatenated string of physician specialties
        public static string getPhysicianSpecialties( PhysicianRenderingModel physician, bool isBCCOnly = false ) {

            string specialties = "";
            List<string> values = null;
            if(isBCCOnly)
            {
               values = physician.BCCSpecialties.Cast<string>().Select(e => HttpUtility.UrlDecode(physician.BCCSpecialties[e])).ToList<string>();
            }
            else
            {
               values = physician.Specialties.Cast<string>().Select(e => HttpUtility.UrlDecode(physician.Specialties[e])).ToList<string>();

            }

            foreach(string specialty in values) {
                specialties += specialty + ", ";
            }
            specialties = specialties.TrimEnd(' ',',');
            
            return specialties;
        }

        //get concatenated string of physician scheduling specialties
        public static string getPhysicianSchedulingSpecialties( PhysicianRenderingModel physician )
        {

            string specialties = "";

            List<string> values = new List<string>();

            if(physician.SchedulingSpecialties != null)
            {
                values = physician.SchedulingSpecialties.Cast<string>().Select(e => HttpUtility.UrlDecode(physician.SchedulingSpecialties[e])).ToList<string>();
            }

            foreach(string specialty in values)
            {
                specialties += specialty + ", ";
            }
            specialties = specialties.TrimEnd(' ', ',');

            return specialties;
        }


        //get all physician locations
        public static List<PhysicianLocation> getPhysicianLocations( PhysicianRenderingModel physician) {

            string folderPath = "/sitecore/content/Baptist/Baptist/Baptistonline/United States/Data/Locations";

            List<PhysicianLocation> lirm = new List<PhysicianLocation>();

            List<string> locationids = physician.Locations.Cast<string>().Select(e => HttpUtility.UrlDecode(physician.Locations[e])).ToList<string>();

            PhysicianUtils pu = new PhysicianUtils();
                        

            List<PhysicianLocation> locs = pu.getDescendantPhysicianLocations(folderPath);
            /*foreach(LocationItemRenderingModel loc in locs) {
                Log.Info("Location ID:" + loc.ClientID + " Type : " + loc.LocationType, typeof(PhysicianHelper));
            }*/
            
            foreach(string locid in locationids) {
                //Sitecore.Diagnostics.Log.Info("Location IDS for :" + physician.FirstName + " " + physician.LastName + " : " + locid, typeof(PhysicianHelper));
                PhysicianLocation locitem = locs.Where(x => x.ClientID == locid).FirstOrDefault();

                if(locitem != null) {
                    lirm.Add(locitem);
                }
            }
            return lirm;
        }

        //get physician primary location
        public static PhysicianLocation getPrimaryLocation( PhysicianRenderingModel physician )
        {
            List<PhysicianLocation> plocations = getPhysicianLocations(physician);

            PhysicianLocation primary = new PhysicianLocation();

            //Log.Info("Physician Primaary --- " + physician.PrimaryLocationID, typeof(PhysicianHelper));

            if(physician.PrimaryLocationID.HasValue() == false && plocations.Count > 0)
            {                
                primary = plocations.First();
            }
            else
            {
                primary = plocations.Where(x => x.ClientID == physician.PrimaryLocationID).FirstOrDefault();
            }

            //Log.Info("Matching with --- " + primary.ClientID, typeof(PhysicianHelper));

            return primary;
        }

        //get meet the doctor video
        public static List<MediaResource> getMeetTheDoctorVideo( PhysicianRenderingModel physician)
        {
            List<MediaResource> resources = new List<MediaResource>();
            if(physician.MediaResources.HasValue())
            {
                try
                {
                    resources = JsonConvert.DeserializeObject<List<MediaResource>>(physician.MediaResources);
                }catch(Exception ex) {
                    Log.Error("PHYSICAN_PROFILE_ERROR Problem loading media resource for " + physician.ClientID + " " + formatPhysicianName(physician) + " " + ex.Message, typeof(PhysicianHelper));
                    resources = new List<MediaResource>();
                }
            }

            try
            {
                if(resources.Count() > 0)
                {
                    foreach(MediaResource res in resources)
                    {
                        if(res.Type.ToLower() != "video")
                        {
                            resources.Remove(res);
                        }
                    }
                    return resources;
                }else {
                    return new List<MediaResource>();
                }
            }catch(Exception ex) {
                Log.Error("PHYSICAN_PROFILE_ERROR Problem loading media resource for " + physician.ClientID + " " + formatPhysicianName(physician) + " " + ex.Message, typeof(PhysicianHelper));
                return new List<MediaResource>();
            }

            
        }

    }
}