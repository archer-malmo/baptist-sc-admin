﻿using System.Web.Mvc;

namespace Sitecore.Baptist.Feature.CareersIcims.Areas.Feature_CareersIcims
{
    public class Feature_CareersIcimsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Feature_CareersIcims";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Feature_CareersIcims_default",
                "Feature_CareersIcims/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}