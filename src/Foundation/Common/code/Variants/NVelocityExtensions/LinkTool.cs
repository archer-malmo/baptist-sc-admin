﻿using Newtonsoft.Json;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Links;


namespace Sitecore.Baptist.Foundation.Common.Variants.NVelocityExtensions
{
    public class LinkTool
    {
        public static string GetItemLink(Item item, bool includeServerUrl = false)
        {
            var options = UrlOptions.DefaultOptions.Clone() as UrlOptions;

            if(includeServerUrl) {
                options.AlwaysIncludeServerUrl = true;
            }

            return LinkManager.GetItemUrl(item, options);
        }

        public static string getText(Item item) {

            ID linkid = ID.Parse("{DD05144A-BD66-4EA3-956A-4B7104DAABA3}");
            Sitecore.Data.Fields.LinkField link = item.Fields["Link"];
            return link.Text;
        }
        
        public static string getTarget(Item item) {
            ID linkid = ID.Parse("{DD05144A-BD66-4EA3-956A-4B7104DAABA3}");
            Sitecore.Data.Fields.LinkField link = item.Fields["Link"];
            return link.Target;
        }

        public static string getUrl( Item item )
        {
            ID linkid = ID.Parse("{DD05144A-BD66-4EA3-956A-4B7104DAABA3}");
            Sitecore.Data.Fields.LinkField link = item.Fields["Link"];
            return link.Url;
        }

        public static string getTitle(Item item) {
            ID linkid = ID.Parse("{DD05144A-BD66-4EA3-956A-4B7104DAABA3}");
            Sitecore.Data.Fields.LinkField link = item.Fields["Link"];
            return link.Title;
        }

        public static string getClass(Item item) {
            ID linkid = ID.Parse("{DD05144A-BD66-4EA3-956A-4B7104DAABA3}");
            Sitecore.Data.Fields.LinkField link = item.Fields["Link"];
            return link.Class;
        }

        public static string getItemJson(Item item) {
            return JsonConvert.SerializeObject(item);
        }
    }
}