using Sitecore.Data.Items;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Baptist.Feature.PhysicianDirectory.Templates.Custom;
using Sitecore.XA.Foundation.Mvc.Models;
using Sitecore.Data;
using System.Runtime.InteropServices;
using Fortis.Model.Fields;
#region Physician Specialty Listing (Custom)
namespace Sitecore.Baptist.Feature.PhysicianDirectory.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Physician Specialty Listing</para>
	/// <para>ID: {1EE05F1C-ADD1-4B7D-96C6-2B3BEC0CA8AC}</para>
	/// <para>/sitecore/templates/Feature/Experience Accelerator/Physicians Directory/Rendering Parameters/Physician Specialty Listing</para>
	/// </summary>
	[TemplateMapping("{1EE05F1C-ADD1-4B7D-96C6-2B3BEC0CA8AC}", "InterfaceMap")]
	public partial interface IPhysicianSpecialtyListingItem : IItemWrapper 
	{		
		/// <summary>
		/// <para>Template: Physician Specialty Listing</para><para>Field: BCCOnly</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("bcc_only")]
		IBooleanFieldWrapper BCCOnly { get; }

		/// <summary>
		/// <para>Template: Physician Specialty Listing</para><para>Field: BCCOnly</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("bcc_only")]
		bool BCCOnlyValue { get; }
		/// <summary>
		/// <para>Template: Physician Specialty Listing</para><para>Field: BMGOnly</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("bmg_only")]
		IBooleanFieldWrapper BMGOnly { get; }

		/// <summary>
		/// <para>Template: Physician Specialty Listing</para><para>Field: BMGOnly</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("bmg_only")]
		bool BMGOnlyValue { get; }
		/// <summary>
		/// <para>Template: Physician Specialty Listing</para><para>Field: SpecialtyResultPage</para><para>Data type: Droptree</para>
		/// </summary>
		[IndexField("specialty_result_page")]
		ILinkFieldWrapper SpecialtyResultPage { get; }

		/// <summary>
		/// <para>Template: Physician Specialty Listing</para><para>Field: SpecialtyResultPage</para><para>Data type: Droptree</para>
		/// </summary>
		[IndexField("specialty_result_page")]
		Guid SpecialtyResultPageValue { get; }
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Feature/Experience Accelerator/Physicians Directory/Rendering Parameters/Physician Specialty Listing</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{1EE05F1C-ADD1-4B7D-96C6-2B3BEC0CA8AC}", typeof(Guid))]
	[TemplateMapping("{1EE05F1C-ADD1-4B7D-96C6-2B3BEC0CA8AC}", "")]
	public partial class PhysicianSpecialtyListingItem : ItemWrapper, IPhysicianSpecialtyListingItem
	{
		private Item _item = null;

		public PhysicianSpecialtyListingItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public PhysicianSpecialtyListingItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public PhysicianSpecialtyListingItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public PhysicianSpecialtyListingItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

		/// <summary><para>Template: Physician Specialty Listing</para><para>Field: BCCOnly</para><para>Data type: Checkbox</para></summary>
		[IndexField("bcc_only")]
		public virtual IBooleanFieldWrapper BCCOnly
		{
			get { return GetField<BooleanFieldWrapper>("BCC Only", "bcc_only"); }
		}

		/// <summary><para>Template: Physician Specialty Listing</para><para>Field: BCCOnly</para><para>Data type: Checkbox</para></summary>
		[IndexField("bcc_only")]
		public bool BCCOnlyValue
		{
			get { return BCCOnly.Value; }
		}
		/// <summary><para>Template: Physician Specialty Listing</para><para>Field: BMGOnly</para><para>Data type: Checkbox</para></summary>
		[IndexField("bmg_only")]
		public virtual IBooleanFieldWrapper BMGOnly
		{
			get { return GetField<BooleanFieldWrapper>("BMG Only", "bmg_only"); }
		}

		/// <summary><para>Template: Physician Specialty Listing</para><para>Field: BMGOnly</para><para>Data type: Checkbox</para></summary>
		[IndexField("bmg_only")]
		public bool BMGOnlyValue
		{
			get { return BMGOnly.Value; }
		}
		/// <summary><para>Template: Physician Specialty Listing</para><para>Field: SpecialtyResultPage</para><para>Data type: Droptree</para></summary>
		[IndexField("specialty_result_page")]
		public virtual ILinkFieldWrapper SpecialtyResultPage
		{
			get { return GetField<LinkFieldWrapper>("Specialty Result Page", "specialty_result_page"); }
		}

		/// <summary><para>Template: Physician Specialty Listing</para><para>Field: SpecialtyResultPage</para><para>Data type: Droptree</para></summary>
		[IndexField("specialty_result_page")]
		public Guid SpecialtyResultPageValue
		{
			get { return SpecialtyResultPage.Value; }
		}
	
	}
	public class PhysicianSpecialtyListingTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct PhysicianSpecialtyListing
	    {
	    	public static ID ID = ID.Parse("{1EE05F1C-ADD1-4B7D-96C6-2B3BEC0CA8AC}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
						public static readonly ID BCCOnly = new ID("{402CDF99-0565-4F66-81CA-7767B5B203E9}");
						public static readonly ID BMGOnly = new ID("{1EF93209-DB30-44AE-AC39-23D54FDA71F7}");
						public static readonly ID SpecialtyResultPage = new ID("{AB6E34D4-1F2E-4532-A62F-B13DEF3E882E}");
				
	        }
	    }
	}

	public class PhysicianSpecialtyListingRenderingModel : RenderingModelBase
	{
				
				/// <summary>
			/// <para>Template: Physician Specialty Listing</para><para>Field: BCCOnly</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("bcc_only")]
				 public virtual bool BCCOnly { get; set; }
			
				/// <summary>
			/// <para>Template: Physician Specialty Listing</para><para>Field: BMGOnly</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("bmg_only")]
				 public virtual bool BMGOnly { get; set; }
			
				/// <summary>
			/// <para>Template: Physician Specialty Listing</para><para>Field: SpecialtyResultPage</para><para>Data type: Droptree</para>
			/// </summary>
			[IndexField("specialty_result_page")]
				 public virtual Guid SpecialtyResultPage { get; set; }
		
	}

}


#endregion
#region Physicians Folder (Custom)
namespace Sitecore.Baptist.Feature.PhysicianDirectory.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Physicians Folder</para>
	/// <para>ID: {25BDFE30-4EDF-4AB9-8A7E-D19BF458D7E2}</para>
	/// <para>/sitecore/templates/Feature/Experience Accelerator/Physicians Directory/Physicians Folder</para>
	/// </summary>
	[TemplateMapping("{25BDFE30-4EDF-4AB9-8A7E-D19BF458D7E2}", "InterfaceMap")]
	public partial interface IPhysiciansFolderItem : IItemWrapper 
	{		
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Feature/Experience Accelerator/Physicians Directory/Physicians Folder</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{25BDFE30-4EDF-4AB9-8A7E-D19BF458D7E2}", typeof(Guid))]
	[TemplateMapping("{25BDFE30-4EDF-4AB9-8A7E-D19BF458D7E2}", "")]
	public partial class PhysiciansFolderItem : ItemWrapper, IPhysiciansFolderItem
	{
		private Item _item = null;

		public PhysiciansFolderItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public PhysiciansFolderItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public PhysiciansFolderItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public PhysiciansFolderItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

	
	}
	public class PhysiciansFolderTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct PhysiciansFolder
	    {
	    	public static ID ID = ID.Parse("{25BDFE30-4EDF-4AB9-8A7E-D19BF458D7E2}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
				
	        }
	    }
	}

	public class PhysiciansFolderRenderingModel : RenderingModelBase
	{
			
	}

}


#endregion
#region Physician Specialty Result (Custom)
namespace Sitecore.Baptist.Feature.PhysicianDirectory.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Physician Specialty Result</para>
	/// <para>ID: {80825E17-F464-4189-8BB5-84EF8B11E8AD}</para>
	/// <para>/sitecore/templates/Feature/Experience Accelerator/Physicians Directory/Rendering Parameters/Physician Specialty Result</para>
	/// </summary>
	[TemplateMapping("{80825E17-F464-4189-8BB5-84EF8B11E8AD}", "InterfaceMap")]
	public partial interface IPhysicianSpecialtyResultItem : IItemWrapper 
	{		
		/// <summary>
		/// <para>Template: Physician Specialty Result</para><para>Field: BCCOnly</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("bcc_only")]
		IBooleanFieldWrapper BCCOnly { get; }

		/// <summary>
		/// <para>Template: Physician Specialty Result</para><para>Field: BCCOnly</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("bcc_only")]
		bool BCCOnlyValue { get; }
		/// <summary>
		/// <para>Template: Physician Specialty Result</para><para>Field: BMGDoctorLogo</para><para>Data type: Image</para>
		/// </summary>
		IImageFieldWrapper BMGDoctorLogo { get; }
		string BMGDoctorLogoValue { get; }
		/// <summary>
		/// <para>Template: Physician Specialty Result</para><para>Field: BMGOnly</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("bmg_only")]
		IBooleanFieldWrapper BMGOnly { get; }

		/// <summary>
		/// <para>Template: Physician Specialty Result</para><para>Field: BMGOnly</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("bmg_only")]
		bool BMGOnlyValue { get; }
		/// <summary>
		/// <para>Template: Physician Specialty Result</para><para>Field: PhysicianProfileBaseUrl</para><para>Data type: Droptree</para>
		/// </summary>
		[IndexField("physician_profile_base_url")]
		ILinkFieldWrapper PhysicianProfileBaseUrl { get; }

		/// <summary>
		/// <para>Template: Physician Specialty Result</para><para>Field: PhysicianProfileBaseUrl</para><para>Data type: Droptree</para>
		/// </summary>
		[IndexField("physician_profile_base_url")]
		Guid PhysicianProfileBaseUrlValue { get; }
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Feature/Experience Accelerator/Physicians Directory/Rendering Parameters/Physician Specialty Result</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{80825E17-F464-4189-8BB5-84EF8B11E8AD}", typeof(Guid))]
	[TemplateMapping("{80825E17-F464-4189-8BB5-84EF8B11E8AD}", "")]
	public partial class PhysicianSpecialtyResultItem : ItemWrapper, IPhysicianSpecialtyResultItem
	{
		private Item _item = null;

		public PhysicianSpecialtyResultItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public PhysicianSpecialtyResultItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public PhysicianSpecialtyResultItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public PhysicianSpecialtyResultItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

		/// <summary><para>Template: Physician Specialty Result</para><para>Field: BCCOnly</para><para>Data type: Checkbox</para></summary>
		[IndexField("bcc_only")]
		public virtual IBooleanFieldWrapper BCCOnly
		{
			get { return GetField<BooleanFieldWrapper>("BCC Only", "bcc_only"); }
		}

		/// <summary><para>Template: Physician Specialty Result</para><para>Field: BCCOnly</para><para>Data type: Checkbox</para></summary>
		[IndexField("bcc_only")]
		public bool BCCOnlyValue
		{
			get { return BCCOnly.Value; }
		}
		/// <summary><para>Template: Physician Specialty Result</para><para>Field: BMGDoctorLogo</para><para>Data type: Image</para></summary>
		public virtual IImageFieldWrapper BMGDoctorLogo
		{
			get { return GetField<ImageFieldWrapper>("BMG Doctor Logo"); }
		}

		/// <summary><para>Template: Physician Specialty Result</para><para>Field: BMGDoctorLogo</para><para>Data type: Image</para></summary>
		public string BMGDoctorLogoValue
		{
			get { return BMGDoctorLogo.Value; }
		}
		/// <summary><para>Template: Physician Specialty Result</para><para>Field: BMGOnly</para><para>Data type: Checkbox</para></summary>
		[IndexField("bmg_only")]
		public virtual IBooleanFieldWrapper BMGOnly
		{
			get { return GetField<BooleanFieldWrapper>("BMG Only", "bmg_only"); }
		}

		/// <summary><para>Template: Physician Specialty Result</para><para>Field: BMGOnly</para><para>Data type: Checkbox</para></summary>
		[IndexField("bmg_only")]
		public bool BMGOnlyValue
		{
			get { return BMGOnly.Value; }
		}
		/// <summary><para>Template: Physician Specialty Result</para><para>Field: PhysicianProfileBaseUrl</para><para>Data type: Droptree</para></summary>
		[IndexField("physician_profile_base_url")]
		public virtual ILinkFieldWrapper PhysicianProfileBaseUrl
		{
			get { return GetField<LinkFieldWrapper>("Physician Profile Base Url", "physician_profile_base_url"); }
		}

		/// <summary><para>Template: Physician Specialty Result</para><para>Field: PhysicianProfileBaseUrl</para><para>Data type: Droptree</para></summary>
		[IndexField("physician_profile_base_url")]
		public Guid PhysicianProfileBaseUrlValue
		{
			get { return PhysicianProfileBaseUrl.Value; }
		}
	
	}
	public class PhysicianSpecialtyResultTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct PhysicianSpecialtyResult
	    {
	    	public static ID ID = ID.Parse("{80825E17-F464-4189-8BB5-84EF8B11E8AD}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
						public static readonly ID BCCOnly = new ID("{399D0561-1D36-4EB5-A026-61A8BFCF752C}");
						public static readonly ID BMGDoctorLogo = new ID("{01C54A3D-F32A-45FE-8073-0FC063326A68}");
						public static readonly ID BMGOnly = new ID("{795FDB4E-964C-456A-ADAC-B6629CA44489}");
						public static readonly ID PhysicianProfileBaseUrl = new ID("{DDBAE55D-8AAD-45E4-AE1B-F81A1D885248}");
				
	        }
	    }
	}

	public class PhysicianSpecialtyResultRenderingModel : RenderingModelBase
	{
				
				/// <summary>
			/// <para>Template: Physician Specialty Result</para><para>Field: BCCOnly</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("bcc_only")]
				 public virtual bool BCCOnly { get; set; }
			
				 public virtual string BMGDoctorLogo { get; set; }
			
				/// <summary>
			/// <para>Template: Physician Specialty Result</para><para>Field: BMGOnly</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("bmg_only")]
				 public virtual bool BMGOnly { get; set; }
			
				/// <summary>
			/// <para>Template: Physician Specialty Result</para><para>Field: PhysicianProfileBaseUrl</para><para>Data type: Droptree</para>
			/// </summary>
			[IndexField("physician_profile_base_url")]
				 public virtual Guid PhysicianProfileBaseUrl { get; set; }
		
	}

}


#endregion
#region Physician (Custom)
namespace Sitecore.Baptist.Feature.PhysicianDirectory.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Physician</para>
	/// <para>ID: {C1F76E27-A48F-4850-9475-76A5E0392ED4}</para>
	/// <para>/sitecore/templates/Feature/Experience Accelerator/Physicians Directory/Datasource/Physician</para>
	/// </summary>
	[TemplateMapping("{C1F76E27-A48F-4850-9475-76A5E0392ED4}", "InterfaceMap")]
	public partial interface IPhysicianItem : IItemWrapper 
	{		
		/// <summary>
		/// <para>Template: Physician</para><para>Field: Bio</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("bio")]
		ITextFieldWrapper Bio { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: Bio</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("bio")]
		string BioValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: Education</para><para>Data type: Name Value List</para>
		/// </summary>
		INameValueListFieldWrapper Education { get; }
		System.Collections.Specialized.NameValueCollection EducationValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: MeetTheDoctor</para><para>Data type: Name Value List</para>
		/// </summary>
		INameValueListFieldWrapper MeetTheDoctor { get; }
		System.Collections.Specialized.NameValueCollection MeetTheDoctorValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: ProfessionalRecognition</para><para>Data type: Name Value List</para>
		/// </summary>
		INameValueListFieldWrapper ProfessionalRecognition { get; }
		System.Collections.Specialized.NameValueCollection ProfessionalRecognitionValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: HospitalAffiliations</para><para>Data type: Name Value List</para>
		/// </summary>
		INameValueListFieldWrapper HospitalAffiliations { get; }
		System.Collections.Specialized.NameValueCollection HospitalAffiliationsValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: Locations</para><para>Data type: Name Value List</para>
		/// </summary>
		INameValueListFieldWrapper Locations { get; }
		System.Collections.Specialized.NameValueCollection LocationsValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: PrimaryLocationAddress1</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("primary_location_address_1")]
		ITextFieldWrapper PrimaryLocationAddress1 { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: PrimaryLocationAddress1</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("primary_location_address_1")]
		string PrimaryLocationAddress1Value { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: PrimaryLocationAddress2</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("primary_location_address_2")]
		ITextFieldWrapper PrimaryLocationAddress2 { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: PrimaryLocationAddress2</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("primary_location_address_2")]
		string PrimaryLocationAddress2Value { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: PrimaryLocationCity</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("primary_location_city")]
		ITextFieldWrapper PrimaryLocationCity { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: PrimaryLocationCity</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("primary_location_city")]
		string PrimaryLocationCityValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: PrimaryLocationID</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("primary_location_id")]
		ITextFieldWrapper PrimaryLocationID { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: PrimaryLocationID</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("primary_location_id")]
		string PrimaryLocationIDValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: PrimaryLocationLatitude</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("primary_location_latitude")]
		ITextFieldWrapper PrimaryLocationLatitude { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: PrimaryLocationLatitude</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("primary_location_latitude")]
		string PrimaryLocationLatitudeValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: PrimaryLocationLongitude</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("primary_location_longitude")]
		ITextFieldWrapper PrimaryLocationLongitude { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: PrimaryLocationLongitude</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("primary_location_longitude")]
		string PrimaryLocationLongitudeValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: PrimaryLocationMainPhoneNumber</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("primary_location_main_phone_number")]
		ITextFieldWrapper PrimaryLocationMainPhoneNumber { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: PrimaryLocationMainPhoneNumber</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("primary_location_main_phone_number")]
		string PrimaryLocationMainPhoneNumberValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: PrimaryLocationName</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("primary_location_name")]
		ITextFieldWrapper PrimaryLocationName { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: PrimaryLocationName</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("primary_location_name")]
		string PrimaryLocationNameValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: PrimaryLocationPostalCode</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("primary_location_postal_code")]
		ITextFieldWrapper PrimaryLocationPostalCode { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: PrimaryLocationPostalCode</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("primary_location_postal_code")]
		string PrimaryLocationPostalCodeValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: PrimaryLocationState</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("primary_location_state")]
		ITextFieldWrapper PrimaryLocationState { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: PrimaryLocationState</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("primary_location_state")]
		string PrimaryLocationStateValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: PrimaryLocationType</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("primary_location_type")]
		ITextFieldWrapper PrimaryLocationType { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: PrimaryLocationType</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("primary_location_type")]
		string PrimaryLocationTypeValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: PrimaryLocation</para><para>Data type: Droptree</para>
		/// </summary>
		[IndexField("primary_location")]
		ILinkFieldWrapper PrimaryLocation { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: PrimaryLocation</para><para>Data type: Droptree</para>
		/// </summary>
		[IndexField("primary_location")]
		Guid PrimaryLocationValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: PrimaryPhoneIsPublic</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("primary_phone_is_public")]
		IBooleanFieldWrapper PrimaryPhoneIsPublic { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: PrimaryPhoneIsPublic</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("primary_phone_is_public")]
		bool PrimaryPhoneIsPublicValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: PrimaryPhoneNumber</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("primary_phone_number")]
		ITextFieldWrapper PrimaryPhoneNumber { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: PrimaryPhoneNumber</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("primary_phone_number")]
		string PrimaryPhoneNumberValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: MediaResources</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("media_resources")]
		ITextFieldWrapper MediaResources { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: MediaResources</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("media_resources")]
		string MediaResourcesValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: ClientID</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("clientid")]
		ITextFieldWrapper ClientID { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: ClientID</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("clientid")]
		string ClientIDValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: ImageImportedPath</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("image_imported_path")]
		ITextFieldWrapper ImageImportedPath { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: ImageImportedPath</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("image_imported_path")]
		string ImageImportedPathValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: ImportProperties</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("import_properties")]
		ITextFieldWrapper ImportProperties { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: ImportProperties</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("import_properties")]
		string ImportPropertiesValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: ImportTaxonomy</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("import_taxonomy")]
		ITextFieldWrapper ImportTaxonomy { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: ImportTaxonomy</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("import_taxonomy")]
		string ImportTaxonomyValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: IsActive</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("is_active")]
		IBooleanFieldWrapper IsActive { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: IsActive</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("is_active")]
		bool IsActiveValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: IsBCC</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("is_bcc")]
		IBooleanFieldWrapper IsBCC { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: IsBCC</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("is_bcc")]
		bool IsBCCValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: IsBMG</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("is_bmg")]
		IBooleanFieldWrapper IsBMG { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: IsBMG</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("is_bmg")]
		bool IsBMGValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: Regions</para><para>Data type: Name Value List</para>
		/// </summary>
		INameValueListFieldWrapper Regions { get; }
		System.Collections.Specialized.NameValueCollection RegionsValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: FeaturedProvider</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("featured_provider")]
		IBooleanFieldWrapper FeaturedProvider { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: FeaturedProvider</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("featured_provider")]
		bool FeaturedProviderValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: HideSchedulingWidget</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("hide_scheduling_widget")]
		IBooleanFieldWrapper HideSchedulingWidget { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: HideSchedulingWidget</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("hide_scheduling_widget")]
		bool HideSchedulingWidgetValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: ListedProvider</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("listed_provider")]
		IBooleanFieldWrapper ListedProvider { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: ListedProvider</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("listed_provider")]
		bool ListedProviderValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: OneCareID</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("onecare_id")]
		ITextFieldWrapper OneCareID { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: OneCareID</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("onecare_id")]
		string OneCareIDValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: OneCareVT</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("onecare_vt")]
		ITextFieldWrapper OneCareVT { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: OneCareVT</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("onecare_vt")]
		string OneCareVTValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: BCCSpecialties</para><para>Data type: Name Value List</para>
		/// </summary>
		INameValueListFieldWrapper BCCSpecialties { get; }
		System.Collections.Specialized.NameValueCollection BCCSpecialtiesValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: DiseasesAndConditions</para><para>Data type: Rich Text</para>
		/// </summary>
		[IndexField("diseases_and_conditions")]
		IRichTextFieldWrapper DiseasesAndConditions { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: DiseasesAndConditions</para><para>Data type: Rich Text</para>
		/// </summary>
		[IndexField("diseases_and_conditions")]
		string DiseasesAndConditionsValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: FirstName</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("first_name")]
		ITextFieldWrapper FirstName { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: FirstName</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("first_name")]
		string FirstNameValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: Languages</para><para>Data type: Rich Text</para>
		/// </summary>
		[IndexField("languages")]
		IRichTextFieldWrapper Languages { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: Languages</para><para>Data type: Rich Text</para>
		/// </summary>
		[IndexField("languages")]
		string LanguagesValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: LastName</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("last_name")]
		ITextFieldWrapper LastName { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: LastName</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("last_name")]
		string LastNameValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: MIddleName</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("middle_name")]
		ITextFieldWrapper MIddleName { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: MIddleName</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("middle_name")]
		string MIddleNameValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: PreferredName</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("preferred_name")]
		ITextFieldWrapper PreferredName { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: PreferredName</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("preferred_name")]
		string PreferredNameValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: ProfessionalTitles</para><para>Data type: Name Value List</para>
		/// </summary>
		INameValueListFieldWrapper ProfessionalTitles { get; }
		System.Collections.Specialized.NameValueCollection ProfessionalTitlesValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: SchedulingSpecialties</para><para>Data type: Name Value List</para>
		/// </summary>
		INameValueListFieldWrapper SchedulingSpecialties { get; }
		System.Collections.Specialized.NameValueCollection SchedulingSpecialtiesValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: SpecialDesignations</para><para>Data type: Name Value List</para>
		/// </summary>
		INameValueListFieldWrapper SpecialDesignations { get; }
		System.Collections.Specialized.NameValueCollection SpecialDesignationsValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: Specialties</para><para>Data type: Name Value List</para>
		/// </summary>
		INameValueListFieldWrapper Specialties { get; }
		System.Collections.Specialized.NameValueCollection SpecialtiesValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: SpecialtyInterests</para><para>Data type: Rich Text</para>
		/// </summary>
		[IndexField("specialty_interests")]
		IRichTextFieldWrapper SpecialtyInterests { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: SpecialtyInterests</para><para>Data type: Rich Text</para>
		/// </summary>
		[IndexField("specialty_interests")]
		string SpecialtyInterestsValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: Suffix</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("suffix")]
		ITextFieldWrapper Suffix { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: Suffix</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("suffix")]
		string SuffixValue { get; }
		/// <summary>
		/// <para>Template: Physician</para><para>Field: TreatmentsAndServices</para><para>Data type: Rich Text</para>
		/// </summary>
		[IndexField("treatments_and_services")]
		IRichTextFieldWrapper TreatmentsAndServices { get; }

		/// <summary>
		/// <para>Template: Physician</para><para>Field: TreatmentsAndServices</para><para>Data type: Rich Text</para>
		/// </summary>
		[IndexField("treatments_and_services")]
		string TreatmentsAndServicesValue { get; }
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Feature/Experience Accelerator/Physicians Directory/Datasource/Physician</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{C1F76E27-A48F-4850-9475-76A5E0392ED4}", typeof(Guid))]
	[TemplateMapping("{C1F76E27-A48F-4850-9475-76A5E0392ED4}", "")]
	public partial class PhysicianItem : ItemWrapper, IPhysicianItem
	{
		private Item _item = null;

		public PhysicianItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public PhysicianItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public PhysicianItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public PhysicianItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

		/// <summary><para>Template: Physician</para><para>Field: Bio</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("bio")]
		public virtual ITextFieldWrapper Bio
		{
			get { return GetField<TextFieldWrapper>("Bio", "bio"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: Bio</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("bio")]
		public string BioValue
		{
			get { return Bio.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: Education</para><para>Data type: Name Value List</para></summary>
		public virtual INameValueListFieldWrapper Education
		{
			get { return GetField<NameValueListFieldWrapper>("Education"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: Education</para><para>Data type: Name Value List</para></summary>
		public System.Collections.Specialized.NameValueCollection EducationValue
		{
			get { return Education.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: MeetTheDoctor</para><para>Data type: Name Value List</para></summary>
		public virtual INameValueListFieldWrapper MeetTheDoctor
		{
			get { return GetField<NameValueListFieldWrapper>("Meet The Doctor"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: MeetTheDoctor</para><para>Data type: Name Value List</para></summary>
		public System.Collections.Specialized.NameValueCollection MeetTheDoctorValue
		{
			get { return MeetTheDoctor.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: ProfessionalRecognition</para><para>Data type: Name Value List</para></summary>
		public virtual INameValueListFieldWrapper ProfessionalRecognition
		{
			get { return GetField<NameValueListFieldWrapper>("Professional Recognition"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: ProfessionalRecognition</para><para>Data type: Name Value List</para></summary>
		public System.Collections.Specialized.NameValueCollection ProfessionalRecognitionValue
		{
			get { return ProfessionalRecognition.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: HospitalAffiliations</para><para>Data type: Name Value List</para></summary>
		public virtual INameValueListFieldWrapper HospitalAffiliations
		{
			get { return GetField<NameValueListFieldWrapper>("Hospital Affiliations"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: HospitalAffiliations</para><para>Data type: Name Value List</para></summary>
		public System.Collections.Specialized.NameValueCollection HospitalAffiliationsValue
		{
			get { return HospitalAffiliations.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: Locations</para><para>Data type: Name Value List</para></summary>
		public virtual INameValueListFieldWrapper Locations
		{
			get { return GetField<NameValueListFieldWrapper>("Locations"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: Locations</para><para>Data type: Name Value List</para></summary>
		public System.Collections.Specialized.NameValueCollection LocationsValue
		{
			get { return Locations.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: PrimaryLocationAddress1</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("primary_location_address_1")]
		public virtual ITextFieldWrapper PrimaryLocationAddress1
		{
			get { return GetField<TextFieldWrapper>("Primary Location Address 1", "primary_location_address_1"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: PrimaryLocationAddress1</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("primary_location_address_1")]
		public string PrimaryLocationAddress1Value
		{
			get { return PrimaryLocationAddress1.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: PrimaryLocationAddress2</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("primary_location_address_2")]
		public virtual ITextFieldWrapper PrimaryLocationAddress2
		{
			get { return GetField<TextFieldWrapper>("Primary Location Address 2", "primary_location_address_2"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: PrimaryLocationAddress2</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("primary_location_address_2")]
		public string PrimaryLocationAddress2Value
		{
			get { return PrimaryLocationAddress2.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: PrimaryLocationCity</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("primary_location_city")]
		public virtual ITextFieldWrapper PrimaryLocationCity
		{
			get { return GetField<TextFieldWrapper>("Primary Location City", "primary_location_city"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: PrimaryLocationCity</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("primary_location_city")]
		public string PrimaryLocationCityValue
		{
			get { return PrimaryLocationCity.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: PrimaryLocationID</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("primary_location_id")]
		public virtual ITextFieldWrapper PrimaryLocationID
		{
			get { return GetField<TextFieldWrapper>("Primary Location ID", "primary_location_id"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: PrimaryLocationID</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("primary_location_id")]
		public string PrimaryLocationIDValue
		{
			get { return PrimaryLocationID.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: PrimaryLocationLatitude</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("primary_location_latitude")]
		public virtual ITextFieldWrapper PrimaryLocationLatitude
		{
			get { return GetField<TextFieldWrapper>("Primary Location Latitude", "primary_location_latitude"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: PrimaryLocationLatitude</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("primary_location_latitude")]
		public string PrimaryLocationLatitudeValue
		{
			get { return PrimaryLocationLatitude.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: PrimaryLocationLongitude</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("primary_location_longitude")]
		public virtual ITextFieldWrapper PrimaryLocationLongitude
		{
			get { return GetField<TextFieldWrapper>("Primary Location Longitude", "primary_location_longitude"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: PrimaryLocationLongitude</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("primary_location_longitude")]
		public string PrimaryLocationLongitudeValue
		{
			get { return PrimaryLocationLongitude.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: PrimaryLocationMainPhoneNumber</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("primary_location_main_phone_number")]
		public virtual ITextFieldWrapper PrimaryLocationMainPhoneNumber
		{
			get { return GetField<TextFieldWrapper>("Primary Location Main Phone Number", "primary_location_main_phone_number"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: PrimaryLocationMainPhoneNumber</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("primary_location_main_phone_number")]
		public string PrimaryLocationMainPhoneNumberValue
		{
			get { return PrimaryLocationMainPhoneNumber.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: PrimaryLocationName</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("primary_location_name")]
		public virtual ITextFieldWrapper PrimaryLocationName
		{
			get { return GetField<TextFieldWrapper>("Primary Location Name", "primary_location_name"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: PrimaryLocationName</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("primary_location_name")]
		public string PrimaryLocationNameValue
		{
			get { return PrimaryLocationName.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: PrimaryLocationPostalCode</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("primary_location_postal_code")]
		public virtual ITextFieldWrapper PrimaryLocationPostalCode
		{
			get { return GetField<TextFieldWrapper>("Primary Location Postal Code", "primary_location_postal_code"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: PrimaryLocationPostalCode</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("primary_location_postal_code")]
		public string PrimaryLocationPostalCodeValue
		{
			get { return PrimaryLocationPostalCode.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: PrimaryLocationState</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("primary_location_state")]
		public virtual ITextFieldWrapper PrimaryLocationState
		{
			get { return GetField<TextFieldWrapper>("Primary Location State", "primary_location_state"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: PrimaryLocationState</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("primary_location_state")]
		public string PrimaryLocationStateValue
		{
			get { return PrimaryLocationState.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: PrimaryLocationType</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("primary_location_type")]
		public virtual ITextFieldWrapper PrimaryLocationType
		{
			get { return GetField<TextFieldWrapper>("Primary Location Type", "primary_location_type"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: PrimaryLocationType</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("primary_location_type")]
		public string PrimaryLocationTypeValue
		{
			get { return PrimaryLocationType.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: PrimaryLocation</para><para>Data type: Droptree</para></summary>
		[IndexField("primary_location")]
		public virtual ILinkFieldWrapper PrimaryLocation
		{
			get { return GetField<LinkFieldWrapper>("Primary Location", "primary_location"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: PrimaryLocation</para><para>Data type: Droptree</para></summary>
		[IndexField("primary_location")]
		public Guid PrimaryLocationValue
		{
			get { return PrimaryLocation.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: PrimaryPhoneIsPublic</para><para>Data type: Checkbox</para></summary>
		[IndexField("primary_phone_is_public")]
		public virtual IBooleanFieldWrapper PrimaryPhoneIsPublic
		{
			get { return GetField<BooleanFieldWrapper>("Primary Phone Is Public", "primary_phone_is_public"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: PrimaryPhoneIsPublic</para><para>Data type: Checkbox</para></summary>
		[IndexField("primary_phone_is_public")]
		public bool PrimaryPhoneIsPublicValue
		{
			get { return PrimaryPhoneIsPublic.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: PrimaryPhoneNumber</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("primary_phone_number")]
		public virtual ITextFieldWrapper PrimaryPhoneNumber
		{
			get { return GetField<TextFieldWrapper>("Primary Phone Number", "primary_phone_number"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: PrimaryPhoneNumber</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("primary_phone_number")]
		public string PrimaryPhoneNumberValue
		{
			get { return PrimaryPhoneNumber.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: MediaResources</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("media_resources")]
		public virtual ITextFieldWrapper MediaResources
		{
			get { return GetField<TextFieldWrapper>("Media Resources", "media_resources"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: MediaResources</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("media_resources")]
		public string MediaResourcesValue
		{
			get { return MediaResources.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: ClientID</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("clientid")]
		public virtual ITextFieldWrapper ClientID
		{
			get { return GetField<TextFieldWrapper>("ClientID", "clientid"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: ClientID</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("clientid")]
		public string ClientIDValue
		{
			get { return ClientID.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: ImageImportedPath</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("image_imported_path")]
		public virtual ITextFieldWrapper ImageImportedPath
		{
			get { return GetField<TextFieldWrapper>("Image Imported Path", "image_imported_path"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: ImageImportedPath</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("image_imported_path")]
		public string ImageImportedPathValue
		{
			get { return ImageImportedPath.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: ImportProperties</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("import_properties")]
		public virtual ITextFieldWrapper ImportProperties
		{
			get { return GetField<TextFieldWrapper>("Import Properties", "import_properties"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: ImportProperties</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("import_properties")]
		public string ImportPropertiesValue
		{
			get { return ImportProperties.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: ImportTaxonomy</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("import_taxonomy")]
		public virtual ITextFieldWrapper ImportTaxonomy
		{
			get { return GetField<TextFieldWrapper>("Import Taxonomy", "import_taxonomy"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: ImportTaxonomy</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("import_taxonomy")]
		public string ImportTaxonomyValue
		{
			get { return ImportTaxonomy.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: IsActive</para><para>Data type: Checkbox</para></summary>
		[IndexField("is_active")]
		public virtual IBooleanFieldWrapper IsActive
		{
			get { return GetField<BooleanFieldWrapper>("Is Active", "is_active"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: IsActive</para><para>Data type: Checkbox</para></summary>
		[IndexField("is_active")]
		public bool IsActiveValue
		{
			get { return IsActive.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: IsBCC</para><para>Data type: Checkbox</para></summary>
		[IndexField("is_bcc")]
		public virtual IBooleanFieldWrapper IsBCC
		{
			get { return GetField<BooleanFieldWrapper>("Is BCC", "is_bcc"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: IsBCC</para><para>Data type: Checkbox</para></summary>
		[IndexField("is_bcc")]
		public bool IsBCCValue
		{
			get { return IsBCC.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: IsBMG</para><para>Data type: Checkbox</para></summary>
		[IndexField("is_bmg")]
		public virtual IBooleanFieldWrapper IsBMG
		{
			get { return GetField<BooleanFieldWrapper>("Is BMG", "is_bmg"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: IsBMG</para><para>Data type: Checkbox</para></summary>
		[IndexField("is_bmg")]
		public bool IsBMGValue
		{
			get { return IsBMG.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: Regions</para><para>Data type: Name Value List</para></summary>
		public virtual INameValueListFieldWrapper Regions
		{
			get { return GetField<NameValueListFieldWrapper>("Regions"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: Regions</para><para>Data type: Name Value List</para></summary>
		public System.Collections.Specialized.NameValueCollection RegionsValue
		{
			get { return Regions.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: FeaturedProvider</para><para>Data type: Checkbox</para></summary>
		[IndexField("featured_provider")]
		public virtual IBooleanFieldWrapper FeaturedProvider
		{
			get { return GetField<BooleanFieldWrapper>("Featured Provider", "featured_provider"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: FeaturedProvider</para><para>Data type: Checkbox</para></summary>
		[IndexField("featured_provider")]
		public bool FeaturedProviderValue
		{
			get { return FeaturedProvider.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: HideSchedulingWidget</para><para>Data type: Checkbox</para></summary>
		[IndexField("hide_scheduling_widget")]
		public virtual IBooleanFieldWrapper HideSchedulingWidget
		{
			get { return GetField<BooleanFieldWrapper>("Hide Scheduling Widget", "hide_scheduling_widget"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: HideSchedulingWidget</para><para>Data type: Checkbox</para></summary>
		[IndexField("hide_scheduling_widget")]
		public bool HideSchedulingWidgetValue
		{
			get { return HideSchedulingWidget.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: ListedProvider</para><para>Data type: Checkbox</para></summary>
		[IndexField("listed_provider")]
		public virtual IBooleanFieldWrapper ListedProvider
		{
			get { return GetField<BooleanFieldWrapper>("Listed Provider", "listed_provider"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: ListedProvider</para><para>Data type: Checkbox</para></summary>
		[IndexField("listed_provider")]
		public bool ListedProviderValue
		{
			get { return ListedProvider.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: OneCareID</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("onecare_id")]
		public virtual ITextFieldWrapper OneCareID
		{
			get { return GetField<TextFieldWrapper>("OneCare ID", "onecare_id"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: OneCareID</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("onecare_id")]
		public string OneCareIDValue
		{
			get { return OneCareID.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: OneCareVT</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("onecare_vt")]
		public virtual ITextFieldWrapper OneCareVT
		{
			get { return GetField<TextFieldWrapper>("OneCare VT", "onecare_vt"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: OneCareVT</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("onecare_vt")]
		public string OneCareVTValue
		{
			get { return OneCareVT.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: BCCSpecialties</para><para>Data type: Name Value List</para></summary>
		public virtual INameValueListFieldWrapper BCCSpecialties
		{
			get { return GetField<NameValueListFieldWrapper>("BCC Specialties"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: BCCSpecialties</para><para>Data type: Name Value List</para></summary>
		public System.Collections.Specialized.NameValueCollection BCCSpecialtiesValue
		{
			get { return BCCSpecialties.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: DiseasesAndConditions</para><para>Data type: Rich Text</para></summary>
		[IndexField("diseases_and_conditions")]
		public virtual IRichTextFieldWrapper DiseasesAndConditions
		{
			get { return GetField<RichTextFieldWrapper>("Diseases and Conditions", "diseases_and_conditions"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: DiseasesAndConditions</para><para>Data type: Rich Text</para></summary>
		[IndexField("diseases_and_conditions")]
		public string DiseasesAndConditionsValue
		{
			get { return DiseasesAndConditions.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: FirstName</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("first_name")]
		public virtual ITextFieldWrapper FirstName
		{
			get { return GetField<TextFieldWrapper>("First Name", "first_name"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: FirstName</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("first_name")]
		public string FirstNameValue
		{
			get { return FirstName.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: Languages</para><para>Data type: Rich Text</para></summary>
		[IndexField("languages")]
		public virtual IRichTextFieldWrapper Languages
		{
			get { return GetField<RichTextFieldWrapper>("Languages", "languages"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: Languages</para><para>Data type: Rich Text</para></summary>
		[IndexField("languages")]
		public string LanguagesValue
		{
			get { return Languages.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: LastName</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("last_name")]
		public virtual ITextFieldWrapper LastName
		{
			get { return GetField<TextFieldWrapper>("Last Name", "last_name"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: LastName</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("last_name")]
		public string LastNameValue
		{
			get { return LastName.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: MIddleName</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("middle_name")]
		public virtual ITextFieldWrapper MIddleName
		{
			get { return GetField<TextFieldWrapper>("MIddle Name", "middle_name"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: MIddleName</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("middle_name")]
		public string MIddleNameValue
		{
			get { return MIddleName.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: PreferredName</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("preferred_name")]
		public virtual ITextFieldWrapper PreferredName
		{
			get { return GetField<TextFieldWrapper>("Preferred Name", "preferred_name"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: PreferredName</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("preferred_name")]
		public string PreferredNameValue
		{
			get { return PreferredName.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: ProfessionalTitles</para><para>Data type: Name Value List</para></summary>
		public virtual INameValueListFieldWrapper ProfessionalTitles
		{
			get { return GetField<NameValueListFieldWrapper>("Professional Titles"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: ProfessionalTitles</para><para>Data type: Name Value List</para></summary>
		public System.Collections.Specialized.NameValueCollection ProfessionalTitlesValue
		{
			get { return ProfessionalTitles.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: SchedulingSpecialties</para><para>Data type: Name Value List</para></summary>
		public virtual INameValueListFieldWrapper SchedulingSpecialties
		{
			get { return GetField<NameValueListFieldWrapper>("Scheduling Specialties"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: SchedulingSpecialties</para><para>Data type: Name Value List</para></summary>
		public System.Collections.Specialized.NameValueCollection SchedulingSpecialtiesValue
		{
			get { return SchedulingSpecialties.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: SpecialDesignations</para><para>Data type: Name Value List</para></summary>
		public virtual INameValueListFieldWrapper SpecialDesignations
		{
			get { return GetField<NameValueListFieldWrapper>("Special Designations"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: SpecialDesignations</para><para>Data type: Name Value List</para></summary>
		public System.Collections.Specialized.NameValueCollection SpecialDesignationsValue
		{
			get { return SpecialDesignations.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: Specialties</para><para>Data type: Name Value List</para></summary>
		public virtual INameValueListFieldWrapper Specialties
		{
			get { return GetField<NameValueListFieldWrapper>("Specialties"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: Specialties</para><para>Data type: Name Value List</para></summary>
		public System.Collections.Specialized.NameValueCollection SpecialtiesValue
		{
			get { return Specialties.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: SpecialtyInterests</para><para>Data type: Rich Text</para></summary>
		[IndexField("specialty_interests")]
		public virtual IRichTextFieldWrapper SpecialtyInterests
		{
			get { return GetField<RichTextFieldWrapper>("Specialty Interests", "specialty_interests"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: SpecialtyInterests</para><para>Data type: Rich Text</para></summary>
		[IndexField("specialty_interests")]
		public string SpecialtyInterestsValue
		{
			get { return SpecialtyInterests.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: Suffix</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("suffix")]
		public virtual ITextFieldWrapper Suffix
		{
			get { return GetField<TextFieldWrapper>("Suffix", "suffix"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: Suffix</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("suffix")]
		public string SuffixValue
		{
			get { return Suffix.Value; }
		}
		/// <summary><para>Template: Physician</para><para>Field: TreatmentsAndServices</para><para>Data type: Rich Text</para></summary>
		[IndexField("treatments_and_services")]
		public virtual IRichTextFieldWrapper TreatmentsAndServices
		{
			get { return GetField<RichTextFieldWrapper>("Treatments and Services", "treatments_and_services"); }
		}

		/// <summary><para>Template: Physician</para><para>Field: TreatmentsAndServices</para><para>Data type: Rich Text</para></summary>
		[IndexField("treatments_and_services")]
		public string TreatmentsAndServicesValue
		{
			get { return TreatmentsAndServices.Value; }
		}
	
	}
	public class PhysicianTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct Physician
	    {
	    	public static ID ID = ID.Parse("{C1F76E27-A48F-4850-9475-76A5E0392ED4}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
						public static readonly ID Bio = new ID("{713133EC-8021-4CB9-BD70-C2C04F6D9EE5}");
						public static readonly ID Education = new ID("{A4555ABA-EFEE-49FD-BEEC-08C960497C25}");
						public static readonly ID MeetTheDoctor = new ID("{E021707F-110F-4587-9578-00C8B63C39EF}");
						public static readonly ID ProfessionalRecognition = new ID("{4B747256-3580-4C5A-AB01-88152614DF3C}");
						public static readonly ID HospitalAffiliations = new ID("{CD65FBD5-992C-4C87-AD08-00CEBDE5D09A}");
						public static readonly ID Locations = new ID("{62A3D38C-EFD1-452C-8AA3-3BF1A687C8E6}");
						public static readonly ID PrimaryLocationAddress1 = new ID("{01F28D7A-CE9A-4507-910E-C003E5BD3306}");
						public static readonly ID PrimaryLocationAddress2 = new ID("{884CC77F-0C10-406A-A584-26524B29D67C}");
						public static readonly ID PrimaryLocationCity = new ID("{BFD784B8-86C3-44E7-9F44-2D76DDD579DC}");
						public static readonly ID PrimaryLocationID = new ID("{33C3A1AC-332F-4F01-840A-BDB2D0AEBF9C}");
						public static readonly ID PrimaryLocationLatitude = new ID("{E105B8A8-81C5-44A0-808F-AC34096FB316}");
						public static readonly ID PrimaryLocationLongitude = new ID("{B805EBA4-A526-45A1-A8EA-2E9389BD726A}");
						public static readonly ID PrimaryLocationMainPhoneNumber = new ID("{EFA2E343-A2B7-47F3-A179-AABAF5B24FD6}");
						public static readonly ID PrimaryLocationName = new ID("{4C5236E3-BD03-4A93-84C0-348F6C4FB8DA}");
						public static readonly ID PrimaryLocationPostalCode = new ID("{FA873FED-9BAC-437F-8CE3-102440C4264B}");
						public static readonly ID PrimaryLocationState = new ID("{9D44512A-76F2-49FC-B6C3-902BDE2A2411}");
						public static readonly ID PrimaryLocationType = new ID("{2992DE7F-A04C-421B-B41B-2BCBBCB1DE60}");
						public static readonly ID PrimaryLocation = new ID("{F7C53FCC-5030-454F-A323-D314F0F6BEBC}");
						public static readonly ID PrimaryPhoneIsPublic = new ID("{34A43178-AA5D-43E4-A367-4E0A3EC9B36B}");
						public static readonly ID PrimaryPhoneNumber = new ID("{6298D4F2-1BBF-499E-A383-A8F4976E9BA2}");
						public static readonly ID MediaResources = new ID("{2E857050-FC1C-490D-9467-DAF3C4CE6E0D}");
						public static readonly ID ClientID = new ID("{11E35CBC-7CBA-4869-A540-7B03D040B96A}");
						public static readonly ID ImageImportedPath = new ID("{BA7B4E5D-EEF3-440E-99E9-9E98B57EFD40}");
						public static readonly ID ImportProperties = new ID("{4D694006-5FA3-4FC8-9671-FBE673B0295A}");
						public static readonly ID ImportTaxonomy = new ID("{8D20FD80-94C8-4430-BFEC-3A6DFF62F4EA}");
						public static readonly ID IsActive = new ID("{1F3B6CDC-56C5-49BB-A364-C538EDC555AC}");
						public static readonly ID IsBCC = new ID("{66D3148F-44CB-4FEC-9468-D435DA3A6315}");
						public static readonly ID IsBMG = new ID("{9B004B79-B3E5-45BB-AB37-B777CC8BBE00}");
						public static readonly ID Regions = new ID("{1CB752A9-D9C8-48A8-97A9-54B0E5AF2E37}");
						public static readonly ID FeaturedProvider = new ID("{0AD240DC-E6E1-4612-8C97-D071F7821CB6}");
						public static readonly ID HideSchedulingWidget = new ID("{863D03C5-3BA0-4E02-BE1B-6637CBC5AD8F}");
						public static readonly ID ListedProvider = new ID("{DB3FE078-05BD-4098-88FE-B324F03DDC32}");
						public static readonly ID OneCareID = new ID("{02EFDCCF-7F6D-43E4-A1C2-5A6DAD46BDC9}");
						public static readonly ID OneCareVT = new ID("{3945C74B-F7AB-4742-8C17-D3C9CEDA160E}");
						public static readonly ID BCCSpecialties = new ID("{806DE5F3-5C2B-4EB0-B89A-85637ED3B3CD}");
						public static readonly ID DiseasesandConditions = new ID("{D6AAA425-685F-4075-BE44-6D9053EF23F8}");
						public static readonly ID FirstName = new ID("{B11F045C-86AC-4CA4-BF57-1D4533731DD1}");
						public static readonly ID Languages = new ID("{5339B476-15E8-4B19-8527-9C057A6FA53A}");
						public static readonly ID LastName = new ID("{A38A4998-9D0E-4DA5-B946-1B29E68AEC8A}");
						public static readonly ID MIddleName = new ID("{04CCCAE2-86C7-4D51-A572-EF65AEE6D4F5}");
						public static readonly ID PreferredName = new ID("{927F0743-BA92-4084-A4B3-EAA2D5D51CEF}");
						public static readonly ID ProfessionalTitles = new ID("{5A90D6A9-C6D8-4277-A4A6-80132FCC9F78}");
						public static readonly ID SchedulingSpecialties = new ID("{943EB72D-D194-4B8F-AB63-8244973E7454}");
						public static readonly ID SpecialDesignations = new ID("{6AED044A-C9F4-46CC-BA0D-2A61ED620F09}");
						public static readonly ID Specialties = new ID("{B5CBE89F-80F5-470D-9053-5D6A0BFB9654}");
						public static readonly ID SpecialtyInterests = new ID("{15E38A39-9CDE-445E-9621-7A9866DAA5B5}");
						public static readonly ID Suffix = new ID("{EDB3FE77-32BD-44A1-8970-44EE3E08EB73}");
						public static readonly ID TreatmentsandServices = new ID("{6F5B3834-82EF-4E7E-B999-9C363A14A9F9}");
				
	        }
	    }
	}

	public class PhysicianRenderingModel : RenderingModelBase
	{
				
				/// <summary>
			/// <para>Template: Physician</para><para>Field: Bio</para><para>Data type: Multi-Line Text</para>
			/// </summary>
			[IndexField("bio")]
				 public virtual string Bio { get; set; }
			
				 public virtual System.Collections.Specialized.NameValueCollection Education { get; set; }
			
				 public virtual System.Collections.Specialized.NameValueCollection MeetTheDoctor { get; set; }
			
				 public virtual System.Collections.Specialized.NameValueCollection ProfessionalRecognition { get; set; }
			
				 public virtual System.Collections.Specialized.NameValueCollection HospitalAffiliations { get; set; }
			
				 public virtual System.Collections.Specialized.NameValueCollection Locations { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: PrimaryLocationAddress1</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("primary_location_address_1")]
				 public virtual string PrimaryLocationAddress1 { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: PrimaryLocationAddress2</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("primary_location_address_2")]
				 public virtual string PrimaryLocationAddress2 { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: PrimaryLocationCity</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("primary_location_city")]
				 public virtual string PrimaryLocationCity { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: PrimaryLocationID</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("primary_location_id")]
				 public virtual string PrimaryLocationID { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: PrimaryLocationLatitude</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("primary_location_latitude")]
				 public virtual string PrimaryLocationLatitude { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: PrimaryLocationLongitude</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("primary_location_longitude")]
				 public virtual string PrimaryLocationLongitude { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: PrimaryLocationMainPhoneNumber</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("primary_location_main_phone_number")]
				 public virtual string PrimaryLocationMainPhoneNumber { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: PrimaryLocationName</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("primary_location_name")]
				 public virtual string PrimaryLocationName { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: PrimaryLocationPostalCode</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("primary_location_postal_code")]
				 public virtual string PrimaryLocationPostalCode { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: PrimaryLocationState</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("primary_location_state")]
				 public virtual string PrimaryLocationState { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: PrimaryLocationType</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("primary_location_type")]
				 public virtual string PrimaryLocationType { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: PrimaryLocation</para><para>Data type: Droptree</para>
			/// </summary>
			[IndexField("primary_location")]
				 public virtual Guid PrimaryLocation { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: PrimaryPhoneIsPublic</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("primary_phone_is_public")]
				 public virtual bool PrimaryPhoneIsPublic { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: PrimaryPhoneNumber</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("primary_phone_number")]
				 public virtual string PrimaryPhoneNumber { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: MediaResources</para><para>Data type: Multi-Line Text</para>
			/// </summary>
			[IndexField("media_resources")]
				 public virtual string MediaResources { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: ClientID</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("clientid")]
				 public virtual string ClientID { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: ImageImportedPath</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("image_imported_path")]
				 public virtual string ImageImportedPath { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: ImportProperties</para><para>Data type: Multi-Line Text</para>
			/// </summary>
			[IndexField("import_properties")]
				 public virtual string ImportProperties { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: ImportTaxonomy</para><para>Data type: Multi-Line Text</para>
			/// </summary>
			[IndexField("import_taxonomy")]
				 public virtual string ImportTaxonomy { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: IsActive</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("is_active")]
				 public virtual bool IsActive { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: IsBCC</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("is_bcc")]
				 public virtual bool IsBCC { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: IsBMG</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("is_bmg")]
				 public virtual bool IsBMG { get; set; }
			
				 public virtual System.Collections.Specialized.NameValueCollection Regions { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: FeaturedProvider</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("featured_provider")]
				 public virtual bool FeaturedProvider { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: HideSchedulingWidget</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("hide_scheduling_widget")]
				 public virtual bool HideSchedulingWidget { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: ListedProvider</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("listed_provider")]
				 public virtual bool ListedProvider { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: OneCareID</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("onecare_id")]
				 public virtual string OneCareID { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: OneCareVT</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("onecare_vt")]
				 public virtual string OneCareVT { get; set; }
			
				 public virtual System.Collections.Specialized.NameValueCollection BCCSpecialties { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: DiseasesAndConditions</para><para>Data type: Rich Text</para>
			/// </summary>
			[IndexField("diseases_and_conditions")]
				 public virtual string DiseasesAndConditions { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: FirstName</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("first_name")]
				 public virtual string FirstName { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: Languages</para><para>Data type: Rich Text</para>
			/// </summary>
			[IndexField("languages")]
				 public virtual string Languages { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: LastName</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("last_name")]
				 public virtual string LastName { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: MIddleName</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("middle_name")]
				 public virtual string MIddleName { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: PreferredName</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("preferred_name")]
				 public virtual string PreferredName { get; set; }
			
				 public virtual System.Collections.Specialized.NameValueCollection ProfessionalTitles { get; set; }
			
				 public virtual System.Collections.Specialized.NameValueCollection SchedulingSpecialties { get; set; }
			
				 public virtual System.Collections.Specialized.NameValueCollection SpecialDesignations { get; set; }
			
				 public virtual System.Collections.Specialized.NameValueCollection Specialties { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: SpecialtyInterests</para><para>Data type: Rich Text</para>
			/// </summary>
			[IndexField("specialty_interests")]
				 public virtual string SpecialtyInterests { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: Suffix</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("suffix")]
				 public virtual string Suffix { get; set; }
			
				/// <summary>
			/// <para>Template: Physician</para><para>Field: TreatmentsAndServices</para><para>Data type: Rich Text</para>
			/// </summary>
			[IndexField("treatments_and_services")]
				 public virtual string TreatmentsAndServices { get; set; }
		
	}

}


#endregion
