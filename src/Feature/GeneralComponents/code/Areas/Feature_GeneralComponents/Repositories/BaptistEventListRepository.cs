﻿using Sitecore.XA.Feature.Events.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using Sitecore.Baptist.Feature.GeneralComponents.Areas.Feature_GeneralComponents.Models;
using Sitecore.Data.Items;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Models;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Repositories;
using Sitecore.Data;
using Sitecore.Layouts;
using Sitecore.XA.Foundation.RenderingVariants.Repositories;

namespace Sitecore.Baptist.Feature.GeneralComponents.Areas.Feature_GeneralComponents.Repositories
{
    public class BaptistEventListRepository : EventListRepository, IBaptistEventListRepository
    {
        public Item EventListItem { get; set; }

        public override IRenderingModelBase GetModel()
        {
            BaptistEventListRenderingModel model = (BaptistEventListRenderingModel)new BaptistEventCalendarRenderingModel();
            

            if(this.EventListItem != null)
            {
                model.DataSourceItem = EventListItem;                
                Sitecore.Diagnostics.Log.Info("BELRM overriding eventlistitem", this);
            }

            this.FillBaseProperties((object)model);            
            

            if(this.EventListItem != null) {                
                model.EventItems = this.EventListItem.Children.Select<Item, BaptistEventRenderingModel>((Func<Item, BaptistEventRenderingModel>)(item => new BaptistEventRenderingModel(item, model.RenderingWebEditingParams)));
                Sitecore.Diagnostics.Log.Info("BELRM got items " + model.EventItems.Count(), this);
            }
            else {
                model.EventItems = this.GetItems().Select<Item, BaptistEventRenderingModel>((Func<Item, BaptistEventRenderingModel>)(item => new BaptistEventRenderingModel(item, model.RenderingWebEditingParams)));
            }

            model.EventListCategory = this.GetEventListCategory();
            model.Location = this.GetEventLocation();

            Sitecore.Diagnostics.Log.Info("Returning from Get MOdel", this);
            return (IRenderingModelBase)model;
        }

        /*public IRenderingModelBase GetCombinedModel(List<string> ids)
        {
            
        }*/


        //
        protected virtual LocationItemRenderingModel GetEventLocation()
        {

            LocationItemRepository lir = new LocationItemRepository();

            if(this.EventListItem != null) {

                ID eventlistitemid = new ID();
                if(ID.TryParse(this.EventListItem[Templates.Custom.BaptistEventListTemplate.BaptistEventList.Fields.Location], out eventlistitemid))
                {
                    lir.LocationItem = Sitecore.Context.Database.GetItem(eventlistitemid);
                    return (LocationItemRenderingModel)lir.GetModel();
                }
                else
                {
                    return new LocationItemRenderingModel();
                }                 
                
            }else if(this.Rendering.DataSourceItem != null) {
                
                lir.LocationItem = Sitecore.Context.Database.GetItem(ID.Parse(this.Rendering.DataSourceItem[Templates.Custom.BaptistEventListTemplate.BaptistEventList.Fields.Location]));

                return (LocationItemRenderingModel)lir.GetModel();
               
            }else {
                return new LocationItemRenderingModel();
            }
            
        }

        protected virtual string GetEventListCategory()
        {
            if(this.EventListItem != null)
                return this.EventListItem[Templates.Custom.BaptistEventListTemplate.BaptistEventList.Fields.EventListCategory] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[Templates.Custom.BaptistEventListTemplate.BaptistEventList.Fields.EventListCategory] ?? string.Empty;
            return string.Empty;
        }

    }

}