﻿function setdetailmap(lat, lng, mapid) {
  var uluru = { lat: parseFloat(lat), lng: parseFloat(lng) };
  //console.log(uluru);
  //console.log(mapid);

  var mapdiv = document.getElementById(mapid);
  //console.log(mapdiv);
  var map = new google.maps.Map(document.getElementById(mapid), {
    zoom: 15,
    center: uluru,
    scrollwheel: false,
    navigationControl: false,
    mapTypeControl: false,
    scaleControl: false,
    draggable: false,
    streetViewControl: false,
    zoomControl: false
  });
  var marker = new google.maps.Marker({
    position: uluru,
    map: map
  });

  google.maps.event.addListener(map, 'tilesloaded', function () {
    $(this.getDiv()).find('img').each(function (i, eimg) {
      if (!eimg.alt || eimg.alt === '') {
        eimg.alt = 'Click to see this area on Google Maps';
      }
    });
  });
}


function updatedetailmaplocation(location) {

  var lat, lng, locname, mapid, clientid;

  lat = location.find('input[name=location-lat]').val();
  lng = location.find('input[name=location-lng]').val();
  locname = location.find('input[name="location-name"]').val();
  mapid = location.data('mapid');
  clientid = location.data('clientid');

  setdetailmap(lat, lng, mapid);
}

function dmgetdirectionslink(lat, lng, locname) {
  return "https://www.google.com/maps/place/" + locname;
  //return "https://www.google.com/maps/place/" + locname + "/@" + lat + "," + lng + ",17z";
}

$(function () {
  var startdetailmap = $('div.location-box-text-right:not(.embed-only)[data-clientid]');
  startdetailmap.each(function () {
    var detailmap = $(this);
    setTimeout(function () {
      updatedetailmaplocation(detailmap);
    }, 600);
  });
});