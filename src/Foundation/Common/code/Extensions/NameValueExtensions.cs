﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using Sitecore.Baptist.Foundation.Common.Utils;

namespace Sitecore.Baptist.Foundation.Common.Extensions
{
    public static class NameValueExtensions
    {
        public static bool AKeyHasValue(this NameValueCollection nvc)
        {            
            var items = nvc.AllKeys.SelectMany(nvc.GetValues, ( k, v ) => new { key = k, value = v });
            foreach(var item in items) {
                if(item.value.Trim().HasValue()) {
                    return true;
                }
            }
            return false;
        }

        public static List<string> getCollectionValues( this NameValueCollection nvc )
        {
            return nvc.Cast<string>().Select(e => HttpUtility.UrlDecode(nvc[e])).ToList<string>();
        }
    }
}