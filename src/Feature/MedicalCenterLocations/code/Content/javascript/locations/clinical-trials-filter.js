﻿var ctfilter = ctfilter || {};
var sortfilter = sortfilter || {};

ctfilter = {

  setresults: function (selector, styletag, datatag, tagtype) {

    //console.log("something");


    if (selector.val()) {
      //alert(selector.val());
      var selval = selector.val().toLowerCase();

      //console.log(styletag);
      if (tagtype == 'filter_style') {

        $('.filter_style').each(function () {
          $(this).empty();
        });

        $('.form-search select').each(function () {
          if ($(this).attr('id') != selector.attr('id')) {
            $(this).val('');
          }
        });
      }

      styletag.innerHTML = ".searchable:not([" + datatag + "^=\"" + selval + "\"])"
        + ":not([" + datatag + "*=\"" + selval + "\"])"
        + ":not([" + datatag + "$=\" " + selval + " \"])"
        + ":not([" + datatag + "=\"" + selval + "\"]) { display: none; }";
    } else {
      styletag.innerHTML = "";
    }

  },
  dropDownFilterInit: function (dropdowninput, searchFilter, indexattribute, tagtype) {
    var selectbox = $(dropdowninput);

    $(dropdowninput).change(function (event) {
      ctfilter.setresults(selectbox, searchFilter, indexattribute, tagtype);
      if (selectbox.val() != "") {
        window.location.hash = selectbox.val();
      } else {
        window.location = window.location.href.replace(window.location.hash, '');
      }
    });
  },

  ctfilterinit: function () {
    var ctSearchFilter = document.getElementById('ct_search_filter');
    var anSearchFilter = document.getElementById('an_site_filter');
    var ttSearchFilter = document.getElementById('trial_type_search_filter');
    var tpSearchFilter = document.getElementById('trial_phase_search_filter');
    //console.log('init');

    $(function () {

      if (ctSearchFilter) {
        //var searchbox = document.getElementById('tabSearch');
        var jqsearchbox = $('input#searchfilter');

        document.getElementById('searchfilter').addEventListener('input', function () {
          ctfilter.setresults(jqsearchbox, ctSearchFilter, 'data-index', 'search_style');
        });

      }

      if (anSearchFilter) {
        ctfilter.dropDownFilterInit('#selanatomicsite', anSearchFilter, 'data-ansite-index', 'filter_style');
      }
      if (ttSearchFilter) {
        ctfilter.dropDownFilterInit('#seltrialtype', ttSearchFilter, 'data-trial-type-index', 'filter_style');
      }
      if (tpSearchFilter) {
        ctfilter.dropDownFilterInit('#seltrialphase', tpSearchFilter, 'data-trial-phase-index', 'filter_style');
      }

    });
  }

}

$(function () {
  ctfilter.ctfilterinit();

  if ($('form#anatomic-site').length > 0 && location.hash) {
    var hash = location.hash.replace('#', '');
    //alert(hash);
    $("#selanatomicsite option[value='" + hash + "']").attr('selected', 'selected');
    var anselbox = $('#selanatomicsite');
    var ansfilt = document.getElementById('an_site_filter');
    ctfilter.setresults(anselbox, ansfilt, 'data-ansite-index', 'filter_style');
  }
});

sortfilter = {

  sort: function (triggerselector, elementselector, sortbyattribute, sortcontainersel, direction) {

    var orderedels = $(elementselector).sort(function (a, b) {

      var first = $(a).attr(sortbyattribute);
      var second = $(b).attr(sortbyattribute);

      if (direction == 'dsc') {
        return String.prototype.localeCompare.call(first, second);
      } else {
        return String.prototype.localeCompare.call(second, first);
      }
    });

    var container = $(sortcontainersel).empty().append(orderedels);
    console.log(direction);
    $(triggerselector).data('sortdir', direction);

  },
  changeArrow: function (iconelementsel, sortdir) {

    var icon = $(iconelementsel);
    icon.attr('class', '');

    switch (sortdir) {
      case 'init':
        icon.addClass('fa').addClass('fa-sort');
        break;
      case 'asc':
        icon.addClass('fa').addClass('fa-sort-down');
        break;
      case 'dsc':
        icon.addClass('fa').addClass('fa-sort-up');
        break;
    }
  },
  setSortDirection: function (sorttriggersel, sortelementsel, sortbyelement, sortcontainer) {

    var sortdir = $(sorttriggersel).data('sortdir');
    var nextsortdir = 'init';
    var iconsortelement = sorttriggersel + ' i.fa';
    //console.log(sortdir);
    switch (sortdir) {
      case 'init':
        nextsortdir = 'asc';
        break;
      case 'asc':
        nextsortdir = 'dsc';
        break;
      case 'dsc':
        nextsortdir = 'init';
        sortbyelement = 'data-numeric-index';
        break;
    }
    //console.log(nextsortdir);

    sortfilter.sort(sorttriggersel, sortelementsel, sortbyelement, sortcontainer, nextsortdir);
    sortfilter.changeArrow(iconsortelement, nextsortdir);

    sortfilter.bindClick();
  },
  bindClick: function () {

    $('.sortable').click(function (e) {

      e.preventDefault();
      sortfilter.setSortDirection('.sortable', '.searchable', 'data-ansite-index', '.sort-container');

      return false;
    });
  }

}

$(function () {
  sortfilter.bindClick();
});