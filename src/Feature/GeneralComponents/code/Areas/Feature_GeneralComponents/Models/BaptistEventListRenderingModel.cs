﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.XA.Foundation.RenderingVariants.Models;
using System.Collections.Generic;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Models;

namespace Sitecore.Baptist.Feature.GeneralComponents.Areas.Feature_GeneralComponents.Models
{
    public class BaptistEventListRenderingModel : VariantListsRenderingModel
    {
        public IEnumerable<BaptistEventRenderingModel> EventItems { get; set; }
        public LocationItemRenderingModel Location { get; set; }        
        public string EventListCategory { get; set; }
    }
}