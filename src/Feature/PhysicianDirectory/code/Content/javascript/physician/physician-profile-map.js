﻿function setphysmap(lat, lng, mapid) {
  var uluru = { lat: parseFloat(lat), lng: parseFloat(lng) };
  //console.log(uluru);
  //console.log(mapid);

  var mapdiv = document.getElementById(mapid);
  //console.log(mapdiv);
  var map = new google.maps.Map(document.getElementById(mapid), {
    zoom: 15,
    center: uluru,
    scrollwheel: false,
    navigationControl: false,
    mapTypeControl: false,
    scaleControl: false,
    draggable: false,
    streetViewControl: false,
    zoomControl: false
  });
  var marker = new google.maps.Marker({
    position: uluru,
    map: map
  });
}


function updatephysmaplocation(location) {

  var lat, lng, locname, mapid, clientid;

  lat = location.find('input[name=location-lat]').val();
  lng = location.find('input[name=location-lng]').val();
  locname = location.find('input[name="location-name"]').val();
  mapid = location.data('mapid');
  clientid = location.data('clientid');

  setphysmap(lat, lng, mapid);
}

function getdirectionslink(lat, lng, locname) {
  return "https://www.google.com/maps/place/" + locname + "/@" + lat + "," + lng + ",17z";
}

$(function () {
  var startphysmap = $('div.doctor-profile-primary-location-map:not(.embed-only)[data-clientid]');
  startphysmap.each(function () {
    var physmap = $(this);    
    setTimeout(function() {
      updatephysmaplocation(physmap)
    }, 600);    
  });
});