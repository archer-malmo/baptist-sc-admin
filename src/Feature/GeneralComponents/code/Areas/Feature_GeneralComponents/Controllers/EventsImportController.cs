﻿using Sitecore.Sites;
using Sitecore.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sitecore.Baptist.Feature.GeneralComponents.Areas.Feature_GeneralComponents.Controllers
{
    public class EventsImportController : Controller
    {
        // GET: Feature_GeneralComponents/EventsImport
        [HttpGet]
        public ActionResult Import()
        {
            var shellSite = SiteContext.GetSite("shell");

            using(new SiteContextSwitcher(shellSite))
            {
                var urlString = new UrlString(UIUtil.GetUri("control:PowerShellRunner"));
                urlString.Append("scriptId", "{E874F54C-DE6E-41AF-B92D-D2BBE0497C70}");
                urlString.Append("scriptDb", "master");

                return new JsonResult { Data = urlString.ToString(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        [HttpGet]
        public ActionResult CleanExpired()
        {
            var shellSite = SiteContext.GetSite("shell");

            using(new SiteContextSwitcher(shellSite))
            {
                var urlString = new UrlString(UIUtil.GetUri("control:PowerShellRunner"));
                urlString.Append("scriptId", "{6C1702BE-17AD-4588-9B5F-8EF7BE1EC930}");
                urlString.Append("scriptDb", "master");

                return new JsonResult { Data = urlString.ToString(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

       
    }
}