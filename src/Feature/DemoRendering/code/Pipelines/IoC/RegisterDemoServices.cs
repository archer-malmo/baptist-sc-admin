﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Baptist.Feature.DemoRendering.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.XA.Foundation.IOC.Pipelines.IOC;

namespace Sitecore.Baptist.Feature.DemoRendering.Pipelines.IoC
{
  public class RegisterDemoServices : IocProcessor
  {
    public override void Process(IocArgs args)
    {            
      args.ServiceCollection.AddTransient<IDemoRenderingRepository, DemoRenderingRepository>();
    }
  }
}
