﻿using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Repositories;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom;
using Sitecore.XA.Foundation.Mvc.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Controllers
{
    public class ClinicalTrialsController : StandardController
    {
        // GET: Feature_MedicalCenterLocations/ClinicalTrials

        protected readonly IClinicalTrialRepository ClinicalTrialRepository;

        public ClinicalTrialsController (IClinicalTrialRepository clinicalTrialRepository) {
            this.ClinicalTrialRepository = clinicalTrialRepository;
        }
        
    }

    public class ClinicalTrialsFolderController : StandardController {

        protected readonly IClinicalTrialsFolderRepository ClinicalTrialsFolderRepository;

        public ClinicalTrialsFolderController ( IClinicalTrialsFolderRepository clinicalTrailsFolderRepository )
        {
            this.ClinicalTrialsFolderRepository = clinicalTrailsFolderRepository;
        }

        public ActionResult ClinicalTrialsListing() {

            List<ClinicalTrialRenderingModel> trials = this.ClinicalTrialsFolderRepository.GetTrials();

            return (ActionResult)this.PartialView("ClinicalTrialsListing", trials);

        }



    }
}