﻿function getlocation(elem) {
  //console.log("getlocation");
  var facility = elem.data('er-location-string');
  //console.log(facility);
  return facility;
}

function addresult(elem, data) {

  var currentdata = JSON.parse(data);
  elem.find("span[data-er-wait-value]").html(currentdata.CV_ED_Wait);

}

function getwaittimes(elem) {

  if (getlocation(elem) == "no-match" || getlocation(elem) == "restorative-care" || getlocation(elem) == "germantown") {
    return;
  }

  var wturl = 'https://sites.bmhcc.org/api/waittimes/waittimes.php?fac=' + getlocation(elem);
  $.ajax({
    url: wturl,
    cache: false,
    method: 'GET',
    success: function (data) {
      addresult(elem, data);
    },
    error: function (data) {
      console.log(elem.data('er-location-string') + " did not return an appropriate result.");
    }
  });
}

$(function () {
  //console.log("loaded er-wait");
  if ($('div[data-er-wait-anywhere]').length > 0) {
    //console.log("found er wait markup");
    $('div[data-er-wait-anywhere').each(function () {
      getwaittimes($(this));
    });
  }
});

/*
 * Basic Markup for this component 
 * <div data-er-wait-anywhere data-er-location-string="childrens">
 *     <strong>ER Wait Time: <span data-er-wait-value></span>&nbsp;Minutes
 * </div> 
 * 
 */