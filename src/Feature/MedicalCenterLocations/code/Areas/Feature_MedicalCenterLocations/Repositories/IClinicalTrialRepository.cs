﻿using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Repositories
{
    public interface IClinicalTrialRepository : IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
    }
}