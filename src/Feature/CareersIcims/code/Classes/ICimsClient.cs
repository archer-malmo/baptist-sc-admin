﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using Sitecore.Diagnostics;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using Sitecore.Baptist.Foundation.Common.Utils;

namespace Sitecore.Baptist.Feature.CareersIcims.Classes
{
    public class ICimsClient
    {
        protected string authbase { get; set; }
        protected string customerID { get; set; }
        protected string endpointUri { get; set; }
        private DateTime today = DateTime.Now;
        private string format = "MM/dd/yyyy HH:mm:ss";

        public ICimsClient()
        {
            this.authbase = Sitecore.Configuration.Settings.GetSetting("Sitecore.Baptist.Feature.CareersIcims.Apikey");
            this.customerID = Sitecore.Configuration.Settings.GetSetting("Sitecore.Baptist.Feature.CareersIcims.CustomerID");
            this.endpointUri = Sitecore.Configuration.Settings.GetSetting("Sitecore.Baptist.Feature.CareersIcims.EndpointUri");
        }

        public string GetICimsResponse(string requestpath = "")
        {
            string requri = this.endpointUri + this.customerID + requestpath;

            //return requri;
            //Uri reqi = new Uri(requri);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            HttpWebRequest htwr = (HttpWebRequest)WebRequest.Create(requri);

            //NetworkCredential creds = new NetworkCredential("archermalmoapiuser", "Qn29D1$m;09Km");

            string creds = this.authbase;
            //CredentialCache credcache = new CredentialCache();

            //credcache.Add(reqi, "Basic", creds);
            
            htwr.Headers["Authorization"] =  "Basic " + creds;
            htwr.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36";

            //return htwr.Headers["Authorization"];
            //htwr.PreAuthenticate = true;            

            //htwr.Headers.Add("Authorization", "Basic " + this.authbase);

            //return htwr.Headers.ToString();            
            try
            {
                HttpWebResponse htwebres = htwr.GetResponse() as HttpWebResponse;



                Stream responsestream = htwebres.GetResponseStream();

                if(responsestream == null) return null;

                StreamReader streamread = new StreamReader(responsestream, Encoding.UTF8);
                string json = streamread.ReadToEnd();

                responsestream.Close();
                htwebres.Close();

                return json;

            }catch(WebException we) {
                
                HttpWebResponse errResponse = we.Response as HttpWebResponse;

                return errResponse.StatusCode.ToString();
            }
        }        

        /*public string PostICimsRequest(string RequestPath = "", string RequestJSON = "") {

            string response = "";

            return response;

        }*/

        //get portal jobs
        public string SearchPortalJobs( string portalname, string searchJson = null )
        {
            if(searchJson == null)
            {
                return this.GetICimsResponse("/search/portals/" + portalname);
            }
            else 
            {
                string encSearchJson = HttpUtility.UrlEncode(searchJson);

                string searchurl = "/search/portals/" + portalname + "?searchJson=" + encSearchJson;

                return this.GetICimsResponse(searchurl);
            }
        }

        public string GetPortalJob( string portalname, string jobnumber ) {

            string expcachekey = "Portal_Job_Posting_Expires_" + portalname + "_" + jobnumber;
            string cachekey = "Portal_Job_Posting_CacheKey_" + portalname + "_" + jobnumber;

            string response = "";

            if(ICimsCacheManager.GetCache(expcachekey).HasValue() && ICimsCacheManager.GetCache(expcachekey).CacheIsExpired() == false)
            {
                response = ICimsCacheManager.GetCache(cachekey);
            }
            else
            {
                response = this.GetICimsResponse("/portals/" + portalname + "/" + jobnumber);

                ICimsCacheManager.SetCache(expcachekey, this.today.AddDays(2).ToString(format));
                ICimsCacheManager.SetCache(cachekey, response);
            }

            return response;

        }


        //get filters for portal search
        public string GetPortalFilters(string portalname) 
        {
            string cachekey = "Portal_Filters_Cache_" + portalname;
            string expcachekey = "Portal_Filters_Expires_" + portalname;

            string portalfilters = String.Empty;

            if(ICimsCacheManager.GetCache(expcachekey).HasValue() && ICimsCacheManager.GetCache(expcachekey).CacheIsExpired() == false)
            {
                portalfilters = ICimsCacheManager.GetCache(cachekey);
            }
            else
            {
                portalfilters = this.GetICimsResponse("/search/portals/" + portalname + "/filters");

                ICimsCacheManager.SetCache(expcachekey, this.today.AddHours(2).ToString(format));
                ICimsCacheManager.SetCache(cachekey, portalfilters);
            }

            return portalfilters;
        }        


        public List<string> GetBaptistPortals() {

            List<string> portals = new List<string>();
            
            //For External Applicants
            //http://careers-bmhcc.icims.com/jobs/intro?hashed=0
            portals.Add("careers");

            //For Current Employee Transfers
            //http://employees-bmhcc.icims.com/jobs/intro?hashed=0
            portals.Add("employees");

            //Baptist Information Technology
            //https://baptisttechnologyservices-bmhcc.icims.com/jobs/search?pr=0&mobile=false&width=930&height=500&bga=true&needsRedirect=false&jan1offset=-360&jun1offset=-300
            portals.Add("baptisttechnologyservices");

            //Baptist Medical Group
            //FYI there was url string for BMG... but it didn't do anything or was empty ¯\_(ツ)_/¯
            //https://careers-bmhcc.icims.com/jobs/search?ss=1&searchKeyword=&searchLocation=&searchCategory=&searchCompany=206410&searchPostedDate=&searchPositionType=
            //Also Careers?

            //Nursing Applications
            //http://nursing-bmhcc.icims.com/jobs/search?ss=1&searchKeyword=&searchLocation=&searchCategory=14545&searchCategory=18334&searchCompany=&searchPostedDate=&searchPositionType=
            portals.Add("nursing");

            //Baptist College of Health Sciences
            //http://bchs-bmhcc.icims.com/jobs/intro?hashed=0
            portals.Add("bchs");

            


            return portals;
        }
    }
}