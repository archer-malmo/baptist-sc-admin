﻿using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Models;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Repositories;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom;
using Microsoft.Practices.ServiceLocation;
using Sitecore.XA.Foundation.Mvc.Controllers;
using Sitecore.Mvc.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Sitecore.Data.Items;
using Sitecore.XA.Foundation.RenderingVariants.Repositories;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Utils;
using Sitecore.Data.Fields;
using Sitecore.Data;
using Newtonsoft.Json;
using System.Net;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.Linq.Utilities;
using Sitecore.ContentSearch.Linq.Solr;
using Sitecore.ContentSearch.SolrProvider;
using Sitecore.ContentSearch.SolrProvider.SolrNetIntegration;
using Sitecore.XA.Foundation.Search;
using Sitecore.XA.Foundation.Search.Spatial;
using Sitecore.XA.Foundation.Search.Models;
using System.IO;
using System.Reflection;
using Sitecore.Diagnostics;
using SolrNet;
using SolrNet.Commands.Parameters;
using Sitecore.XA.Foundation.Search.Providers.Solr;
using Newtonsoft.Json.Linq;
using Sitecore.Baptist.Foundation.Common.Utils;
//using Sitecore.ContentSearch.Spatial.Solr;


namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Controllers
{
    public class LocationsController : StandardController
    {
        protected readonly ILocationItemRepository LocationRepository;

        public LocationsController( ILocationItemRepository locationRepository )
        {
            this.LocationRepository = locationRepository;
        }

        public ActionResult LocationsIndex()
        {
            return (ActionResult)this.PartialView("LocationsIndex", this.GetModel());
        }

        protected override object GetModel()
        {
            return (object)this.LocationRepository.GetModel();
        }

        public ActionResult LocationDetail()
        {
            LocationDetailModel ldm = new LocationDetailModel();
            //ldm.pageitem = RenderingContext.Current.Rendering.Item;

            LocationItemRepository lir = new LocationItemRepository();
            lir.LocationItem = RenderingContext.Current.Rendering.Item;

            //ldm.location = (Models.LocationItemRenderingModel)lir.GetModel();
            
            return (ActionResult)this.PartialView("LocationDetail", lir.GetModel());
        }

        public ActionResult LocationDetailsBox()
        {
            Item pgitem = RenderingContext.Current.Rendering.Item;
            HospitalItemRepository hir = new HospitalItemRepository();

            hir.LocationItem = ((LookupField)pgitem.Fields["Location"]).TargetItem;                       

            Models.HospitalItemRenderingModel hirm = (Models.HospitalItemRenderingModel)hir.GetModel();

            return (ActionResult)this.PartialView("LocationDetailsBox", hirm);
        }

        public ActionResult ServicesProvidedContent()
        {
        
            ServicesProvidedContentRepository scpr = new ServicesProvidedContentRepository();

            scpr.ServicesProvidedContentItem = RenderingContext.Current.Rendering.Item;

           
    
            return (ActionResult)this.PartialView("ServicesProvidedContent", (ServicesProvidedContentRenderingModel)scpr.GetModel());

        }
    }

    public class LocationsFolderController : StandardController
    {
        protected readonly ILocationFolderRepository LocationFolderRepository;

        public LocationsFolderController( ILocationFolderRepository locationFolderRepository )
        {
            this.LocationFolderRepository = locationFolderRepository;
        }

        public ActionResult LocationsListing( bool viewall = false )
        {
            LocationUtils lu = new LocationUtils();
            LocationListingSearch ll = new LocationListingSearch();

            lu.req = Request;
            lu.respnse = Response;
            string datasourceItemPath = this.LocationFolderRepository.GetModel().DataSourceItem.Paths.Path;

            string locationType = LocationUtils.locationTypeFromPath(datasourceItemPath);
            
            string radius = RenderingContext.Current.Rendering.Parameters["Search radius"];

            int rad = 500;
            if(!string.IsNullOrEmpty(radius)) {
                int.TryParse(radius, out rad);
            }

            List<LocationWithDistanceSearch> lwd = lu.getLocationsAndDistanceFromUserSolr(locationType, rad);
            
            ll.radius = rad;
            ll.locationtype = locationType;
            ll.scheduling_only = (RenderingContext.Current.Rendering.Parameters["OMW HMP Scheduling Only"] == "1") ? true : false;
            ll.locations = (ll.scheduling_only) ? lwd.Where(p => p.location.SchedulingCode.HasValue()).ToList<LocationWithDistanceSearch>() : lwd;

            if(datasourceItemPath.ToUpper().Contains("HOSPITAL"))
            {
                ll.hospitallistings = true;
            }
            else
            {
                ll.hospitallistings = false;
            }

            return (ActionResult)this.PartialView("LocationsListing", ll);
        }

        public ActionResult ClinicsListing( bool viewall = false )
        {

            LocationUtils lu = new LocationUtils();
            LocationListingSearch ll = new LocationListingSearch();

            lu.req = Request;
            lu.respnse = Response;

            string radius = RenderingContext.Current.Rendering.Parameters["Search radius"];

            bool bmgonly = (RenderingContext.Current.Rendering.Parameters["BMG Only"] == "1") ? true : false;
            //On My Way/Hold My Place only
            bool schedulinglocations = (RenderingContext.Current.Rendering.Parameters["OMW HMP Scheduling Only"] == "1") ? true : false;

            int rad = 10;
            if(!string.IsNullOrEmpty(radius))
            {
                int.TryParse(radius, out rad);
            }            

            List<LocationWithDistanceSearch> lwd = lu.getClinicsAndDistanceFromUser(rad, bmgonly);
          
            ll.radius = rad;
            ll.locationtype = "Clinic";
            ll.scheduling_only = (RenderingContext.Current.Rendering.Parameters["OMW HMP Scheduling Only"] == "1") ? true : false;
            ll.locations = (ll.scheduling_only) ? lwd.Where(p => p.location.SchedulingCode.HasValue()).ToList<LocationWithDistanceSearch>() : lwd;
            //return Content(ll.locations.Count.ToString()); 

            ll.hospitallistings = false;

            return (ActionResult)this.PartialView("ClinicsListing", ll);
        }

        public ActionResult LocationsListingSearch( string query, string dsiID, Guid uniqueID, bool viewall = false, int radius = 500, bool bmgonly = false, bool scheduling_only = false )
        {
            LocationUtils lu = new LocationUtils();

            LocationListingResSearch llr = new LocationListingResSearch();

            LocationListingSearch ll = new Models.LocationListingSearch();
            lu.req = Request;
            lu.respnse = Response;

            ID dsi = ID.Parse(dsiID);

            string dsitempath = Sitecore.Context.Database.GetItem(dsi).Paths.Path;

            //return Content(dsitempath);            

            //List<LocationWithDistanceSearch> lwd = lu.getLocationsAndDistanceFromQuerySolr(query, dsitempath);

            string locationType = LocationUtils.locationTypeFromPath(dsitempath);

            List<LocationWithDistanceSearch> lwd = lu.getLocationsAndDistanceFromQuerySolr(query, locationType, radius, bmgonly);

            //return Content(JsonConvert.SerializeObject(lwd));

            ll.radius = radius;
            ll.locationtype = locationType;
            ll.scheduling_only = scheduling_only;
            ll.locations = (ll.scheduling_only) ? lwd.Where(p => p.location.SchedulingCode.HasValue()).ToList<LocationWithDistanceSearch>() : lwd;

            //RenderingContext.CurrentOrNull.Rendering.DataSource = dsiID;

            RenderingContext rc = new RenderingContext();
            Rendering rend = new Mvc.Presentation.Rendering();
          
            rend.DataSource = dsiID;
            rend.UniqueId = uniqueID;

            rc.Rendering = rend;
            rc.Rendering.Parameters["BMG Only"] = (bmgonly) ? "1" : "0";
            rc.Rendering.Parameters["OMW HMP Scheduling Only"] = (scheduling_only) ? "1" : "0";

            llr.locationlisting = ll;
            llr.renderingcontext = rc;
            llr.originalquery = WebUtility.UrlDecode(query);
            llr.viewall = viewall;
            llr.locationtype = locationType;

            return (ActionResult)this.PartialView("~/Areas/Feature_MedicalCenterLocations/Views/LocationsFolder/LocationListingResult.cshtml", llr);
        }

        public ActionResult LocationsListingSearchByType( string query, string dsiID, string locationType, Guid uniqueID, bool viewall = false, int radius = 500, bool bmgonly = false, bool scheduling_only = false )
        {
            LocationUtils lu = new LocationUtils();

            LocationListingResSearch llr = new LocationListingResSearch();

            LocationListingSearch ll = new Models.LocationListingSearch();
            lu.req = Request;
            lu.respnse = Response;

            ID dsi = ID.Parse(dsiID);

            string dsitempath = Sitecore.Context.Database.GetItem(dsi).Paths.Path;

            //return Content(dsitempath);            

            List<LocationWithDistanceSearch> lwd = lu.getLocationsAndDistanceFromQuerySolr(query, locationType, radius, bmgonly);

            //return Content(JsonConvert.SerializeObject(lwd));

            //return Content(JsonConvert.SerializeObject(lwd));

            ll.radius = radius;
            ll.scheduling_only = scheduling_only;
            ll.locations = (ll.scheduling_only) ? lwd.Where(p => p.location.SchedulingCode.HasValue()).ToList<LocationWithDistanceSearch>() : lwd;

            //RenderingContext.CurrentOrNull.Rendering.DataSource = dsiID;

            RenderingContext rc = new RenderingContext();
            Rendering rend = new Mvc.Presentation.Rendering();

            rend.DataSource = dsiID;
            rend.UniqueId = uniqueID;

            rc.Rendering = rend;
            rc.Rendering.Parameters["BMG Only"] = (bmgonly) ? "1" : "0";
            rc.Rendering.Parameters["OMW HMP Scheduling Only"] = (scheduling_only) ? "1" : "0";

            llr.locationlisting = ll;
            llr.renderingcontext = rc;
            llr.originalquery = WebUtility.UrlDecode(query);
            llr.viewall = viewall;
            llr.locationtype = locationType;

            return (ActionResult)this.PartialView("~/Areas/Feature_MedicalCenterLocations/Views/LocationsFolder/LocationTypeListingResult.cshtml", llr);
        }

        public ActionResult LocationsNearYou()
        {
            LocationUtils lu = new LocationUtils();
            LocationListing ll = new LocationListing();

            lu.req = Request;
            lu.respnse = Response;
            string datasourceItemPath = this.LocationFolderRepository.GetModel().DataSourceItem.Paths.Path;

            List<LocationWithDistance> lwd = lu.getLocationsAndDistanceFromUser(datasourceItemPath);
            ll.locations = lwd;

            if(datasourceItemPath.ToUpper().Contains("HOSPITAL"))
            {
                ll.hospitallistings = true;
            }
            else
            {
                ll.hospitallistings = false;
            }

            return (ActionResult)this.PartialView("LocationsNearYou", ll);
        }

        protected override object GetModel()
        {
            return (object)this.LocationFolderRepository.GetModel();
        }

        /*public JsonResult locations()
        {

            Database db = Sitecore.Context.Database;

            List<Item> cancercenters = db.SelectItems("/sitecore/content/Baptist/Baptist/Baptistcancercenter/United States/Data/Locations//*[@@templatename = 'Location Item']").ToList<Item>();

            List<cancercenteritem> locationlist = new List<cancercenteritem>();

            foreach(Item cc in cancercenters)
            {

                locationlist.Add(new cancercenteritem { cancercenterid = cc.Fields["ClientID"].ToString(), locationname = cc.Fields["Location Name"].ToString(), sitecoreid = cc.ID.Guid.ToString(), address1 = cc.Fields["Address Line 1"].ToString(), address2 = cc.Fields["Address Line 2"].ToString(), city = cc.Fields["City"].ToString(), state = cc.Fields["State"].ToString(), });
            }

            return Json(locationlist, JsonRequestBehavior.AllowGet);
        }*/
        
        //Dead end
        public ActionResult getLocationsFromPointSolr( float latitude, float longitude, string locationtype, int radius = 500 )
        {
            

            List<LocationWithDistanceSearch> locationswithdistance = new List<LocationWithDistanceSearch>();


            //var userpoint = string.Format("POINT({1} {0})", latitude, longitude);

            //DbGeography dbgeo = DbGeography.FromText(userpoint);

            string index_name = string.Format("baptist_locations_{0}_index", Sitecore.Context.Database.Name.ToLower());

            //var index = ContentSearchManager.GetIndex(index_name);
            var index = ContentSearchManager.GetIndex(index_name);
           
            //ContentSearchManager csm = 
            //ContentSearch.SolrProvider.SolrSearchIndex ssi = new ContentSearch.SolrProvider.SolrSearchIndex(index.Name, index.Name, index.PropertyStore, index.Group.Name);
            //ssi.Initialize();           
            //ssi.CreateSearchContext()

            var predicate = PredicateBuilder.True<LocationSearchResultItem>();


            string serviceBaseAddress = Sitecore.Configuration.Settings.GetSetting("ContentSearch.Solr.ServiceBaseAddress");

            string coreaddress = serviceBaseAddress + "/" + index_name;


            string requesturl = serviceBaseAddress + "/" + index_name + "/select?d=" + radius.ToString() + "&fl=_dist_:geodist(),*&fq={!geofilt%20pt=" + latitude + "," + longitude + "%20sfield=coordinates_rpt%20d=20}&indent=on&pt=" + latitude + "," + longitude + "&q=*:*&sfield=coordinates_rpt&wt=json&sort=geodist()%20asc";

            //return (Content(requesturl));

            //var scontext = index.CreateSearchContext();

            //var sq = scontext.GetQueryable<LocationSearchResultItem>();

            HttpWebRequest hrquest = (HttpWebRequest)WebRequest.Create(requesturl);

            List<LocationSearchResultItem> locationres = new List<LocationSearchResultItem>();

            using(HttpWebResponse response = hrquest.GetResponse() as HttpWebResponse)
            {
                 StreamReader readStream = new StreamReader(response.GetResponseStream());
                 string rspns = readStream.ReadToEnd();

                 //Sitecore.Diagnostics.Log.Info("[IP INFO]" + geoipinfo, this);
                 //var obj = JsonConvert.DeserializeObject<UserGeo>(geoipinfo);

                 dynamic objt = JsonConvert.DeserializeObject<dynamic>(rspns);

                
                 foreach(var loc in objt.response.docs) {                    

                    LocationSearchResultItem res = LocationUtils.SetFromResult(loc);

                    locationres.Add(res);
                    
                 }

             }
            
            
            /*Location userloc = new Location(latitude, longitude);

            var solrQuery = new SolrQueryByField("location_type_t", locationtype);
            var queryOptions = new QueryOptions();
                         
            queryOptions.FilterQueries.Add(new SolrQueryByDistance("coordinates_rpt", userloc, distance: radius, accuracy: CalculationAccuracy.Radius));
            queryOptions.Fields = new[] { "_dist_:geodist()", "*" };
            queryOptions.ExtraParams = new Dictionary<string, string> {
             { "pt", userloc.ToString() },
             { "d", radius.ToString() },
             { "sfield", "coordinates_rpt" }
            };
            queryOptions.OrderBy = new[] { new SortOrder("geodist()", Order.ASC) };            

            Startup.Container.Clear();           
            Startup.InitContainer();
            Startup.Init<LocationSearchResultItem>(coreaddress);

           

            var solr = ServiceLocator.Current.GetInstance<ISolrOperations<LocationSearchResultItem>>();
            var results = solr.Query(solrQuery, queryOptions);

            //return Content(JsonConvert.SerializeObject(results));

            */

            return PartialView("~/Areas/Feature_MedicalCenterLocations/Views/LocationsFolder/getLocationsFromPointSolr.cshtml", locationres);
            
            



            /*using(var context = index.CreateSearchContext())
            {
                context.                              
                float lat, lon;

                //predicate = predicate.And(f => dbgeo.Distance(DbGeography.FromText(string.Format("POINT({0} {1})", ((!float.TryParse(f.Latitude, out lat)) ? (float)0.0 : lat), ((!float.TryParse(f.Longitude, out lon)) ? (float)0.0 : lon)))) < radius);
                predicate = predicate.And(f => f.LocationType == "Hospital");
                //predicate = predicate.Or(f => f.GetField("Specialties").ToString().ToLower().decodeUrl().Contains(q.decodeUrl().ToLower()));
                //predicate = predicate.Or(f => f.Fields["primary_location_postal_code_t"] == q);
                //predicate = predicate.Or(f => f["Primary Location City"].ToString().Contains(q));

                string coords = latitude + "," + longitude;
                var searchQuery = context.GetQueryable<LocationSearchResultItem>()
                    .WithinTheRadius(i => i.Coordinates, latitude, longitude, 15);

                //  .Where(predicate);                              

                var resultItems = searchQuery.ToList();
                //var totalCount = searchQuery.Count();

                return Content(JsonConvert.SerializeObject(resultItems));

                //return Json("Nothing", JsonRequestBehavior.AllowGet);
            }*/

        }
        //Dead end
        public static LocationSearchResultItem SetFromResult( dynamic result )
        {
            LocationSearchResultItem loca = new LocationSearchResultItem();            
            loca.ItemId = ID.Parse(Guid.ParseExact(result._group.ToString(), "N"));
            loca.LocationName = result.location_name_t[0].ToString();
            loca.ShortName = (!string.IsNullOrEmpty(result.short_name_t[0].ToString())) ? result.short_name_t[0].ToString() : "";
            loca.AddressLine1 = (!string.IsNullOrEmpty(result.address_line_1_t[0].ToString())) ? result.address_line_1_t[0].ToString() : "";
            loca.AddressLine2 = (result.address_line_2_t != null && !string.IsNullOrEmpty(result.address_line_2_t[0].ToString())) ? result.address_line_2_t[0].ToString() : "";
            loca.City = (!string.IsNullOrEmpty(result.city_t[0].ToString())) ? result.city_t[0].ToString() : "";
            loca.State = (!string.IsNullOrEmpty(result.state_t[0].ToString())) ? result.state_t[0].ToString() : "";
            loca.PostalCode = (!string.IsNullOrEmpty(result.postal_code_t[0].ToString())) ? result.postal_code_t[0].ToString() : "";
            loca.Latitude = (!string.IsNullOrEmpty(result.latitude_t[0].ToString())) ? result.latitude_t[0].ToString() : "";
            loca.Longitude = (!string.IsNullOrEmpty(result.longitude_t[0].ToString())) ? result.longitude_t[0].ToString() : "";
            loca.Coordinates = (!string.IsNullOrEmpty(result.coordinates_rpt.ToString())) ? result.coordinates_rpt.ToString() : "";
            loca.MainPhoneNumber = (!string.IsNullOrEmpty(result.main_phone_number_t[0].ToString())) ? result.main_phone_number_t[0].ToString() : "";
            loca.ERWaitURLKey = (result.er_wait_url_key_t != null && !string.IsNullOrEmpty(result.er_wait_url_key_t[0].ToString())) ? result.er_wait_url_key_t[0].ToString() : "";
            loca.LocationType = (!string.IsNullOrEmpty(result.location_type_t[0].ToString())) ? result.location_type_t[0].ToString() : "";
            loca.ClientID = (result.clientid_t != null && !string.IsNullOrEmpty(result.clientid_t[0].ToString())) ? result.clientid_t[0].ToString() : "";            
            loca.MainEmailAddress = (result.main_email_address_t != null && !string.IsNullOrEmpty(result.main_email_address_t[0].ToString())) ? result.main_email_address_t[0].ToString() : "";

            loca.AlternateLocationListingPage = (result.alternate_location_listing_page_t != null && !string.IsNullOrEmpty(result.alternate_location_listing_page_t[0].ToString())) ? result.alternate_location_listing_page_t[0].ToString() : "";
            loca.EnableAlternateLink = result.enable_alternate_link_b;
            loca.OverrideWithMapsLink = result.override_with_maps_link_b;

            loca.IsBMG = result.is_bmg_b;
            loca.IsActive = result.is_active_b;
            loca.IsPubliclySearchable = result.is_publicly_searchable_b;

            loca.SchedulingType = (!string.IsNullOrEmpty(result.scheduling_type_t[0].ToString())) ? result.scheduling_type_t[0].ToString() : "";
            loca.SchedulingLabel = (!string.IsNullOrEmpty(result.scheduling_label_t[0].ToString())) ? result.scheduling_label_t[0].ToString() : "";
            loca.SchedulingCode = (!string.IsNullOrEmpty(result.scheduling_code_t[0].ToString())) ? result.scheduling_code_t[0].ToString() : "";         
            loca.Distance = result._dist_;
            
            return loca;
        }
       

    }
    // Was a dead end
    /*public static class SearchExtensions
    {
        public static IQueryable<TSource> WithinTheRadius<TSource, TKey>( this IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, double lat, double lon, int radius )
        {
            Assert.ArgumentNotNull((object)source, "source");
            Assert.ArgumentNotNull((object)keySelector, "keySelector");
            MethodInfo method = ((MethodInfo)MethodBase.GetCurrentMethod()).MakeGenericMethod(typeof(TSource), typeof(TKey));

            Expression[] exporessionArray = new Expression[5]
            {
                source.Expression,
                (Expression)Expression.Constant((Expression)keySelector),
                (Expression)Expression.Constant((object)lat, typeof(double)),
                (Expression)Expression.Constant((object)lon, typeof(double)),
                (Expression)Expression.Constant((object)radius, typeof(int))
            };

            return source.Provider.CreateQuery<TSource>((Expression)Expression.Call((Expression)null, method, exporessionArray));
        }
        public static IQueryable<TSource> OrderByNearest<TSource>( this IQueryable<TSource> source ) where TSource : LocationSearchResultItem
        {
            if(source == null)
                throw new ArgumentNullException("source");
            return (IQueryable<TSource>)source.OrderBy<TSource, string>((Expression<Func<TSource, string>>)(i => i["score"]));
        }
    }*/
       
}

