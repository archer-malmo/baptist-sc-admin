﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Sitecore.Baptist.Foundation.Common.Classes
{
    public static class CommunicationConduit
    {

        public static readonly string MetadataConduitKey = "Metadata_Conduit";

        public static void SendMetadata( string nextLink, string prevLink )
        {
            StringBuilder markup = new StringBuilder();

            if(!string.IsNullOrWhiteSpace(nextLink))
            {
                markup.AppendFormat("<link rel=\"next\" href=\"{0}\"/>", nextLink);
            }

            if(!string.IsNullOrWhiteSpace(prevLink))
            {
                markup.AppendFormat("<link rel=\"prev\" href=\"{0}\"/>", prevLink);
            }

            HtmlString html = new HtmlString(markup.ToString());

            HttpContext.Current.Items[MetadataConduitKey] = html;
        }

        public static HtmlString ReceiveMetadata()
        {
            HtmlString result = null;

            if(HttpContext.Current.Items.Contains(MetadataConduitKey))
            {
                result = HttpContext.Current.Items[MetadataConduitKey] as HtmlString;
            }

            if(result == null)
            {
                result = new HtmlString(string.Empty);
            }

            return result;
        }
    }
}