﻿using Sitecore.Baptist.Feature.PhysicianDirectory.Areas.Feature_PhysicianDirectory.Models;
using Sitecore.Baptist.Foundation.Common.Utils;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Pipelines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sitecore.Baptist.Feature.PhysicianDirectory.Areas.Feature_PhysicianDirectory.Utils
{
    public class PhysicianCacheWarmer
    {

        private DateTime today = DateTime.Now;
        private string format = "MM/dd/yyyy HH:mm:ss";

        public static void warmCaches() {
           
             //Load Locations
            Log.Info("Physician Locations Cache Start loading", typeof(PhysicianCacheWarmer));
            new PhysicianCacheWarmer().loadLocationsCache("/sitecore/content/Baptist/Baptist");           
            Log.Info("Physician Locations Cache Process loaded", typeof(PhysicianCacheWarmer));

            //Load Physician Index
            Log.Info("Physician Directory Index Cache Loading Start", typeof(PhysicianCacheWarmer));
            warmFADIndex();
            Log.Info("Physician Directory Index Cache Loading Complete", typeof(PhysicianCacheWarmer));

        }


        //Warm Find-A-Doctor Cache first 
        public static void warmFADIndex() {

            DateTime today = DateTime.Now;
            string format = "MM/dd/yyyy HH:mm:ss";

            string cachekey = "Physician_List_Index_Data";
            string expcachekey = "Physician_List_Index_Expires";

            string bmgcachekey = "Physician_List_Index_Data_BMGOnly";
            string bmgexpcachekey = "Physician_List_index_Expires_BMGOnly";            

            PhysicianListing pl = null;
            PhysicianListing plBMG = null;

            PhysicianUtils phyutil = new PhysicianUtils();

            //First all of the doctors                        
            pl = phyutil.getSpecialtiesAndCounts();

            plBMG = phyutil.getSpecialtiesAndCounts(true);

            PhysicianListingCacheManager.SetCache(expcachekey, today.AddDays(30).ToString(format));
            PhysicianListingCacheManager.SetListingCache(cachekey, pl);

            //Diagnostics.Log.Info("[CACHE_Physician] - Find A Doctor Cache Loaded", typeof(PhysicianCacheWarmer));
            Log.Info("[CACHE_Physician] - Find A Doctor Cache Loaded", typeof(PhysicianCacheWarmer));

        }


        public static void warmFADSpecialty() {

            DateTime today = DateTime.Now;
            string format = "MM/dd/yyyy HH:mm:ss";

            string fadcachekey = "Physician_List_Index_Data";            

            string bmgfadcachekey = "Physician_List_Index_Data_BMGOnly";            

            //get fad cache
            PhysicianListing pl = PhysicianListingCacheManager.GetListingCache(fadcachekey);
            PhysicianListing plbmg = PhysicianListingCacheManager.GetListingCache(fadcachekey);

            foreach(Specialty spec in pl.Specialties) {

                string specialty = spec.specialtyUrl;

                PhysicianUtils phyutil = new PhysicianUtils();

                string cachekey = "Physician_List_Specialty_" + specialty + "_Data";
                string expcachekey = "Physician_List_Specialty_" + specialty + "_Expires";

                PhysicianListingBySpecialtyCached plbs = null;
                PhysicianListingBySpecialtyCached plbsbmg = null;

                string bmgcachekey = "Physician_List_Specialty_" + specialty + "_Data_BMGOnly";
                string bmgexpcachekey = "Physician_List_Specialty_" + specialty + "_Expires_BMGOnly";

                plbsbmg = phyutil.getPhysiciansBySpecialty(specialty, true);
                plbs = phyutil.getPhysiciansBySpecialty(specialty);

                PhysicianListingBySpecialtyCacheManager.SetCache(expcachekey, today.AddDays(30).ToString(format));
                PhysicianListingBySpecialtyCacheManager.SetListingCache(cachekey, plbs);

                //Diagnostics.Log.Info("[CACHE_Physician] - Specialty Cache loaded for " + spec.specialtyName, typeof(PhysicianCacheWarmer));
                Log.Info("[CACHE_Physician] - Specialty Cache loaded for " + spec.specialtyName, typeof(PhysicianCacheWarmer));

                PhysicianListingBySpecialtyCacheManager.SetCache(bmgexpcachekey, today.AddDays(30).ToString(format));
                PhysicianListingBySpecialtyCacheManager.SetListingCache(bmgcachekey, plbsbmg);

                //Diagnostics.Log.Info("[CACHE_Physician] - BMG Only Specialty Cache loaded for " + spec.specialtyName, typeof(PhysicianCacheWarmer));
                Log.Info("[CACHE_Physician] - BMG Only Specialty Cache loaded for " + spec.specialtyName, typeof(PhysicianCacheWarmer));

            }

            //Diagnostics.Log.Info("[CACHE_Physician] - All Specialty Caches Loaded!", typeof(PhysicianCacheWarmer));
            Log.Info("[CACHE_Physician] - All Specialty Caches Loaded!", typeof(PhysicianCacheWarmer));

        }

        public virtual void Process(PipelineArgs args) {

            //Load Locations
            Log.Info("Physician Locations Cache Start loading", this);
            this.loadLocationsCache("/sitecore/content/Baptist/Baptist/Baptistonline/United States/Data/Locations");           
            Log.Info("Physician Loactions Cache Process loaded", this);

            //Load Physician Index
            Log.Info("Physician Directory Index Cache Loading Start", this);
            warmFADIndex();
            Log.Info("Physician Directory Index Cache Loading Complete", this);

            //Load Physician By Specialty
           // Log.Info("Physician Load Specialties Start", this);
            //warmFADSpecialty();
            //Log.Info("Physician Load Specialties Complete", this);

        }


        public void loadLocationsCache(string folderPath)
        {
            string cachekey = "Physician_Locations";
            string expcachekey = "Physician_Location_List_Expires";

            //Log.Info("Location lookup", this);
            IEnumerable<Item> locations = null;
           
            locations = Sitecore.Data.Database.GetDatabase("web").GetItem(folderPath).Axes.GetDescendants().Where(x => x.TemplateName == "Location Item" || x.TemplateName == "Hospital Item");

            List<PhysicianLocation> lirm = new List<PhysicianLocation>();

            int cnt = 0;

            foreach(var loc in locations)
            {

                PhysicianLocation ploc = new PhysicianLocation();

                //Log.Info(loc["ClientID"] + " Loaded into cache", this);

                ploc.ClientID = loc["ClientID"];
                ploc.LocationName = loc["Location Name"];
                ploc.Address1 = loc["Address Line 1"];
                ploc.Address2 = loc["Address Line 2"];
                ploc.City = loc["City"];
                ploc.State = loc["State"];
                ploc.Latitude = loc["Latitude"];
                ploc.Longitude = loc["Longitude"];
                ploc.MainPhoneNumber = loc["Main Phone Number"];
                ploc.PostalCode = loc["Postal Code"];
                ploc.LocationType = loc["Location Type"];
                ploc.ItemID = loc.ID;
                //Sitecore.Diagnostics.Log.Info("[location info] " + tlocation.Title, this);
                lirm.Add(ploc);
                cnt++;
            }

            Log.Info(cnt.ToString() + " Locations Loaded into cache", this);

            PhysicianLocationCacheManger.SetCache(expcachekey, today.AddDays(30).ToString(format));
            PhysicianLocationCacheManger.SetListingCache(cachekey, lirm);                
            
        }      

    }

}