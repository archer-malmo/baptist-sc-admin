﻿using Newtonsoft.Json;
using Sitecore.Baptist.Feature.PhysicianDirectory.Areas.Feature_PhysicianDirectory.Models;
using Sitecore.Baptist.Feature.PhysicianDirectory.Areas.Feature_PhysicianDirectory.Utils;
using Sitecore.Mvc.Presentation;
using Sitecore.XA.Foundation.Mvc.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sitecore.XA.Foundation.MarkupDecorator.Extensions;
using Sitecore.Baptist.Foundation.Common.Utils;
using Sitecore.Baptist.Feature.PhysicianDirectory.Templates.Custom;
using Sitecore.Xml;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using Sitecore.Baptist.Foundation.Common.Classes;
using Sitecore.Web.UI.HtmlControls;
using System.Web.UI.HtmlControls;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.ContentSearch.Linq.Utilities;
using System.Text.RegularExpressions;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Utils;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Models;
using System.Globalization;
using System.Threading;
using Sitecore.Baptist.Foundation.Common.Models;
using System.Runtime.Remoting;
using Newtonsoft.Json.Linq;
using Sitecore.Data;
using System.IO;
using Sitecore.ContentSearch.Linq;

namespace Sitecore.Baptist.Feature.PhysicianDirectory.Areas.Feature_PhysicianDirectory.Controllers
{
    public class PhysicianController : StandardController
    {
        // GET: Feature_PhysicianDirectory/Physician

        private DateTime today = DateTime.Now;
        private string format = "MM/dd/yyyy HH:mm:ss";

        public ActionResult PhysicianProfile(string physician)
        {
            string rawurl = Sitecore.Context.RawUrl;
            physician = rawurl.poplast();
            var rc = RenderingContext.CurrentOrNull;

            PhysicianUtils phyutil = new PhysicianUtils();
            phyutil.req = Request;
            phyutil.respnse = Response;

            PhysicianRenderingModel prm = phyutil.getPhysicianByUrl(physician);            

            if((prm.FirstName == null || prm.FirstName.Trim() == "") ) {                              

                Response.TrySkipIisCustomErrors = true;
                Response.StatusCode = 404;
                Response.End();                
            }                                   

            return (ActionResult)this.PartialView("~/Areas/Feature_PhysicianDirectory/Views/Physician/PhysicianProfile.cshtml", prm);
        }

        public ActionResult PhysicianOneCareProfile(string onecareid)
        {
            //string rawurl = Sitecore.Context.RawUrl;
            //onecareid = rawurl.poplast();
            //var rc = RenderingContext.CurrentOrNull;

            PhysicianUtils phyutil = new PhysicianUtils();
            phyutil.req = Request;
            phyutil.respnse = Response;

            PhysicianRenderingModel prm = phyutil.getPhysicianByOneCareID(onecareid);

            if(prm == null) {
                //   Response.TrySkipIisCustomErrors = true;
                Response.TrySkipIisCustomErrors = true;
                Response.StatusCode = 404;
                Response.End();
            }

            return (ActionResult)this.PartialView("~/Areas/Feature_PhysicianDirectory/Views/Physician/PhysicianOneCareProfile.cshtml", prm);

        }

        public ActionResult PhysicianIndexBySpecialties(bool bmgonly = false) 
        {             
            //return Content((bmgonly) ? "true" : "false");
            //return Content(bmgonly.ToString());
            var rc = RenderingContext.CurrentOrNull;
            bool bcconly = false;
            if(rc != null) {
                var parms = rc.Rendering.Parameters;
                //return Content(parms["BMG Only"]);
                bmgonly = (parms["BMG Only"] == "1") ? true : false;
                bcconly = (parms["BCC Only"] == "1") ? true : false;
            }

            PhysicianUtils phyutil = new PhysicianUtils();
            phyutil.req = Request;
            phyutil.respnse = Response;

            string cachekey = "Physician_List_Index_Data";
            string expcachekey = "Physician_List_Index_Expires";

            string folderpath = "/sitecore/content/Baptist/Baptist/Baptistonline/United States/Data/Physicians";            

            bool bmgorig = bmgonly;

            //if(RenderingContext.Current.Rendering.DataSource.HasValue())
            //{

                //Sitecore.Diagnostics.Log.Info("[RENDERING CONTEXT] find a doctor has value", this);

                Item renderingitem = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(RenderingContext.Current.Rendering.DataSource));
                

                //if(renderingitem.Paths.FullPath != folderpath)
                //{
                    folderpath = renderingitem.Paths.FullPath;
                //}

                if(folderpath.Contains("Baptistmedicalgroup"))
                {
                    bmgonly = true;
                }
           // }

            if(bmgonly) {                

                if(folderpath.Contains("Baptistmedicalgroup"))
                {
                    cachekey = "Physician_List_Index_Data_BMGOnly_BMGWebsite";
                    expcachekey = "Physician_List_index_Expires_BMGOnly_BMGWebsite";               
                }else {
                    cachekey = "Physician_List_Index_Data_BMGOnly";
                    expcachekey = "Physician_List_index_Expires_BMGOnly";
                }
                //Sitecore.Diagnostics.Log.Info("[RENDERING CONTEXT] find a doctor has value BMG Only", this);
            }

            if(bcconly) {
                if(folderpath.Contains("Baptistcancercenter"))
                {
                    cachekey = "Physician_List_Index_Data_BCCOnly_BCCWebsite";
                    expcachekey = "Physician_List_index_Expires_BCCOnly_BCCWebsite";
                }
                else
                {
                    cachekey = "Physician_List_Index_Data_BCCOnly";
                    expcachekey = "Physician_List_index_Expires_BCCOnly";
                }
            }
            
            PhysicianListing pl = null;

            

            //return Content(PhysicianListingCacheManager.GetCache(expcachekey).HasValue().ToString());

            if(PhysicianListingCacheManager.GetCache(expcachekey).HasValue() && PhysicianListingCacheManager.GetCache(expcachekey).CacheIsExpired() == false)
            {
                //return Content("Nothing");                
                pl = PhysicianListingCacheManager.GetListingCache(cachekey);

                if(pl.Specialties.Count < 1) {
                    pl = (bmgorig) ? phyutil.getSpecialtiesAndCounts(true, folderpath, bcconly) : phyutil.getSpecialtiesAndCounts(false, folderpath, bcconly);

                    PhysicianListingCacheManager.SetCache(expcachekey, this.today.AddDays(30).ToString(format));
                    PhysicianListingCacheManager.SetListingCache(cachekey, pl);
                }
            }
            else
            {
                
                pl = (bmgorig) ? phyutil.getSpecialtiesAndCounts(true, folderpath, bcconly) : phyutil.getSpecialtiesAndCounts(false, folderpath, bcconly);
              
                PhysicianListingCacheManager.SetCache(expcachekey, this.today.AddDays(30).ToString(format));
                PhysicianListingCacheManager.SetListingCache(cachekey, pl);
            }
            

            return (ActionResult)this.PartialView("~/Areas/Feature_PhysicianDirectory/Views/Physician/PhysicianIndexBySpecialties.cshtml", pl);

            //return Content(JsonConvert.SerializeObject(pl));
        }

        public ActionResult PhysicianListingBySpecialty(string specialty, bool bmgonly = false)
        {
            bool bcconly = false;

            string rawurl = Sitecore.Context.RawUrl;
            specialty = rawurl.poplast();

            string query = (rawurl.hasQuery()) ? rawurl.popquery() : "";            

            //return Content("<h1>Specialty: " + specialty + "</h1>");
            var rc = RenderingContext.CurrentOrNull;
            string imageurl = "";
            if(rc != null) {
                var parms = rc.Rendering.Parameters;

                bmgonly = (parms["BMG Only"] == "1") ? true : false;
                bcconly = (parms["BCC Only"] == "1") ? true : false;

                if(query.Contains("bmgonly=1")) {
                    bmgonly = true;
                }

                //get bmg logo
                var imageId = XmlUtil.GetAttribute("mediaid", XmlUtil.LoadXml(parms["BMG Doctor Logo"]));
                MediaItem imageitem = Sitecore.Context.Database.GetItem(imageId);

                imageurl = MediaManager.GetMediaUrl(imageitem);
            }
           

            //Get Listing From Cache
            PhysicianUtils phyutil = new PhysicianUtils();
            phyutil.req = Request;
            phyutil.respnse = Response;

            bool bmgorig = bmgonly;

            string folderpath = "/sitecore/content/Baptist/Baptist/Baptistonline/United States/Data/Physicians";
            

            if(RenderingContext.Current.Rendering.DataSource.HasValue())
            {

                Item renderingitem = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(RenderingContext.Current.Rendering.DataSource));

                if(renderingitem.Paths.FullPath != folderpath)
                {
                    folderpath = renderingitem.Paths.FullPath;
                }

                if(folderpath.Contains("Baptistmedicalgroup"))
                {
                    bmgonly = true;
                }

                if(folderpath.Contains("Baptistcancercenter"))
                {
                    bcconly = true;
                }
            }

            string cachekey = "Physician_List_Specialty_" + specialty + "_Data";
            string expcachekey = "Physician_List_Specialty_" + specialty + "_Expires";

            if(bmgorig) {                
                cachekey = "Physician_List_Specialty_" + specialty + "_Data_BMGOnly";
                expcachekey = "Physician_List_Specialty_" + specialty + "_Expires_BMGOnly";
            }

            if(folderpath.Contains("Baptistmedicalgroup"))
            {
                cachekey = "Physician_List_Specialty_" + specialty + "_Data_BMGOnly_BMGWebsite";
                expcachekey = "Physician_List_Specialty_" + specialty + "_Expires_BMGOnly_BMGWebsite";
            }

            if(bcconly)
            {
                if(folderpath.Contains("Baptistcancercenter"))
                {
                    cachekey = "Physician_List_Specialty_" + specialty + "_Data_BCCOnly_BCCWebsite";
                    expcachekey = "Physician_List_Specialty_" + specialty + "_Expires_BCCOnly_BCCWebsite";
                }
                else
                {
                    cachekey = "Physician_List_Specialty_" + specialty + "_Data_BCCOnly";
                    expcachekey = "Physician_List_Specialty_" + specialty + "_Expires_BCCOnly";
                }
            }

            //PhysicianListingBySpecialty plbs = null;
            PhysicianListingBySpecialtyCached plbs = null;


            if(PhysicianListingBySpecialtyCacheManager.GetCache(expcachekey).HasValue() && PhysicianListingBySpecialtyCacheManager.GetCache(expcachekey).CacheIsExpired() == false)
            {
                plbs = PhysicianListingBySpecialtyCacheManager.GetListingCache(cachekey);
                          
            }
            else
            {
                plbs = (bmgorig) ? phyutil.getPhysiciansBySpecialty(specialty, true, folderpath, bcconly) : phyutil.getPhysiciansBySpecialty(specialty, false, folderpath, bcconly);

                PhysicianListingBySpecialtyCacheManager.SetCache(expcachekey, this.today.AddDays(30).ToString(format));                
                PhysicianListingBySpecialtyCacheManager.SetListingCache(cachekey, plbs);
            }


            plbs.renderingContext = RenderingContext.Current;
            plbs.bmglogourl = imageurl;

            return (ActionResult)this.PartialView("~/Areas/Feature_PhysicianDirectory/Views/Physician/PhysicianListingBySpecialty.cshtml", plbs);
        }

        public ActionResult PhysicianIndexByRegion()
        {
            return Content("hello");
        }


        public ActionResult PhysicianSearch(string q, bool precise = false, bool bmgonly = false, bool bcconly = false, bool isspecialty = false, string specialty = null, double minSimilarity = 0.8) {

            float similarity = float.Parse(minSimilarity.ToString());

            string index_name = string.Format("baptist_physician_{0}_index", Sitecore.Context.Database.Name.ToLower());

            var index = ContentSearchManager.GetIndex(index_name);

            var predicate = PredicateBuilder.True<SearchResultItem>();

            string query = HttpUtility.UrlDecode(q).StripHTML().Trim();

            string fullpathpredicate = "";
            string siteName = Sitecore.Context.Site.Name;

            if(siteName == "United States-2") //Baptist online
            {
                fullpathpredicate = "Baptistonline";
            }
            else if(siteName == "United States") //Cancer center
            {
                fullpathpredicate = "Baptistcancercenter";
            }
            else if(siteName == "United States-1") //Medical group
            {
                fullpathpredicate = "Baptistmedicalgroup";
            }

            PhysicianSearchResultSet psrs = new PhysicianSearchResultSet();
            psrs.bcconly = bcconly;
            psrs.bmgonly = bmgonly;

            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;


            if(query.HasValue())
            {

                using(var context = index.CreateSearchContext())
                {

                    List<string> terms = this.GetTermsFromString(HttpUtility.UrlDecode(q).StripHTML());

                    int cnt = 0;

                    string specialtiesfield = "specialties_sm";

                    if(bcconly) {
                        specialtiesfield = "bcc_specialties_sm";
                    }

                    foreach(string term in terms)
                    {
                        //specialties search
                        if(isspecialty && specialty.HasValue())
                        {
                            if(precise) {
                                predicate = predicate.And(f => f.Content.Like(term, similarity));
                            }
                            else {
                                predicate = predicate.Or(f => f.Content.Like(term, similarity));
                            }
                        }
                        else if(precise)
                        {
                            predicate = predicate.And(f => f.Content.Like(term, similarity));
                            predicate = predicate.Or(f => f[specialtiesfield].Contains(textInfo.ToTitleCase(term)));
                        }
                        else
                        {
                            predicate = predicate.Or(f => f.Content.Like(term, similarity));
                            predicate = predicate.Or(f => f[specialtiesfield].Contains(textInfo.ToTitleCase(term)));
                        }

                    }

                    

                    Sitecore.Diagnostics.Log.Info("[SPECIALTY IS] " + isspecialty.ToString(), this);
                    Sitecore.Diagnostics.Log.Info("[SPECIALTY VALUE] " + specialty, this);

                    //if is specialty
                    if(isspecialty && specialty.HasValue()) {
                        predicate = predicate.And(f => f[specialtiesfield].Contains(textInfo.ToTitleCase(specialty.Replace(" ", "\\ ")).Replace("\\ And\\ ", "\\ \\and\\ ") ));
                    }

                    //define site scope
                    predicate = predicate.And(f => f["_fullpath"].Contains(fullpathpredicate));

                    if(bmgonly) {
                      predicate = predicate.And(f => f["is_bmg_b"] == "1");
                    }

                    if(bcconly) {
                      predicate = predicate.And(f => f["is_bcc_b"] == "1");
                    }
                    

                    //predicate = predicate.Or(f => f.GetField("Specialties").ToString().ToLower().decodeUrl().Contains(q.decodeUrl().ToLower()));
                    //predicate = predicate.Or(f => f.Fields["primary_location_postal_code_t"] == q);
                    //predicate = predicate.Or(f => f["Primary Location City"].ToString().Contains(q));

                    var searchQuery = context.GetQueryable<SearchResultItem>()
                    .Where(predicate);

                    var resultItems = searchQuery.ToList();
                    var totalCount = searchQuery.Count();


                    //return (ActionResult)PartialView("")

                    //return Content(string.Format("<h1>Terms:</h1><p>{0}</p><h1>Count: {1}  </h1><h1>Result Items</h1><br /><p>{2}</p>", JsonConvert.SerializeObject(terms), totalCount, JsonConvert.SerializeObject(resultItems)));                    

                    //favor the bmg people
                    List<SearchResultItem> sortedbmg = new List<SearchResultItem>();
                    List<SearchResultItem> nonbmg = new List<SearchResultItem>();
                    
                    foreach( SearchResultItem sri in resultItems ) {
                        if(bool.Parse(sri["is_bmg_b"])) {
                            sortedbmg.Add(sri);
                        } else {
                            nonbmg.Add(sri);
                        }
                    }

                    sortedbmg.AddRange(nonbmg);

                    psrs.results = sortedbmg;
                    //psrs.results = resultItems;                                        

                }
            }else {

                psrs.results = new List<SearchResultItem>();
                
            }
            return (ActionResult)this.PartialView("~/Areas/Feature_PhysicianDirectory/Views/Physician/PhysicianSearchResults.cshtml", psrs);
        }


        private List<string> GetTermsFromString(string query) {

            List<string> terms = new List<string>();

            query = Regex.Replace(query, "[^a-zA-Z0-9 ]+", " ");            

            foreach(string term in query.Split(' ')) {
                terms.Add(term);
            }

            return terms;
        }

        public ActionResult SchedulingPhysicians() {
            
            var rc = RenderingContext.CurrentOrNull;
            string imageurl = "";
            bool bmgonly = false;
            
            if(rc != null) {
                var parms = rc.Rendering.Parameters;

                bmgonly = (parms["BMG Only"] == "1") ? true : false;                

                //get bmg logo
                var imageId = XmlUtil.GetAttribute("mediaid", XmlUtil.LoadXml(parms["BMG Doctor Logo"]));
                MediaItem imageitem = Sitecore.Context.Database.GetItem(imageId);
                imageurl = MediaManager.GetMediaUrl(imageitem);
            }                

            PhysicianUtils pu = new PhysicianUtils();
            
            //            SchedulePhysicianAppointment model = new SchedulePhysicianAppointment();
            SchedulePhysicianAppointmentDistance model = new SchedulePhysicianAppointmentDistance();

            LocationUtils lu = new LocationUtils();
            lu.req = Request;
            lu.respnse = Response;

            UserGeo userloc = lu.getUserGeo();

            List<PhysicianWithDistance> schedPhys = pu.getSchedulingPhysiciansFromPoint( (double) new decimal(userloc.latitude), (double) new decimal(userloc.longitude) ).OrderBy(x => x.distance).ToList<PhysicianWithDistance>();

            model.doctors = schedPhys;
            model.bmglogourl = imageurl;

            return (ActionResult)this.PartialView("~/Areas/Feature_PhysicianDirectory/Views/Physician/PhysicianScheduling.cshtml", model);
            
        }


        public ActionResult SearchPhysicians( string q, int radius = 100, bool listed_providers = false, string featured_providers = "True", bool paging = false, int rows = 12, int start = 0, string specialties = "", string scheduling_specialties = "", bool sitescopeonly = true, string[] prams = null )
        { 
            var rc = RenderingContext.CurrentOrNull;
            q = q.Trim();

            string solrquery = "q=primary_location_latitude_t:(* TO *)&q=primary_location_longitude_t:(* TO *)&q=";
            string filterquery = "";

            if(sitescopeonly)
            {
                string searchscope = PhysicianUtils.searchScopeFromContext(Sitecore.Context.Site.RootPath);
                filterquery += string.Format("&fq=_fullpath:(*{0}*)", searchscope);
            }

            if(listed_providers && featured_providers.ToLower() != "only")
            {
                filterquery += string.Format("&fq=listed_provider_b:true");
            }
            
            if( featured_providers.ToLower() == "false") {
                filterquery += string.Format("&fq=featured_provider_b:false");
            }
            else if (featured_providers.ToLower()== "only")
            {
                filterquery += string.Format("&fq=featured_provider_b:true");
            }
            
            string fieldlist = "&fl=*,score";
            string sortquery = "&sort=featured_provider_b desc,geodist()+asc";
            string resformat = "&wt=json";
            string pagination = "";

            LocationUtils lu = new LocationUtils();
            PhysicianUtils pu = new PhysicianUtils();
            pu.req = Request;
            pu.respnse = Response;
            lu.req = Request;
            lu.respnse = Response;

            UserGeo userloc = lu.getUserGeo();

            if(specialties != "") {
                filterquery += mvfieldquery(specialties, "specialties_sm");
            }
            if(scheduling_specialties != "") {
                filterquery += mvfieldquery(scheduling_specialties, "scheduling_specialties_sm");
            }

            if( paging ) {
                pagination = string.Format("&rows={0}&start={1}", rows, start);
            }
            
            if( specialties != "") {
                string specialparams = "";
                string[] listspecialties = specialties.Split('+');

                specialparams += @"&fq=scheduling_specialties_sm:(";
                for(int i = 0; i < listspecialties.Length; i++) {
                    if(i == 0 ) {
                        specialparams += @"""" + listspecialties[i] + @"""";
                    }else {
                        specialparams += @" OR """ + listspecialties[i] + @"""";
                    }                    
                }
                specialparams += ")";
                
            }

            if( Regex.IsMatch(q, "^[0-9]{5}(?:-[0-9]{4})?$" ) || Geo.isCityStateMatch(q) ) {
                GMapResponse geores = Geo.geocodeSearchCommon(q);

                var geolocation = geores.results.FirstOrDefault().geometry.location;

                solrquery += string.Format("{{!func}}geodist()&pt={0},{1}&d={2}&sfield=primary_coordinates_rpt", geolocation.lat, geolocation.lng, radius);
                //solrquery += string.Format("{{!func}}geodist()&pt={0},{1}&d={2}&sfield=primary_coordinates_rpt", geolocation.lat, geolocation.lng, radius);
                //solrquery += "*:*";
                fieldlist += ",_dist_:geodist()";
                //distance from user
                //string userdist = string.Format("dist(1,{0},{1},{2},{3})", userloc.latitude, userloc.longitude, geolocation.lat, geolocation.lng);
                //string userpt = string.Format("&fq={{!geofilt}}&pt={0},{1}&sfield=primary_coordiantes_rpt&d={2}", geolocation.lat, geolocation.lng, radius);
                //filterquery += userpt;
                //fieldlist += ",user_dist:geodist()";                
            }else {

                if(q.ToLower() != "false")
                {
                    solrquery += "_content:(" + q + "~)";
                }else {
                    solrquery += string.Format("{{!func}}geodist()&pt={0},{1}&d={2}&sfield=primary_coordinates_rpt", userloc.latitude, userloc.longitude, radius);
                }

                string userpt = string.Format("&fq={{!geofilt}}&pt={0},{1}&sfield=primary_coordinates_rpt&d={2}", userloc.latitude, userloc.longitude, radius);
                filterquery += userpt;
                fieldlist += ",_dist_:geodist()";                
            }            

            solrquery += filterquery + fieldlist + sortquery + resformat + pagination;

            dynamic results = pu.solrPhysicianQuery(solrquery);
            int totalresults = 0;
           // return Content(JsonConvert.SerializeObject(results) + "<hr/>" + JsonConvert.SerializeObject(userloc));
            
            if(results != null)
            {                
                totalresults = int.Parse((string)results.response.numFound);
                for(int i = 0; i < results.response.docs.Count; i++)
                {
                    var item = results.response.docs[i];
                    try
                    {                        
                        results.response.docs[i].user_distance = Geo.getUserDistance(userloc.latitude.ToString(), userloc.longitude.ToString(), item.primary_location_latitude_t[0].ToString(), item.primary_location_longitude_t[0].ToString());                        
                    }
                    catch(Exception ex)
                    {                        
                        
                        Sitecore.Diagnostics.Log.Error(ex.Message, this);
                        continue;
                    }
                }                

                List<dynamic> docs = ((JArray)results.response.docs).ToList<dynamic>();
                //results = docs.OrderBy(p => p.user_distance);
                results = docs;
            }
            else
            {
                results = false;
            }
           
            //            SchedulePhysicianAppointment model = new SchedulePhysicianAppointment();
            SchedulePhysicianAppointmentDistanceQuery model = new SchedulePhysicianAppointmentDistanceQuery();
            
            model.doctors = PhysicianUtils.physResultsToModel(results);
            model.bmglogourl = getBMGLogoUrl();

            int next_start = start + rows;
            model.parameters = new Dictionary<string,string> {
                { "q", q } ,
                { "radius", radius.ToString() },                
                { "listed_providers", listed_providers.ToString() },
                { "featured_providers", featured_providers.ToString() },
                { "paging", paging.ToString() },
                { "rows", rows.ToString() },
                { "start", start.ToString() },
                { "next_start", next_start.ToString() },
                { "specialties", specialties },
                { "scheduling_specialties", scheduling_specialties },
                { "sitescopeonly", sitescopeonly.ToString() },
                { "total_results",  totalresults.ToString() }
            };

            //return (ActionResult)Content(solrquery + "<hr/>" + Newtonsoft.Json.JsonConvert.SerializeObject(results) );
            return this.PartialView("~/Areas/Feature_PhysicianDirectory/Views/Physician/PhysicianSchedulingSearchResults.cshtml", model);

        }

        public string getBMGLogoUrl()
        {
            string imageurl = "";
            Sitecore.Data.Database db = Sitecore.Context.Database;

            TemplateItem template = db.GetTemplate(ID.Parse("{80825E17-F464-4189-8BB5-84EF8B11E8AD}"));

            Item templateSV = template.StandardValues;
            //get bmg logo

            var imageId = XmlUtil.GetAttribute("mediaid", XmlUtil.LoadXml(templateSV.Fields["BMG Doctor Logo"].Value));
            MediaItem imageitem = Sitecore.Context.Database.GetItem(imageId);
            imageurl = MediaManager.GetMediaUrl(imageitem);
            return imageurl;
        }       
   
        public string mvfieldquery(string mvValue, string fieldtargetname = "scheduling_specilaties_sm")
        {
            string returnquery = "";
            if(mvValue != null && mvValue.Trim() != "")
            {
                returnquery = "";
                string[] listitems = mvValue.Split('+');

                returnquery += string.Format(@"&fq={0}:(", fieldtargetname);
                for(int i = 0; i < listitems.Length; i++)
                {
                    if(i == 0)
                    {
                        returnquery += @"""" + listitems[i] + @"""";
                    }
                    else
                    {
                        returnquery += @" OR """ + listitems[i] + @"""";
                    }
                }
                returnquery += ")";
            }
            return returnquery;
            
        }

        public ActionResult ProviderSearch( string q, int radius = 25, bool listed_providers = false, string featured_providers = "True", bool paging = false, int rows = 12, int start = 0, string specialties = "", string scheduling_specialties = "", bool sitescopeonly = true, string[] prams = null, bool json = false, bool rowmarkup = false, bool bmgonly = false, bool bcconly = false)
        {
            var rc = RenderingContext.CurrentOrNull;
            q = q.Trim();

            string solrquery = "q=";
            string filterquery = "";

            if(sitescopeonly)
            {
                string searchscope = PhysicianUtils.searchScopeFromContext(Sitecore.Context.Site.RootPath);
                filterquery += string.Format("&fq=_fullpath:(*{0}*)", searchscope);
            }

            if(listed_providers && featured_providers.ToLower() != "only")
            {
                filterquery += string.Format("&fq=listed_provider_b:true");
            }
            
            if( featured_providers.ToLower() == "false") {
                filterquery += string.Format("&fq=featured_provider_b:false");
            }
            else if (featured_providers.ToLower()== "only")
            {
                filterquery += string.Format("&fq=featured_provider_b:true");
            }
            
            string fieldlist = "&fl=*,score";
            string sortquery = "&sort=geodist()+asc,score+asc";
            string resformat = "&wt=json";
            string pagination = "";

            LocationUtils lu = new LocationUtils();
            PhysicianUtils pu = new PhysicianUtils();
            pu.req = Request;
            pu.respnse = Response;
            lu.req = Request;
            lu.respnse = Response;

            UserGeo userloc = lu.getUserGeo();

            if(specialties != "") {
                filterquery += mvfieldquery(specialties, "specialties_sm");
            }
            if(scheduling_specialties != "") {
                filterquery += mvfieldquery(scheduling_specialties, "scheduling_specialties_sm");
            }

            if( paging ) {
                pagination = string.Format("&rows={0}&start={1}", rows, start);
            }
            
            if( specialties != "") {
                string specialparams = "";
                string[] listspecialties = specialties.Split('+');

                specialparams += @"&fq=scheduling_specialties_sm:(";
                for(int i = 0; i < listspecialties.Length; i++) {
                    if(i == 0 ) {
                        specialparams += @"""" + listspecialties[i] + @"""";
                    }else {
                        specialparams += @" OR """ + listspecialties[i] + @"""";
                    }                    
                }
                specialparams += ")";
                
            }

            if( Regex.IsMatch(q, "^[0-9]{5}(?:-[0-9]{4})?$" ) || Geo.isCityStateMatch(q) ) {
                GMapResponse geores = Geo.geocodeSearchCommon(q);

                var geolocation = geores.results.FirstOrDefault().geometry.location;

                solrquery += string.Format("{{!func}}geodist()&pt={0},{1}&d={2}&sfield=primary_coordinates_rpt", geolocation.lat, geolocation.lng, radius);
                //solrquery += string.Format("{{!func}}geodist()&pt={0},{1}&d={2}&sfield=primary_coordinates_rpt", geolocation.lat, geolocation.lng, radius);
                //solrquery += "*:*";
                fieldlist += ",_dist_:geodist()";
                //distance from user
                //string userdist = string.Format("dist(1,{0},{1},{2},{3})", userloc.latitude, userloc.longitude, geolocation.lat, geolocation.lng);
                //string userpt = string.Format("&fq={{!geofilt}}&pt={0},{1}&sfield=primary_coordiantes_rpt&d={2}", geolocation.lat, geolocation.lng, radius);
                //filterquery += userpt;
                //fieldlist += ",user_dist:geodist()";            
                    
            }else {

                if(q.ToLower() != "false")
                {
                    solrquery += "_content:(" + q + "~)";
                }else {
                    solrquery += string.Format("{{!func}}geodist()&pt={0},{1}&d={2}&sfield=primary_coordinates_rpt", userloc.latitude, userloc.longitude, radius);
                }

                string userpt = string.Format("&fq={{!geofilt}}&pt={0},{1}&sfield=primary_coordinates_rpt&d={2}", userloc.latitude, userloc.longitude, radius);
                filterquery += userpt;
                fieldlist += ",_dist_:geodist()";                
            }            

            solrquery += filterquery + fieldlist + sortquery + resformat + pagination;

            dynamic results = pu.solrPhysicianQuery(solrquery);
            int totalresults = 0;
           // return Content(JsonConvert.SerializeObject(results) + "<hr/>" + JsonConvert.SerializeObject(userloc));
            
            if(results != null)
            {                
                totalresults = int.Parse((string)results.response.numFound);
                for(int i = 0; i < results.response.docs.Count; i++)
                {
                    var item = results.response.docs[i];
                    try
                    {                        
                        results.response.docs[i].user_distance = Geo.getUserDistance(userloc.latitude.ToString(), userloc.longitude.ToString(), item.primary_location_latitude_t[0].ToString(), item.primary_location_longitude_t[0].ToString());                        
                    }
                    catch(Exception ex)
                    {                        
                        
                        Sitecore.Diagnostics.Log.Error(ex.Message, this);
                        continue;
                    }
                }                

                List<dynamic> docs = ((JArray)results.response.docs).ToList<dynamic>();
                //results = docs.OrderBy(p => p.user_distance);
                results = docs;
            }
            else
            {
                results = false;
            }
           
            //            SchedulePhysicianAppointment model = new SchedulePhysicianAppointment();
            SchedulePhysicianAppointmentDistanceQuery model = new SchedulePhysicianAppointmentDistanceQuery();
            
            model.doctors = PhysicianUtils.physResultsToModel(results);
            model.bmglogourl = getBMGLogoUrl();
            model.bmgonly = bmgonly;
            model.bcconly = bcconly;

            int next_start = start + rows;
            model.parameters = new Dictionary<string,string> {
                { "q", q } ,
                { "radius", radius.ToString() },                
                { "listed_providers", listed_providers.ToString() },
                { "featured_providers", featured_providers.ToString() },
                { "paging", paging.ToString() },
                { "rows", rows.ToString() },
                { "start", start.ToString() },
                { "next_start", next_start.ToString() },
                { "specialties", specialties },
                { "scheduling_specialties", scheduling_specialties },
                { "sitescopeonly", sitescopeonly.ToString() },
                { "total_results",  totalresults.ToString() },
                { "query_url", solrquery }
            };

            //return (ActionResult)Content(solrquery + "<hr/>" + Newtonsoft.Json.JsonConvert.SerializeObject(results) );
            //return this.PartialView("~/Areas/Feature_PhysicianDirectory/Views/Physician/PhysicianSchedulingSearchResults.cshtml", model);
            if(json)
            {
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if(rowmarkup)
                {
                    return this.PartialView("~/Areas/Feature_PhysicianDirectory/Views/Physician/ProviderSearchResultRows.cshtml", model);
                }
                else
                {
                    return this.PartialView("~/Areas/Feature_PhysicianDirectory/Views/Physician/ProviderSearchResults.cshtml", model);
                }
            }
        }

    }
}