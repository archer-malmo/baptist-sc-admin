﻿using Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom;
using Sitecore.Data.Items;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Repositories
{
    public class ServicesProvidedContentRepository : ModelRepository, IServicesProvidedContentRepositoryItem, IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        public Item ServicesProvidedContentItem { get; set; }

        public override IRenderingModelBase GetModel()
        {
            ServicesProvidedContentRenderingModel spcrm = new ServicesProvidedContentRenderingModel();

            spcrm.ServicesTitle = this.GetTitle();
            spcrm.ContentBefore = this.GetContentBefore();
            spcrm.ContentAfter = this.GetContentAfter();
            spcrm.ListingType = this.GetListingType();
            return spcrm;
        }

        protected virtual string GetTitle()
        {
            if(this.ServicesProvidedContentItem != null)
                return this.ServicesProvidedContentItem[ServicesProvidedContentTemplate.ServicesProvidedContent.Fields.ServicesTitle] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[ServicesProvidedContentTemplate.ServicesProvidedContent.Fields.ServicesTitle] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetContentBefore()
        {
            if(this.ServicesProvidedContentItem != null)
                return this.ServicesProvidedContentItem[ServicesProvidedContentTemplate.ServicesProvidedContent.Fields.ContentBefore] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[ServicesProvidedContentTemplate.ServicesProvidedContent.Fields.ContentBefore] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetContentAfter()
        {
            if(this.ServicesProvidedContentItem != null)
                return this.ServicesProvidedContentItem[ServicesProvidedContentTemplate.ServicesProvidedContent.Fields.ContentAfter] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[ServicesProvidedContentTemplate.ServicesProvidedContent.Fields.ContentAfter] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetListingType()
        {
            if(this.ServicesProvidedContentItem != null)
                return this.ServicesProvidedContentItem[ServicesProvidedContentTemplate.ServicesProvidedContent.Fields.ListingType] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[ServicesProvidedContentTemplate.ServicesProvidedContent.Fields.ListingType] ?? string.Empty;
            return string.Empty;
        }
    }
}