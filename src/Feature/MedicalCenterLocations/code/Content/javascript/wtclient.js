function getlocation()
{
  var facility = $('input#closest_er').val();
  return facility;
}

function addresult(data)
{
  var currentdata = JSON.parse(data);
  $("#CV_ED_Wait").html(currentdata.CV_ED_Wait + "min");
  $("#CV_ED_WaitToTriage").html(currentdata.CV_ED_Wait + "min");
}

function errresult(data) {
  $("#CV_ED_Wait").html(" currently unavailable.");
  $("#CV_ED_WaitToTriage").html(" currently unavailable.");
}

function getwaittimes() {
  var wturl = 'https://sites.bmhcc.org/api/waittimes/waittimes.php?fac=' + getlocation();
  $.ajax({
    url: wturl,
    cache: false,
    method: 'GET',
    success: function (data) {
      addresult(data);
    },
    error: function (data) {
      errresult(data);
    }
  });
}