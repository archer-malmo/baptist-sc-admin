﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sitecore.Data.Items;
using Sitecore.XA.Foundation.RenderingVariants.Repositories;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Utils;
using Sitecore.Data.Fields;
using Sitecore.Data;
using Sitecore.XA.Foundation.Mvc.Controllers;

namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Controllers
{
    public class CancercenterController : StandardController
    {
        // GET: Feature_MedicalCenterLocations/Cancercenter
        public JsonResult locations() {

            Database db = Sitecore.Context.Database;

            List<Item> cancercenters = db.SelectItems("/sitecore/content/Baptist/Baptist/Baptistcancercenter/United States/Data/Locations//*[@@templatename = 'Location Item']").ToList<Item>();

            List<cancercenteritem> locationlist = new List<cancercenteritem>();

            foreach(Item cc in cancercenters) {

                locationlist.Add(new cancercenteritem { cancercenterid = cc.Fields["ClientID"].ToString(), locationname = cc.Fields["Location Name"].ToString(), sitecoreid = cc.ID.Guid.ToString(), address1 = cc.Fields["Address Line 1"].ToString(), address2 = cc.Fields["Address Line 2"].ToString(), city = cc.Fields["City"].ToString(), state = cc.Fields["State"].ToString(), });
            }
            
            return Json(locationlist, JsonRequestBehavior.AllowGet);
        }


    }

    public class cancercenteritem {

        public string cancercenterid { get; set; }
        public string locationname { get; set; }
        public string sitecoreid { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string postalcode { get; set; }
    }
}