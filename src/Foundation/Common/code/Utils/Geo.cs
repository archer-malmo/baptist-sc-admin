﻿using Sitecore.Baptist.Foundation.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using System.Device.Location;
//using Google.Maps.Geocoding;

namespace Sitecore.Baptist.Foundation.Common.Utils
{
    public class Geo
    {
        protected string endpoint;
        protected string login;
        protected string password;

        public Geo() {
            this.endpoint = "https://geo2.archermalmo.agency/api/v1/zip/";
            this.login = "amgeo2";
            this.password = "8239sjkfdhf983j";
        }

        public Models.GeocodeResponse geocodeSearch( string query ) {

            String credentials = System.Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(this.login + ":" + this.password));
            string urquery = WebUtility.UrlEncode(query);
            HttpWebRequest hrquest = (HttpWebRequest)WebRequest.Create(this.endpoint + urquery);

            hrquest.Headers.Add("Authorization", "Basic " + credentials);

            string georesult;

            using(HttpWebResponse response = hrquest.GetResponse() as HttpWebResponse)
            {
                StreamReader readStream = new StreamReader(response.GetResponseStream());
                georesult = readStream.ReadToEnd();

                //Sitecore.Diagnostics.Log.Info("[IP INFO]" + geoipinfo, this);
                var obj = JsonConvert.DeserializeObject<Models.GeocodeResponse>(georesult);

                return obj;
            }
        }

        /*public static Google.Maps.Geocoding.GeocodeResponse geocodeSearchCommon( string query ) {            

            var request = new GeocodingRequest();
            request.Address = query;
            request.Sensor = false;

            GeocodingService gecs = new GeocodingService();

            var response = gecs.GetResponse(request);

            return response;
            */
            /*GeoResult georesult = new GeoResult();
            georesult.query = query;
            georesult.zip = 

            return null;*/
        //}

        public static GMapResponse geocodeSearchCommon(string query) {

            var cachekey = "GMAP_Result_" + query.ToUrlSlug().Replace('-', '_');

            GMapResponse resp;

            if(GeocodeCacheManager.GetCache(cachekey).HasValue()) {

                resp = GeocodeCacheManager.GetGeoResultCache(cachekey);

                //bad things happened
                if(resp.status == "OVER_QUERY_LIMIT") {

                    //var url = String.Format("http://maps.google.com/maps/api/geocode/json?address={0}&sensor=false&apikey=AIzaSyDEnRKFd3wvn2deWnClojgqd2fcePl-Ujk", HttpContext.Current.Server.UrlEncode(query));
                    var url = String.Format("https://maps.googleapis.com/maps/api/geocode/json?address={0}&sensor=false&key=AIzaSyCDKVQl3xV-X04v-ntn7wJazpJbFQ56pEU", HttpContext.Current.Server.UrlEncode(query));
                    var result = new System.Net.WebClient().DownloadString(url);

                    resp = JsonConvert.DeserializeObject<GMapResponse>(result);
                    resp.cachekey = cachekey;

                    GeocodeCacheManager.SetGeoResultCache(cachekey, resp);
                }
            
            } else {

                //var url = String.Format("http://maps.google.com/maps/api/geocode/json?address={0}&sensor=false&apikey=AIzaSyDEnRKFd3wvn2deWnClojgqd2fcePl-Ujk", HttpContext.Current.Server.UrlEncode(query));
                var url = String.Format("https://maps.googleapis.com/maps/api/geocode/json?address={0}&sensor=false&key=AIzaSyCDKVQl3xV-X04v-ntn7wJazpJbFQ56pEU", HttpContext.Current.Server.UrlEncode(query));
                var result = new System.Net.WebClient().DownloadString(url);

                resp = JsonConvert.DeserializeObject<GMapResponse>(result);
                resp.cachekey = cachekey;

                GeocodeCacheManager.SetGeoResultCache(cachekey, resp);

            }
                       
            return resp;
        }

        public static bool isCityStateMatch( string query ) {
            
            if( Regex.IsMatch( query, @"^[a-zA-Z',.\s-]{1,30},\s?(Alabama|Alaska|American\sSamoa|Arizona|Arkansas|California|Colorado|Connecticut|Delaware|District\sof\sColumbia|Florida|Georgia|Guam|Hawaii|Idaho|Illinois|Indiana|Iowa|Kansas|Kentucky|Louisiana|Maine|Maryland|Massachusetts|Michigan|Minnesota|Minor\sOutlying\sIslands|Mississippi|Missouri|Montana|Nebraska|Nevada|New\sHampshire|New\sJersey|New\sMexico|New\sYork|North\sCarolina|North\sDakota|Northern\sMariana\sIslands|Ohio|Oklahoma|Oregon|Pennsylvania|Puerto\sRico|Rhode\sIsland|South\sCarolina|South\sDakota|Tennessee|Texas|U\.S\.\sVirgin\sIslands|Utah|Vermont|Virginia|Washington|West\sVirginia|Wisconsin|Wyoming|AK|AL|AR|AS|AZ|CA|CO|CT|DC|DE|FL|GA|GU|HI|IA|ID|IL|IN|KS|KY|LA|MA|MD|ME|MI|MN|MO|MP|MS|MT|NC|ND|NE|NH|NJ|NM|NV|NY|OH|OK|OR|PA|PR|RI|SC|SD|TN|TX|UM|UT|VA|VI|VT|WA|WI|WV|WY)+$" ) ) {
                return true;
            }else {
                return false;
            }

        }

        public static double getUserDistance( string userlat, string userlon, string physlat, string physlon )
        {
            var userCoords = new GeoCoordinate( double.Parse(userlat), double.Parse(userlon) );
            var physCoords = new GeoCoordinate( double.Parse(physlat), double.Parse(physlon) );

            return userCoords.GetDistanceTo(physCoords) / 1609.344;            

        }

    }  


    public static class GeocodeCacheManager
    {
        private static readonly BaptistCustomCache Cache;

        static GeocodeCacheManager()
        {
            Cache = new BaptistCustomCache("Baptist_Geocode_Cache", StringUtil.ParseSizeString("20MB"));
        }

        public static string GetCache( string key )
        {
            return Cache.GetString(key);
        }

        public static void SetCache( string key, string value )
        {
            Cache.SetString(key, value);
        }

        public static GMapResponse GetGeoResultCache( string key )
        {
            string serial = Cache.GetString(key);
            //JavaScriptSerializer jserial = new JavaScriptSerializer();
            //jserial.RecursionLimit = 5000;

            return JsonConvert.DeserializeObject<GMapResponse>(serial);
            //return jserial.Deserialize<PhysicianListing>(serial);
        }

        public static void SetGeoResultCache( string key, GMapResponse value )
        {
            string storagedvalue = JsonConvert.SerializeObject(value);
            //JavaScriptSerializer jserial = new JavaScriptSerializer();
            //jserial.RecursionLimit = 5000;
            //string storagedvalue = jserial.Serialize(value);            
            Cache.SetString(key, storagedvalue);
        }
    }

  
}