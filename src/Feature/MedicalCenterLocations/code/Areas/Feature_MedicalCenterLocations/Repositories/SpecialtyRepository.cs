﻿using Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.XA.Foundation.Multisite.LinkManagers;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Repositories
{
    public class SpecialtyRepository : ModelRepository, ISpecialtyRepository, IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        public Item SpecialtyItem { get; set; }

        public override IRenderingModelBase GetModel()
        {            
            Models.SpecialtyRenderingModel srp = new Models.SpecialtyRenderingModel();
            this.FillBaseProperties((object)srp);
            srp.Title = this.GetTitle();
            srp.ServicePage = this.GetServicePage();
            srp.Image = this.GetImage();
            srp.Locations = this.GetLocations();
            
            return srp;
        }

        protected virtual string GetTitle()
        {
            if(this.SpecialtyItem != null)
                return this.SpecialtyItem[SpecialtyTemplate.Specialty.Fields.Title] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[SpecialtyTemplate.Specialty.Fields.Title] ?? string.Empty;
            return string.Empty;
        }

        protected virtual LinkField GetServicePage()
        {
            if(this.SpecialtyItem != null)
                return (LinkField)this.SpecialtyItem.Fields[SpecialtyTemplate.Specialty.Fields.ServicePage];
            else if(this.Rendering.DataSourceItem != null)
                return (LinkField)this.Rendering.DataSourceItem.Fields[SpecialtyTemplate.Specialty.Fields.ServicePage];
            return null;
        }

        protected virtual ImageField GetImage() {
            if(this.SpecialtyItem != null)
                return (ImageField)this.SpecialtyItem.Fields[SpecialtyTemplate.Specialty.Fields.Image];
            else if(this.Rendering.DataSourceItem != null)
                return (ImageField)this.Rendering.DataSourceItem.Fields[SpecialtyTemplate.Specialty.Fields.Image];
            return null;            
        }

        protected virtual List<Guid> GetLocations() {

            //Sitecore.Diagnostics.Log.Info("The type " + this.SpecialtyItem.Fields[SpecialtyTemplate.Specialty.Fields.Locations], this);            

            if(this.SpecialtyItem != null)
                return ParseToGuids(this.SpecialtyItem.Fields[SpecialtyTemplate.Specialty.Fields.Locations].ToString());
            else if(this.Rendering.DataSourceItem != null)
                return ParseToGuids(this.Rendering.DataSourceItem.Fields[SpecialtyTemplate.Specialty.Fields.Locations].ToString());
            return null;
        }

       public List<Guid> ParseToGuids(string value, char delimiter = '|') {
            List<Guid> guids = new List<Guid>();

            foreach(string guid in value.Split(delimiter)) {
                guids.Add(new Guid(guid));
            }

            return guids;
        }
    }
}