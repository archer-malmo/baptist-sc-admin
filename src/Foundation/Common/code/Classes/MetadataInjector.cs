﻿using Sitecore.Mvc.Pipelines.Response.GetXmlBasedLayoutDefinition;
using System.Linq;
using System.Xml.Linq;

public class MetadataInjector : GetXmlBasedLayoutDefinitionProcessor
{
    public static readonly string PlaceholderName = "NameOfThePlaceholder";

    private string UniqueBindingID = "{DC9B51A9-5897-4F2C-8410-E0B228FC54DE}";
    private string DeviceID = "{FE5D7FDF-89C0-4D99-9AA3-B5FBD009C9F3}";
    private string MetadataRenderingID = "{C8EB6246-CC62-47CB-9843-17EBAEF7F319}";

    public override void Process( GetXmlBasedLayoutDefinitionArgs args )
    {
        XElement device = args.Result
            .Elements("d")
            .Where(e => e.Attribute("id").Value == DeviceID)
            .FirstOrDefault();

        if(device != null)
        {
            device.Add(
                new XElement("r",
                    new XAttribute("uid", UniqueBindingID),
                    new XAttribute("id", MetadataRenderingID),
                    new XAttribute("ph", PlaceholderName)
                )
            );
        }
    }
}