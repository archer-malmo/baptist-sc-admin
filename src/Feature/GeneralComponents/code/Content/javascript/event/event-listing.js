﻿var eventlisting = eventlisting || {};

var topicoptions;

eventlisting = {

  setresults: function (selector, styletag, datatag) {

    console.log("something");

    if (selector.val()) {

      var selval = selector.val();


      console.log(styletag);

      styletag.innerHTML = ".searchable:not([" + datatag + "^=\"" + selval + "\"])"
        + ":not([" + datatag + "*=\" " + selval + " \"])"
        + ":not([" + datatag + "$=\" " + selval + "\"])"
        + ":not([" + datatag + "=\"" + selval + "\"]) { display: none; }";
    } else {
      styletag.innerHTML = "";
    }
    eventlisting.hideshowempty('ul#event-listing li', 'h1.empty-results');
  },

  hideshowempty: function (selector, hideshowselector) {

    var vissel = selector + ":visible";

    if ($(selector).length == 0) {
      $(hideshowselector).css('display', 'block');
    } else if ($(vissel).length == 0) {
      $(hideshowselector).css('display', 'block');
    } else {
      $(hideshowselector).css('display', 'none');
    }
  },

  updatetopicoptions: function (selector) {
    if (topicoptions != undefined) {
      $('select[name="filter-event-topics"]').empty().append(topicoptions);
      $('select[name="filter-event-topics"] option:first-child()').attr('selected', 'selected');
    }

    var vissel = selector + ":visible";

    var showthese = [];
    $(vissel).each(function () {
      var topic = $(this).data('filter-event-topic');
      if (topic !== undefined && (showthese.length == 0 || showthese.indexOf(topic) == -1)) {
        showthese.push(topic);
      }
    });

    topicoptions = $("select[name='filter-event-topics'] option");

    var filteredoptions = "";
    $(showthese).each(function (i, v) {
      filteredoptions += ":not([value='" + v + "'])";
    });
    var removed = $("select[name='filter-event-topics'] option:not([value=''])" + filteredoptions).detach();
  },

  eventfilterinit: function () {
    var topicFilter = document.getElementById('topic_filter');
    var locationFilter = document.getElementById('location_filter');
    var categoryFilter = document.getElementById('category_filter');

    $(function () {
      var topicselect = $('select[name="filter-event-topics"]');
      var locationselect = $('select[name="filter-location"]');
      var categoryselect = $('select[name="filter-category"]');

      if ($('#eventlistinit').length > 0) {

        //console.log('eventfilterloaded');
        topicselect.change(function () {
          //console.log('topic changed');
          eventlisting.setresults($(this), topicFilter, 'data-filter-event-topic');

          if ($(this).find(':selected').data('topic-hash-selector') != "") {
            window.location.hash = $(this).find(':selected').data('topic-hash-selector');
          } else {
            window.location.hash = "";
          }

        });

        locationselect.change(function () {
          //console.log('location changed');
          eventlisting.setresults($(this), locationFilter, 'data-filter-event-location');

          if ($(this).find(':selected').data('location-hash-selector') != "") {
            window.location.hash = $(this).find(':selected').data('location-hash-selector');
          } else {
            window.location.hash = "";
          }

        });

        categoryselect.change(function () {
          eventlisting.setresults($(this), categoryFilter, 'data-filter-event-category');

          if ($(this).find(':selected').data('category-hash-selector') != "") {
            window.location.hash = $(this).find(':selected').data('category-hash-selector');
          } else {
            window.location.hash = "";
          }

          topicFilter.innerHTML = "";

          eventlisting.updatetopicoptions('ul#event-listing li');

        });
      }

    });
  },

}

$(function () {
  eventlisting.eventfilterinit();

  if (($('#topic_filter').length > 0 || $('#location_filter').length > 0) && location.hash) {
    var hash = location.hash.replace('#', '');
    //alert(hash);
    var selectbox, filterstyle, dataselector, selectdata;
    if (hash.startsWith('loc-')) {

      selectbox = $('select[name="filter-location"]');
      filterstyle = document.getElementById('location_filter');
      selectdata = '[data-location-hash-selector="' + hash + '"]';
      dataselector = 'data-filter-event-location';

    } else if (hash.startsWith('cat-')) {

      selectbox = $('select[name="filter-category"]');
      filterstyle = document.getElementById('category_filter');
      selectdata = '[data-category-hash-selector="' + hash + '"]';
      dataselector = 'data-filter-event-category'

    } else {

      selectbox = $('select[name="filter-event-topics"]');
      filterstyle = document.getElementById('topic_filter');
      selectdata = '[data-topic-hash-selector="' + hash + '"]';
      dataselector = 'data-filter-event-topic';

    }

    if (selectbox != undefined && filterstyle != undefined && dataselector != undefined && selectdata != undefined) {
      //console.log("found");

      selectbox.find(selectdata).attr('selected', 'selected');
      eventlisting.setresults(selectbox, filterstyle, dataselector);

      if (hash.startsWith('cat-')) {
        eventlisting.updatetopicoptions('ul#event-listing li');
      }

    }

  }
});