﻿using Newtonsoft.Json;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Models;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Repositories;
using Sitecore.Baptist.Foundation.Common.Models;
using Sitecore.Baptist.Foundation.Common.Utils;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq.Utilities;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.Data.Items;
using Sitecore.XA.Foundation.Mvc.Controllers;
using Sitecore.XA.Foundation.Search;
using Sitecore.XA.Foundation.Search.Spatial;
using Sitecore.XA.Feature.Search;
using System;
using System.Collections.Generic;
using System.Data.Spatial;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;

using static Sitecore.Configuration.State;
using Sitecore.XA.Foundation.Search.Models;
using Sitecore.Data;
using Newtonsoft.Json.Linq;

namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Utils
{
    public class LocationUtils : StandardController
    {

        public HttpRequestBase req { get; set; }
        public HttpResponseBase respnse { get; set; }

        DateTime today = DateTime.Now;
        string format = "MM/dd/yyyy HH:mm:ss";

        public List<LocationItemRenderingModel> getLocations( string folderPath )
        {
            IEnumerable<Item> locations = Sitecore.Context.Database.GetItem(folderPath).Children.Where(x => x.TemplateName == "Location Item" || x.TemplateName == "Hospital Item");
            List<LocationItemRenderingModel> lirm = new List<LocationItemRenderingModel>();

            foreach(var loc in locations)
            {
                LocationItemRepository lir = new LocationItemRepository();
                lir.LocationItem = loc;

                LocationItemRenderingModel tlocation = (LocationItemRenderingModel)lir.GetModel();
                //Sitecore.Diagnostics.Log.Info("[location info] " + tlocation.Title, this);
                lirm.Add(tlocation);
            }
            //Sitecore.Diagnostics.Log.Info("[hospital count] " + lirm.Count().ToString(), this);
            return lirm;
        }

        public List<LocationItemRenderingModel> getDescendantLocations( string folderPath )
        {
            IEnumerable<Item> locations = Sitecore.Context.Database.GetItem(folderPath).Axes.GetDescendants().Where(x => x.TemplateName == "Location Item" || x.TemplateName == "Hospital Item");
            List<LocationItemRenderingModel> lirm = new List<LocationItemRenderingModel>();

            foreach(var loc in locations)
            {

                LocationItemRepository lir = new LocationItemRepository();
                lir.LocationItem = loc;

                LocationItemRenderingModel tlocation = (LocationItemRenderingModel)lir.GetModel();
                //Sitecore.Diagnostics.Log.Info("[location info] " + tlocation.Title, this);
                lirm.Add(tlocation);
            }
            //Sitecore.Diagnostics.Log.Info("[hospital count] " + lirm.Count().ToString(), this);
            return lirm;
        }

        public List<LocationWithDistance> getLocationsAndDistanceFromUser( string locationfolder )
        {
            //get/set user geoip
            UserGeo usergeo = new UserGeo();

            if(this.req.Cookies.AllKeys.Contains("USER_GEO"))
            {                
                usergeo = JsonConvert.DeserializeObject<UserGeo>(this.req.Cookies["USER_GEO"].Value);
            }
            else
            {
                usergeo = this.setusergeo();
            }

            List<LocationWithDistance> lwd = this.getLocationsFromPoint(usergeo.latitude, usergeo.longitude, locationfolder);

            return (List<LocationWithDistance>)lwd.OrderBy(x => x.distance).ToList();
        }

        public UserGeo getUserGeo()
        {
            if(this.req.Cookies.AllKeys.Contains("USER_GEO"))
            {
                return JsonConvert.DeserializeObject<UserGeo>(this.req.Cookies["USER_GEO"].Value);
            }else {
                return this.setusergeo();
            }
        }

        public List<LocationWithDistance> getLocationsAndDistanceFromLatLon( string locationfolder, float latitude, float longitude )
        {
            //get/set user geoip
            /*UserGeo usergeo = new UserGeo();

            if(this.req.Cookies.AllKeys.Contains("USER_GEO"))
            {
                usergeo = JsonConvert.DeserializeObject<UserGeo>(this.req.Cookies["USER_GEO"].Value);
            }
            else
            {
                usergeo = this.setusergeo();
            }*/

            List<LocationWithDistance> lwd = this.getLocationsFromPoint(latitude, longitude, locationfolder);

            return (List<LocationWithDistance>)lwd.OrderBy(x => x.distance).ToList();
        }


        public List<LocationWithDistanceSearch> getLocationsAndDistanceFromUserSolr( string locationtype = "Hospital", int radius = 500 )
        {
            //get/set user geoip
            UserGeo usergeo = new UserGeo();

            if(this.req.Cookies.AllKeys.Contains("USER_GEO"))
            {
                usergeo = JsonConvert.DeserializeObject<UserGeo>(this.req.Cookies["USER_GEO"].Value);
            }
            else
            {
                usergeo = this.setusergeo();
            }

            //Sitecore.Diagnostics.Log.Info("USER GEO" + Newtonsoft.Json.JsonConvert.SerializeObject(usergeo), this);

            List<LocationWithDistanceSearch> lwd = this.getLocationsFromPointSolr(usergeo.latitude, usergeo.longitude, locationtype, radius);

            return (List<LocationWithDistanceSearch>)lwd.OrderBy(x => x.distance).ToList();
        }



        public List<LocationWithDistanceSearch> getClinicsAndDistanceFromUser( int radius = 10, bool bmgonly = false )
        {
            //get/set user geoip
            UserGeo usergeo = new UserGeo();

            if(this.req.Cookies.AllKeys.Contains("USER_GEO"))
            {
                usergeo = JsonConvert.DeserializeObject<UserGeo>(this.req.Cookies["USER_GEO"].Value);
            }
            else
            {
                usergeo = this.setusergeo();
            }

            List<LocationWithDistanceSearch> lwd = this.getLocationsFromPointSolrClinics(usergeo.latitude, usergeo.longitude, "Clinic", radius, bmgonly);

            return (List<LocationWithDistanceSearch>)lwd.OrderBy(x => x.distance).ToList();
        }



        public List<LocationWithDistance> getLocationsAndDistanceFromQuery( string query, string locationfolder )
        {

            try
            {
                //Geo geocode = new Geo();
                //GeocodeResponse geores = geocode.geocodeSearch(query);
                GMapResponse geores = Geo.geocodeSearchCommon(query);


                //Diagnostics.Log.Info("GEOCODE RESPONSE" + JsonConvert.SerializeObject(geores), this);

                //List<LocationWithDistance> lwd = this.getLocationsFromPoint(geores.latitude, geores.longitude, locationfolder);
                var geolocation = geores.results.FirstOrDefault().geometry.location;
                List<LocationWithDistance> lwd = this.getLocationsFromPoint(geolocation.lat, geolocation.lng, locationfolder);

                return lwd.OrderBy(x => x.distance).ToList();

            }
            catch(Exception ex)
            {
                Diagnostics.Log.Info("GEOCODE ERROR" + ex.Message, this);
                return new List<LocationWithDistance>();
            }

        }

        public List<LocationWithDistanceSearch> getLocationsAndDistanceFromQuerySolr( string query, string locationtype, int radius = 500, bool bmgonly = false )
        {

            try
            {
                //Geo geocode = new Geo();
                //GeocodeResponse geores = geocode.geocodeSearch(query);
                GMapResponse geores = Geo.geocodeSearchCommon(query);

                //Diagnostics.Log.Info("GEOCODE RESPONSE" + JsonConvert.SerializeObject(geores), this);

                //List<LocationWithDistanceSearch> lwd = this.getLocationsFromPointSolr(geores.latitude, geores.longitude, locationtype, radius);
                var geolocation = geores.results.FirstOrDefault().geometry.location;
                List<LocationWithDistanceSearch> lwd = this.getLocationsFromPointSolr(geolocation.lat, geolocation.lng, locationtype, radius, bmgonly);
                //return lwd.OrderBy(x => x.distance).ToList();
                return lwd;

            }
            catch(Exception ex)
            {
                Diagnostics.Log.Info("GEOCODE ERROR" + ex.Message, this);
                return new List<LocationWithDistanceSearch>();
            }

        }



        public List<LocationWithDistance> getLocationsFromPoint( float latitude, float longitude, string locationfolder )
        {
            //get list of hosptials
            List<LocationItemRenderingModel> locations = this.getLocations(locationfolder);

            var userpoint = string.Format("POINT({1} {0})", latitude, longitude);


            DbGeography dbgeo = DbGeography.FromText(userpoint);

            List<LocationWithDistance> locationswithdistance = new List<LocationWithDistance>();

            foreach(var locatn in locations)
            {
                float lat, lon;
                if(!float.TryParse(locatn.Latitude, out lat))
                {
                    lat = (float)0.0;
                }
                if(!float.TryParse(locatn.Longitude, out lon))
                {
                    lon = (float)0.0;
                }

                var hospitalpoint = string.Format("POINT({1} {0})", lat, lon);
                DbGeography hospgeo = DbGeography.FromText(hospitalpoint);
                double? distance = dbgeo.Distance(hospgeo);

                ERWaitModel waittime;

                //only dial er wait time if there is an ER
                string locString = this.getLocationString(locatn);

                if(locString != "no-match")
                {
                    waittime = this.getLocationWaitTime(locString);
                }
                else
                {
                    waittime = new ERWaitModel();
                    waittime.CV_ED_Wait = null;
                }

                LocationWithDistance lwd = new LocationWithDistance
                {
                    location = locatn,
                    distance = (distance * 0.000621371),
                    er_wait = waittime
                };
                locationswithdistance.Add(lwd);
            }

            return locationswithdistance;
        }

        /* public List<LocationWithDistanceSearch> getLocationsFromPointSolr( float latitude, float longitude, string locationtype, double? radius = 5 )
         {

             List<LocationWithDistanceSearch> locationswithdistance = new List<LocationWithDistanceSearch>();


             var userpoint = string.Format("POINT({1} {0})", latitude, longitude);

             DbGeography dbgeo = DbGeography.FromText(userpoint);

             string index_name = string.Format("baptist_locations_{0}_index", Sitecore.Context.Database.Name.ToLower());

             var index = ContentSearchManager.GetIndex(index_name);


             var predicate = PredicateBuilder.True<LocationSearchResultItem>();


             using(var context = index.CreateSearchContext())
             {

                 float lat, lon;

                 //predicate = predicate.And(f => dbgeo.Distance(DbGeography.FromText(string.Format("POINT({0} {1})", ((!float.TryParse(f.Latitude, out lat)) ? (float)0.0 : lat), ((!float.TryParse(f.Longitude, out lon)) ? (float)0.0 : lon)))) < radius);
                 predicate = predicate.And(f => f.LocationType == locationtype);

                 //predicate = predicate.Or(f => f.GetField("Specialties").ToString().ToLower().decodeUrl().Contains(q.decodeUrl().ToLower()));
                 //predicate = predicate.Or(f => f.Fields["primary_location_postal_code_t"] == q);
                 //predicate = predicate.Or(f => f["Primary Location City"].ToString().Contains(q));


                 var searchQuery = context.GetQueryable<LocationSearchResultItem>()
                     .Where(predicate);


                 var resultItems = searchQuery.ToList();
                 var totalCount = searchQuery.Count();               

                 foreach(var locatn in resultItems)
                 {              

                     //LocationItemRenderingModel locatn = this.ResultToLocationItem(lresult);
                     float dlat, dlon;
                     if(!float.TryParse(locatn.Latitude, out dlat))
                     {
                         dlat = (float)0.0;
                     }
                     if(!float.TryParse(locatn.Longitude, out dlon))
                     {
                         dlon = (float)0.0;
                     }

                     var hospitalpoint = string.Format("POINT({1} {0})", dlat, dlon);
                     DbGeography hospgeo = DbGeography.FromText(hospitalpoint);
                     double? distance = dbgeo.Distance(hospgeo);

                     ERWaitModel waittime;


                         //only dial er wait time if there is an ER
                     if(!string.IsNullOrEmpty(this.getLocationStringSolr(locatn)))
                     {
                         waittime = this.getLocationWaitTime(this.getLocationStringSolr(locatn));
                     }
                     else
                     {
                         waittime = new ERWaitModel();
                         waittime.CV_ED_Wait = null;
                     }


                     LocationWithDistanceSearch lwd = new LocationWithDistanceSearch
                     {
                         location = locatn,
                         distance = (distance * 0.000621371),
                         er_wait = waittime
                     };


                     locationswithdistance.Add(lwd);
                 }

                 //return (ActionResult)PartialView("")

                 //return Content(string.Format("<h1>Terms:</h1><p>{0}</p><h1>Count: {1}  </h1><h1>Result Items</h1><br /><p>{2}</p>", JsonConvert.SerializeObject(terms), totalCount, JsonConvert.SerializeObject(resultItems)));

                 return locationswithdistance;

             }
         }*/

        //Rewritten to perform Geolocation in Solr.
        public List<LocationWithDistanceSearch> getLocationsFromPointSolr( float latitude, float longitude, string locationtype, int radius = 500, bool bmgonly = false )
        {

            List<LocationWithDistanceSearch> locationswithdistance = new List<LocationWithDistanceSearch>();

            //double kmradius = Math.Ceiling(radius * 1.60934);

            //int ikmradius = int.Parse(kmradius.ToString());

            List<LocationSearchResultItem> resultItems = LocationsNearCenterpointSolr(latitude, longitude, locationtype, radius, bmgonly);

            //Sitecore.Diagnostics.Log.Info("LOCATIONS DEBUG " + Newtonsoft.Json.JsonConvert.SerializeObject(resultItems), this);

                foreach(var locatn in resultItems)
                {

                    
                    ERWaitModel waittime;

                    //only dial er wait time if there is an ER
                    string locString = this.getLocationStringSolr(locatn);
                    if(locString != "no-match")
                    {
                        waittime = this.getLocationWaitTime(locString);
                    }
                    else
                    {
                        waittime = new ERWaitModel();
                        waittime.CV_ED_Wait = null;
                    }


                    LocationWithDistanceSearch lwd = new LocationWithDistanceSearch
                    {
                        location = locatn,
                        distance = double.Parse((locatn.Distance).ToString()),
                        er_wait = waittime
                    };

                    locationswithdistance.Add(lwd);
                }
                

                return locationswithdistance;
           
        }

        /*public List<LocationWithDistanceSearch> getLocationsFromPointSolrClinics( float latitude, float longitude, string locationtype, double? radius = 5 )
        {

            List<LocationWithDistanceSearch> locationswithdistance = new List<LocationWithDistanceSearch>();


            var userpoint = string.Format("POINT({1} {0})", latitude, longitude);

            DbGeography dbgeo = DbGeography.FromText(userpoint);

            string index_name = string.Format("baptist_locations_{0}_index", Sitecore.Context.Database.Name.ToLower());

            var index = ContentSearchManager.GetIndex(index_name);


            var predicate = PredicateBuilder.True<LocationSearchResultItem>();


            using(var context = index.CreateSearchContext())
            {

                float lat, lon;

                //predicate = predicate.And(f => dbgeo.Distance(DbGeography.FromText(string.Format("POINT({0} {1})", ((!float.TryParse(f.Latitude, out lat)) ? (float)0.0 : lat), ((!float.TryParse(f.Longitude, out lon)) ? (float)0.0 : lon)))) < radius);
                predicate = predicate.And(f => f.LocationType == locationtype);

                //predicate = predicate.Or(f => f.GetField("Specialties").ToString().ToLower().decodeUrl().Contains(q.decodeUrl().ToLower()));
                //predicate = predicate.Or(f => f.Fields["primary_location_postal_code_t"] == q);
                //predicate = predicate.Or(f => f["Primary Location City"].ToString().Contains(q));


                var searchQuery = context.GetQueryable<LocationSearchResultItem>()
                    .Where(predicate);


                var resultItems = searchQuery.ToList();
                var totalCount = searchQuery.Count();

                foreach(var locatn in resultItems)
                {

                    //LocationItemRenderingModel locatn = this.ResultToLocationItem(lresult);

                    float dlat, dlon;
                    if(!float.TryParse(locatn.Latitude, out dlat))
                    {
                        dlat = (float)0.0;
                    }
                    if(!float.TryParse(locatn.Longitude, out dlon))
                    {
                        dlon = (float)0.0;
                    }

                    var hospitalpoint = string.Format("POINT({1} {0})", dlat, dlon);
                    DbGeography hospgeo = DbGeography.FromText(hospitalpoint);
                    double? distance = dbgeo.Distance(hospgeo);

                    ERWaitModel waittime;


                    //only dial er wait time if there is an ER
                    /* if(!string.IsNullOrEmpty(this.getLocationString(locatn)))
                     {
                         waittime = this.getLocationWaitTime(this.getLocationString(locatn));
                     }
                     else
                     {/
                    waittime = new ERWaitModel();
                    waittime.CV_ED_Wait = null;
                    //}


                    LocationWithDistanceSearch lwd = new LocationWithDistanceSearch
                    {
                        location = locatn,
                        distance = (distance * 0.000621371),
                        er_wait = waittime
                    };


                    locationswithdistance.Add(lwd);
                }

                //return (ActionResult)PartialView("")

                //return Content(string.Format("<h1>Terms:</h1><p>{0}</p><h1>Count: {1}  </h1><h1>Result Items</h1><br /><p>{2}</p>", JsonConvert.SerializeObject(terms), totalCount, JsonConvert.SerializeObject(resultItems)));

                return locationswithdistance;

            }
        }*/

        public List<LocationWithDistanceSearch> getLocationsFromPointSolrClinics( float latitude, float longitude, string locationtype, int radius = 500 , bool bmgonly = false)
        {

            List<LocationWithDistanceSearch> locationswithdistance = new List<LocationWithDistanceSearch>();

            //double kmradius = Math.Ceiling(radius * 1.60934);

            //int ikmradius = int.Parse(kmradius.ToString());


            List<LocationSearchResultItem> resultItems = LocationsNearCenterpointSolr(latitude, longitude, locationtype, radius, bmgonly);

            foreach(var locatn in resultItems)
            {
                //Only part really different
                ERWaitModel waittime;
                waittime = new ERWaitModel();
                waittime.CV_ED_Wait = null;
              

                LocationWithDistanceSearch lwd = new LocationWithDistanceSearch
                {
                    location = locatn,
                    distance = double.Parse((locatn.Distance).ToString()),
                    er_wait = waittime
                };


                locationswithdistance.Add(lwd);
            }
              
            return locationswithdistance;
            
        }


        public List<LocationWithDistance> getERLocations()
        {
            List<LocationWithDistance> lwd = new List<LocationWithDistance>();
            
            foreach(LocationWithDistance loc in this.getLocationsAndDistanceFromUser("/sitecore/content/Baptist/Baptist/Baptistonline/United States/Data/Locations/Hospitals"))
            {

                if(string.IsNullOrEmpty(loc.er_wait.CV_ED_Wait))
                {
                    continue;
                }
                else
                {
                    lwd.Add(loc);
                }
            }

            return lwd;
        }

        public List<LocationWithDistanceSearch> getERLocationsSearch()
        {
            
            List<LocationWithDistanceSearch> lwds = this.getLocationsAndDistanceFromUserSolr("Hospital", 6000);          

            //Sitecore.Diagnostics.Log.Info("DEBUG Locations Search " + Newtonsoft.Json.JsonConvert.SerializeObject(lwd), this);

            return lwds.Where(x => string.IsNullOrEmpty(x.er_wait.CV_ED_Wait) == false).ToList<LocationWithDistanceSearch>();
        }

        public List<LocationWithDistance> getERLocationsLatLonList( float latitude, float longitude )
        {

            List<LocationWithDistance> lwd = new List<LocationWithDistance>();

            foreach(LocationWithDistance loc in this.getLocationsAndDistanceFromLatLon("/sitecore/content/Baptist/Baptist/Baptistonline/United States/Data/Locations/Hospitals", latitude, longitude))
            {

                if(string.IsNullOrEmpty(loc.er_wait.CV_ED_Wait))
                {
                    continue;
                }
                else
                {
                    lwd.Add(loc);
                }
            }

            return lwd;
        }

        public UserGeo setusergeo()
        {
            var ip = this.req.ServerVariables["REMOTE_ADDR"];
            if(Regex.IsMatch(ip, @"^(0\.0\.0\.0|127\.0\.0\.1)$"))
            {
                ip = "216.37.75.146";
                //ip = "184.106.137.126";
                //ip = "162.242.233.5";                
            }
            string ipCacheKey = "IP_GEO_RESULT_" + ip.Replace(".","_");
            string ipCacheExpires = "IP_GEO_EXP_" + ip.Replace(".","_");

            if(IP2GeoCacheManager.GetCache(ipCacheExpires).HasValue() && IP2GeoCacheManager.GetCache(ipCacheExpires).CacheIsExpired() == false)
            {
                var obj = JsonConvert.DeserializeObject<UserGeo>(IP2GeoCacheManager.GetCache(ipCacheKey));
                return obj;
            }
            else
            {
                string url = "http://api.ipstack.com/" + ip + "?access_key=9e66358067039db3468aee2dd479f489&output=json&legacy=1";
                //string url = "https://freegeoip.net/json/" + ip;
                string geoipinfo;
                HttpWebRequest hrquest = (HttpWebRequest)WebRequest.Create(url);
                using(HttpWebResponse response = hrquest.GetResponse() as HttpWebResponse)
                {
                    StreamReader readStream = new StreamReader(response.GetResponseStream());
                    geoipinfo = readStream.ReadToEnd();

                    //Sitecore.Diagnostics.Log.Info("[IP INFO]" + geoipinfo, this);
                    IP2GeoCacheManager.SetCache(ipCacheKey, geoipinfo);
                    IP2GeoCacheManager.SetCache(ipCacheExpires, this.today.AddMinutes(720).ToString(format));

                    var obj = JsonConvert.DeserializeObject<UserGeo>(geoipinfo);                                                           

                    HttpCookie geocook = new HttpCookie("USER_GEO");
                    geocook.Value = JsonConvert.SerializeObject(obj);
                    geocook.Expires = DateTime.MinValue;

                    this.respnse.Cookies.Add(geocook);
                    return obj;
                }
            }
        }

        public UserGeo setusergeofrombrowser(string latitude, string longitude) {            
            
            var ip = this.req.ServerVariables["REMOTE_ADDR"];

            if(Regex.IsMatch(ip, @"^(0\.0\.0\.0|127\.0\.0\.1)$"))
            {
                //ip = "216.37.75.146";
                //ip = "184.106.137.126";
                ip = "162.242.233.5";
            }
            string ipCacheKey = "IP_GEO_RESULT_" + ip.Replace(".", "_");
            string ipCacheExpires = "IP_GEO_EXP_" + ip.Replace(".", "_");
            var obj = new UserGeo();

            if(IP2GeoCacheManager.GetCache(ipCacheExpires).HasValue() && IP2GeoCacheManager.GetCache(ipCacheExpires).CacheIsExpired() == false)
            {
                obj = JsonConvert.DeserializeObject<UserGeo>(IP2GeoCacheManager.GetCache(ipCacheKey));
                obj.latitude = float.Parse(latitude);
                obj.longitude = float.Parse(longitude);
                
            }
            else
            {
                string url = "http://api.ipstack.com/" + ip + "?access_key=9e66358067039db3468aee2dd479f489&output=json&legacy=1";
                //string url = "https://freegeoip.net/json/" + ip;

                string geoipinfo;
                HttpWebRequest hrquest = (HttpWebRequest)WebRequest.Create(url);
                using(HttpWebResponse response = hrquest.GetResponse() as HttpWebResponse)
                {
                    StreamReader readStream = new StreamReader(response.GetResponseStream());
                    geoipinfo = readStream.ReadToEnd();

                    //Sitecore.Diagnostics.Log.Info("[IP INFO]" + geoipinfo, this);
                    IP2GeoCacheManager.SetCache(ipCacheKey, geoipinfo);
                    IP2GeoCacheManager.SetCache(ipCacheExpires, this.today.AddMinutes(720).ToString(format));

                    obj = JsonConvert.DeserializeObject<UserGeo>(geoipinfo);
                    obj.latitude = float.Parse(latitude);
                    obj.longitude = float.Parse(longitude);                   
                   
                }
            }
            HttpCookie geocook = new HttpCookie("USER_GEO");
            geocook.Value = JsonConvert.SerializeObject(obj);
            geocook.Expires = DateTime.MinValue;

            this.respnse.Cookies.Add(geocook);
            return obj;
        }


        public LocationWithDistance setDefaultLocation( string clientID )
        {
            if(!clientID.HasValue()) {
                clientID = "MEMPH";
            }

            LocationWithDistance lwd = this.getERLocations().Where(x => x.location.ClientID == clientID).FirstOrDefault();

            HttpCookie defloc = new HttpCookie("DEFAULT_LOCATION_ID");
            defloc.Value = lwd.location.ClientID;
            defloc.Expires = DateTime.MinValue;
            this.respnse.Cookies.Add(defloc);

            return lwd;
        }

 
        public LocationWithDistanceSearch setDefaultLocationSearch( string clientID, List<LocationWithDistanceSearch> erLocations = null )
        {
            if(!clientID.HasValue())
            {
                clientID = "MEMPH";
            }

            if(erLocations == null) {
                erLocations = this.getERLocationsSearch();
            }

            LocationWithDistanceSearch lwd = erLocations.Where(x => x.location.ClientID == clientID).FirstOrDefault();

            HttpCookie defloc = new HttpCookie("DEFAULT_LOCATION_ID");
            defloc.Value = lwd.location.ClientID;
            defloc.Expires = DateTime.MinValue;
            this.respnse.Cookies.Add(defloc);

            return lwd;
        }

        public LocationWithDistanceSearch setChosenLocationSearch( string clientID, List<LocationWithDistanceSearch> erLocations = null )
        {
            if(!clientID.HasValue())
            {
                clientID = "MEMPH";
            }

            if(erLocations == null)
            {
                erLocations = this.getERLocationsSearch();
            }

            LocationWithDistanceSearch lwd = erLocations.Where(x => x.location.ClientID == clientID).FirstOrDefault();

            HttpCookie defloc = new HttpCookie("CHOSEN_LOCATION_ID");
            defloc.Value = lwd.location.ClientID;
            defloc.Expires = DateTime.MinValue;
            this.respnse.Cookies.Add(defloc);

            return lwd;
        }

        public LocationWithDistance getDefaultLocation()
        {
            LocationWithDistance defloc;
            string locid;
            if(this.req.Cookies.AllKeys.Contains("DEFAULT_LOCATION_ID"))
            {
                locid = (this.req.Cookies["DEFAULT_LOCATION_ID"].Value);
                if(locid != null)
                {
                    defloc = this.getERLocations().Where(x => x.location.ClientID == locid).First();
                }else {
                    defloc = this.getERLocations().Where(x => x.location.ClientID == "MEMPH").First();
                }
            }
            else
            {
                defloc = null;
                defloc = this.getERLocations().Where(x => x.location.ClientID == "MEMPH").First();
            }
            return defloc;
        }
        public LocationWithDistanceSearch getDefaultLocationSearch(List<LocationWithDistanceSearch> erLocations = null)
        {
            LocationWithDistanceSearch defloc;
            string locid;

            if(erLocations == null) {
                erLocations = this.getERLocationsSearch();
            }

            if(this.req.Cookies.AllKeys.Contains("DEFAULT_LOCATION_ID"))
            {
                locid = (this.req.Cookies["DEFAULT_LOCATION_ID"].Value);
                if(locid != null)
                {
                    defloc = erLocations.Where(x => x.location.ClientID == locid).First();
                }
                else
                {
                    defloc = erLocations.Where(x => x.location.ClientID == "MEMPH").First();
                }
            }
            else
            {
                defloc = null;
                defloc = erLocations.Where(x => x.location.ClientID == "MEMPH").First();
            }
            return defloc;
        }

        public LocationWithDistanceSearch getChosenLocationSearch( List<LocationWithDistanceSearch> erLocations = null )
        {
            LocationWithDistanceSearch defloc;
            string locid;

            if(erLocations == null)
            {
                erLocations = this.getERLocationsSearch();
            }

            if(this.req.Cookies.AllKeys.Contains("CHOSEN_LOCATION_ID"))
            {
                locid = (this.req.Cookies["CHOSEN_LOCATION_ID"].Value);
                if(locid != null)
                {
                    defloc = erLocations.Where(x => x.location.ClientID == locid).First();
                }
                else
                {
                    defloc = erLocations.Where(x => x.location.ClientID == "MEMPH").First();
                }
            }
            else
            {
                defloc = null;
                defloc = erLocations.Where(x => x.location.ClientID == "MEMPH").First();
            }
            return defloc;
        }

        public LocationWithDistance getFallBackLocation()
        {
            return this.getERLocations().Where(x => x.location.ClientID == "MEMPH").First();
        }

        public bool defaultLocationIsSet()
        {
            if(this.req.Cookies.AllKeys.Contains("DEFAULT_LOCATION_ID"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        public string getLocationString( LocationItemRenderingModel location )
        {

            if(location.ERWaitURLKey.HasValue() && location.LocationType == "Hospital")
            {

                return location.ERWaitURLKey;

            }
            else if(location.LocationType == "Hospital")
            {

                switch(location.ClientID)
                {

                    case "BOON":
                        return "booneville";
                    case "CALH":
                        return "calhoun";
                    case "CVL":
                        return "collierville";
                    case "DESH":
                        return "desoto";
                    //case "WOMN":
                    //    return "womens";
                    case "GTR":
                        return "golden-triangle";
                    case "HUNT":
                        return "huntingdon";
                    case "MEMPH":
                        return "memphis";
                    case "OXFH":
                        return "north-mississippi";
                    case "TIPH":
                        return "tipton";
                    case "UCH":
                        return "union-city";
                    case "NA":
                        return "union-county";
                    //case "RCH":
                    //    return "restorative-care";
                    //case "GERM":
                    //    return "germantown";
                    case "NEAH":
                        return "nea";
                    case "BMCJACKSONMS":
                        return "baptist-medical-center";
                    case "BMCATTALAMS":
                        return "baptist-medical-center-attala";
                    case "BMCLEAKEMS":
                        return "baptist-medical-center-leake";
                    case "BMCYAZOOMS":
                        return "baptist-medical-center-yazoo";
                    default:
                        return "no-match";
                }
            }else {
                return "no-match";
            }
        }

        public List<string> getHardCodedERClientIDs() {
            return new string[] { "BOON", "CALH", "CVL","DESH","GTR","HUNT","MEMPH","OXFH", "TIPH", "UCH", "NA", "RCH", "GERM", "NEAH", "BMCJACKSONMS", "BMCATTALAMS", "BMCLEAKEMS", "BMCYAZOOMS" }.ToList<string>();
        }

        public string getLocationStringSolr( LocationSearchResultItem location )
        {

            if(location.ERWaitURLKey.HasValue() && location.LocationType == "Hospital")
            {

                return location.ERWaitURLKey;

            }
            else if(location.LocationType == "Hospital")
            {

                switch(location.ClientID)
                {

                    case "BOON":
                        return "booneville";
                    case "CALH":
                        return "calhoun";
                    case "CVL":
                        return "collierville";
                    case "DESH":
                        return "desoto";
                    //case "WOMN":
                    //    return "womens";
                    case "GTR":
                        return "golden-triangle";
                    case "HUNT":
                        return "huntingdon";
                    case "MEMPH":
                        return "memphis";
                    case "OXFH":
                        return "north-mississippi";
                    case "TIPH":
                        return "tipton";
                    case "UCH":
                        return "union-city";
                    case "NA":
                        return "union-county";
                    //case "RCH":
                    //    return "restorative-care";
                    //case "GERM":
                    //    return "germantown";
                    case "NEAH":
                        return "nea";
                    case "BMCJACKSONMS":
                        return "baptist-medical-center";
                    case "BMCATTALAMS":
                        return "baptist-medical-center-attala";
                    case "BMCLEAKEMS":
                        return "baptist-medical-center-leake";
                    case "BMCYAZOOMS":
                        return "baptist-medical-center-yazoo";
                    default:
                        return "no-match";
                }
            }else {
                return "no-match";
            }
        }

        public ERWaitModel getLocationWaitTime( string location )
        {
            var wturl = "https://sites.bmhcc.org/api/waittimes/waittimes.php?fac=" + location;
            string resp;

            string locationCacheKey = "ER_WAIT_TIME_" + location;
            string locationCacheExpires = "ER_WAIT_TIME_EXP_" + location;
            
           try
           {
                if(ERWaitCacheManager.GetCache(locationCacheExpires).HasValue() && ERWaitCacheManager.GetCache(locationCacheExpires).CacheIsExpired() == false) { 
                    
                    var obj = JsonConvert.DeserializeObject<ERWaitModel>(ERWaitCacheManager.GetCache(locationCacheKey));
                    return obj;

                }else {                   
                    
                    HttpWebRequest hrequest = (HttpWebRequest)WebRequest.Create(wturl);
                    hrequest.Timeout = 300;
                    using(HttpWebResponse response = hrequest.GetResponse() as HttpWebResponse)
                    {
                        StreamReader readStream = new StreamReader(response.GetResponseStream());
                        resp = readStream.ReadToEnd();

                        var obj = JsonConvert.DeserializeObject<ERWaitModel>(resp);

                        ERWaitCacheManager.SetCache(locationCacheKey, JsonConvert.SerializeObject(obj));

                        //set random expiration between 2 to 5 minutes
                        Random rando = new Random();
                        int minutes = 2 + (rando.Next(0, 3));

                        ERWaitCacheManager.SetCache(locationCacheExpires, this.today.AddMinutes(minutes).ToString(format));

                        //deserialize object
                        
                        return obj;

                    }
                }
            }
            catch(Exception ex)
            {
               return new ERWaitModel();
            }
        }

        public void cacheLocationWaitTime( string location, int expiration )
        {
            var wturl = "https://sites.bmhcc.org/api/waittimes/waittimes.php?fac=" + location;
            string resp;

            string locationCacheKey = "ER_WAIT_TIME_" + location;
            string locationCacheExpires = "ER_WAIT_TIME_EXP_" + location;

            try
            {
                HttpWebRequest hrequest = (HttpWebRequest)WebRequest.Create(wturl);
                hrequest.Timeout = 300;
                using(HttpWebResponse response = hrequest.GetResponse() as HttpWebResponse)
                {
                    StreamReader readStream = new StreamReader(response.GetResponseStream());
                    resp = readStream.ReadToEnd();

                    var obj = JsonConvert.DeserializeObject<ERWaitModel>(resp);

                    ERWaitCacheManager.SetCache(locationCacheKey, JsonConvert.SerializeObject(obj));

                    //set random expiration between 2 to 5 minutes
                    Random rando = new Random();
                    int minutes = expiration;

                    ERWaitCacheManager.SetCache(locationCacheExpires, this.today.AddMinutes(minutes).ToString(format));

                    //deserialize object                  

                }
            }
            catch(Exception ex)
            {               
                
            }
        }

        public LocationItemRenderingModel ResultToLocationItem( LocationSearchResultItem item )
        {

            LocationItemRenderingModel locatn = new LocationItemRenderingModel();
            
            locatn.LocationName = item.LocationName.ToString();
            locatn.ShortName = (item.ShortName.HasValue()) ? item.ShortName : "";
            locatn.AddressLine1 = (item.AddressLine1.HasValue()) ? item.AddressLine1 : "";
            locatn.AddressLine2 = (item.AddressLine2.HasValue()) ? item.AddressLine2 : "";
            locatn.City = (item.City.HasValue()) ? item.City : "";
            locatn.State = (item.State.HasValue()) ? item.State : "";
            locatn.PostalCode = (item.PostalCode.HasValue()) ? item.PostalCode : "";
            locatn.Latitude = (item.Latitude.HasValue()) ? item.Latitude : "";
            locatn.Longitude = (item.Longitude.HasValue()) ? item.Longitude : "";
            locatn.MainPhoneNumber = (item.MainPhoneNumber.HasValue()) ? item.MainPhoneNumber : "";
            locatn.ERWaitURLKey = (item.ERWaitURLKey.HasValue()) ? item.ERWaitURLKey : "";
            locatn.LocationType = (item.LocationType.HasValue()) ? item.LocationType : "";
            locatn.IsBMG = item.IsBMG;
            locatn.IsActive = item.IsActive;
            locatn.IsPubliclySearchable = item.IsPubliclySearchable;                        
            locatn.ItemID = ID.Parse(item.ItemId);            

            return locatn;
        }

        //Return list of LocationSearchResultsItems directly from Solr already geosorted (waaay faster), not easier though.
        public List<LocationSearchResultItem> LocationsNearCenterpointSolr(float latitude, float longitude, string locationtype, int radius = 500, bool bmgonly = false)
        {
            string databaseName = "";
            if(Sitecore.Context.Database == null ) {
                databaseName = Sitecore.Data.Database.GetDatabase("web").Name.ToLower();
            }
            else {
                databaseName = Sitecore.Context.Database.Name.ToLower();
            }

            string index_name = string.Format("baptist_locations_{0}_index", databaseName);

            string serviceBaseAddress = Sitecore.Configuration.Settings.GetSetting("ContentSearch.Solr.ServiceBaseAddress");

            //address of index core
            string coreaddress = serviceBaseAddress + "/" + index_name;

            //create request url for Solr
            //string requesturl = serviceBaseAddress + "/" + index_name + "/select?d=" + radius.ToString() + "&fq=location_type_t_en:(" + locationtype + ")&fl=_dist_:geodist(),*&fq={!geofilt%20pt=" + latitude + "," + longitude + "%20sfield=coordinates_rpt%20d=" + radius.ToString() + "}&indent=on&pt=" + latitude + "," + longitude + "&q=*:*&sfield=coordinates_rpt&wt=json&sort=geodist()%20asc";
            string bmgquery = "";
            if(bmgonly) {
                bmgquery = "&fq=is_bmg_b:(true)";
            }

            var ip = (this.req != null ) ? this.req.ServerVariables["REMOTE_ADDR"] : "216.37.75.146";

            string requesturl = serviceBaseAddress + "/" + index_name + "/select?d=" + radius.ToString() + "&fq=location_type_t_en:(" + locationtype + ")" + bmgquery + "&fl=_dist_:geodist(),_group,short_name_t_en,location_name_t_en,address_line_1_t_en,address_line_2_t_en,city_t_en,state_t_en,postal_code_t_en,latitude_t_en,longitude_t_en,main_phone_number_t_en,er_wait_url_key_t_en,location_type_t_en,clientid_t_en,main_email_address_t_en,alternate_location_listing_page_t_en,enable_alternate_link_b,override_with_maps_link_b,is_bmg_b,is_active_b,is_publicly_searchable_b,scheduling_type_t,scheduling_label_t,scheduling_code_t&fq={!geofilt%20pt=" + latitude + "," + longitude + "%20sfield=coordinates_rpt%20d=" + radius.ToString() + "}&pt=" + latitude + "," + longitude + "&q=*:*&sfield=coordinates_rpt&wt=json&sort=geodist()%20asc";

            if(Regex.IsMatch(ip, @"^(0\.0\.0\.0|127\.0\.0\.1)$"))
            {
                requesturl = serviceBaseAddress + "/" + index_name + "/select?d=" + radius.ToString() + "&fq=location_type_t:(" + locationtype + ")" + bmgquery + "&fl=_dist_:geodist(),_group,short_name_t,location_name_t,address_line_1_t,address_line_2_t,city_t,state_t,postal_code_t,latitude_t,longitude_t,main_phone_number_t,er_wait_url_key_t,location_type_t,clientid_t,main_email_address_t,alternate_location_listing_page_t,enable_alternate_link_b,override_with_maps_link_b,is_bmg_b,is_active_b,is_publicly_searchable_b,scheduling_type_t,scheduling_label_t,scheduling_code_t&fq={!geofilt%20pt=" + latitude + "," + longitude + "%20sfield=coordinates_rpt%20d=" + radius.ToString() + "}&pt=" + latitude + "," + longitude + "&q=*:*&sfield=coordinates_rpt&wt=json&sort=geodist()%20asc";
            }
            //if(serviceBaseAddress == "http://search.baptist.amdevel.com:8983/solr")
            //{
                requesturl = serviceBaseAddress + "/" + index_name + "/select?d=" + radius.ToString() + "&fq=location_type_t:(" + locationtype + ")" + bmgquery + "&fl=_dist_:geodist(),_group,short_name_t,location_name_t,address_line_1_t,address_line_2_t,city_t,state_t,postal_code_t,latitude_t,longitude_t,main_phone_number_t,er_wait_url_key_t,location_type_t,clientid_t,main_email_address_t,alternate_location_listing_page_t,enable_alternate_link_b,override_with_maps_link_b,is_bmg_b,is_active_b,is_publicly_searchable_b,scheduling_type_t,scheduling_label_t,scheduling_code_t&fq={!geofilt%20pt=" + latitude + "," + longitude + "%20sfield=coordinates_rpt%20d=" + radius.ToString() + "}&pt=" + latitude + "," + longitude + "&q=*:*&sfield=coordinates_rpt&wt=json&sort=geodist()%20asc";
            //}

            requesturl = serviceBaseAddress + "/" + index_name + "/select?d=" + radius.ToString() + "&fq=location_type_t:(" + locationtype + ")" + bmgquery + "&fl=_dist_:geodist(),_group,short_name_t,location_name_t,address_line_1_t,address_line_2_t,city_t,state_t,postal_code_t,latitude_t,longitude_t,main_phone_number_t,er_wait_url_key_t,location_type_t,clientid_t,main_email_address_t,alternate_location_listing_page_t,enable_alternate_link_b,override_with_maps_link_b,is_bmg_b,is_active_b,is_publicly_searchable_b,scheduling_type_t,scheduling_label_t,scheduling_code_t&fq={!geofilt%20pt=" + latitude + "," + longitude + "%20sfield=coordinates_rpt%20d=" + radius.ToString() + "}&pt=" + latitude + "," + longitude + "&q=*:*&sfield=coordinates_rpt&wt=json&sort=geodist()%20asc";

            Sitecore.Diagnostics.Log.Info(requesturl,this);

            HttpWebRequest hrquest = (HttpWebRequest)WebRequest.Create(requesturl);

            List<LocationSearchResultItem> locationres = new List<LocationSearchResultItem>();

            //Requeest to Solr and return LocationSearchResultItems
            using(HttpWebResponse response = hrquest.GetResponse() as HttpWebResponse)
            {
                StreamReader readStream = new StreamReader(response.GetResponseStream());
                string rspns = readStream.ReadToEnd();

                dynamic objt = JsonConvert.DeserializeObject<dynamic>(rspns);

                foreach(var loc in objt.response.docs)
                {

                    LocationSearchResultItem res = SetFromResult(loc);

                    locationres.Add(res); 

                }
            }

            return locationres;

        }

        //set LocationSearchResultItem properties, this could be problematic
        public static LocationSearchResultItem SetFromResult( dynamic result ) 
        {
            LocationSearchResultItem loca = new LocationSearchResultItem();

            loca.ItemId = ID.Parse(Guid.ParseExact(result._group.ToString(), "N"));
            if(result.location_name_t_en != null)
            {
                loca.LocationName = (result.location_name_t_en != null && !string.IsNullOrEmpty(result.location_name_t_en[0].ToString())) ? result.location_name_t_en[0].ToString() : "";
            }else {
                loca.LocationName = (result.location_name_t != null && !string.IsNullOrEmpty(result.location_name_t[0].ToString())) ? result.location_name_t[0].ToString() : "";
            }
            
            if(result.short_name_t_en != null)
            {
                loca.ShortName = (result.short_name_t_en != null && !string.IsNullOrEmpty(result.short_name_t_en[0].ToString())) ? result.short_name_t_en[0].ToString() : "";
            }else {
                loca.ShortName = (result.short_name_t != null && !string.IsNullOrEmpty(result.short_name_t[0].ToString())) ? result.short_name_t[0].ToString() : "";
            }

            if(result.address_line_1_t_en != null)
            {
                loca.AddressLine1 = (result.address_line_1_t_en != null && !string.IsNullOrEmpty(result.address_line_1_t_en[0].ToString())) ? result.address_line_1_t_en[0].ToString() : "";
            }else {
                loca.AddressLine1 = (result.address_line_1_t != null && !string.IsNullOrEmpty(result.address_line_1_t[0].ToString())) ? result.address_line_1_t[0].ToString() : "";
            }

            if(result.address_line_2_t_en != null)
            {
                loca.AddressLine2 = (result.address_line_2_t_en != null && !string.IsNullOrEmpty(result.address_line_2_t_en[0].ToString())) ? result.address_line_2_t_en[0].ToString() : "";
            }else {
                loca.AddressLine2 = (result.address_line_2_t != null && !string.IsNullOrEmpty(result.address_line_2_t[0].ToString())) ? result.address_line_2_t[0].ToString() : "";
            }
            
            if(result.city_t_en != null)
            {
                loca.City = (result.city_t_en != null && !string.IsNullOrEmpty(result.city_t_en[0].ToString())) ? result.city_t_en[0].ToString() : "";
            }else {
                loca.City = (result.city_t != null && !string.IsNullOrEmpty(result.city_t[0].ToString())) ? result.city_t[0].ToString() : "";
            }

            if(result.state_t_en != null)
            {
                loca.State = (result.state_t_en != null && !string.IsNullOrEmpty(result.state_t_en[0].ToString())) ? result.state_t_en[0].ToString() : "";
            }else {
                loca.State = (result.state_t != null && !string.IsNullOrEmpty(result.state_t[0].ToString())) ? result.state_t[0].ToString() : "";
            }

            if(result.postal_code_t_en != null)
            {
                loca.PostalCode = (result.postal_code_t_en != null && !string.IsNullOrEmpty(result.postal_code_t_en[0].ToString())) ? result.postal_code_t_en[0].ToString() : "";
            }else {
                loca.PostalCode = (result.postal_code_t != null && !string.IsNullOrEmpty(result.postal_code_t[0].ToString())) ? result.postal_code_t[0].ToString() : "";
            }

            if(result.latitude_t_en != null)
            {
                loca.Latitude = (result.latitude_t_en != null && !string.IsNullOrEmpty(result.latitude_t_en[0].ToString())) ? result.latitude_t_en[0].ToString() : "";
            }else {
                loca.Latitude = (result.latitude_t != null && !string.IsNullOrEmpty(result.latitude_t[0].ToString())) ? result.latitude_t[0].ToString() : "";
            }

            if(result.longitude_t_en != null)
            {
                loca.Longitude = (result.longitude_t_en != null && !string.IsNullOrEmpty(result.longitude_t_en[0].ToString())) ? result.longitude_t_en[0].ToString() : "";
            }else {
                loca.Longitude = (result.longitude_t != null && !string.IsNullOrEmpty(result.longitude_t[0].ToString())) ? result.longitude_t[0].ToString() : "";
            }

            //loca.Coordinates = (result.coordinates_rpt != null && !string.IsNullOrEmpty(result.coordinates_rpt.ToString())) ? result.coordinates_rpt.ToString() : "";

            if(result.main_phone_number_t_en != null)
            {
                loca.MainPhoneNumber = (result.main_phone_number_t_en != null && !string.IsNullOrEmpty(result.main_phone_number_t_en[0].ToString())) ? result.main_phone_number_t_en[0].ToString() : "";
            }else {
                loca.MainPhoneNumber = (result.main_phone_number_t != null && !string.IsNullOrEmpty(result.main_phone_number_t[0].ToString())) ? result.main_phone_number_t[0].ToString() : "";
            }
            if(result.er_wait_url_key_t_en != null) {
                loca.ERWaitURLKey = (result.er_wait_url_key_t_en != null && !string.IsNullOrEmpty(result.er_wait_url_key_t_en[0].ToString())) ? result.er_wait_url_key_t_en[0].ToString() : "";
            }else {
                loca.ERWaitURLKey = (result.er_wait_url_key_t != null && !string.IsNullOrEmpty(result.er_wait_url_key_t[0].ToString())) ? result.er_wait_url_key_t[0].ToString() : "";
            }

            if(result.location_type_t_en != null)
            {
                loca.LocationType = (result.location_type_t_en != null && !string.IsNullOrEmpty(result.location_type_t_en[0].ToString())) ? result.location_type_t_en[0].ToString() : "";
            }else {
                loca.LocationType = (result.location_type_t != null && !string.IsNullOrEmpty(result.location_type_t[0].ToString())) ? result.location_type_t[0].ToString() : "";
            }

            if(result.clientid_t_en != null)
            {
                loca.ClientID = (result.clientid_t_en != null && !string.IsNullOrEmpty(result.clientid_t_en[0].ToString())) ? result.clientid_t_en[0].ToString() : "";
            }else {
                loca.ClientID = (result.clientid_t != null && !string.IsNullOrEmpty(result.clientid_t[0].ToString())) ? result.clientid_t[0].ToString() : "";
            }

            if(result.main_email_address_t_en != null)
            {
                loca.MainEmailAddress = (result.main_email_address_t_en != null && !string.IsNullOrEmpty(result.main_email_address_t_en[0].ToString())) ? result.main_email_address_t_en[0].ToString() : "";
            }else {
                loca.MainEmailAddress = (result.main_email_address_t != null && !string.IsNullOrEmpty(result.main_email_address_t[0].ToString())) ? result.main_email_address_t[0].ToString() : "";
            }

            if(result.alternate_location_listing_page_t_en != null) {
                loca.AlternateLocationListingPage = (result.alternate_location_listing_page_t_en != null && !string.IsNullOrEmpty(result.alternate_location_listing_page_t_en[0].ToString())) ? result.alternate_location_listing_page_t_en[0].ToString() : "";
            }else {
                loca.AlternateLocationListingPage = (result.alternate_location_listing_page_t != null && !string.IsNullOrEmpty(result.alternate_location_listing_page_t[0].ToString())) ? result.alternate_location_listing_page_t[0].ToString() : "";
            }
            
            if( result.scheduling_type_t_en != null )
            {
                loca.SchedulingType = (result.scheduling_type_t_en != null && !string.IsNullOrEmpty(result.scheduling_type_t_en[0].ToString())) ? result.scheduling_type_t_en[0].ToString() : "";
            }else{
                loca.SchedulingType = (result.scheduling_type_t != null && !string.IsNullOrEmpty(result.scheduling_type_t[0].ToString())) ? result.scheduling_type_t[0].ToString() : "";
            }
            
            if(result.scheduling_label_t_en != null)
            {
                loca.SchedulingLabel = (result.scheduling_label_t_en != null && !string.IsNullOrEmpty(result.scheduling_label_t_en[0].ToString())) ? result.scheduling_label_t_en[0].ToString() : "";
            }else{
                loca.SchedulingLabel = (result.scheduling_label_t != null && !string.IsNullOrEmpty(result.scheduling_label_t[0].ToString())) ? result.scheduling_label_t[0].ToString() : "";
            }

            if(result.scheduling_code_t_en != null)
            {
                loca.SchedulingCode = (result.scheduling_code_t_en != null && !string.IsNullOrEmpty(result.scheduling_code_t_en[0].ToString())) ? result.scheduling_code_t_en[0].ToString() : "";
            }else{
                loca.SchedulingCode = (result.scheduling_code_t != null && !string.IsNullOrEmpty(result.scheduling_code_t[0].ToString())) ? result.scheduling_code_t[0].ToString() : "";
            }

            loca.EnableAlternateLink = result.enable_alternate_link_b;
            loca.OverrideWithMapsLink = result.override_with_maps_link_b;

            loca.IsBMG = result.is_bmg_b;
            loca.IsActive = result.is_active_b;
            loca.IsPubliclySearchable = result.is_publicly_searchable_b;
            loca.Distance = result._dist_;

            return loca;
        }

        public static void refreshWaitCaches()
        {
            LocationUtils lu = new LocationUtils();

            Sitecore.Diagnostics.Log.Info("ERWAITCACHE Loading ER Wait Times to Cache Starting", typeof(LocationUtils));

            float latitude = float.Parse("35.1495");
            float longitude = float.Parse("-90.0490");

            List<LocationSearchResultItem> resultItems = lu.LocationsNearCenterpointSolr(latitude, longitude , "Hospital", 6000, false);

            foreach(LocationSearchResultItem loc in resultItems)
            {
                string locationString = lu.getLocationStringSolr(loc);

                if(locationString != "no-match")
                {
                    lu.cacheLocationWaitTime(locationString, 6);
                }
            }

            Sitecore.Diagnostics.Log.Info("ERWAITCACHE Loading ER Wait Times to Cache Finished", typeof(LocationUtils));
        }

        public static string locationTypeFromPath( string dsitempath )
        {

            string locationType = "Hospital";

            if(dsitempath.ToLower().Contains("minor med"))
            {
                locationType = "Minor Medical";
            }

            if(dsitempath.ToLower().Contains("specialty"))
            {
                locationType = "Specialty";
            }

            if(dsitempath.ToLower().Contains("cancer"))
            {
                locationType = "Cancer Center";
            }

            if(dsitempath.ToLower().Contains("clinic"))
            {
                locationType = "Clinic";
            }

            if(dsitempath.ToLower().Contains("outpatient care"))
            {
                locationType = "Outpatient Care";
            }

            return locationType;
        }

    }    

    public static class ERWaitCacheManager
    {
        private static readonly BaptistCustomCache Cache;

        static ERWaitCacheManager()
        {
            Cache = new BaptistCustomCache("Baptist_ER_Wait", StringUtil.ParseSizeString("2MB"));
        }

        public static string GetCache( string key )
        {
            return Cache.GetString(key);
        }

        public static void SetCache( string key, string value )
        {
            Cache.SetString(key, value);
        }
    }
    
    public static class IP2GeoCacheManager
    {
        private static readonly BaptistCustomCache Cache;

        static IP2GeoCacheManager()
        {
            Cache = new BaptistCustomCache("Baptist_IP_Geo", StringUtil.ParseSizeString("1GB"));
        }

        public static string GetCache( string key )
        {
            return Cache.GetString(key);
        }

        public static void SetCache( string key, string value )
        {
            Cache.SetString(key, value);
        }
    }
    
}