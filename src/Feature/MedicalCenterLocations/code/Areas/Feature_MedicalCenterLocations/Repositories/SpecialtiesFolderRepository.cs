﻿using Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Models;
using Sitecore.Data.Items;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Repositories
{
    

    public class SpecialtiesFolderRepository : ModelRepository, ISpecialtiesFolderRepository, IModelRepository, IAbstractRepository<IRenderingModelBase>
    {

        public Item SpecialtyFolderItem { get; set; }

        public override IRenderingModelBase GetModel()
        {
            SpecialtiesFolderRenderingModel sfrm = new SpecialtiesFolderRenderingModel();            

            this.FillBaseProperties((object)sfrm);

            return (IRenderingModelBase)sfrm;
        }

        public List<Models.SpecialtyRenderingModel> GetSpecialties()
        {
            List<Models.SpecialtyRenderingModel> specrenmod = new List<Models.SpecialtyRenderingModel>();

            if(this.Rendering.DataSourceItem != null)
            {
                IEnumerable<Item> ielirm = this.Rendering.DataSourceItem.Children.Where(x => x.TemplateName == "Specialty");

                foreach(Item specialty in ielirm)
                {

                    SpecialtyRepository spltrep = new SpecialtyRepository();

                    spltrep.SpecialtyItem = specialty;

                    specrenmod.Add((Models.SpecialtyRenderingModel)spltrep.GetModel());

                }

                return specrenmod;
            }
            else
            {
                return specrenmod;
            }
        }
    }
}