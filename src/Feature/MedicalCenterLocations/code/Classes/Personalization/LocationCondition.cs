﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Rules;
using Sitecore.Rules.Conditions;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Utils;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Models;
using Sitecore.Baptist.Foundation.Common.Utils;
using Sitecore.Diagnostics;
using Sitecore.Data;
using Sitecore.Mvc.Presentation;

namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Classes.Personalization
{
    public class LocationCondition<T> : OperatorCondition<T> where T : RuleContext
    {
        public string ItemId { get; set; }
        public string Radius { get; set; }

        protected override bool Execute( T ruleContext )
        {
            //Log.Info("Condition rule happened " + ItemId, this);            

            if(Radius.HasValue() == false) return false;
            if(ItemId.HasValue() == false) return false;
            int radius;
            if(!int.TryParse(Radius, out radius)) return false;
            ID locitemid;
            if(!ID.TryParse(ItemId, out locitemid)) return false;            

            LocationUtils lu = new LocationUtils();
            lu.req = RenderingContext.Current.PageContext.RequestContext.HttpContext.Request;
            lu.respnse = RenderingContext.Current.PageContext.RequestContext.HttpContext.Response;

            List<LocationWithDistanceSearch> lwds = lu.getLocationsAndDistanceFromUserSolr("Hospital", 1000);            

            switch(GetOperator()) {
                case ConditionOperator.Equal:
                    return lwds.Where(x => x.distance == radius && x.location.ItemId == locitemid).Count() > 0;
                case ConditionOperator.GreaterThanOrEqual:
                    return lwds.Where(x => x.distance >= radius && x.location.ItemId == locitemid).Count() > 0;
                case ConditionOperator.GreaterThan:
                    return lwds.Where(x => x.distance > radius && x.location.ItemId == locitemid).Count() > 0;
                case ConditionOperator.LessThanOrEqual:
                    return lwds.Where(x => x.distance <= radius && x.location.ItemId == locitemid).Count() > 0;
                case ConditionOperator.LessThan:
                    return lwds.Where(x => x.distance < radius && x.location.ItemId == locitemid).Count() > 0;
                case ConditionOperator.NotEqual:
                    return lwds.Where(x => x.distance != radius && x.location.ItemId == locitemid).Count() > 0;
                default:
                    return false;                    
            }                        
               
        }
    }

}