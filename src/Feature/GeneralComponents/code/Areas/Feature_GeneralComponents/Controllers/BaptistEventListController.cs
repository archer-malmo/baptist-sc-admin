﻿using Sitecore.Baptist.Feature.GeneralComponents.Areas.Feature_GeneralComponents.Repositories;
using Sitecore.Baptist.Foundation.Common.Utils;
using Sitecore.Mvc.Presentation;
using Sitecore.XA.Feature.Events.Controllers;
using Sitecore.XA.Feature.Events.Repositories;
using Sitecore.XA.Foundation.RenderingVariants.Controllers.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Fields;
using System.Web.Mvc;
using Newtonsoft.Json;
using Sitecore.Baptist.Feature.GeneralComponents.Areas.Feature_GeneralComponents.Models;
using Sitecore.Baptist.Feature.GeneralComponents.Areas.Feature_GeneralComponents.Repositories;
using Sitecore.Data;
using Sitecore.Data.Items;
using System.Globalization;

namespace Sitecore.Baptist.Feature.GeneralComponents.Areas.Feature_GeneralComponents.Controllers
{
    public class BaptistEventListController : PaginableController
    {
        
        protected readonly IBaptistEventListRepository BaptistEventListRepository;
        
        // GET: Feature_GeneralComponents/BaptistEventList
        public BaptistEventListController(IBaptistEventListRepository repository)
        {
            this.BaptistEventListRepository = repository;
            
        }

        protected override object GetModel()
        {
            return (object)this.BaptistEventListRepository.GetModel();
        }

        protected BaptistCombinedEventListing GetCombinedModel(string[] eventLists)
        {

            BaptistCombinedEventListing combinedModel = new BaptistCombinedEventListing();
            List<BaptistEventRenderingModel> events = new List<BaptistEventRenderingModel>();

            combinedModel.eventLists = new List<BaptistEventListRenderingModel>();

            foreach(string eventlistid in eventLists) {

                BaptistEventListRenderingModel belr = new BaptistEventListRenderingModel();

                ID elid = ID.Parse(eventlistid);

                Item elitem = Sitecore.Context.Database.GetItem(elid);

                //Sitecore.Diagnostics.Log.Info("BELRM selected Item :" + elitem.ID.ToString(), this);

                BaptistEventListRepository belrepo = new BaptistEventListRepository();
                belrepo.EventListItem = elitem;

                belr = (BaptistEventListRenderingModel)belrepo.GetModel();

                belr.Item = elitem;

                //Sitecore.Diagnostics.Log.Info("Returned items " + belr.EventItems.Count(), this);

                CultureInfo provider = CultureInfo.InvariantCulture;

                List<BaptistEventRenderingModel> currentitems = new List<BaptistEventRenderingModel>();
                foreach(BaptistEventRenderingModel bermd in belr.EventItems)
                {
                    //Sitecore.Diagnostics.Log.Info("Date : " + bermd.InnterItem[Sitecore.XA.Feature.Events.Templates.CalendarEvent.Fields.EventEnd].ToString(), this);
                    DateTime evdate;
                    if(bermd.InnterItem[Sitecore.XA.Feature.Events.Templates.CalendarEvent.Fields.EventEnd].EndsWith("Z"))
                    {
                        evdate = DateTime.ParseExact(bermd.InnterItem[Sitecore.XA.Feature.Events.Templates.CalendarEvent.Fields.EventEnd], "yyyyMMddTHHmmssZ", provider);
                    }
                    else
                    {
                        evdate = DateTime.ParseExact(bermd.InnterItem[Sitecore.XA.Feature.Events.Templates.CalendarEvent.Fields.EventEnd], "yyyyMMddTHHmmss", provider);
                    }

                    if(evdate > DateTime.Now) {
                        currentitems.Add(bermd);
                    }
                    //Sitecore.Diagnostics.Log.Info("Parsed date : " + info.ToUniversalTime(),this);
                }

                //Add items to model
                //IEnumerable<BaptistEventRenderingModel> curitems = currentitems.AsEnumerable<BaptistEventRenderingModel>().OrderBy(x => x.InnterItem[Sitecore.XA.Feature.Events.Templates.CalendarEvent.Fields.EventStart]);

                //Sitecore.Diagnostics.Log.Info("Current items : " + currentitems.Count(), this);

                events.AddRange(currentitems.AsEnumerable<BaptistEventRenderingModel>());   
                if(currentitems.AsEnumerable<BaptistEventRenderingModel>().Count() > 0) {
                    combinedModel.eventLists.Add(belr);
                }
                
            }
            
            combinedModel.combinedEvents = new BaptistEventListRenderingModel();
            

            combinedModel.combinedEvents.EventItems = events.AsEnumerable<BaptistEventRenderingModel>().OrderBy(x => x.InnterItem[Sitecore.XA.Feature.Events.Templates.CalendarEvent.Fields.EventStart]);


            //Sitecore.Diagnostics.Log.Info("Event Items added to model : " + combinedModel.combinedEvents.EventItems.Count(), this);
            //Sitecore.Diagnostics.Log.Info("Event List Items added to model : " + combinedModel.eventLists.Count(), this);

            return combinedModel;                       

            //return (object)this.BaptistEventListRepository.GetCombinedModel();
        }

        public ActionResult CombinedEventListing()
        {

            //try
            //{
                string eventlist = RenderingContext.Current.Rendering.Parameters["Event Lists"];


                string[] arrayitems = new string[0];
                if(eventlist.HasValue()) {                     
                 arrayitems = eventlist.Split('|');
                }

                //return Content(JsonConvert.SerializeObject(arrayitems));

                return (ActionResult)this.PartialView("CombinedEventListing", this.GetCombinedModel(arrayitems));
            /*} catch(Exception ex) {
                
                return Content("<h1>Events are not available at this time.</h1>");
            }*/
        }                

    }
}