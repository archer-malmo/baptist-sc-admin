﻿using Sitecore.XA.Foundation.Mvc.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Mvc.Controllers;
using System.Web.Mvc;
using Sitecore.Baptist.Foundation.Common.Utils;
using Sitecore.Baptist.Feature.CareersIcims.Classes;
using Sitecore.Baptist.Feature.CareersIcims.Areas.Feature_CareersIcims.Models;
using Newtonsoft.Json;
using System.Collections.Specialized;
using Sitecore.Data.Fields;
using Sitecore.Links;
using System.Text.RegularExpressions;
using Sitecore.XA.Foundation.Multisite;
using Sitecore.Data;
using Sitecore.Data.Items;
using System.Net;
using System.IO;

namespace Sitecore.Baptist.Feature.CareersIcims.Areas.Feature_CareersIcims.Controllers
{
    public class CareersController : StandardController
    {

        private DateTime today = DateTime.Now;
        private string format = "MM/dd/yyyy HH:mm:ss";

        // GET: Feature_CareersIcims/Careers
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CareersSearchIndex()
        {                        
           ICimsClient iclient = new ICimsClient();


            string formAction = string.Empty, fali = string.Empty;
            string portalname = "careers";
            LinkField lf = null;
            var rc = Sitecore.Mvc.Presentation.RenderingContext.CurrentOrNull;

            if(rc != null)
            {
                var parms = rc.Rendering.Parameters;
                fali = parms["Form Action"];
                if(string.IsNullOrEmpty(fali) == false)
                {
                    Sitecore.Data.Items.Item resitem = Sitecore.Context.Database.GetItem(fali);
                    formAction = LinkManager.GetItemUrl(resitem);
                }

                if(parms["ICims Portal Name"] != null && parms["ICims Portal Name"] != "") {
                    portalname = parms["ICims Portal Name"];
                }
            }

            // get stuff about jobs... need post as well
            /*
            //original code

            string portalfilters = String.Empty;            

            string cachekey = "Portal_Filters_Cache_" + portalname;
            string expcachekey = "Portal_Filters_Expires_" + portalname;
          
            portalfilters = iclient.GetPortalFilters(portalname);          
            PortalFilters pf = JsonConvert.DeserializeObject<PortalFilters>(portalfilters);

            pf.portalname = portalname;
            return View("~/Areas/Feature_CareersIcims/Views/Careers/CareersSearchIndex.cshtml",pf);
            */
            // BEGIN PATCH REQUEST FROM
            // without first search screen                                  

            string results = "";
            results = iclient.SearchPortalJobs(portalname);
            PortalSearchResults psr = null;
            try
            {
                psr = JsonConvert.DeserializeObject<PortalSearchResults>(results);
            }
            catch(Exception ex)
            {
                psr = null;
            }
            if(psr == null)
            {
                psr = new PortalSearchResults();
            }

            psr.portalname = portalname;

            //Get Portal Filters
            string portalfilters = iclient.GetPortalFilters(psr.portalname);

            PortalFilters pf = JsonConvert.DeserializeObject<PortalFilters>(portalfilters);
            pf.portalname = psr.portalname;

            //set original search
            psr.originalsearch = new NameValueCollection();
            psr.portalfilters = pf;
            
            return View("~/Areas/Feature_CareersIcims/Views/Careers/CareersSearchResults.cshtml", psr);
        }

        [ActionName("CareersSearchIndex")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CareersSearchIndex_Post() {
            ICimsClient iclient = new ICimsClient();

            NameValueCollection nvc = Request.Form;

            IDictionary<string, string> dict = new Dictionary<string, string>();

            foreach(var k in nvc.AllKeys) {
                if(nvc[k].HasValue() && k != "portal_name")
                {
                    dict.Add(k, nvc[k]);
                }
            }

            string results = "";
            if(dict.Count > 0)
            {

                //string searchJson = @"{ ""filters"": [ {0} ],""operator"":""&""}";

                string filters = "";
                foreach(var ent in dict)
                {
                    string filterJson = "{\"name\":\"" + ent.Key + "\",\"value\":[\"" + ent.Value + "\"],\"operator\":\"=\"},";
                    filters += filterJson;
                }

                string filterstring = "{ \"filters\": [ " + filters + " ],\"operator\":\"&\"}";

                //string requeststring = HttpUtility.UrlEncode(filterstring);

                results = iclient.SearchPortalJobs(nvc["portal_name"], filterstring);
            }else {
                results = iclient.SearchPortalJobs(nvc["portal_name"]);
            }

            // return Content(results);

            PortalSearchResults psr = null;
            try
            {
                psr = JsonConvert.DeserializeObject<PortalSearchResults>(results);
            }catch(Exception ex) {
                psr = null;
            }
            if(psr == null) {
                psr = new PortalSearchResults();
            }
            psr.portalname = nvc["portal_name"];            

            //Get Portal Filters
            string portalfilters = iclient.GetPortalFilters(psr.portalname);

            PortalFilters pf = JsonConvert.DeserializeObject<PortalFilters>(portalfilters);
            pf.portalname = psr.portalname;

            //set original search
            psr.originalsearch = nvc;
            psr.portalfilters = pf;

            return View("~/Areas/Feature_CareersIcims/Views/Careers/CareersSearchResults.cshtml", psr);
        }

        public ActionResult JobDetails(string portalname, string id) {

            ICimsClient iclient = new ICimsClient();

            string resp = iclient.GetPortalJob(portalname, id);

            if(resp != "NotFound")
            {

                ICimsJobPosting job = JsonConvert.DeserializeObject<ICimsJobPosting>(iclient.GetPortalJob(portalname, id));

                job.portalname = portalname;
                job.jobid = id;

                return View("~/Areas/Feature_CareersIcims/Views/Careers/JobDetails.cshtml", job);

            }
            else 
            {
                return Content("404");
            }
            
        }

        public ActionResult JobDetailsFromUrl()
        {
            string rawurl = Sitecore.Context.RawUrl;
            //return Content(rawurl);
            string routeurl = rawurl.poplast();

            string[] routes = Regex.Split(routeurl, "--");                       

            dynamic output = new {
                job_id = routes[0],
                portal_definition = routes[1],
                iccims_slug = (routes.Length > 2) ? routes[2] : ""
            };

            //return Content(JsonConvert.SerializeObject(output));

            return JobDetails(output.portal_definition, output.job_id);
        }

        public static ICimsJobPosting JobModelFromUrl() {
            string rawurl = Sitecore.Context.RawUrl;
            //return Content(rawurl);
            string routeurl = rawurl.poplast();

            string[] routes = Regex.Split(routeurl, "--");

            dynamic output = new
            {
                job_id = routes[0],
                portal_definition = routes[1],
                iccims_slug = (routes.Length > 2) ? routes[2] : ""
            };

            //return Content(JsonConvert.SerializeObject(output));
            ICimsClient iclient = new ICimsClient();

            string resp = iclient.GetPortalJob(output.portal_definition, output.job_id);

            ICimsJobPosting job = JsonConvert.DeserializeObject<ICimsJobPosting>(resp);

            job.portalname = output.portal_definition;
            job.jobid = output.job_id;

            return job;

        }

        public static bool is404FromUrl()
        {

            string rawurl = Sitecore.Context.RawUrl;
            //return Content(rawurl);
            string routeurl = rawurl.poplast();

            string[] routes = Regex.Split(routeurl, "--");

            dynamic output = new
            {
                job_id = routes[0],
                portal_definition = routes[1],
                iccims_slug = (routes.Length > 2) ? routes[2] : ""
            };

            ICimsClient iclient = new ICimsClient();

            string resp = iclient.GetPortalJob(output.portal_definition, output.job_id);

            if(resp == "NotFound")
            {
                return true;
            }
            return false;
        }

        /*public ActionResult CareersSearchResults()
        {
            return View();
        }*/

        /*[HttpPost]
        public ActionResult CareersSearchResults(CareerSearchModel csm)
        {
            return View("~/Areas/Feature_CareersIcims/Views/Careers/CareersSearchResults.cshtml",csm);            
        }*/

        public ActionResult ICimsRequest() {

            ICimsClient iclient = new ICimsClient();

            // get stuff about jobs... need post as well
            string customerinfo = iclient.GetPortalFilters("careers");

            PortalFilters pf = JsonConvert.DeserializeObject<PortalFilters>(customerinfo);


            //return pf;
            return Content(customerinfo);            
        }

    
        public ActionResult GetICimsPortalPost( string portalname, string id )
        {     
            ICimsClient iclient = new ICimsClient();            
            
            string response = iclient.GetPortalJob(portalname, id);
                            
            return Content(response, "application/json", System.Text.Encoding.UTF8);
            
        }


        public ActionResult Respond404() {           

            MultisiteContext msc = new MultisiteContext();

            Item si = msc.GetSettingsItem(Sitecore.Context.Item);

            ID errfield = ID.Parse("{96E5E297-6FD1-46D6-8E92-B03E33A9376C}");

            Item errPage = Sitecore.Context.Database.GetItem(si[errfield]);

            //If 404 page not set in SXA throw Server 404
            if(errPage == null)
            {
                Response.TrySkipIisCustomErrors = true;
                Response.StatusCode = 404;
                Response.End();
            }

            UrlOptions urloptions = new UrlOptions();
            urloptions.LanguageEmbedding = LanguageEmbedding.Never;
            urloptions.SiteResolving = true;
            urloptions.AlwaysIncludeServerUrl = true;

            var errUrl = LinkManager.GetItemUrl(errPage, urloptions);

            HttpWebRequest hrquest = (HttpWebRequest)WebRequest.Create(errUrl);

            //Curl 404 and override response code
            using(HttpWebResponse response = hrquest.GetResponse() as HttpWebResponse)
            {
                StreamReader readStream = new StreamReader(response.GetResponseStream());
                string rspns = readStream.ReadToEnd();
                //WriteLiteral(rspns);
                Response.TrySkipIisCustomErrors = true;
                Response.StatusCode = 404;
                //Response.End();
                return Content(rspns);
            }

        }
       

    }

    /*public class CareeerSearchModel
    {
        public string breakthangs { get; set; }
    }*/
    
}