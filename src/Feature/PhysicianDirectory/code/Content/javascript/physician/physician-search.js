﻿/*/var physicianlisting = physicianlisting || {};

physicianlisting = {

  setresults: function (selector, styletag, datatag) {

    //console.log("something");

    if (selector.val()) {

      var selval = selector.val().toLowerCase();

      console.log(styletag);

      styletag.innerHTML = ".searchable:not([" + datatag + "^=\"" + selval + "\"])"
        + ":not([" + datatag + "*=\"" + selval + "\"])"
        + ":not([" + datatag + "$=\" " + selval + " \"])"
        + ":not([" + datatag + "=\"" + selval + "\"]) { display: none; }";
    } else {
      styletag.innerHTML = "";
    }

  },
  physicianfilterinit: function () {
    var phySearchFilter = document.getElementById('physician_search_filter');
    console.log('init');

    $(function () {

      if (phySearchFilter) {
        //var searchbox = document.getElementById('tabSearch');
        var jqsearchbox = $('input#query1');

        document.getElementById('query1').addEventListener('input', function () {
          console.log('topic changed');
          physicianlisting.setresults(jqsearchbox, phySearchFilter, 'data-index');
        });

      }

    });
  }

}*/


function getSearchResult(query) {

  var bmgquery = "";
  var bccquery = "";
  var specialtyq = "";
  if ($('input[name=searchbmgonly]').length > 0 && $("input[name=searchbmgonly]").val() == "1") {
    //console.log('Search bmg only');
    bmgquery = "&bmgonly=true";
  }

  if ($('input[name=searchbcconly]').length > 0 && $("input[name=searchbcconly").val() == "1") {
    bccquery = "&bcconly=true";
  }

  if ($('input#specialty').length > 0) {
    specialtyq = '&isspecialty=true&specialty=' + $('input#specialty').val();
  }



  var searchurl = "/api/sitecore/Physician/PhysicianSearch?precise=1&q=" + encodeURIComponent(query) + specialtyq + bmgquery + bccquery;
  //searchurl = "/api/sitecore/Physician/ProviderSearch?q=" + query + specialtyq + bmgquery + bccquery + "&radius=25&paging=False&rows=1000&start=0";
  //console.log(searchurl);

  var resultstarget = $('[data-search-return-target="true"]');

  $('div.css-loader').css('display', 'block');

  $.ajax({
    url: searchurl,
    cache: false,
    method: 'GET',
    success: function (data) {
      resultstarget.html(data);
      $('div.css-loader').css('display', 'none');
      //$('.option-list').toggleClass('active');
      //bind_filter_toggles();
    },
    error: function (data) {
      //console.log(data);
      $('div.css-loader').css('display', 'none');
      //$('.option-list').toggleClass('active');
    }
  });

}

$(function () {
  $('input#query1').unbind();
  $('form#physician-search').unbind();

  //console.log("something");

  $('form#physician-search').on('submit', function (event) {
    event.preventDefault();
    var query = $('input#query1').val();

    //if ($('input#specialty').length > 0) {
    //query = $('input#specialty').val() + "+" + query.replace(" ", "+");
    //query = query.replace(" ", "+");
    //}

    getSearchResult(query);
  });

  //getSearchResult('Oncology');

  //physicianlisting.physicianfilterinit();

});

var physicianschedulinglisting = physicianschedulinglisting || {};

physicianschedulinglisting = {

  setresults: function (selector, styletag, datatag) {

    //console.log("something");

    if (selector.val()) {

      //var selval = selector.val().toLowerCase();
      var selval = selector.val().toLowerCase().replace(',', '').split(" ");

      //console.log(styletag);
      var searchable = "";
      $.each(selval, function (index, value) {
        searchable += ".searchable:not([" + datatag + "^=\"" + value + "\"])"
        + ":not([" + datatag + "*=\"" + value + "\"])"
        + ":not([" + datatag + "$=\" " + value + " \"])"
        + ":not([" + datatag + "=\"" + value + "\"]) { display: none; }\r\n";
      });
      styletag.innerHTML = searchable;
    } else {
      styletag.innerHTML = "";
    }

  },
  setexactresults: function (selector, styletag, datatag) {

    //console.log("something");

    if (selector.val()) {

      var selval = selector.val().toLowerCase();

      //console.log(styletag);

      styletag.innerHTML = ".searchable:not([" + datatag + "=\"" + selval + "\"]) { display: none; }";

    } else {
      styletag.innerHTML = "";
    }

  },
  physicianfilterinit: function () {
    var phySearchFilter = document.getElementById('physician_scheduling_search_filter');
    //console.log('init');

    $(function () {

      if (phySearchFilter) {
        //var searchbox = document.getElementById('tabSearch');
        var jqsearchbox = $('input#query-scheduling');

        document.getElementById('query-scheduling').addEventListener('input', function () {

          physicianschedulinglisting.setresults(jqsearchbox, phySearchFilter, 'data-index');
        });
      }
    });
  },
  physicianSpecialtyFilterInit: function (selectbox, filterstyle, dataselector, hashselector) {
    //var physSpecFilter = document.getElementById('physician_scheduling_specialty_filter');

    $(function () {

      if (selectbox) {
        //var searchbox = document.getElementById('tabSearch');
        selectbox.change(function () {
          physicianschedulinglisting.setexactresults($(this), filterstyle, dataselector)

          if ($(this).find(':selected').data(hashselector) != "") {
            window.location.hash = $(this).find(':selected').data(hashselector);
          } else {
            window.location.hash = "";
          }

        });

      }
    });
  }
}

$(function () {
  if ($('#physician_scheduling_search_filter').length > 0) {
    physicianschedulinglisting.physicianfilterinit();
  }

  if ($('#physician_scheduling_specialty_filter').length > 0) {

    var hash = location.hash.replace('#', '');
    //alert(hash);
    var selectbox, filterstyle, dataselector, selectdata, hashselector;
    /*/ if (hash.startsWith('loc-')) {
 
       selectbox = $('select[name="filter-location"]');
       filterstyle = document.getElementById('location_filter');
       selectdata = '[data-location-hash-selector="' + hash + '"]';
       dataselector = 'data-filter-event-location';
 
     } else {
     */
    selectbox = $('select[name="scheduling-filter-by-specialty"]');
    filterstyle = document.getElementById('physician_scheduling_specialty_filter');
    selectdata = '[data-specialty-hash-selector="' + hash + '"]';
    hashselector = 'specialty-hash-selector';
    dataselector = 'data-specialty-filter';

    physicianschedulinglisting.physicianSpecialtyFilterInit(selectbox, filterstyle, dataselector, hashselector);
    //}

    if (selectbox != undefined && filterstyle != undefined && dataselector != undefined && selectdata != undefined) {
      //console.log("found");

      selectbox.find(selectdata).attr('selected', 'selected');
      physicianschedulinglisting.setexactresults(selectbox, filterstyle, dataselector);
 
    }
  }
});


function bind_filter_toggles() {
 
  $('[data-toggle]').click(
		function (event) {

		  event.preventDefault();
		  var filtershown = $('input[name=filtershown]').val();
		 
		  var datatarget = '[data-filter="' + $(this).attr('data-target') + '"]';

		  if ($(this).attr('aria-expanded') == 'true') {
		    var extras = datatarget + ' li:nth-child(n+' + filtershown + ')';

		    $(extras).each(function () {
		      console.log($(this).find('input:checked'));
		      return false;
		      if ($(this).find('input:checked').length == 0) {
		        $(extras).addClass('hidden');
		        console.log($(this).find('input:checked'));
		      }
		    });
		    
		    toggletext($(this), 'false', 'Collapse','Expand');

		  } else {

		    var hidden = datatarget + ' li.hidden';

		    $(hidden).removeClass('hidden');

		    toggletext($(this),'true', 'Expand', 'Collapse');
		  }
		}
	);
}

function toggletext(target, ariastate,targettext,replacetext)
{
  target.attr('aria-expanded', ariastate);
  target.text(target.text().replace(targettext, replacetext));
}