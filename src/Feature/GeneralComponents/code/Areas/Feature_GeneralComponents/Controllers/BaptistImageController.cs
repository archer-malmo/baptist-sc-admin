﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Sitecore.XA.Foundation.Mvc.Controllers;
using Sitecore.Baptist.Feature.GeneralComponents.Repositories;
using Sitecore.XA.Feature.Media.Controllers;
using Sitecore.XA.Feature.Media.Repositories;

namespace Sitecore.Baptist.Feature.GeneralComponents.Areas.Feature_GeneralComponents.Controllers
{
  public class BaptistImageController : StandardController
  {
    protected readonly IImageRepository ImageRepository;

    public BaptistImageController(IImageRepository imageRepository)
    {
      this.ImageRepository = imageRepository;
    }

    public ActionResult ReusableImageIndex()
    {
      return (ActionResult)this.PartialView("Image", this.GetModel());
    }

    protected override object GetModel()
    {
      return (object)this.ImageRepository.GetModel();
    }
  }
}