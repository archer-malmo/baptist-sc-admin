﻿using Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom;
using Sitecore.Data.Items;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Repositories
{
    public class ClinicalTrialsFolderRepository : ModelRepository, IClinicalTrialsFolderRepository, IModelRepository, IAbstractRepository<IRenderingModelBase>
    {

        public Item LocationItem { get; set; }

        public override IRenderingModelBase GetModel()
        {
            ClinicalTrialsFolderRenderingModel ctfr = new ClinicalTrialsFolderRenderingModel();

            this.FillBaseProperties((object)ctfr);            

            return (IRenderingModelBase)ctfr;
        }

        public List<ClinicalTrialRenderingModel> GetTrials()
        {
            List<ClinicalTrialRenderingModel> clrm = new List<ClinicalTrialRenderingModel>();

            if(this.Rendering.DataSourceItem != null)
            {
                IEnumerable<Item> ielirm = this.Rendering.DataSourceItem.Children.Where(x => x.TemplateName == "Clinical Trial");

                foreach(Item trial in ielirm) {

                    ClinicalTrialRepository clr = new ClinicalTrialRepository();

                    clr.ClinicalTrialItem = trial;

                    clrm.Add((ClinicalTrialRenderingModel)clr.GetModel());

                }                

                return clrm;
            }
            else
            {
                return clrm;
            }
        }
    }
}