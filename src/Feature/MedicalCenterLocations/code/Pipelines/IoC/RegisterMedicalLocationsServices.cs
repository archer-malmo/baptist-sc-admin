﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Repositories;
using Sitecore.XA.Foundation.IOC.Pipelines.IOC;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Repositories;

namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Pipelines.IoC
{
    public class RegisterMedicalLocationsServices : IocProcessor
    {
        public override void Process( IocArgs args )
        {
            args.ServiceCollection.AddTransient<ILocationItemRepository, LocationItemRepository>();
            args.ServiceCollection.AddTransient<ILocationFolderRepository, LocationFolderRepository>();
            args.ServiceCollection.AddTransient<IClinicalTrialRepository, ClinicalTrialRepository>();
            args.ServiceCollection.AddTransient<IClinicalTrialsFolderRepository, ClinicalTrialsFolderRepository>();
            args.ServiceCollection.AddTransient<ISpecialtyRepository, SpecialtyRepository>();
            args.ServiceCollection.AddTransient<ISpecialtiesFolderRepository, SpecialtiesFolderRepository>();
            SqlServerTypes.Utilities.LoadNativeAssemblies(Sitecore.IO.FileUtil.MapPath("~/bin"));
        }
    }
}