﻿using Sitecore;
using Sitecore.Baptist.Feature.GeneralComponents.Models;
using Sitecore.Data;
using Sitecore.XA.Foundation.Multisite;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using Sitecore.XA.Foundation.RenderingVariants.Repositories;
using Sitecore.XA.Foundation.SitecoreExtensions.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Sitecore.Configuration.Settings;

namespace Sitecore.Baptist.Feature.GeneralComponents.Repositories
{
  public class GeneralComponentsRepository : VariantsRepository, IGeneralComponentsRepository
  {
    public GeneralComponentsRepository(IVariantsRepository variantsrepo, ISiteInfoResolver siteInfoResolver)
    {
      this.VarientsRepsoitory = variantsrepo;
      this.SiteInfoResolver = siteInfoResolver;
    }

    public ISiteInfoResolver SiteInfoResolver { get; set; }

    public IVariantsRepository VarientsRepsoitory { get; set; }


    public override IRenderingModelBase GetModel()
    {
      var generalComponentsModel = new GeneralComponentsModel();

      this.FillBaseProperties(generalComponentsModel);

      generalComponentsModel.StyleDisplay = !this.IsEdit ? "none" : "block";

      return generalComponentsModel;
    }
  }
}