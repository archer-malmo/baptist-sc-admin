﻿$(function () {
  /*$('a#location-change').click(function (event) {
    event.preventDefault();
    $('.option-list').toggleClass('active');
    if ($('.option-list').hasClass('active')) {
      $('a#location-change').text('cancel');
    } else {
      $('a#location-change').text('change');
    }
  });*/
  var clearSpecifiedLocation = function (event) {
    $.removeCookie('specified-location');
  }

  var setSpecifiedLocation = function (event) {
    var key = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    $.cookie('specified-location', key);
  }

  var reloadOnServices = function () {
    var pname = window.location.pathname;

    if (pname == '/services' || pname == '/services/') {
      setTimeout(
        function () { location.reload(); }, 1000
      );
    }
  }

  var gpschangelocationlistener = function (event) {
    var startPosition;
    var latitude;
    var longitude;

    var geoSuccess = function (position) {
      startPosition = position;
      latitude = startPosition.coords.latitude;
      longitude = startPosition.coords.longitude;

      var usergeoupdateurl = '/api/sitecore/ERWait/UpdateUserGeoFromBrowser?lat=' + latitude + '&lon=' + longitude;

      $.ajax({
        url: usergeoupdateurl,
        cache: false,
        method: 'GET',
        success: function (data) {
          var clientid = data;
          clearSpecifiedLocation();
          updateErWaitTime(clientid);
          $('#header-er-wait').removeClass('hidden');
          //location.reload();
          /*var clientidurl = '/api/sitecore/ERWait/GetDefaultClientId';
            var clientID;
  
            $.ajax({
              url: clientidurl,
              cache: false,
              method: 'GET',
              success: function (data) {
                clientID = data;              
                updateErWaitTime(clientID);
                
              },
              error: function (data) {
                console.log(data);
              }
          });
          */
        },
        error: function (data) {
          console.log(data);
          //$('.option-list').toggleClass('active');
          $('#header-er-wait').addClass('hidden');
        }
      });



    };

    var geoError = function (error) {
      switch (error.code) {
        case error.TIMEOUT:
          // The user didn't accept the callout
          console.log(error);
          break;
      }
    };
    navigator.geolocation.getCurrentPosition(geoSuccess, geoError);
  }

  var locationcheckgpschangelocationlistener = function (event) {

    gpschangelocationlistener();

    var pname = window.location.pathname;

    if (pname == '/locations' || pname == '/locations/') {

      $('#location-option-list').modal('hide');

      $('div.css-loader').css('display', 'block');

      setTimeout(function () {
        $('div.css-loader').css('display', 'none');
      }, 25000);
      location.reload();
    }

    reloadOnServices();
  }

  var changelocationlistener = function (event) {
    var clientID = $(this).val();

    setSpecifiedLocation();
    updateErWaitTime(clientID);
    reloadOnServices();
  };

  var updateErWaitTime = function (clientidval) {
    var clientID = clientidval;
    var updateurl = '/api/sitecore/ERWait/HeaderLocationOverride?clientID=' + clientID;
    //console.log(clientID);

    $.ajax({
      url: updateurl,
      cache: false,
      method: 'GET',
      success: function (data) {
        $('div#header-er-wait').replaceWith(data);

        //$('.option-list').toggleClass('active');
        $('a#location-change').bind('click', function (event) {
          event.preventDefault();
          //$('.option-list').toggleClass('active');
        });
      },
      error: function (data) {
        console.log(data);
        //$('.option-list').toggleClass('active');
      }
    });

    var optionslisturl = '/api/sitecore/ERWait/HeaderUpdatedListOptions?clientID=' + clientID;

    $.ajax({
      url: optionslisturl,
      cache: false,
      method: 'GET',
      success: function (data) {
        //console.log(data);
        $('div#location-option-list div.modal-body').html(data);

        $('button[rel="change-default-location"]').bind('click', changelocationlistener);

        if (navigator.geolocation) {
          $('button[rel="gps-change-default-location"]').bind('click', locationcheckgpschangelocationlistener);
          $('a#forget-chosen').bind('click', locationcheckgpschangelocationlistener);

        } else {
          $('button[rel="gps-change-default-location"]').css('display', 'none');
        }
        //console.log('bound');
      }, error: function (data) {
        console.log(data);
      }
    });

    $('#location-option-list').modal('hide');
  };

  $('button[rel="change-default-location"]').bind('click', changelocationlistener);

  if (navigator.geolocation) {
    $('button[rel="gps-change-default-location"]').bind('click', locationcheckgpschangelocationlistener);
    $('a#forget-chosen').bind('click', locationcheckgpschangelocationlistener);
  } else {
    $('button[rel="gps-change-default-location"]').css('display', 'none');
  }

  $('a[data-change-location]').click(function (event) {
    event.preventDefault();
    $('#location-option-list').modal('toggle');
  });

  if (navigator.permissions != undefined) {

    navigator.permissions.query({ name: 'geolocation' })
        .then(function (permissionStatus) {

          permissionStatus.onchange = function () {
            //prompt,granted,denied
            if (this.state == "granted") {
              gpschangelocationlistener();
              var pname = window.location.pathname;
              if (pname == '/locations' || pname == '/locations/') {
                location.reload();
              }
            }
          }

        });
  }

  if ($.cookie('specified-location') != undefined && $.cookie('specified-location') != null) {
    $('#header-er-wait').removeClass('hidden');
  }
  else if (navigator.geolocation && $.cookie('specified-location') === undefined) {
    gpschangelocationlistener();
  }
})