﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Sitecore.Mvc;
using System.Web;

namespace Sitecore.Baptist.Foundation.Common.Utils
{
    public static class StringUtils
    {
        public static string ToUrlSlug( this string value )
        {

            //First to lower case 
            value = value.ToLowerInvariant();

            //Remove all accents
            var bytes = Encoding.GetEncoding("Cyrillic").GetBytes(value);

            value = Encoding.ASCII.GetString(bytes);

            //Replace spaces 
            value = Regex.Replace(value, @"\s", "-", RegexOptions.Compiled);

            //Remove invalid chars 
            value = Regex.Replace(value, @"[^\w\s\p{Pd}]", "", RegexOptions.Compiled);

            //Trim dashes from end 
            value = value.Trim('-', '_');

            //Replace double occurences of - or \_ 
            value = Regex.Replace(value, @"([-_]){2,}", "$1", RegexOptions.Compiled);

            return value;
        }

        public static bool HasValue( this string s )
        {
            return !string.IsNullOrEmpty(s);
        }

        public static string decodeUrl( this string s) {
            return HttpUtility.UrlDecode(s);
        }

        public static string toUsPhoneNumber( this string s )
        {
            try
            {
                return string.Format("{0:###-###-####}", long.Parse(s));
            } catch(Exception ex) {
                return s;
            }
        }

        public static string StripHTML( this string input )
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }

        /// <summary>
        /// Remove HTML from string with Regex.
        /// </summary>
        public static string StripTagsRegex( this string source )
        {
            return Regex.Replace(source, "<.*?>", string.Empty);
        }

        /// <summary>
        /// Compiled regular expression for performance.
        /// </summary>
        static Regex _htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);

        /// <summary>
        /// Remove HTML from string with compiled Regex.
        /// </summary>
        public static string StripTagsRegexCompiled( this string source )
        {
            return _htmlRegex.Replace(source, string.Empty);
        }

        /// <summary>
        /// Remove HTML tags from string using char array.

        public static string StripHtmlCharArray( this string source )
        {
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for(int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if(let == '<')
                {
                    inside = true;
                    continue;
                }
                if(let == '>')
                {
                    inside = false;
                    continue;
                }
                if(!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }

        public static string toPostalFormat( this string source, bool longformat = false) 
        {
            if(source.Length == 5) {
                return source;
            }
            else if(source.Length == 9 && Regex.IsMatch(source,@"^\d+$") && longformat)
            {
                return source.Substring(0,5) + "-" + source.Substring(5, 4);
            }
            else if(source.Length == 9 && Regex.IsMatch(source, @"^\d+$") && !longformat)
            {
                return source.Substring(0, 5);
            }
            else
            {
                return source;
            }
        }
    }
}