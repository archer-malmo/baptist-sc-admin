﻿using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sitecore.Baptist.Feature.GeneralComponents.Repositories
{
  public interface IGeneralComponentsRepository : IModelRepository, IAbstractRepository<IRenderingModelBase>
  {
  }
}