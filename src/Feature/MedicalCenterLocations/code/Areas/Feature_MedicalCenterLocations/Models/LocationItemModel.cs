﻿using Sitecore.XA.Foundation.Mvc.Models;
using Sitecore.XA.Foundation.Variants.Abstractions.Models;
using System.Collections.Generic;
using Fortis.Model;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom;
using Sitecore.Data;
using Sitecore.ContentSearch;
using Sitecore.XA.Foundation.Multisite.LinkManagers;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.ContentSearch.Spatial.DataTypes;
using Sitecore.XA.Foundation.Search.Models;
using SolrNet.Attributes;
using Sitecore.Baptist.Foundation.Common.Utils;

namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Models
{

    public interface ILocationItemModel
    {
        IRenderingModel<ILocationItemItem, ILocationItemItem> RenderingModel { get; }
        IEnumerable<IItemWrapper> Children { get; }
    }

    public class LocationItemModel : VariantsRenderingModel
    {
        public IRenderingModel<ILocationItemItem, ILocationItemItem> RenderingModel { get; set; }
        public IEnumerable<IItemWrapper> Children { get; set; }
        public virtual string StyleDisplay { get; set; }
    }

    public class LocationItemRenderingModel : RenderingModelBase
    { 
        //Location Section
        [IndexField("itemid")]
        public virtual ID ItemID { get; set; }
        [IndexField("location_name")]
        public virtual string LocationName { get; set; }
        [IndexField("term_name")]
        public virtual string TermName { get; set; }
        [IndexField("short_name")]
        public virtual string ShortName { get; set; }
        [IndexField("address_line_1")]
        public virtual string AddressLine1 { get; set; }
        [IndexField("address_line_2")]
        public virtual string AddressLine2 { get; set; }
        [IndexField("city")]
        public virtual string City { get; set; }
        [IndexField("state")]
        public virtual string State { get; set; }
        [IndexField("postal_code")]
        public virtual string PostalCode { get; set; }
        [IndexField("latitude")]
        public virtual string Latitude { get; set; }
        [IndexField("longitude")]
        public virtual string Longitude { get; set; }
        [IndexField("coordinates")]
        public virtual Coordinates Coordinates { get; set; }

        //Location Meta Section
        [IndexField("clientid")]
        public virtual string ClientID { get; set; }
        [IndexField("is_publicly_searchable")]
        public virtual bool IsPubliclySearchable { get; set; }
        [IndexField("is_bmg")]
        public virtual bool IsActive { get; set; }
        [IndexField("is_bmg")]
        public virtual bool IsBMG { get; set; }
        [IndexField("import_taxonomy")]
        public virtual string ImportTaxonomy { get; set; }
        [IndexField("import_properties")]
        public virtual string ImportProperties { get; set; }
        [IndexField("location_type")]
        public virtual string LocationType { get; set; }

        //Location Contact Section
        [IndexField("main_phone_number")]
        public virtual string MainPhoneNumber { get; set; }
        [IndexField("main_email_address")]
        public virtual string MainEmailAddress { get; set; }
        [IndexField("additional_contact_information")]
        public virtual string AdditionalContactInformation { get; set; }
        [IndexField("emergency_room_phone_number")]
        public virtual string EmergencyRoomPhoneNumber { get; set; }

        //Location Content Section
        [IndexField("title")]
        public virtual string Title { get; set; }
        [IndexField("main_description")]
        public virtual string MainDescription { get; set; }

        //Location Hours Section
        [IndexField("hours_section_title")]
        public virtual string HoursSectionTitle { get; set; }
        [IndexField("hours_of_operation")]
        public virtual System.Collections.Specialized.NameValueCollection HoursOfOperation { get; set; }
        [IndexField("hours_info_message")]
        public virtual string HoursInfoMessage { get; set; }
        [IndexField("show_location_hours")]
        public virtual bool ShowLocationHours { get; set; }

        //ER Settings Section
        /// <summary>
        /// <para>Template: Location Item</para><para>Field: ERWaitURLKey</para><para>Data type: Single-Line Text</para>
        /// </summary>
        [IndexField("er_wait_url_key")]
        public virtual string ERWaitURLKey { get; set; }


        /// <summary>
        /// <para>Template: Location Item</para><para>Field: AlternateLocationListingPage</para><para>Data type: General Link</para>
        /// </summary>
        [IndexField("alternate_location_listing_page")]
        public virtual string AlternateLocationListingPage { get; set; }

        /// <summary>
        /// <para>Template: Location Item</para><para>Field: EnableAlternateLink</para><para>Data type: Checkbox</para>
        /// </summary>
        [IndexField("enable_alternate_link")]
        public virtual bool EnableAlternateLink { get; set; }

        /// <summary>
        /// <para>Template: Location Item</para><para>Field: OverrideWithMapsLink</para><para>Data type: Checkbox</para>
        /// </summary>
        [IndexField("override_with_maps_link")]
        public virtual bool OverrideWithMapsLink { get; set; }

        /// <summary>
        /// <para>Template: Location Item</para><para>Field: ServicesProvidedJson</para><para>Data type: Multi-Line Text</para>
        /// </summary>
        [IndexField("services_provided_json")]
        public virtual string ServicesProvidedJson { get; set; }

        /// <summary>
        /// <para>Template: Location Item</para><para>Field: CancerTypesJson</para><para>Data type: Single-Line Text</para>
        /// </summary>
        [IndexField("cancer_types_json")]
        public virtual string CancerTypesJson { get; set; }

        /// <summary>
        /// <para>Template: Location Item</para><para>Field: SchedulingCode</para><para>Data type: Single-Line Text</para>
        /// </summary>
        [IndexField("scheduling_code")]
        public virtual string SchedulingCode { get; set; }

        /// <summary>
        /// <para>Template: Location Item</para><para>Field: SchedulingLabel</para><para>Data type: Single-Line Text</para>
        /// </summary>
        [IndexField("scheduling_label")]
        public virtual string SchedulingLabel { get; set; }

        /// <summary>
        /// <para>Template: Location Item</para><para>Field: SchedulingType</para><para>Data type: Single-Line Text</para>
        /// </summary>
        [IndexField("scheduling_type")]
        public virtual string SchedulingType { get; set; }
    }


    public class LocationSearchResultItem
    {
        //Location Section
        [IndexField("itemid")]
        public virtual ID ItemId { get; set; }
        [IndexField("location_name")]
        public virtual string LocationName { get; set; }
        [IndexField("term_name")]
        public virtual string TermName { get; set; }
        [IndexField("short_name")]
        public virtual string ShortName { get; set; }
        [IndexField("address_line_1")]
        public virtual string AddressLine1 { get; set; }
        [IndexField("address_line_2")]
        public virtual string AddressLine2 { get; set; }
        [IndexField("city")]
        public virtual string City { get; set; }
        [IndexField("state")]
        public virtual string State { get; set; }
        [IndexField("postal_code")]
        public virtual string PostalCode { get; set; }
        [IndexField("latitude")]
        public virtual string Latitude { get; set; }
        [IndexField("longitude")]
        public virtual string Longitude { get; set; }
        [IndexField("coordinates")]
        public virtual Coordinates Coordinates { get; set; }


        //Location Meta Section
        [IndexField("clientid")]
        public virtual string ClientID { get; set; }
        [IndexField("is_publicly_searchable")]
        public virtual bool IsPubliclySearchable { get; set; }
        [IndexField("is_active")]
        public virtual bool IsActive { get; set; }
        [IndexField("is_bmg")]
        public virtual bool IsBMG { get; set; }
        [IndexField("import_taxonomy")]
        public virtual string ImportTaxonomy { get; set; }
        [IndexField("import_properties")]
        public virtual string ImportProperties { get; set; }
        [IndexField("location_type")]
        public virtual string LocationType { get; set; }

        //Location Contact Section
        [IndexField("main_phone_number")]
        public virtual string MainPhoneNumber { get; set; }
        [IndexField("main_email_address")]
        public virtual string MainEmailAddress { get; set; }
        [IndexField("additional_contact_information")]
        public virtual string AdditionalContactInformation { get; set; }
        [IndexField("emergency_room_phone_number")]
        public virtual string EmergencyRoomPhoneNumber { get; set; }

        //Location Content Section
        [IndexField("title")]
        public virtual string Title { get; set; }
        [IndexField("main_description")]
        public virtual string MainDescription { get; set; }

        //Location Hours Section
        [IndexField("hours_section_title")]
        public virtual string HoursSectionTitle { get; set; }
        [IndexField("hours_of_operation")]
        public virtual System.Collections.Specialized.NameValueCollection HoursOfOperation { get; set; }
        [IndexField("hours_info_message")]
        public virtual string HoursInfoMessage { get; set; }
        [IndexField("show_location_hours")]
        public virtual bool ShowLocationHours { get; set; }

        //ER Settings Section
        /// <summary>
        /// <para>Template: Location Item</para><para>Field: ERWaitURLKey</para><para>Data type: Single-Line Text</para>
        /// </summary>
        [IndexField("er_wait_url_key")]
        public virtual string ERWaitURLKey { get; set; }


        /// <summary>
        /// <para>Template: Location Item</para><para>Field: AlternateLocationListingPage</para><para>Data type: General Link</para>
        /// </summary>
        [IndexField("alternate_location_listing_page")]
        public virtual string AlternateLocationListingPage { get; set; }

        /// <summary>
        /// <para>Template: Location Item</para><para>Field: EnableAlternateLink</para><para>Data type: Checkbox</para>
        /// </summary>
        [IndexField("enable_alternate_link")]
        public virtual bool EnableAlternateLink { get; set; }

        /// <summary>
        /// <para>Template: Location Item</para><para>Field: OverrideWithMapsLink</para><para>Data type: Checkbox</para>
        /// </summary>
        [IndexField("override_with_maps_link")]
        public virtual bool OverrideWithMapsLink { get; set; }

        /// <summary>
        /// <para>Template: Location Item</para><para>Field: ServicesProvidedJson</para><para>Data type: Multi-Line Text</para>
        /// </summary>
        [IndexField("services_provided_json")]
        public virtual string ServicesProvidedJson { get; set; }

        /// <summary>
        /// <para>Template: Location Item</para><para>Field: CancerTypesJson</para><para>Data type: Single-Line Text</para>
        /// </summary>
        [IndexField("cancer_types_json")]
        public virtual string CancerTypesJson { get; set; }

        /// <summary>
        /// <para>Template: Location Item</para><para>Field: SchedulingCode</para><para>Data type: Single-Line Text</para>
        /// </summary>
        [IndexField("scheduling_code")]
        public virtual string SchedulingCode { get; set; }

        /// <summary>
        /// <para>Template: Location Item</para><para>Field: SchedulingLabel</para><para>Data type: Single-Line Text</para>
        /// </summary>
        [IndexField("scheduling_label")]
        public virtual string SchedulingLabel { get; set; }

        /// <summary>
        /// <para>Template: Location Item</para><para>Field: SchedulingType</para><para>Data type: Single-Line Text</para>
        /// </summary>
        [IndexField("scheduling_type")]
        public virtual string SchedulingType { get; set; }

        public virtual string Version { get; set; }
        
        public virtual float Distance { get; set; }

        public virtual float Score { get; set; }

    }    
    
     
}
