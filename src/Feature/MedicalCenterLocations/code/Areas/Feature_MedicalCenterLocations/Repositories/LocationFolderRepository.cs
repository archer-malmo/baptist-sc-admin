﻿using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Models;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using Sitecore.XA.Foundation.RenderingVariants.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Fields;
using Fortis.Model;
using static Sitecore.Configuration.Settings;
using Sitecore.XA.Foundation.Multisite;
using System.Collections.Specialized;
using Sitecore.Data.Items;

namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Repositories
{

    public class LocationFolderRepository : ModelRepository, ILocationFolderRepository, IModelRepository, IAbstractRepository<IRenderingModelBase>
    {

        public Item LocationItem { get; set; }

        public override IRenderingModelBase GetModel()
        {
            LocationFolderRenderingModel locationFolderRenderingModel = new LocationFolderRenderingModel();
            this.FillBaseProperties((object)locationFolderRenderingModel);

            locationFolderRenderingModel.Locations = this.GetLocations();

            return (IRenderingModelBase)locationFolderRenderingModel;
        }

        public virtual IEnumerable<Item> GetLocations()
        {
            if (this.Rendering.DataSourceItem != null)
            {             
                IEnumerable<Item> ielirm =  this.Rendering.DataSourceItem.Children.Where(x => x.TemplateName == "Location Item");
                
                //foreach (var ilitem in ielirm)
                //{
                    //Sitecore.Diagnostics.Log.Info("[DEBUG LOCATION]" + ilitem["Location Name"], this);
                //}

                return ielirm;

            }else
            {
                return null;
            }
        }
    }
}
