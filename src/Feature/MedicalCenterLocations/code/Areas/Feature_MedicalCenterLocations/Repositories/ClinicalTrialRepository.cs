﻿using Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom;
using Sitecore.Data.Items;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Repositories
{
    public class ClinicalTrialRepository : ModelRepository, IClinicalTrialRepository, IModelRepository, IAbstractRepository<IRenderingModelBase>
    {

        public Item ClinicalTrialItem { get; set; }

        public override IRenderingModelBase GetModel()
        {
            ClinicalTrialRenderingModel ctrial = new ClinicalTrialRenderingModel();
            this.FillBaseProperties((object)ctrial);

            ctrial.StudyNumber = this.GetStudyNumber();
            ctrial.FormalStudyName = this.GetFormalStudyName();
            ctrial.InformalStudyName = this.GetInformalStudyName();

            ctrial.AnatomicSite = this.GetAnatomicSite();
            ctrial.Histology = this.GetHistology();            
            ctrial.Mutation = this.GetMutation();
            ctrial.Stage = this.GetStage();

            ctrial.TreatmentPhase = this.GetTreatmentPhase();
            ctrial.StudyLink = this.GetStudyLink();
            ctrial.LocationsWhereOpen = this.GetLocationsWhereOpen();

            ctrial.AdditionalStatusMessages = this.GetAdditionalStatusMessages();
            ctrial.HideTrialFromListing = this.GetHideTrialFromListing();

            ctrial.TrialPhase = this.GetTrialPhase();
            ctrial.TrialType = this.GetTrialType();
            ctrial.PI = this.GetPI();

            return ctrial;
        }

        protected virtual string GetStudyNumber()
        {
            if(this.ClinicalTrialItem != null)
                return this.ClinicalTrialItem[ClinicalTrialTemplate.ClinicalTrial.Fields.StudyNumber] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[ClinicalTrialTemplate.ClinicalTrial.Fields.StudyNumber] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetFormalStudyName()
        {
            if(this.ClinicalTrialItem != null)
                return this.ClinicalTrialItem[ClinicalTrialTemplate.ClinicalTrial.Fields.FormalStudyName] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[ClinicalTrialTemplate.ClinicalTrial.Fields.FormalStudyName] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetInformalStudyName()
        {
            if(this.ClinicalTrialItem != null)
                return this.ClinicalTrialItem[ClinicalTrialTemplate.ClinicalTrial.Fields.InformalStudyName] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[ClinicalTrialTemplate.ClinicalTrial.Fields.InformalStudyName] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetAnatomicSite()
        {
            if(this.ClinicalTrialItem != null)
                return this.ClinicalTrialItem[ClinicalTrialTemplate.ClinicalTrial.Fields.AnatomicSite] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[ClinicalTrialTemplate.ClinicalTrial.Fields.AnatomicSite] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetHistology()
        {
            if(this.ClinicalTrialItem != null)
                return this.ClinicalTrialItem[ClinicalTrialTemplate.ClinicalTrial.Fields.Histology] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[ClinicalTrialTemplate.ClinicalTrial.Fields.Histology] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetMutation()
        {
            if(this.ClinicalTrialItem != null)
                return this.ClinicalTrialItem[ClinicalTrialTemplate.ClinicalTrial.Fields.Mutation] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[ClinicalTrialTemplate.ClinicalTrial.Fields.Mutation] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetStage()
        {
            if(this.ClinicalTrialItem != null)
                return this.ClinicalTrialItem[ClinicalTrialTemplate.ClinicalTrial.Fields.Stage] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[ClinicalTrialTemplate.ClinicalTrial.Fields.Stage] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetTreatmentPhase()
        {
            if(this.ClinicalTrialItem != null)
                return this.ClinicalTrialItem[ClinicalTrialTemplate.ClinicalTrial.Fields.TreatmentPhase] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[ClinicalTrialTemplate.ClinicalTrial.Fields.TreatmentPhase] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetStudyLink()
        {
            if(this.ClinicalTrialItem != null)
                return this.ClinicalTrialItem[ClinicalTrialTemplate.ClinicalTrial.Fields.StudyLink] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[ClinicalTrialTemplate.ClinicalTrial.Fields.StudyLink] ?? string.Empty;
            return string.Empty;
        }

        protected virtual IEnumerable<Guid> GetLocationsWhereOpen()
        {
            if(this.ClinicalTrialItem != null)
                return this.ClinicalTrialItem.Fields[ClinicalTrialTemplate.ClinicalTrial.Fields.LocationsWhereOpen].Value.Split('|').Select(s => Guid.Parse(s)).AsEnumerable<Guid>();
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem.Fields[ClinicalTrialTemplate.ClinicalTrial.Fields.LocationsWhereOpen].Value.Split('|').Select(s => Guid.Parse(s)).AsEnumerable<Guid>();
            return (IEnumerable<Guid>)null;
        }

        protected virtual string GetAdditionalStatusMessages()
        {
            if(this.ClinicalTrialItem != null)
                return this.ClinicalTrialItem[ClinicalTrialTemplate.ClinicalTrial.Fields.AdditionalStatusMessages] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[ClinicalTrialTemplate.ClinicalTrial.Fields.AdditionalStatusMessages] ?? string.Empty;
            return string.Empty;
        }

        protected virtual bool GetHideTrialFromListing()
        {
            if(this.ClinicalTrialItem != null)
                return this.ClinicalTrialItem != null && this.ClinicalTrialItem.Fields[ClinicalTrialTemplate.ClinicalTrial.Fields.HideTrialFromListing] != null && this.ClinicalTrialItem.Fields[ClinicalTrialTemplate.ClinicalTrial.Fields.HideTrialFromListing].Value != string.Empty && this.ClinicalTrialItem.Fields[ClinicalTrialTemplate.ClinicalTrial.Fields.HideTrialFromListing].Value == "1";
            else
                return this.Rendering.DataSourceItem != null && this.Rendering.DataSourceItem.Fields[ClinicalTrialTemplate.ClinicalTrial.Fields.HideTrialFromListing] != null && this.Rendering.DataSourceItem.Fields[ClinicalTrialTemplate.ClinicalTrial.Fields.HideTrialFromListing].Value != string.Empty && this.ClinicalTrialItem.Fields[ClinicalTrialTemplate.ClinicalTrial.Fields.HideTrialFromListing].Value == "1";
        }

        protected virtual string GetTrialPhase()
        {
            if(this.ClinicalTrialItem != null)
                return this.ClinicalTrialItem[ClinicalTrialTemplate.ClinicalTrial.Fields.TrialPhase] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[ClinicalTrialTemplate.ClinicalTrial.Fields.TrialPhase] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetTrialType()
        {
            if(this.ClinicalTrialItem != null)
                return this.ClinicalTrialItem[ClinicalTrialTemplate.ClinicalTrial.Fields.TrialType] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[ClinicalTrialTemplate.ClinicalTrial.Fields.TrialType] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetPI()
        {
            if(this.ClinicalTrialItem != null)
                return this.ClinicalTrialItem[ClinicalTrialTemplate.ClinicalTrial.Fields.PI] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[ClinicalTrialTemplate.ClinicalTrial.Fields.PI] ?? string.Empty;
            return string.Empty;
        }
    }
}