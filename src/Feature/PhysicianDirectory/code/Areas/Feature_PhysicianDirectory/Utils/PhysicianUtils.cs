﻿using Newtonsoft.Json;
using Sitecore.Data.Items;
using Sitecore.XA.Foundation.Mvc.Controllers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using Sitecore.Baptist.Feature.PhysicianDirectory.Areas.Feature_PhysicianDirectory.Models;
using Sitecore.Baptist.Feature.PhysicianDirectory.Areas.Feature_PhysicianDirectory.Repositories;
using Sitecore.Baptist.Feature.PhysicianDirectory.Templates.Custom;
using Sitecore.Baptist.Foundation.Common.Utils;
using Sitecore.Baptist.Foundation.Common.Extensions;
using System.Collections.Specialized;
using Sitecore.Diagnostics;
using System.Web.Script.Serialization;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Models;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.Data;
using System.Device.Location;
using Sitecore.Baptist.Foundation.Common.Models;

namespace Sitecore.Baptist.Feature.PhysicianDirectory.Areas.Feature_PhysicianDirectory.Utils
{
    public class PhysicianUtils : StandardController
    {
        public HttpRequestBase req { get; set; }
        public HttpResponseBase respnse { get; set; }

        public PhysicianListing getSpecialtiesAndCounts( bool isBMGOnly = false, string folderPath = "/sitecore/content/Baptist/Baptist/Baptistonline/United States/Data/Physicians", bool isBCCOnly = false )
        {

            PhysicianListing pl = new PhysicianListing();

            Dictionary<string, int> specialtiescounts = new Dictionary<string, int>();

            List<PhysicianRenderingModel> doctors = this.getAllPhysicians(isBMGOnly, folderPath, isBCCOnly);

            // Log.Info("doctors count " + isBCCOnly.ToString() + " " + doctors.Count.ToString(), true);

            //specialtiescounts.Add("distinctspecialties", specialties.Count());
            //debugs
            //pl.Specialties = specialtiescounts;
            //return pl;
            //Get Specialty Counts
            pl.Specialties = this.getSpecialtiesAndCounts(doctors, isBMGOnly, isBCCOnly);

            return pl;
        }

        public List<Specialty> getSpecialtiesAndCounts( List<PhysicianRenderingModel> doctors, bool isBMGOnly = false, bool isBCCOnly = false )
        {
            List<string> holdspecialties = new List<string>();
            List<Specialty> specialties = new List<Specialty>();

            foreach(var phys in doctors)
            {
                if(isBCCOnly)
                {
                    foreach(string specialty in phys.BCCSpecialties.Cast<string>().Select(e => phys.BCCSpecialties[e]))
                    {
                        //Log.Info("Specialty in BCC" + specialty, true);
                        holdspecialties.Add(specialty);
                    }
                }
                else
                {
                    foreach(string specialty in phys.Specialties.Cast<string>().Select(e => phys.Specialties[e]))
                    {
                        holdspecialties.Add(specialty);
                    }
                }
            }

            //Get the unique specialties
            List<string> distspecialties = holdspecialties.Distinct().ToList<string>();

            foreach(string spec in distspecialties)
            {
                int cnt = 0;
                foreach(PhysicianRenderingModel physician in doctors)
                {
                    NameValueCollection nvm = physician.Specialties;

                    if(isBCCOnly)
                    {
                        nvm = physician.BCCSpecialties;
                    }

                    List<string> values = nvm.Cast<string>().Select(e => nvm[e]).ToList<string>();
                    if(values.Contains(spec))
                    {
                        cnt++;
                    }
                }

                //int cnt = lprm.Where(x => x.Specialties.Cast<string>().Select(e => x.Specialties[e]).Equals(spec)).Count();
                //Log.Info("SPECIALTY: " + HttpUtility.UrlDecode(spec), this);
                //Log.Info("SPEC CNT:  " + cnt, this);

                Specialty specfinal = new Specialty();
                specfinal.count = cnt;

                specfinal.specialtyName = HttpUtility.UrlDecode(spec);
                if(!string.IsNullOrEmpty(specfinal.specialtyName))
                {
                    //Log.Info("DEBUG Physician Specialty: " + specfinal.specialtyName, true);
                    specfinal.specialtyUrl = specfinal.specialtyName.toSlug();
                    //Log.Info("URL SET: " + specfinal.spi)
                    if(isBMGOnly)
                    {
                        specfinal.specialtyUrl += "?bmgonly=1";
                    }
                    //Log.Info("URL SET:" + specfinal.specialtyUrl, true);
                }
                specialties.Add(specfinal);
            }

            return specialties;
        }


        public List<PhysicianRenderingModel> getAllPhysicians( bool isBMGOnly = false, string folderPath = "/sitecore/content/Baptist/Baptist/Baptistonline/United States/Data/Physicians", bool isBCCOnly = false )
        {
            IEnumerable<Item> physicians = null;
            if(Sitecore.Context.Database != null)
            {
                physicians = Sitecore.Context.Database.GetItem(folderPath).Children.Where(x => x.TemplateName == "Physician");
            }
            else
            {
                physicians = Sitecore.Data.Database.GetDatabase("web").GetItem(folderPath).Children.Where(x => x.TemplateName == "Physician");
            }

            List<PhysicianRenderingModel> lprm = new List<PhysicianRenderingModel>();
            foreach(var phys in physicians)
            {
                PhysicianRepository pr = new PhysicianRepository();
                pr.PhysicianItem = phys;

                PhysicianRenderingModel prm = (PhysicianRenderingModel)pr.GetModel();

                if(isBMGOnly && prm.IsBMG)
                {
                    //Log.Info("BMG ONLY SET: " + prm.IsBMG, this);
                    lprm.Add(prm);

                }
                else if(!isBMGOnly)
                {
                    //Log.Info("BMG ONLY NOT SET", this);
                    lprm.Add(prm);
                }
            }

            if(isBCCOnly)
            {
                //Log.Info("Doctors where BCC is true " + lprm.Where(x => x.IsBCC).Count().ToString(),true);
                return lprm.Where(x => x.IsBCC).ToList<PhysicianRenderingModel>();
            }
            return lprm;
        }

        public PhysicianRenderingModel getPhysicianByUrl( string physicianurl )
        {

            List<PhysicianRenderingModel> lprm = this.getAllPhysicians();

            PhysicianRenderingModel prm = lprm.Where(x => PhysicianHelper.formatPhysicianName(x).toSlug() == physicianurl).FirstOrDefault();

            return prm;

        }

        public PhysicianRenderingModel getPhysicianByOneCareID( string onecareid )
        {
            List<PhysicianRenderingModel> lprm = this.getAllPhysicians();

            PhysicianRenderingModel prm = lprm.Where(x => x.OneCareID == onecareid).FirstOrDefault();

            return prm;
        }

        public List<PhysicianRenderingModel> getSchedulingPhysicians( string folderPath = "/sitecore/content/Baptist/Baptist/Baptistonline/United States/Data/Physicians" )
        {

            IEnumerable<Item> physicians = null;

            ID oncarefieldid = ID.Parse("{02EFDCCF-7F6D-43E4-A1C2-5A6DAD46BDC9}");
            ID featuredprovider = ID.Parse("{0AD240DC-E6E1-4612-8C97-D071F7821CB6}");
            ID listedprovider = ID.Parse("{DB3FE078-05BD-4098-88FE-B324F03DDC32}");
            if(Sitecore.Context.Database != null)
            {
                //physicians = Sitecore.Context.Database.GetItem(folderPath).Children.Where(x => x.TemplateName == "Physician" && x.Fields[oncarefieldid].ToString().HasValue());
                physicians = Sitecore.Context.Database.GetItem(folderPath).Children.Where(x => x.TemplateName == "Physician" && x.Fields[listedprovider].ToString() == "1");
            }
            else
            {
                //physicians = Sitecore.Data.Database.GetDatabase("web").GetItem(folderPath).Children.Where(x => x.TemplateName == "Physician" && x.Fields[oncarefieldid].ToString().HasValue());
                physicians = physicians = Sitecore.Data.Database.GetDatabase("web").GetItem(folderPath).Children.Where(x => x.TemplateName == "Physician" && x.Fields[listedprovider].ToString() == "1");
            }

            List<PhysicianRenderingModel> lprm = new List<PhysicianRenderingModel>();

            foreach(var phys in physicians)
            {
                PhysicianRepository pr = new PhysicianRepository();
                pr.PhysicianItem = phys;

                PhysicianRenderingModel prm = (PhysicianRenderingModel)pr.GetModel();

                lprm.Add(prm);
            }
            //string resp = JsonConvert.SerializeObject(lprm);
            return lprm;
        }


        public List<PhysicianWithDistance> getSchedulingPhysiciansFromPoint( double latitude, double longitude, string folderPath = "/sitecore/content/Baptist/Baptist/Baptistonline/United States/Data/Physicians" )
        {

            IEnumerable<Item> physicians = null;

            ID oncarefieldid = ID.Parse("{02EFDCCF-7F6D-43E4-A1C2-5A6DAD46BDC9}");
            ID featuredprovider = ID.Parse("{0AD240DC-E6E1-4612-8C97-D071F7821CB6}");
            ID listedprovider = ID.Parse("{DB3FE078-05BD-4098-88FE-B324F03DDC32}");

            if(Sitecore.Context.Database != null)
            {
                //physicians = Sitecore.Context.Database.GetItem(folderPath).Children.Where(x => x.TemplateName == "Physician" && x.Fields[oncarefieldid].ToString().HasValue());
                physicians = Sitecore.Context.Database.GetItem(folderPath).Children.Where(x => x.TemplateName == "Physician" && x.Fields[listedprovider].ToString() == "1" );
            }
            else
            {
                //physicians = Sitecore.Data.Database.GetDatabase("web").GetItem(folderPath).Children.Where(x => x.TemplateName == "Physician" && x.Fields[oncarefieldid].ToString().HasValue());
                physicians = physicians = Sitecore.Data.Database.GetDatabase("web").GetItem(folderPath).Children.Where(x => x.TemplateName == "Physician" && x.Fields[listedprovider].ToString() == "1" );
            }

            //List<PhysicianRenderingModel> lprm = new List<PhysicianRenderingModel>();

            List<PhysicianWithDistance> lprm = new List<PhysicianWithDistance>();


            foreach(var phys in physicians)
            {

                PhysicianRepository pr = new PhysicianRepository();
                pr.PhysicianItem = phys;

                PhysicianRenderingModel prm = (PhysicianRenderingModel)pr.GetModel();

                PhysicianWithDistance pwd = new PhysicianWithDistance();

                pwd.physician = prm;

                if(prm.PrimaryLocationLatitude.HasValue() && prm.PrimaryLocationLongitude.HasValue())
                {

                    var sCoord = new GeoCoordinate(latitude, longitude);

                    double lat = double.Parse(prm.PrimaryLocationLatitude);
                    double lng = double.Parse(prm.PrimaryLocationLongitude);

                    var eCoord = new GeoCoordinate(lat, lng);

                    double distance = sCoord.GetDistanceTo(eCoord);

                    pwd.distance = (distance / 1609.344);

                }
                else
                {
                    pwd.distance = 9999999;
                }

                lprm.Add(pwd);
            }
            //string resp = JsonConvert.SerializeObject(lprm);
            return lprm;
        }


        //Because
        public List<PhysicianLocation> getDescendantPhysicianLocations( string folderPath = "/sitecore/Baptist/Baptist/Baptistonline/United Sates/Data/Locations" )
        {
            DateTime today = DateTime.Now;
            string format = "MM/dd/yyyy HH:mm:ss";

            string cachekey = "Physician_Locations";
            string expcachekey = "Physician_Location_List_Expires";

            List<PhysicianLocation> lirm = new List<PhysicianLocation>();

            //return Content(PhysicianListingCacheManager.GetCache(expcachekey).HasValue().ToString());

            if(PhysicianLocationCacheManger.GetCache(expcachekey).HasValue() && PhysicianLocationCacheManger.GetCache(expcachekey).CacheIsExpired() == false)
            {
                //return Content("Nothing");       
                //Log.Info("Location cache hit", this);
                lirm = PhysicianLocationCacheManger.GetListingCache(cachekey);
            }
            else
            {


                //Log.Info("Location cache miss", this);

            }

            //Sitecore.Diagnostics.Log.Info("[hospital count] " + lirm.Count().ToString(), this);
            return lirm;
        }



        public PhysicianListingBySpecialtyCached getPhysiciansBySpecialty( string specialty, bool isBMGOnly = false, string folderPath = "/sitecore/content/Baptist/Baptist/Baptistonline/United States/Data/Physicians", bool isBCCOnly = false )
        {

            string cachekey = "Physician_List_Index_Data";
            string expcachekey = "Physician_List_Index_Expires";

            //PhysicianListingBySpecialty plbs = new PhysicianListingBySpecialty();

            PhysicianListingBySpecialtyCached plbscached = new PhysicianListingBySpecialtyCached();


            plbscached.doctors = new List<PhysicianListingRow>();

            IEnumerable<Item> physicians = null;
            if(Sitecore.Context.Database != null)
            {
                physicians = Sitecore.Context.Database.GetItem(folderPath).Children.Where(x => x.TemplateName == "Physician");
            }
            else
            {
                physicians = Sitecore.Data.Database.GetDatabase("web").GetItem(folderPath).Children.Where(x => x.TemplateName == "Physician");
            }

            foreach(var phys in physicians)
            {
                PhysicianRepository pr = new PhysicianRepository();
                pr.PhysicianItem = phys;

                PhysicianRenderingModel prm = (PhysicianRenderingModel)pr.GetModel();

                PhysicianListingRow plr = new PhysicianListingRow();
                plr.ImageImportedPath = prm.ImageImportedPath;
                plr.IsBMG = prm.IsBMG;


                if(prm.HideSchedulingWidget == false && prm.OneCareVT.HasValue() && prm.OneCareID.HasValue())
                {
                    plr.ScheduleAppt = true;
                }
                else
                {
                    plr.ScheduleAppt = false;
                }

                plr.LastName = prm.LastName;
                plr.PhysicianName = PhysicianHelper.formatPhysicianName(prm);

                plr.specialties = PhysicianHelper.getPhysicianSpecialties(prm, isBCCOnly);
                //PhysicianLocation physlocation = PhysicianHelper.getPrimaryLocation(prm);                

                PhysicianLocation physlocation = new PhysicianLocation();

                if(prm.PrimaryLocationID.HasValue() && prm.PrimaryLocationName.HasValue())
                {

                    PhysicianLocation pln = new PhysicianLocation();
                    pln.Address1 = prm.PrimaryLocationAddress1;
                    pln.Address2 = prm.PrimaryLocationAddress2;
                    pln.City = prm.PrimaryLocationCity;
                    pln.ClientID = prm.PrimaryLocationID;
                    pln.ItemID = ID.Parse(prm.PrimaryLocation);
                    pln.Latitude = prm.PrimaryLocationLatitude;
                    pln.Longitude = prm.PrimaryLocationLongitude;
                    pln.PostalCode = prm.PrimaryLocationPostalCode;
                    pln.State = prm.PrimaryLocationState;
                    pln.LocationName = prm.PrimaryLocationName;
                    pln.LocationType = prm.PrimaryLocationType;
                    pln.MainPhoneNumber = prm.PrimaryLocationMainPhoneNumber;
                    physlocation = pln;

                }
                else
                {
                    //Sitecore.Diagnostics.Log.Info("Using Legacy Search", this);
                    physlocation = PhysicianHelper.getPrimaryLocation(prm);
                }

                plr.location = physlocation;



                if(isBCCOnly && prm.IsBCC)
                {

                    if(isBMGOnly && prm.IsBMG && this.physicianHasSpecialty(prm.BCCSpecialties, specialty))
                    {
                        plbscached.doctors.Add(plr);

                        //lprm.Add(prm);
                    }
                    else if(!isBMGOnly && this.physicianHasSpecialty(prm.BCCSpecialties, specialty))
                    {
                        plbscached.doctors.Add(plr);
                        //lprm.Add(prm);
                    }
                }
                else
                {
                    if(isBMGOnly && prm.IsBMG && this.physicianHasSpecialty(prm.Specialties, specialty))
                    {
                        plbscached.doctors.Add(plr);

                        //lprm.Add(prm);
                    }
                    else if(!isBMGOnly && this.physicianHasSpecialty(prm.Specialties, specialty))
                    {
                        plbscached.doctors.Add(plr);
                        //lprm.Add(prm);
                    }
                }
            }

            //plbs.doctors = lprm.OrderByDescending(m => m.IsBMG).ThenBy( n => n.LastName).ToList<PhysicianRenderingModel>();
            //plbs.Specialty = this.getSpecialtyName(specialty);            
            plbscached.Specialty = this.getSpecialtyName(specialty, isBCCOnly, folderPath);
            plbscached.doctors = plbscached.doctors.OrderByDescending(m => m.IsBMG).ThenBy(n => n.LastName).ToList<PhysicianListingRow>();
            return plbscached;
        }


        public bool physicianHasSpecialty( NameValueCollection specialties, string specialty )
        {
            List<string> values = specialties.Cast<string>().Select(e => HttpUtility.UrlDecode(specialties[e]).toSlug()).ToList<string>();


            if(values.Contains(specialty))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string getSpecialtyName( string specialty, bool isBCCOnly = false, string folderPath = "/sitecore/content/Baptist/Baptist/Baptistonline/United States/Data/Physicians" )
        {

            List<Specialty> spec = this.getSpecialtiesAndCounts(this.getAllPhysicians(false, folderPath, isBCCOnly), isBCCOnly: isBCCOnly);
            return spec.Where(n => n.specialtyUrl == specialty).First().specialtyName;

        }

        public dynamic solrPhysicianQuery( string query )
        {
            try
            {
                string databaseName = "";
                if(Sitecore.Context.Database == null)
                {
                    databaseName = Sitecore.Data.Database.GetDatabase("web").Name.ToLower();
                }
                else
                {
                    databaseName = Sitecore.Context.Database.Name.ToLower();
                }

                string index_name = string.Format("baptist_physician_{0}_index", databaseName);

                string serviceBaseAddress = Sitecore.Configuration.Settings.GetSetting("ContentSearch.Solr.ServiceBaseAddress");

                //address of index core
                string coreaddress = serviceBaseAddress + "/" + index_name;

                string requesturl = coreaddress + "/select?" + query;

                //Sitecore.Diagnostics.Log.Info(requesturl, this);

                HttpWebRequest hrquest = (HttpWebRequest)WebRequest.Create(requesturl);

                using(HttpWebResponse response = hrquest.GetResponse() as HttpWebResponse)
                {
                    StreamReader readStream = new StreamReader(response.GetResponseStream());
                    string rspns = readStream.ReadToEnd();

                    dynamic objt = JsonConvert.DeserializeObject<dynamic>(rspns);

                    return objt;
                }

            }
            catch(Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, this);
                return null;
            }
        }


        public static List<PhysicianWithDistance> physResultsToModel( dynamic results )
        {

            try
            {
                List<PhysicianWithDistance> lpwd = new List<PhysicianWithDistance>();

                Sitecore.Data.Database db = Sitecore.Context.Database;

                foreach(var result in results)
                {
                    PhysicianWithDistance pwd = new PhysicianWithDistance();

                    PhysicianRepository prepos = new PhysicianRepository();
                    prepos.PhysicianItem = db.GetItem((string)result._fullpath);
                    pwd.physician = (PhysicianRenderingModel)prepos.GetModel();

                    //pwd.distance = Double.Parse(string.Format("{0}", result.user_distance));
                    pwd.distance = Double.Parse(string.Format("{0}", result._dist_));
                    pwd.score = Double.Parse(string.Format("{0}", result.score));

                    lpwd.Add(pwd);
                }

                return lpwd;
            }catch(Exception ex) {
                return new List<PhysicianWithDistance>();
            }
        }

        public static string searchScopeFromContext( string sitepath, string removeString = "/sitecore/content/Baptist/Baptist/" ) {
            string[] pathSplit = sitepath.Replace(removeString, "").Split('/');
            return pathSplit[0].ToLower();
        }
    }

    //Physician List By Specialty Cache
    public static class PhysicianListingBySpecialtyCacheManager
    {
        private static readonly BaptistCustomCache Cache;

        static PhysicianListingBySpecialtyCacheManager()
        {
            Cache = new BaptistCustomCache("Baptist_Physician_Listing_Specialty", StringUtil.ParseSizeString("10MB"));
        }

        public static string GetCache(string key)
        {
            return Cache.GetString(key);
        }

        public static void SetCache(string key, string value)
        {            
            Cache.SetString(key, value);
        }

        public static PhysicianListingBySpecialtyCached GetListingCache( string key )
        {
            string serial = Cache.GetString(key);
            return JsonConvert.DeserializeObject<PhysicianListingBySpecialtyCached>(serial);
        }

        public static void SetListingCache(string key, PhysicianListingBySpecialtyCached value)
        {
            string storagedvalue = JsonConvert.SerializeObject(value);            
            Cache.SetString(key, storagedvalue);
        }
    }


    //Physicican Listing Index
    public static class PhysicianListingCacheManager
    {
        private static readonly BaptistCustomCache Cache;

        static PhysicianListingCacheManager()
        {
            Cache = new BaptistCustomCache("Baptist_Physician_Listing_Index", StringUtil.ParseSizeString("10MB"));
        }

        public static string GetCache( string key )
        {
            return Cache.GetString(key);
        }

        public static void SetCache( string key, string value )
        {
            Cache.SetString(key, value);
        }

        public static PhysicianListing GetListingCache( string key )
        {
            string serial = Cache.GetString(key);
            //JavaScriptSerializer jserial = new JavaScriptSerializer();
            //jserial.RecursionLimit = 5000;

            return JsonConvert.DeserializeObject<PhysicianListing>(serial);
            //return jserial.Deserialize<PhysicianListing>(serial);
        }

        public static void SetListingCache( string key, PhysicianListing value )
        {
            string storagedvalue = JsonConvert.SerializeObject(value);
            //JavaScriptSerializer jserial = new JavaScriptSerializer();
            //jserial.RecursionLimit = 5000;
            //string storagedvalue = jserial.Serialize(value);
                                  
            Cache.SetString(key, storagedvalue);
        }
    }

    public static class PhysicianLocationCacheManger
    {
        private static readonly BaptistCustomCache Cache;

        static PhysicianLocationCacheManger()
        {
            Cache = new BaptistCustomCache("Baptist_Physician_Location_Listings", StringUtil.ParseSizeString("10MB"));
        }

        public static string GetCache( string key )
        {
            return Cache.GetString(key);
        }

        public static void SetCache( string key, string value )
        {            
            Cache.SetString(key, value);
        }

        public static List<PhysicianLocation> GetListingCache( string key )
        {
            string serial = Cache.GetString(key);
            //JavaScriptSerializer jserial = new JavaScriptSerializer();
            //jserial.RecursionLimit = 5000;

            return JsonConvert.DeserializeObject<List<PhysicianLocation>>(serial);
            //return jserial.Deserialize<PhysicianListing>(serial);
        }

        public static void SetListingCache( string key, List<PhysicianLocation> value )
        {
            string storagedvalue = JsonConvert.SerializeObject(value);
            //JavaScriptSerializer jserial = new JavaScriptSerializer();
            //jserial.RecursionLimit = 5000;
            //string storagedvalue = jserial.Serialize(value);            
            Cache.SetString(key, storagedvalue);
        }
    }

}
