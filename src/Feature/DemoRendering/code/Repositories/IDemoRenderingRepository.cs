﻿using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sitecore.Baptist.Feature.DemoRendering.Repositories
{
  public interface IDemoRenderingRepository : IModelRepository, IAbstractRepository<IRenderingModelBase>
  { 
  }
}