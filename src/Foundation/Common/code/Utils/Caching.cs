﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Caching;
using Sitecore;

namespace Sitecore.Baptist.Foundation.Common.Utils
{
   public class BaptistCustomCache : CustomCache {
        
        public BaptistCustomCache(string name, long maxSize) : base(name, maxSize)
        {

        }

        new public void SetString(string key, string value) {
            base.SetString(key, value);
        }

        new public string GetString(string key)
        {
            return base.GetString(key);
        }        
   }

   public static class CacheStringUtils {

        public static bool CacheIsExpired( this string s )
        {

            DateTime today = DateTime.Now;

            string format = "MM/dd/yyyy HH:mm:ss";

            if(DateTime.Parse(s).CompareTo(DateTime.Now) > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

    }
}