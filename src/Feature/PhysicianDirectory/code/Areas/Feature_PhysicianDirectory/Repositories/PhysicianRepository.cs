﻿using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using Sitecore.Baptist.Feature.PhysicianDirectory.Areas.Feature_PhysicianDirectory.Controllers;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Baptist.Feature.PhysicianDirectory.Templates.Custom;
using System.Collections.Specialized;
using System;
using Newtonsoft.Json;

namespace Sitecore.Baptist.Feature.PhysicianDirectory.Areas.Feature_PhysicianDirectory.Repositories
{
    public class PhysicianRepository : ModelRepository, IPhysicianRepository, IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        public Item PhysicianItem { get; set; }

        public override IRenderingModelBase GetModel()
        {
            PhysicianRenderingModel physicianRenderingModel = new PhysicianRenderingModel();
            try
            {
                this.FillBaseProperties((object)physicianRenderingModel);
            }
            catch(Exception ex)
            {                
            }

            //set physician section fields
            physicianRenderingModel.FirstName = this.GetFirstName();
            physicianRenderingModel.LastName = this.GetLastName();
            physicianRenderingModel.MIddleName = this.GetMiddleName();
            physicianRenderingModel.PreferredName = this.GetPreferredName();
            physicianRenderingModel.Suffix = this.GetSuffix();
            physicianRenderingModel.ProfessionalTitles = this.GetProfessionalTitles();
            physicianRenderingModel.SpecialDesignations = this.GetSpecialDesignations();
            physicianRenderingModel.Specialties = this.GetSpecialties();
            physicianRenderingModel.SchedulingSpecialties = this.GetSchedulingSpecialties();
            physicianRenderingModel.BCCSpecialties = this.GetBCCSpecialties();
            physicianRenderingModel.Languages = this.GetLanguages();
            physicianRenderingModel.TreatmentsAndServices = this.GetTreatmentsAndServices();
            physicianRenderingModel.DiseasesAndConditions = this.GetDiseasesAndConditions();

            //set physician met fields
            physicianRenderingModel.ClientID = this.GetClientID();
            physicianRenderingModel.IsActive = this.GetIsActive();
            physicianRenderingModel.IsBMG = this.GetIsBMG();
            physicianRenderingModel.IsBCC = this.GetIsBCC();
            physicianRenderingModel.ImportTaxonomy = this.GetImportTaxonomy();
            physicianRenderingModel.ImportProperties = this.GetImportProperties();
            physicianRenderingModel.ImageImportedPath = this.GetImageImportedPath();
            physicianRenderingModel.Regions = this.GetRegions();

            //set physician contact
            physicianRenderingModel.PrimaryLocationID = this.GetPrimaryLocationID();
            physicianRenderingModel.PrimaryPhoneNumber = this.GetPrimaryPhoneNumber();
            physicianRenderingModel.PrimaryPhoneIsPublic = this.GetPrimaryPhoneIsPublic();
            physicianRenderingModel.Locations = this.GetLocations();
            physicianRenderingModel.HospitalAffiliations = this.GetHospitalAffiliations();
            physicianRenderingModel.PrimaryLocationName = this.GetPrimaryLocationName();
            physicianRenderingModel.PrimaryLocationAddress1 = this.GetPrimaryLocationAddress1();
            physicianRenderingModel.PrimaryLocationAddress2 = this.GetPrimaryLocationAddress2();
            physicianRenderingModel.PrimaryLocationCity = this.GetPrimaryLocationCity();
            physicianRenderingModel.PrimaryLocationState = this.GetPrimaryLocationState();
            physicianRenderingModel.PrimaryLocationPostalCode = this.GetPrimaryLocationPostalCode();
            physicianRenderingModel.PrimaryLocationLatitude = this.GetPrimaryLocationLatitude();
            physicianRenderingModel.PrimaryLocationLongitude = this.GetPrimaryLocationLongitude();
            physicianRenderingModel.PrimaryLocationMainPhoneNumber = this.GetPrimaryLocationMainPhoneNumber();
            physicianRenderingModel.PrimaryLocationType = this.GetPrimaryLocationType();
            physicianRenderingModel.PrimaryLocation = this.GetPrimaryLocation();

            //set physician biographical
            physicianRenderingModel.Bio = this.GetBio();
            physicianRenderingModel.Education = this.GetEducation();
            physicianRenderingModel.ProfessionalRecognition = this.GetProfessionalRecognition();
            physicianRenderingModel.MeetTheDoctor = this.GetMeetTheDoctor();

            //one care sections
            physicianRenderingModel.OneCareID = this.GetOneCareID();
            physicianRenderingModel.OneCareVT = this.GetOneCareVT();
            physicianRenderingModel.HideSchedulingWidget = this.GetHideSchedulingWidget();
            physicianRenderingModel.FeaturedProvider = this.GetFeaturedProvider();
            physicianRenderingModel.ListedProvider = this.GetListedProvider();

            //media resources
            physicianRenderingModel.MediaResources = this.GetMediaResources();

            return (IRenderingModelBase)physicianRenderingModel;
        }

        protected virtual string GetFirstName()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.FirstName] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.FirstName] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetLastName()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.LastName] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.LastName] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetMiddleName()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.MIddleName] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.MIddleName] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetPreferredName()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.PreferredName] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.PreferredName] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetSuffix()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.Suffix] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.Suffix] ?? string.Empty;
            return string.Empty;
        }        

        protected virtual NameValueCollection GetProfessionalTitles()
        {
            if(this.PhysicianItem != null)
                return new NameValueCollection(((NameValueListField)this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.ProfessionalTitles]).NameValues);
            else if(this.Rendering.DataSourceItem != null)
                return new NameValueCollection(((NameValueListField)this.Rendering.DataSourceItem.Fields[PhysicianTemplate.Physician.Fields.ProfessionalTitles]).NameValues);
            return (NameValueCollection)null;
        }

        protected virtual NameValueCollection GetSpecialDesignations()
        {
            if(this.PhysicianItem != null)
                return new NameValueCollection(((NameValueListField)this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.SpecialDesignations]).NameValues);
            else if(this.Rendering.DataSourceItem != null)
                return new NameValueCollection(((NameValueListField)this.Rendering.DataSourceItem.Fields[PhysicianTemplate.Physician.Fields.SpecialDesignations]).NameValues);
            return (NameValueCollection)null;
        }

        protected virtual NameValueCollection GetSpecialties()
        {
            if(this.PhysicianItem != null)
                return new NameValueCollection(((NameValueListField)this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.Specialties]).NameValues);
            else if(this.Rendering.DataSourceItem != null)
                return new NameValueCollection(((NameValueListField)this.Rendering.DataSourceItem.Fields[PhysicianTemplate.Physician.Fields.Specialties]).NameValues);
            return (NameValueCollection)null;
        }

        protected virtual NameValueCollection GetSchedulingSpecialties()
        {
            if(this.PhysicianItem != null)
                return new NameValueCollection(((NameValueListField)this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.SchedulingSpecialties]).NameValues);
            else if(this.Rendering.DataSourceItem != null)
                return new NameValueCollection(((NameValueListField)this.Rendering.DataSourceItem.Fields[PhysicianTemplate.Physician.Fields.SchedulingSpecialties]).NameValues);
            return (NameValueCollection)null;
        }

        protected virtual NameValueCollection GetBCCSpecialties()
        {
            if(this.PhysicianItem != null)
                return new NameValueCollection(((NameValueListField)this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.BCCSpecialties]).NameValues);
            else if(this.Rendering.DataSourceItem != null)
                return new NameValueCollection(((NameValueListField)this.Rendering.DataSourceItem.Fields[PhysicianTemplate.Physician.Fields.BCCSpecialties]).NameValues);
            return (NameValueCollection)null;
        }

        protected virtual string GetLanguages()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.Languages] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.Languages] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetTreatmentsAndServices()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.TreatmentsandServices] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.TreatmentsandServices] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetDiseasesAndConditions()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.DiseasesandConditions] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.DiseasesandConditions] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetSpecialtyInterests()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.SpecialtyInterests] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.SpecialtyInterests] ?? string.Empty;
            return string.Empty;
        }

        //Physician Meta
        protected virtual string GetClientID()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.ClientID] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.ClientID] ?? string.Empty;
            return string.Empty;
        }

        protected virtual bool GetIsActive()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem != null && this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.IsActive] != null && this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.IsActive].Value != string.Empty && this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.IsActive].Value == "1";
            else
                return this.Rendering.DataSourceItem != null && this.Rendering.DataSourceItem.Fields[PhysicianTemplate.Physician.Fields.IsActive] != null && this.Rendering.DataSourceItem.Fields[PhysicianTemplate.Physician.Fields.IsActive].Value != string.Empty;
        }

        protected virtual bool GetIsBMG()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem != null && this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.IsBMG] != null && this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.IsBMG].Value != string.Empty && this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.IsBMG].Value == "1";            
            else
                return this.Rendering.DataSourceItem != null && this.Rendering.DataSourceItem.Fields[PhysicianTemplate.Physician.Fields.IsBMG] != null && this.Rendering.DataSourceItem.Fields[PhysicianTemplate.Physician.Fields.IsBMG].Value != string.Empty;            
        }

        protected virtual bool GetIsBCC()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem != null && this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.IsBCC] != null && this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.IsBCC].Value != string.Empty && this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.IsBCC].Value == "1";
            else
                return this.Rendering.DataSourceItem != null && this.Rendering.DataSourceItem.Fields[PhysicianTemplate.Physician.Fields.IsBCC] != null && this.Rendering.DataSourceItem.Fields[PhysicianTemplate.Physician.Fields.IsBCC].Value != string.Empty && this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.IsBCC].Value == "1";
        }


        protected virtual string GetImportTaxonomy()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.ImportTaxonomy] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.ImportTaxonomy] ?? string.Empty;
            return string.Empty;
        }


        protected virtual string GetImportProperties()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.ImportProperties] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.ImportProperties] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetImageImportedPath()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.ImageImportedPath] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.ImageImportedPath] ?? string.Empty;
            return string.Empty;
        }

        protected virtual NameValueCollection GetRegions()
        {
            if(this.PhysicianItem != null)
                return new NameValueCollection(((NameValueListField)this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.Regions]).NameValues);
            else if(this.Rendering.DataSourceItem != null)
                return new NameValueCollection(((NameValueListField)this.Rendering.DataSourceItem.Fields[PhysicianTemplate.Physician.Fields.Regions]).NameValues);
            return (NameValueCollection)null;
        }

        //Physician Contact
        protected virtual string GetPrimaryLocationID()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.PrimaryLocationID] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.PrimaryLocationID] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetPrimaryPhoneNumber()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.PrimaryPhoneNumber] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.PrimaryPhoneNumber] ?? string.Empty;
            return string.Empty;
        }

        protected virtual bool GetPrimaryPhoneIsPublic()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.PrimaryPhoneIsPublic] != null && this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.PrimaryPhoneIsPublic].Value != string.Empty;
            else
                return this.Rendering.DataSourceItem != null && this.Rendering.DataSourceItem.Fields[PhysicianTemplate.Physician.Fields.PrimaryPhoneIsPublic] != null && this.Rendering.DataSourceItem.Fields[PhysicianTemplate.Physician.Fields.PrimaryPhoneIsPublic].Value != string.Empty;
        }

        protected virtual NameValueCollection GetLocations()
        {
            if(this.PhysicianItem != null)
                return new NameValueCollection(((NameValueListField)this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.Locations]).NameValues);
            else if(this.Rendering.DataSourceItem != null)
                return new NameValueCollection(((NameValueListField)this.Rendering.DataSourceItem.Fields[PhysicianTemplate.Physician.Fields.Locations]).NameValues);
            return (NameValueCollection)null;
        }

        protected virtual NameValueCollection GetHospitalAffiliations()
        {
            if(this.PhysicianItem != null)
                return new NameValueCollection(((NameValueListField)this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.HospitalAffiliations]).NameValues);
            else if(this.Rendering.DataSourceItem != null)
                return new NameValueCollection(((NameValueListField)this.Rendering.DataSourceItem.Fields[PhysicianTemplate.Physician.Fields.HospitalAffiliations]).NameValues);
            return (NameValueCollection)null;
        }

        protected virtual string GetPrimaryLocationName()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.PrimaryLocationName] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.PrimaryLocationName] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetPrimaryLocationAddress1()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.PrimaryLocationAddress1] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.PrimaryLocationAddress1] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetPrimaryLocationAddress2()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.PrimaryLocationAddress2] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.PrimaryLocationAddress2] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetPrimaryLocationCity()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.PrimaryLocationCity] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.PrimaryLocationCity] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetPrimaryLocationState()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.PrimaryLocationState] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.PrimaryLocationState] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetPrimaryLocationPostalCode()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.PrimaryLocationPostalCode] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.PrimaryLocationPostalCode] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetPrimaryLocationLatitude()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.PrimaryLocationLatitude] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.PrimaryLocationLatitude] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetPrimaryLocationLongitude()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.PrimaryLocationLongitude] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.PrimaryLocationLongitude] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetPrimaryLocationMainPhoneNumber()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.PrimaryLocationMainPhoneNumber] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.PrimaryLocationMainPhoneNumber] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetPrimaryLocationType()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.PrimaryLocationType] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.PrimaryLocationType] ?? string.Empty;
            return string.Empty;
        }

        protected virtual Guid GetPrimaryLocation()
        {
            Guid returnguid = new Guid();
            if(this.PhysicianItem != null)
            {
                Guid.TryParse(this.PhysicianItem[PhysicianTemplate.Physician.Fields.PrimaryLocation], out returnguid);
                return returnguid;
            }
            else if(this.Rendering.DataSourceItem != null)
            {
                Guid.TryParse(this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.PrimaryLocation], out returnguid);
                return returnguid;
            }
            return Guid.Empty;
        }

        //Physician Biographical Secition
        protected virtual string GetBio()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.Bio] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.Bio] ?? string.Empty;
            return string.Empty;
        }

        protected virtual NameValueCollection GetEducation()
        {
            if(this.PhysicianItem != null)
                return new NameValueCollection(((NameValueListField)this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.Education]).NameValues);
            else if(this.Rendering.DataSourceItem != null)
                return new NameValueCollection(((NameValueListField)this.Rendering.DataSourceItem.Fields[PhysicianTemplate.Physician.Fields.Education]).NameValues);
            return (NameValueCollection)null;
        }

        protected virtual NameValueCollection GetProfessionalRecognition()
        {
            if(this.PhysicianItem != null)
                return new NameValueCollection(((NameValueListField)this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.ProfessionalRecognition]).NameValues);
            else if(this.Rendering.DataSourceItem != null)
                return new NameValueCollection(((NameValueListField)this.Rendering.DataSourceItem.Fields[PhysicianTemplate.Physician.Fields.ProfessionalRecognition]).NameValues);
            return (NameValueCollection)null;
        }

        protected virtual NameValueCollection GetMeetTheDoctor()
        {
            if(this.PhysicianItem != null)
                return new NameValueCollection(((NameValueListField)this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.MeetTheDoctor]).NameValues);
            else if(this.Rendering.DataSourceItem != null)
                return new NameValueCollection(((NameValueListField)this.Rendering.DataSourceItem.Fields[PhysicianTemplate.Physician.Fields.MeetTheDoctor]).NameValues);
            return (NameValueCollection)null;
        }

        protected virtual string GetOneCareID()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.OneCareID] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.OneCareID] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetOneCareVT()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.OneCareVT] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.OneCareVT] ?? string.Empty;
            return string.Empty;
        }

        protected virtual bool GetHideSchedulingWidget()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem != null && this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.HideSchedulingWidget] != null && this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.HideSchedulingWidget].Value != string.Empty && this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.HideSchedulingWidget].Value == "1";
            else
                return this.Rendering.DataSourceItem != null && this.Rendering.DataSourceItem.Fields[PhysicianTemplate.Physician.Fields.HideSchedulingWidget] != null && this.Rendering.DataSourceItem.Fields[PhysicianTemplate.Physician.Fields.HideSchedulingWidget].Value != string.Empty && this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.HideSchedulingWidget].Value == "1";
        }

        protected virtual string GetMediaResources()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem[PhysicianTemplate.Physician.Fields.MediaResources] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[PhysicianTemplate.Physician.Fields.MediaResources] ?? string.Empty;
            return string.Empty;
        }

        protected virtual bool GetFeaturedProvider()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem != null && this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.FeaturedProvider] != null && this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.FeaturedProvider].Value != string.Empty && this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.FeaturedProvider].Value == "1";
            else
                return this.Rendering.DataSourceItem != null && this.Rendering.DataSourceItem.Fields[PhysicianTemplate.Physician.Fields.FeaturedProvider] != null && this.Rendering.DataSourceItem.Fields[PhysicianTemplate.Physician.Fields.FeaturedProvider].Value != string.Empty && this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.FeaturedProvider].Value == "1";
        }

        protected virtual bool GetListedProvider()
        {
            if(this.PhysicianItem != null)
                return this.PhysicianItem != null && this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.ListedProvider] != null && this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.ListedProvider].Value != string.Empty && this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.ListedProvider].Value == "1";
            else
                return this.Rendering.DataSourceItem != null && this.Rendering.DataSourceItem.Fields[PhysicianTemplate.Physician.Fields.ListedProvider] != null && this.Rendering.DataSourceItem.Fields[PhysicianTemplate.Physician.Fields.ListedProvider].Value != string.Empty && this.PhysicianItem.Fields[PhysicianTemplate.Physician.Fields.ListedProvider].Value == "1";
        }





    }
}