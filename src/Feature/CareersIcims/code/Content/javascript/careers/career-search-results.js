﻿var jobpostresults = jobpostresults || {};

jobpostresults = {

  processResults : function(portalname, resp, templateid, containerid, hiddenrows){

    $.each(resp, function(i, data){

      var pn = portalname;
      var churl = "/api/sitecore/Careers/GetICimsPortalPost?portalname=" + pn + "&id=" + data.id;

      var reckid = data.id;
      var hiddenr = "";
      if(hiddenrows == true){
        hiddenr = " masked"
      }

      $.ajax({
        type: "GET",
        url: churl,
        dataType: 'json',
        success: function(d){
          var dom = jsel(d);

          var context = {
            "id" : reckid,
            "positioncode" : dom.select("//*[@@label='Position Code']").value,
            "positiontitle" : dom.select("//*[@@label='Position Title']").value,
            "status" : dom.select("//*[@@label='Status']").value,
            "department" : dom.select("//*[@@label='Department']").value,
            "location" : dom.select("//*[@@label='Location']").value,
            "posteddate" : dom.select("//*[@@label='Posted Date']").value,
            "hidden": hiddenr,
            "portalurl": data.portalurl
          };

          var tempScript = $(templateid).html();
          var tempComp = Handlebars.compile(tempScript);
          //var head = dom.select("//*label");

          var output = tempComp(context);

          $(containerid).append(output);
        }
      });
    });

  }

}


$(function () {
  var resp = initresp;

  if ($('#page-career-search-results').length > 0 && resp !== undefined) {

    if (resp.length <= 20) {
      $('button#load-more').hide();
    } else {
      $('button#load-more').show()
    }


    //console.log(resp);
    var nextset = resp.slice(0, 20);
    console.log(nextset);
    //var kill = resp.splice(0,20);
    resp.splice(0, 20);
    //console.log(resp);

    //resp = kill;

    //console.log(resp);

    var templateid = "#job-row-template";
    var containerid = "#responsecontainer";


    jobpostresults.processResults(pn, nextset, templateid, containerid, false);

    jobpostresults.processResults(pn, resp, templateid, containerid, true);

    $('button#load-more').click(function (event) {
      event.preventDefault();

      /*nextset = resp.slice(0,20);
      console.log(nextset);

      resp.splice(0,20);
      console.log(resp);

      //resp = kill;

      jobpostresults.processResults(pn, nextset,templateid,containerid);*/
      $('.row.search-result.masked').removeClass('masked');

      if ($('.row.search-result.masked').length == 0) {
        $('button#load-more').hide();
      }
    });
    $('button#updateresults').click(function (event) {
      event.preventDefault();
      $('input#keywordpost').val($('input#careerSearch').val());

      $('form#refine-your-search').submit();
    });

    $('input#careerSearch').change(function (event) {
      if (event.keyCode = 13) {
        $('input#keywordpost').val($('input#careerSearch').val());

        $('form#refine-your-search').submit();
      }
    });

    $("[data-collapse-group='save-filters']").click(function () {
      var $this = $(this);
      $("[data-collapse-group='save-filters']:not([data-target='" + $this.data("target") + "'])").each(function () {
        $(this).addClass('collapsed');
        $($(this).data("target")).removeClass("in").addClass('collapse');
      });
    });
  }
});
