﻿using Sitecore.ContentSearch;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.XA.Foundation.Multisite.LinkManagers;
using Sitecore.XA.Foundation.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Models
{

    public class SpecialtyRenderingModel : RenderingModelBase
    {

        public virtual ImageField Image { get; set; }

        /// <summary>
        /// <para>Template: Specialty</para><para>Field: Locations</para><para>Data type: Multilist</para>
        /// </summary>
        [IndexField("locations")]
        public virtual IEnumerable<Guid> Locations { get; set; }

        /// <summary>
        /// <para>Template: Specialty</para><para>Field: ServicePage</para><para>Data type: General Link</para>
        /// </summary>
        [IndexField("service_page")]
        public virtual LinkField ServicePage { get; set; }

        /// <summary>
        /// <para>Template: Specialty</para><para>Field: Title</para><para>Data type: Single-Line Text</para>
        /// </summary>
        [IndexField("title")]
        public virtual string Title { get; set; }

    }
}