using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using RazorGenerator.Mvc;
//using Sitecore.Baptist.Feature.MedicalCenterLocations.App_Start;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(Sitecore.Baptist.Feature.MedicalCenterLocations.RazorGeneratorMvcStart), "Start")]

namespace Sitecore.Baptist.Feature.MedicalCenterLocations {
    public static class RazorGeneratorMvcStart {
        public static void Start() {
            var engine = new PrecompiledMvcEngine(typeof(RazorGeneratorMvcStart).Assembly) {
                UsePhysicalViewsIfNewer = HttpContext.Current.Request.IsLocal
            };

            ViewEngines.Engines.Insert(0, engine);

            // StartPage lookups are done by WebPages. 
            VirtualPathFactoryManager.RegisterVirtualPathFactory(engine);            

        }
    }
}
