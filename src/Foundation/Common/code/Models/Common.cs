﻿using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sitecore.Baptist.Foundation.Common.Models
{
    public class GeocodeResponse
    {
        public string id { get; set; }
        public string zip { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public float latitude { get; set; }
        public float longitude { get; set; }
        public string timezone { get; set; }
        public string dst { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
    }

    public class GeoResult
    {
        public string query { get; set; }
        public string zip { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public int timzezone { get; set; }
        public int dst { get; set; }
    }

    public class GMapResponse {
        public List<GMapResult> results { get; set; }
        public string status { get; set; }
        public string cachekey { get; set; }
    }

    public class GMapResult {
        public List<GAddressComponents> address_components { get; set; }
        public string formatted_address { get; set; }
        public GGeometry geometry { get; set; }
        public string place_id { get; set; }
        public string[] types { get; set; }
    }

    public class GAddressComponents {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public string[] types { get; set; }
    }

    public class GGeometry {
        public Bounds bounds { get; set; }
        public GCoordinates location { get; set; }
        public string location_type { get; set; }
        public Bounds viewport { get; set; }
    }

    public class GCoordinates {
        public float lat { get; set; }
        public float lng { get; set; }
    }

    public class Bounds {
        public GCoordinates northeast { get; set; }
        public GCoordinates southwest { get; set; }
    }
    
}