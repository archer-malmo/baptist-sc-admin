﻿define(["sitecore","jquery"], function (Sitecore) {
  var Events_PageCode = Sitecore.Definitions.App.extend({
    initialized: function () {
      this.DialogWindow1.hide();
    },
    triggerImport: function () {
      var app = this;

      app.Frame1.set("sourceUrl", "");
      //alert("Hello, Nurld!");
      //return false;
      $.ajax({
        type: "GET",
        dataType: "json",
        url: "/api/sitecore/EventsImport/Import",
        cache: false,
        success: function (data) {
          //window.top.dialogClose();
          app.Frame1.set("sourceUrl", data);
          
        }

      })
    },
    cleanExpired: function () {
      var app = this;

      app.Frame1.set("sourceUrl", "");
      //alert("Hello, Nurld!");
      //return false;
      $.ajax({
        type: "GET",
        dataType: "json",
        url: "/api/sitecore/EventsImport/CleanExpired",
        cache: false,
        success: function (data) {
          //window.top.dialogClose();
          app.Frame1.set("sourceUrl", data);

        }

      })
    }
  });

  return Events_PageCode;
});