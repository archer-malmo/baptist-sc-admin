﻿using Newtonsoft.Json;
using Sitecore.XA.Foundation.Mvc.Controllers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Models;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Repositories;
using Sitecore.Data.Items;
using System.Data.Spatial;
using System.Text.RegularExpressions;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Utils;

namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Controllers
{
    public class ERWaitController : StandardController
    {

        public string testApiPath() {
            return "Welcome to the ER";
        }        

        // GET: Feature_MedicalCenterLocations/ERWait
        public ActionResult HeaderERWait()
        {
            //LocationWithDistance lwd = this.getNearestHospital();
            LocationUtils lu = new LocationUtils();
            lu.req = Request;
            lu.respnse = Response;

            //List<LocationWithDistance> locationswdistance = this.getHospitalsAndDistanceFromUser();            

            //List<LocationWithDistance> lwd = lu.getERLocations();
            List<LocationWithDistanceSearch> lwds = lu.getERLocationsSearch();

            if(lu.defaultLocationIsSet() == false && lwds.Count() > 0) {
              LocationWithDistanceSearch deflwd = lu.setDefaultLocationSearch(lwds.First().location.ClientID);
            } 

            return (ActionResult)this.PartialView("HeaderERWait", lwds);
        }

        public ActionResult HeaderLocationOverride(string clientID )
        {
            LocationUtils lu = new LocationUtils();
            lu.req = Request;
            lu.respnse = Response;
            
            LocationWithDistanceSearch lwds = lu.setDefaultLocationSearch(clientID);            

            return (ActionResult)this.PartialView("~/Areas/Feature_MedicalCenterLocations/Views/ERWait/HeaderLocationOverride.cshtml", lwds);
        }        

        public LocationWithDistance getNearestHospital()
        {            
            return (LocationWithDistance)this.getHospitalsAndDistanceFromUser().First();
        }        

        public List<LocationWithDistance> getHospitalsAndDistanceFromUser()
        {
            //get/set user geoip
            UserGeo usergeo = new UserGeo();
            LocationUtils lu = new LocationUtils();
            lu.req = Request;
            lu.respnse = Response;

            if (Request.Cookies.AllKeys.Contains("USER_GEO"))
            {
                usergeo = JsonConvert.DeserializeObject<UserGeo>(Request.Cookies["USER_GEO"].Value);
            }
            else
            {   
                usergeo = lu.setusergeo();
            }

            //get list of hosptials
            List<LocationItemRenderingModel> hospitals = this.getHospitals();
            
            var userpoint = string.Format("POINT({1} {0})", usergeo.latitude, usergeo.longitude);

            DbGeography dbgeo = DbGeography.FromText(userpoint);

            List<LocationWithDistance> hospitalswithdistance = new List<LocationWithDistance>();

            foreach (var hospital in hospitals)
            {
                float lat, lon;
                if (!float.TryParse(hospital.Latitude, out lat))
                {
                    lat = (float)0.0;
                }
                if (!float.TryParse(hospital.Longitude, out lon))
                {
                    lon = (float)0.0;
                }

                var hospitalpoint = string.Format("POINT({1} {0})", lat, lon);
                DbGeography hospgeo = DbGeography.FromText(hospitalpoint);
                double? distance = dbgeo.Distance(hospgeo);

                LocationWithDistance lwd = new LocationWithDistance
                {
                    location = hospital,
                    distance = (distance * 0.000621371)
                };
                hospitalswithdistance.Add(lwd);
            }

            return (List<LocationWithDistance>)hospitalswithdistance.OrderBy(x => x.distance).ToList();
        }


        public List<LocationItemRenderingModel> getHospitals()
        {
            IEnumerable<Item> hospitals = Sitecore.Context.Database.GetItem("/sitecore/content/Baptist/Baptist/Baptistonline/United States/Data/Locations/Hospitals").Children.Where(x => x.TemplateName == "Location Item");
            List<LocationItemRenderingModel> lirm = new List<LocationItemRenderingModel>();            

            foreach (var hosp in hospitals)
            {
                LocationItemRepository lir = new LocationItemRepository();
                lir.LocationItem = hosp;
                
                LocationItemRenderingModel tlocation = (LocationItemRenderingModel)lir.GetModel();
                //Sitecore.Diagnostics.Log.Info("[location info] " + tlocation.Title, this);
                lirm.Add(tlocation);
            }
            //Sitecore.Diagnostics.Log.Info("[hospital count] " + lirm.Count().ToString(), this);
            return lirm;
        }        

        //public UserGeo setusergeo()
        //{
        //    var ip = Request.ServerVariables["REMOTE_ADDR"];
        //    if (Regex.IsMatch(ip, @"^(0\.0\.0\.0|127\.0\.0\.1)$")) {
        //        ip = "216.37.75.146";
        //    }

        //    string url = "https://freegeoip.net/json/" + ip;
        //    string geoipinfo;
        //    HttpWebRequest hrquest = (HttpWebRequest)WebRequest.Create(url);
        //    using (HttpWebResponse response = hrquest.GetResponse() as HttpWebResponse)
        //    {
        //        StreamReader readStream = new StreamReader(response.GetResponseStream());
        //        geoipinfo = readStream.ReadToEnd();

        //        //Sitecore.Diagnostics.Log.Info("[IP INFO]" + geoipinfo, this);
        //        var obj = JsonConvert.DeserializeObject<UserGeo>(geoipinfo);           
        //        HttpCookie geocook = new HttpCookie("USER_GEO");                
        //        geocook.Value = JsonConvert.SerializeObject(obj);
        //        geocook.Expires = DateTime.MinValue;                

        //        Response.Cookies.Add(geocook);
        //        return obj;
        //    }            
        //}        
        public ActionResult HeaderUpdatedListOptions( string clientID ) {
            try
            {
                LocationUtils lu = new LocationUtils();
                lu.req = Request;
                lu.respnse = Response;
                LocationWithDistanceSearch lwd = lu.setDefaultLocationSearch(clientID);

                ERWaitListingsSearch erwl = new ERWaitListingsSearch();
                erwl.def = lwd;
                erwl.locations = lu.getERLocationsSearch();

                return (ActionResult)this.PartialView("~/Areas/Feature_MedicalCenterLocations/Views/ERWait/ListOptions.cshtml", erwl);
            }catch(Exception ex) {
                return Content(ex.Message);
            }

        }        

        public string UpdateUserGeoFromBrowser(string lat, string lon) 
        {
            LocationUtils lu = new LocationUtils();
            lu.req = Request;
            lu.respnse = Response;
            lu.setusergeofrombrowser(lat, lon);

            float latitude = float.Parse(lat);
            float longitude = float.Parse(lon);            
            LocationWithDistance lwd = lu.setDefaultLocation(lu.getERLocationsLatLonList(float.Parse(lat), float.Parse(lon)).First().location.ClientID);            

            return lwd.location.ClientID;

            //return true;                                                      
        }                     

        public string GetDefaultClientId() {
            LocationUtils lu = new LocationUtils();
            lu.req = Request;
            lu.respnse = Response;
            return lu.getDefaultLocation().location.ClientID;
        }
    }
   
}