﻿using Sitecore.Data;
using System.Runtime.InteropServices;

namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations
{
    public class Templates
    {
        [StructLayout(LayoutKind.Sequential, Size = 1)]
        public struct LocationItem
        {
            public static ID ID = ID.Parse("{B0006FC0-D788-44E5-9E28-698AC9AF1A72}");

            [StructLayout(LayoutKind.Sequential, Size = 1)]
            public struct Fields
            {
                //Location Section
                public static readonly ID LocationName = new ID("{3D452702-F00C-465C-8A51-AE2A1568C700}");
                public static readonly ID TermName = new ID("{3868F97A-6944-45A0-9CA5-F53C0849108E}");
                public static readonly ID ShortName = new ID("{581E3789-5C6B-48B4-9BED-98E19614F377}");
                public static readonly ID AddressLine1 = new ID("{65E4A4B0-F3C9-4165-898A-47C0E2447E89}");
                public static readonly ID AddressLine2 = new ID("{442D1707-B401-4025-88BB-743E4F20EAA2}");
                public static readonly ID City = new ID("{898890D8-9D1B-49DC-A1E9-1B10BD2B106D}");
                public static readonly ID State = new ID("{63C9665A-2DE5-4C82-B1CE-EE9BBC5D5081}");
                public static readonly ID PostalCode = new ID("{07EB1B84-51BA-4906-BD67-7DDBCFC5F75F}");
                public static readonly ID Latitude = new ID("{D7BBC520-58BF-43D4-BD59-9B3598A73C10}");
                public static readonly ID Longitude = new ID("{5AE39BA1-76EA-440B-B105-4577847BCFF4}");
                public static readonly ID Coordinates = new ID("{0B26459A-0FB8-446D-98B9-32D05114BA61}");

                //Location Meta Section
                public static readonly ID ClientID = new ID("{CD06DF25-D099-4A6D-A877-D6FF3353F502}");
                public static readonly ID IsPubliclySearchable = new ID("{8E4C8001-BC00-4770-B05E-2A8BA44FD2E3}");
                public static readonly ID IsActive = new ID("{AE1F4E11-7103-457F-B8E4-5191EBF4F256}");
                public static readonly ID IsBMG = new ID("{D5AC0D3C-267A-4B0E-91D8-12E02F60E903}");
                public static readonly ID ImportTaxonomy = new ID("{12419BB4-971D-4282-87FD-3954E326CA04}");
                public static readonly ID ImportProperties = new ID("{7B3A8B13-C3A6-4322-A160-5049646A6962}");
                public static readonly ID LocationType = new ID("{B4AE7EA9-D220-4C28-BA5B-3B9FA648931B}");

                //Location Contact Section
                public static readonly ID MainPhoneNumber = new ID("{6B9FF352-B715-4EA9-B02F-CE4A8E02B23D}");
                public static readonly ID MainEmailAddress = new ID("{B542FB6F-18AD-4C9B-B1B7-530E4D3CA774}");
                public static readonly ID AdditionalContactInformation = new ID("{80895AF3-4B4D-4CAA-B1A0-0A6E03EF97D6}");
                public static readonly ID EmergencyRoomPhoneNumber = new ID("{DA6AA1EF-ABC7-42F7-9C57-2B207A93F1BA}");

                //Location Content Section
                public static readonly ID Title = new ID("{E16F9295-5F4F-42EB-926B-1A983DE2D46F}");
                public static readonly ID MainDescription = new ID("{BC6001DD-5F66-4E14-9117-5784D85FD8CA}");                

                //Location Hours Section
                public static readonly ID HoursSectionTitle = new ID("{88AD1738-A4D9-4F05-A7AF-3A78C25BFAE0}");
                public static readonly ID HoursOfOperation = new ID("{9C4F23A9-622D-4B82-B087-D0FFD67955B7}");
                public static readonly ID HoursInfoMessage = new ID("{A670A6E2-09E3-4408-B95D-54A51BDE8879}");
                public static readonly ID ShowLocationHours = new ID("{DC42CEE4-32CC-403C-BD44-5E5BB5AACEEB}");

            }
        }
    }
}