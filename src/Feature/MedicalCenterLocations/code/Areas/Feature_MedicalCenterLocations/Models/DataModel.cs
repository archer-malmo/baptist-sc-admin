using Sitecore.Data.Items;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom;
using Sitecore.XA.Foundation.Mvc.Models;
using Sitecore.Data;
using System.Runtime.InteropServices;
using Fortis.Model.Fields;
#region Locations Folder (Custom)
namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Locations Folder</para>
	/// <para>ID: {05A48579-0E1E-4AC4-88B7-77CF0A1F8177}</para>
	/// <para>/sitecore/templates/Feature/Experience Accelerator/Medical Center Locations/Locations Folder</para>
	/// </summary>
	[TemplateMapping("{05A48579-0E1E-4AC4-88B7-77CF0A1F8177}", "InterfaceMap")]
	public partial interface ILocationsFolderItem : IItemWrapper 
	{		
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Feature/Experience Accelerator/Medical Center Locations/Locations Folder</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{05A48579-0E1E-4AC4-88B7-77CF0A1F8177}", typeof(Guid))]
	[TemplateMapping("{05A48579-0E1E-4AC4-88B7-77CF0A1F8177}", "")]
	public partial class LocationsFolderItem : ItemWrapper, ILocationsFolderItem
	{
		private Item _item = null;

		public LocationsFolderItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public LocationsFolderItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public LocationsFolderItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public LocationsFolderItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

	
	}
	public class LocationsFolderTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct LocationsFolder
	    {
	    	public static ID ID = ID.Parse("{05A48579-0E1E-4AC4-88B7-77CF0A1F8177}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
				
	        }
	    }
	}

	public class LocationsFolderRenderingModel : RenderingModelBase
	{
			
	}

}


#endregion
#region Specialty (Custom)
namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Specialty</para>
	/// <para>ID: {08926920-49E9-4B69-B519-EC1580D04201}</para>
	/// <para>/sitecore/templates/Feature/Experience Accelerator/Medical Center Locations/Composites/Datasource/Specialties/Specialty</para>
	/// </summary>
	[TemplateMapping("{08926920-49E9-4B69-B519-EC1580D04201}", "InterfaceMap")]
	public partial interface ISpecialtyItem : IItemWrapper 
	{		
		/// <summary>
		/// <para>Template: Specialty</para><para>Field: Image</para><para>Data type: Image</para>
		/// </summary>
		IImageFieldWrapper Image { get; }
		string ImageValue { get; }
		/// <summary>
		/// <para>Template: Specialty</para><para>Field: Locations</para><para>Data type: Multilist</para>
		/// </summary>
		[IndexField("locations")]
		IListFieldWrapper Locations { get; }

		/// <summary>
		/// <para>Template: Specialty</para><para>Field: Locations</para><para>Data type: Multilist</para>
		/// </summary>
		[IndexField("locations")]
		IEnumerable<Guid> LocationsValue { get; }
		/// <summary>
		/// <para>Template: Specialty</para><para>Field: ServicePage</para><para>Data type: General Link</para>
		/// </summary>
		[IndexField("service_page")]
		IGeneralLinkFieldWrapper ServicePage { get; }

		/// <summary>
		/// <para>Template: Specialty</para><para>Field: ServicePage</para><para>Data type: General Link</para>
		/// </summary>
		[IndexField("service_page")]
		string ServicePageValue { get; }
		/// <summary>
		/// <para>Template: Specialty</para><para>Field: Title</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("title")]
		ITextFieldWrapper Title { get; }

		/// <summary>
		/// <para>Template: Specialty</para><para>Field: Title</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("title")]
		string TitleValue { get; }
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Feature/Experience Accelerator/Medical Center Locations/Composites/Datasource/Specialties/Specialty</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{08926920-49E9-4B69-B519-EC1580D04201}", typeof(Guid))]
	[TemplateMapping("{08926920-49E9-4B69-B519-EC1580D04201}", "")]
	public partial class SpecialtyItem : ItemWrapper, ISpecialtyItem
	{
		private Item _item = null;

		public SpecialtyItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public SpecialtyItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public SpecialtyItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public SpecialtyItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

		/// <summary><para>Template: Specialty</para><para>Field: Image</para><para>Data type: Image</para></summary>
		public virtual IImageFieldWrapper Image
		{
			get { return GetField<ImageFieldWrapper>("Image"); }
		}

		/// <summary><para>Template: Specialty</para><para>Field: Image</para><para>Data type: Image</para></summary>
		public string ImageValue
		{
			get { return Image.Value; }
		}
		/// <summary><para>Template: Specialty</para><para>Field: Locations</para><para>Data type: Multilist</para></summary>
		[IndexField("locations")]
		public virtual IListFieldWrapper Locations
		{
			get { return GetField<ListFieldWrapper>("Locations", "locations"); }
		}

		/// <summary><para>Template: Specialty</para><para>Field: Locations</para><para>Data type: Multilist</para></summary>
		[IndexField("locations")]
		public IEnumerable<Guid> LocationsValue
		{
			get { return Locations.Value; }
		}
		/// <summary><para>Template: Specialty</para><para>Field: ServicePage</para><para>Data type: General Link</para></summary>
		[IndexField("service_page")]
		public virtual IGeneralLinkFieldWrapper ServicePage
		{
			get { return GetField<GeneralLinkFieldWrapper>("Service Page", "service_page"); }
		}

		/// <summary><para>Template: Specialty</para><para>Field: ServicePage</para><para>Data type: General Link</para></summary>
		[IndexField("service_page")]
		public string ServicePageValue
		{
			get { return ServicePage.Value; }
		}
		/// <summary><para>Template: Specialty</para><para>Field: Title</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("title")]
		public virtual ITextFieldWrapper Title
		{
			get { return GetField<TextFieldWrapper>("Title", "title"); }
		}

		/// <summary><para>Template: Specialty</para><para>Field: Title</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("title")]
		public string TitleValue
		{
			get { return Title.Value; }
		}
	
	}
	public class SpecialtyTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct Specialty
	    {
	    	public static ID ID = ID.Parse("{08926920-49E9-4B69-B519-EC1580D04201}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
						public static readonly ID Image = new ID("{2B34661D-6956-4732-B8D9-3DAB417A1AA4}");
						public static readonly ID Locations = new ID("{BF8D9893-2478-42A7-8FC0-5CB6FB0C44F4}");
						public static readonly ID ServicePage = new ID("{A5C3ED48-1C75-4562-BF28-434AA1AA3B90}");
						public static readonly ID Title = new ID("{4F3E74BD-2EC7-45F6-A66E-2FE350243F81}");
				
	        }
	    }
	}

	public class SpecialtyRenderingModel : RenderingModelBase
	{
				
				 public virtual string Image { get; set; }
			
				/// <summary>
			/// <para>Template: Specialty</para><para>Field: Locations</para><para>Data type: Multilist</para>
			/// </summary>
			[IndexField("locations")]
				 public virtual IEnumerable<Guid> Locations { get; set; }
			
				/// <summary>
			/// <para>Template: Specialty</para><para>Field: ServicePage</para><para>Data type: General Link</para>
			/// </summary>
			[IndexField("service_page")]
				 public virtual string ServicePage { get; set; }
			
				/// <summary>
			/// <para>Template: Specialty</para><para>Field: Title</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("title")]
				 public virtual string Title { get; set; }
		
	}

}


#endregion
#region Clinics Folder (Custom)
namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Clinics Folder</para>
	/// <para>ID: {28C30848-0541-4A19-B8B9-B315E0E35313}</para>
	/// <para>/sitecore/templates/Feature/Experience Accelerator/Medical Center Locations/Clinics Folder</para>
	/// </summary>
	[TemplateMapping("{28C30848-0541-4A19-B8B9-B315E0E35313}", "InterfaceMap")]
	public partial interface IClinicsFolderItem : IItemWrapper 
	{		
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Feature/Experience Accelerator/Medical Center Locations/Clinics Folder</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{28C30848-0541-4A19-B8B9-B315E0E35313}", typeof(Guid))]
	[TemplateMapping("{28C30848-0541-4A19-B8B9-B315E0E35313}", "")]
	public partial class ClinicsFolderItem : ItemWrapper, IClinicsFolderItem
	{
		private Item _item = null;

		public ClinicsFolderItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public ClinicsFolderItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public ClinicsFolderItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public ClinicsFolderItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

	
	}
	public class ClinicsFolderTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct ClinicsFolder
	    {
	    	public static ID ID = ID.Parse("{28C30848-0541-4A19-B8B9-B315E0E35313}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
				
	        }
	    }
	}

	public class ClinicsFolderRenderingModel : RenderingModelBase
	{
			
	}

}


#endregion
#region Clinical Trials Folder (Custom)
namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Clinical Trials Folder</para>
	/// <para>ID: {36235A93-4DDE-4369-8309-F90F3F79EFE0}</para>
	/// <para>/sitecore/templates/Feature/Experience Accelerator/Medical Center Locations/Clinical Trials Folder</para>
	/// </summary>
	[TemplateMapping("{36235A93-4DDE-4369-8309-F90F3F79EFE0}", "InterfaceMap")]
	public partial interface IClinicalTrialsFolderItem : IItemWrapper 
	{		
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Feature/Experience Accelerator/Medical Center Locations/Clinical Trials Folder</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{36235A93-4DDE-4369-8309-F90F3F79EFE0}", typeof(Guid))]
	[TemplateMapping("{36235A93-4DDE-4369-8309-F90F3F79EFE0}", "")]
	public partial class ClinicalTrialsFolderItem : ItemWrapper, IClinicalTrialsFolderItem
	{
		private Item _item = null;

		public ClinicalTrialsFolderItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public ClinicalTrialsFolderItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public ClinicalTrialsFolderItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public ClinicalTrialsFolderItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

	
	}
	public class ClinicalTrialsFolderTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct ClinicalTrialsFolder
	    {
	    	public static ID ID = ID.Parse("{36235A93-4DDE-4369-8309-F90F3F79EFE0}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
				
	        }
	    }
	}

	public class ClinicalTrialsFolderRenderingModel : RenderingModelBase
	{
			
	}

}


#endregion
#region Clinical Trial (Custom)
namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Clinical Trial</para>
	/// <para>ID: {3FE48ECD-F466-4E6F-9DD4-BE9CE3E238B2}</para>
	/// <para>/sitecore/templates/Feature/Experience Accelerator/Medical Center Locations/Composites/Datasource/Clinical Trials/Clinical Trial</para>
	/// </summary>
	[TemplateMapping("{3FE48ECD-F466-4E6F-9DD4-BE9CE3E238B2}", "InterfaceMap")]
	public partial interface IClinicalTrialItem : IItemWrapper 
	{		
		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: AdditionalStatusMessages</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("additional_status_messages")]
		ITextFieldWrapper AdditionalStatusMessages { get; }

		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: AdditionalStatusMessages</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("additional_status_messages")]
		string AdditionalStatusMessagesValue { get; }
		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: AnatomicSite</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("anatomic_site")]
		ITextFieldWrapper AnatomicSite { get; }

		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: AnatomicSite</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("anatomic_site")]
		string AnatomicSiteValue { get; }
		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: FormalStudyName</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("formal_study_name")]
		ITextFieldWrapper FormalStudyName { get; }

		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: FormalStudyName</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("formal_study_name")]
		string FormalStudyNameValue { get; }
		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: HideTrialFromListing</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("hide_trial_from_listing")]
		IBooleanFieldWrapper HideTrialFromListing { get; }

		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: HideTrialFromListing</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("hide_trial_from_listing")]
		bool HideTrialFromListingValue { get; }
		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: Histology</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("histology")]
		ITextFieldWrapper Histology { get; }

		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: Histology</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("histology")]
		string HistologyValue { get; }
		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: InformalStudyName</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("informal_study_name")]
		ITextFieldWrapper InformalStudyName { get; }

		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: InformalStudyName</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("informal_study_name")]
		string InformalStudyNameValue { get; }
		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: LocationsWhereOpen</para><para>Data type: Multilist</para>
		/// </summary>
		[IndexField("locations_where_open")]
		IListFieldWrapper LocationsWhereOpen { get; }

		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: LocationsWhereOpen</para><para>Data type: Multilist</para>
		/// </summary>
		[IndexField("locations_where_open")]
		IEnumerable<Guid> LocationsWhereOpenValue { get; }
		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: Mutation</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("mutation")]
		ITextFieldWrapper Mutation { get; }

		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: Mutation</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("mutation")]
		string MutationValue { get; }
		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: PI</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("pi")]
		ITextFieldWrapper PI { get; }

		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: PI</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("pi")]
		string PIValue { get; }
		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: Stage</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("stage")]
		ITextFieldWrapper Stage { get; }

		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: Stage</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("stage")]
		string StageValue { get; }
		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: StudyLink</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("study_link")]
		ITextFieldWrapper StudyLink { get; }

		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: StudyLink</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("study_link")]
		string StudyLinkValue { get; }
		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: StudyNumber</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("study_number")]
		ITextFieldWrapper StudyNumber { get; }

		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: StudyNumber</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("study_number")]
		string StudyNumberValue { get; }
		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: TreatmentPhase</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("treatment_phase")]
		ITextFieldWrapper TreatmentPhase { get; }

		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: TreatmentPhase</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("treatment_phase")]
		string TreatmentPhaseValue { get; }
		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: TrialPhase</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("trial_phase")]
		ITextFieldWrapper TrialPhase { get; }

		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: TrialPhase</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("trial_phase")]
		string TrialPhaseValue { get; }
		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: TrialType</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("trial_type")]
		ITextFieldWrapper TrialType { get; }

		/// <summary>
		/// <para>Template: Clinical Trial</para><para>Field: TrialType</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("trial_type")]
		string TrialTypeValue { get; }
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Feature/Experience Accelerator/Medical Center Locations/Composites/Datasource/Clinical Trials/Clinical Trial</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{3FE48ECD-F466-4E6F-9DD4-BE9CE3E238B2}", typeof(Guid))]
	[TemplateMapping("{3FE48ECD-F466-4E6F-9DD4-BE9CE3E238B2}", "")]
	public partial class ClinicalTrialItem : ItemWrapper, IClinicalTrialItem
	{
		private Item _item = null;

		public ClinicalTrialItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public ClinicalTrialItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public ClinicalTrialItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public ClinicalTrialItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

		/// <summary><para>Template: Clinical Trial</para><para>Field: AdditionalStatusMessages</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("additional_status_messages")]
		public virtual ITextFieldWrapper AdditionalStatusMessages
		{
			get { return GetField<TextFieldWrapper>("Additional Status Messages", "additional_status_messages"); }
		}

		/// <summary><para>Template: Clinical Trial</para><para>Field: AdditionalStatusMessages</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("additional_status_messages")]
		public string AdditionalStatusMessagesValue
		{
			get { return AdditionalStatusMessages.Value; }
		}
		/// <summary><para>Template: Clinical Trial</para><para>Field: AnatomicSite</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("anatomic_site")]
		public virtual ITextFieldWrapper AnatomicSite
		{
			get { return GetField<TextFieldWrapper>("Anatomic Site", "anatomic_site"); }
		}

		/// <summary><para>Template: Clinical Trial</para><para>Field: AnatomicSite</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("anatomic_site")]
		public string AnatomicSiteValue
		{
			get { return AnatomicSite.Value; }
		}
		/// <summary><para>Template: Clinical Trial</para><para>Field: FormalStudyName</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("formal_study_name")]
		public virtual ITextFieldWrapper FormalStudyName
		{
			get { return GetField<TextFieldWrapper>("Formal Study Name", "formal_study_name"); }
		}

		/// <summary><para>Template: Clinical Trial</para><para>Field: FormalStudyName</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("formal_study_name")]
		public string FormalStudyNameValue
		{
			get { return FormalStudyName.Value; }
		}
		/// <summary><para>Template: Clinical Trial</para><para>Field: HideTrialFromListing</para><para>Data type: Checkbox</para></summary>
		[IndexField("hide_trial_from_listing")]
		public virtual IBooleanFieldWrapper HideTrialFromListing
		{
			get { return GetField<BooleanFieldWrapper>("Hide Trial From Listing", "hide_trial_from_listing"); }
		}

		/// <summary><para>Template: Clinical Trial</para><para>Field: HideTrialFromListing</para><para>Data type: Checkbox</para></summary>
		[IndexField("hide_trial_from_listing")]
		public bool HideTrialFromListingValue
		{
			get { return HideTrialFromListing.Value; }
		}
		/// <summary><para>Template: Clinical Trial</para><para>Field: Histology</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("histology")]
		public virtual ITextFieldWrapper Histology
		{
			get { return GetField<TextFieldWrapper>("Histology", "histology"); }
		}

		/// <summary><para>Template: Clinical Trial</para><para>Field: Histology</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("histology")]
		public string HistologyValue
		{
			get { return Histology.Value; }
		}
		/// <summary><para>Template: Clinical Trial</para><para>Field: InformalStudyName</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("informal_study_name")]
		public virtual ITextFieldWrapper InformalStudyName
		{
			get { return GetField<TextFieldWrapper>("Informal Study Name", "informal_study_name"); }
		}

		/// <summary><para>Template: Clinical Trial</para><para>Field: InformalStudyName</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("informal_study_name")]
		public string InformalStudyNameValue
		{
			get { return InformalStudyName.Value; }
		}
		/// <summary><para>Template: Clinical Trial</para><para>Field: LocationsWhereOpen</para><para>Data type: Multilist</para></summary>
		[IndexField("locations_where_open")]
		public virtual IListFieldWrapper LocationsWhereOpen
		{
			get { return GetField<ListFieldWrapper>("Locations Where Open", "locations_where_open"); }
		}

		/// <summary><para>Template: Clinical Trial</para><para>Field: LocationsWhereOpen</para><para>Data type: Multilist</para></summary>
		[IndexField("locations_where_open")]
		public IEnumerable<Guid> LocationsWhereOpenValue
		{
			get { return LocationsWhereOpen.Value; }
		}
		/// <summary><para>Template: Clinical Trial</para><para>Field: Mutation</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("mutation")]
		public virtual ITextFieldWrapper Mutation
		{
			get { return GetField<TextFieldWrapper>("Mutation", "mutation"); }
		}

		/// <summary><para>Template: Clinical Trial</para><para>Field: Mutation</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("mutation")]
		public string MutationValue
		{
			get { return Mutation.Value; }
		}
		/// <summary><para>Template: Clinical Trial</para><para>Field: PI</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("pi")]
		public virtual ITextFieldWrapper PI
		{
			get { return GetField<TextFieldWrapper>("PI", "pi"); }
		}

		/// <summary><para>Template: Clinical Trial</para><para>Field: PI</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("pi")]
		public string PIValue
		{
			get { return PI.Value; }
		}
		/// <summary><para>Template: Clinical Trial</para><para>Field: Stage</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("stage")]
		public virtual ITextFieldWrapper Stage
		{
			get { return GetField<TextFieldWrapper>("Stage", "stage"); }
		}

		/// <summary><para>Template: Clinical Trial</para><para>Field: Stage</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("stage")]
		public string StageValue
		{
			get { return Stage.Value; }
		}
		/// <summary><para>Template: Clinical Trial</para><para>Field: StudyLink</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("study_link")]
		public virtual ITextFieldWrapper StudyLink
		{
			get { return GetField<TextFieldWrapper>("Study Link", "study_link"); }
		}

		/// <summary><para>Template: Clinical Trial</para><para>Field: StudyLink</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("study_link")]
		public string StudyLinkValue
		{
			get { return StudyLink.Value; }
		}
		/// <summary><para>Template: Clinical Trial</para><para>Field: StudyNumber</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("study_number")]
		public virtual ITextFieldWrapper StudyNumber
		{
			get { return GetField<TextFieldWrapper>("Study Number", "study_number"); }
		}

		/// <summary><para>Template: Clinical Trial</para><para>Field: StudyNumber</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("study_number")]
		public string StudyNumberValue
		{
			get { return StudyNumber.Value; }
		}
		/// <summary><para>Template: Clinical Trial</para><para>Field: TreatmentPhase</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("treatment_phase")]
		public virtual ITextFieldWrapper TreatmentPhase
		{
			get { return GetField<TextFieldWrapper>("Treatment Phase", "treatment_phase"); }
		}

		/// <summary><para>Template: Clinical Trial</para><para>Field: TreatmentPhase</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("treatment_phase")]
		public string TreatmentPhaseValue
		{
			get { return TreatmentPhase.Value; }
		}
		/// <summary><para>Template: Clinical Trial</para><para>Field: TrialPhase</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("trial_phase")]
		public virtual ITextFieldWrapper TrialPhase
		{
			get { return GetField<TextFieldWrapper>("Trial Phase", "trial_phase"); }
		}

		/// <summary><para>Template: Clinical Trial</para><para>Field: TrialPhase</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("trial_phase")]
		public string TrialPhaseValue
		{
			get { return TrialPhase.Value; }
		}
		/// <summary><para>Template: Clinical Trial</para><para>Field: TrialType</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("trial_type")]
		public virtual ITextFieldWrapper TrialType
		{
			get { return GetField<TextFieldWrapper>("Trial Type", "trial_type"); }
		}

		/// <summary><para>Template: Clinical Trial</para><para>Field: TrialType</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("trial_type")]
		public string TrialTypeValue
		{
			get { return TrialType.Value; }
		}
	
	}
	public class ClinicalTrialTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct ClinicalTrial
	    {
	    	public static ID ID = ID.Parse("{3FE48ECD-F466-4E6F-9DD4-BE9CE3E238B2}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
						public static readonly ID AdditionalStatusMessages = new ID("{BB01DB13-9BB6-42F8-85A6-E41DDD649B35}");
						public static readonly ID AnatomicSite = new ID("{FF465C0B-E37C-4C75-9B3D-53AC24E63721}");
						public static readonly ID FormalStudyName = new ID("{EABFB8EB-115E-4571-A35E-DB37BF4F2CB7}");
						public static readonly ID HideTrialFromListing = new ID("{65D89C95-BE87-4ED6-B00D-4C2FF31DF318}");
						public static readonly ID Histology = new ID("{BD395D4B-DF83-4585-A68A-1D2F5A99E46F}");
						public static readonly ID InformalStudyName = new ID("{23BB57EC-D14A-4447-82EA-1F9FAFD06A29}");
						public static readonly ID LocationsWhereOpen = new ID("{E51D57C7-CC82-4F37-B22D-515DE689D6F8}");
						public static readonly ID Mutation = new ID("{D22BECD5-E394-43E5-AE2C-E66D6C45364A}");
						public static readonly ID PI = new ID("{48C50E0B-BD07-42AF-BE06-7C082DC34AE5}");
						public static readonly ID Stage = new ID("{B3EF8928-95D6-4DD9-B014-9E23B77CA3F7}");
						public static readonly ID StudyLink = new ID("{34D6CCF5-5246-45EC-BA0A-818C8A3A18DA}");
						public static readonly ID StudyNumber = new ID("{80F1D3D1-B911-4CC9-90E1-4FB4C21B2001}");
						public static readonly ID TreatmentPhase = new ID("{A33E91AB-E862-45B0-86DF-1CD4FA043867}");
						public static readonly ID TrialPhase = new ID("{7AE233DE-8E0F-482F-A79C-7C8199800307}");
						public static readonly ID TrialType = new ID("{A2FA69C8-19B6-4707-B2C4-04E815775C85}");
				
	        }
	    }
	}

	public class ClinicalTrialRenderingModel : RenderingModelBase
	{
				
				/// <summary>
			/// <para>Template: Clinical Trial</para><para>Field: AdditionalStatusMessages</para><para>Data type: Multi-Line Text</para>
			/// </summary>
			[IndexField("additional_status_messages")]
				 public virtual string AdditionalStatusMessages { get; set; }
			
				/// <summary>
			/// <para>Template: Clinical Trial</para><para>Field: AnatomicSite</para><para>Data type: Multi-Line Text</para>
			/// </summary>
			[IndexField("anatomic_site")]
				 public virtual string AnatomicSite { get; set; }
			
				/// <summary>
			/// <para>Template: Clinical Trial</para><para>Field: FormalStudyName</para><para>Data type: Multi-Line Text</para>
			/// </summary>
			[IndexField("formal_study_name")]
				 public virtual string FormalStudyName { get; set; }
			
				/// <summary>
			/// <para>Template: Clinical Trial</para><para>Field: HideTrialFromListing</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("hide_trial_from_listing")]
				 public virtual bool HideTrialFromListing { get; set; }
			
				/// <summary>
			/// <para>Template: Clinical Trial</para><para>Field: Histology</para><para>Data type: Multi-Line Text</para>
			/// </summary>
			[IndexField("histology")]
				 public virtual string Histology { get; set; }
			
				/// <summary>
			/// <para>Template: Clinical Trial</para><para>Field: InformalStudyName</para><para>Data type: Multi-Line Text</para>
			/// </summary>
			[IndexField("informal_study_name")]
				 public virtual string InformalStudyName { get; set; }
			
				/// <summary>
			/// <para>Template: Clinical Trial</para><para>Field: LocationsWhereOpen</para><para>Data type: Multilist</para>
			/// </summary>
			[IndexField("locations_where_open")]
				 public virtual IEnumerable<Guid> LocationsWhereOpen { get; set; }
			
				/// <summary>
			/// <para>Template: Clinical Trial</para><para>Field: Mutation</para><para>Data type: Multi-Line Text</para>
			/// </summary>
			[IndexField("mutation")]
				 public virtual string Mutation { get; set; }
			
				/// <summary>
			/// <para>Template: Clinical Trial</para><para>Field: PI</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("pi")]
				 public virtual string PI { get; set; }
			
				/// <summary>
			/// <para>Template: Clinical Trial</para><para>Field: Stage</para><para>Data type: Multi-Line Text</para>
			/// </summary>
			[IndexField("stage")]
				 public virtual string Stage { get; set; }
			
				/// <summary>
			/// <para>Template: Clinical Trial</para><para>Field: StudyLink</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("study_link")]
				 public virtual string StudyLink { get; set; }
			
				/// <summary>
			/// <para>Template: Clinical Trial</para><para>Field: StudyNumber</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("study_number")]
				 public virtual string StudyNumber { get; set; }
			
				/// <summary>
			/// <para>Template: Clinical Trial</para><para>Field: TreatmentPhase</para><para>Data type: Multi-Line Text</para>
			/// </summary>
			[IndexField("treatment_phase")]
				 public virtual string TreatmentPhase { get; set; }
			
				/// <summary>
			/// <para>Template: Clinical Trial</para><para>Field: TrialPhase</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("trial_phase")]
				 public virtual string TrialPhase { get; set; }
			
				/// <summary>
			/// <para>Template: Clinical Trial</para><para>Field: TrialType</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("trial_type")]
				 public virtual string TrialType { get; set; }
		
	}

}


#endregion
#region Hospitals Folder (Custom)
namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Hospitals Folder</para>
	/// <para>ID: {585FA0CE-4073-4832-A98E-02507891FF79}</para>
	/// <para>/sitecore/templates/Feature/Experience Accelerator/Medical Center Locations/Hospitals Folder</para>
	/// </summary>
	[TemplateMapping("{585FA0CE-4073-4832-A98E-02507891FF79}", "InterfaceMap")]
	public partial interface IHospitalsFolderItem : IItemWrapper 
	{		
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Feature/Experience Accelerator/Medical Center Locations/Hospitals Folder</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{585FA0CE-4073-4832-A98E-02507891FF79}", typeof(Guid))]
	[TemplateMapping("{585FA0CE-4073-4832-A98E-02507891FF79}", "")]
	public partial class HospitalsFolderItem : ItemWrapper, IHospitalsFolderItem
	{
		private Item _item = null;

		public HospitalsFolderItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public HospitalsFolderItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public HospitalsFolderItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public HospitalsFolderItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

	
	}
	public class HospitalsFolderTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct HospitalsFolder
	    {
	    	public static ID ID = ID.Parse("{585FA0CE-4073-4832-A98E-02507891FF79}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
				
	        }
	    }
	}

	public class HospitalsFolderRenderingModel : RenderingModelBase
	{
			
	}

}


#endregion
#region Minor Medical Centers Folder (Custom)
namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Minor Medical Centers Folder</para>
	/// <para>ID: {5CA3C2A3-FBB7-4F05-AB4C-B5836701850B}</para>
	/// <para>/sitecore/templates/Feature/Experience Accelerator/Medical Center Locations/Minor Medical Centers Folder</para>
	/// </summary>
	[TemplateMapping("{5CA3C2A3-FBB7-4F05-AB4C-B5836701850B}", "InterfaceMap")]
	public partial interface IMinorMedicalCentersFolderItem : IItemWrapper 
	{		
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Feature/Experience Accelerator/Medical Center Locations/Minor Medical Centers Folder</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{5CA3C2A3-FBB7-4F05-AB4C-B5836701850B}", typeof(Guid))]
	[TemplateMapping("{5CA3C2A3-FBB7-4F05-AB4C-B5836701850B}", "")]
	public partial class MinorMedicalCentersFolderItem : ItemWrapper, IMinorMedicalCentersFolderItem
	{
		private Item _item = null;

		public MinorMedicalCentersFolderItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public MinorMedicalCentersFolderItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public MinorMedicalCentersFolderItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public MinorMedicalCentersFolderItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

	
	}
	public class MinorMedicalCentersFolderTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct MinorMedicalCentersFolder
	    {
	    	public static ID ID = ID.Parse("{5CA3C2A3-FBB7-4F05-AB4C-B5836701850B}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
				
	        }
	    }
	}

	public class MinorMedicalCentersFolderRenderingModel : RenderingModelBase
	{
			
	}

}


#endregion
#region LocationItemParameters (Custom)
namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: LocationItemParameters</para>
	/// <para>ID: {6A59744E-9F99-4E53-B8C8-3A46C2A8DC7D}</para>
	/// <para>/sitecore/templates/Feature/Experience Accelerator/Medical Center Locations/Composites/Rendering Parameters/LocationItemParameters</para>
	/// </summary>
	[TemplateMapping("{6A59744E-9F99-4E53-B8C8-3A46C2A8DC7D}", "InterfaceMap")]
	public partial interface ILocationItemParametersItem : IItemWrapper 
	{		
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Feature/Experience Accelerator/Medical Center Locations/Composites/Rendering Parameters/LocationItemParameters</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{6A59744E-9F99-4E53-B8C8-3A46C2A8DC7D}", typeof(Guid))]
	[TemplateMapping("{6A59744E-9F99-4E53-B8C8-3A46C2A8DC7D}", "")]
	public partial class LocationItemParametersItem : ItemWrapper, ILocationItemParametersItem
	{
		private Item _item = null;

		public LocationItemParametersItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public LocationItemParametersItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public LocationItemParametersItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public LocationItemParametersItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

	
	}
	public class LocationItemParametersTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct LocationItemParameters
	    {
	    	public static ID ID = ID.Parse("{6A59744E-9F99-4E53-B8C8-3A46C2A8DC7D}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
				
	        }
	    }
	}

	public class LocationItemParametersRenderingModel : RenderingModelBase
	{
			
	}

}


#endregion
#region Hospital Detail Page (Custom)
namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Hospital Detail Page</para>
	/// <para>ID: {6B41E150-344E-4B4B-A07A-F32E51ED6D2A}</para>
	/// <para>/sitecore/templates/Project/Baptist/Baptist/Locations/Hospital Detail Page</para>
	/// </summary>
	[TemplateMapping("{6B41E150-344E-4B4B-A07A-F32E51ED6D2A}", "InterfaceMap")]
	public partial interface IHospitalDetailPageItem : IItemWrapper , Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom.ILocationDetailPageItem
	{		
		/// <summary>
		/// <para>Template: Hospital Detail Page</para><para>Field: IsSinglePage</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("is_single_page")]
		IBooleanFieldWrapper IsSinglePage { get; }

		/// <summary>
		/// <para>Template: Hospital Detail Page</para><para>Field: IsSinglePage</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("is_single_page")]
		bool IsSinglePageValue { get; }
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Project/Baptist/Baptist/Locations/Hospital Detail Page</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{6B41E150-344E-4B4B-A07A-F32E51ED6D2A}", typeof(Guid))]
	[TemplateMapping("{6B41E150-344E-4B4B-A07A-F32E51ED6D2A}", "")]
	public partial class HospitalDetailPageItem : ItemWrapper, IHospitalDetailPageItem
	{
		private Item _item = null;

		public HospitalDetailPageItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public HospitalDetailPageItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public HospitalDetailPageItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public HospitalDetailPageItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

		/// <summary><para>Template: Hospital Detail Page</para><para>Field: IsHospital</para><para>Data type: Checkbox</para></summary>
		[IndexField("is_hospital")]
		public virtual IBooleanFieldWrapper IsHospital
		{
			get { return GetField<BooleanFieldWrapper>("Is Hospital", "is_hospital"); }
		}

		/// <summary><para>Template: Hospital Detail Page</para><para>Field: IsHospital</para><para>Data type: Checkbox</para></summary>
		[IndexField("is_hospital")]
		public bool IsHospitalValue
		{
			get { return IsHospital.Value; }
		}
		/// <summary><para>Template: Hospital Detail Page</para><para>Field: Location</para><para>Data type: Droptree</para></summary>
		[IndexField("location")]
		public virtual ILinkFieldWrapper Location
		{
			get { return GetField<LinkFieldWrapper>("Location", "location"); }
		}

		/// <summary><para>Template: Hospital Detail Page</para><para>Field: Location</para><para>Data type: Droptree</para></summary>
		[IndexField("location")]
		public Guid LocationValue
		{
			get { return Location.Value; }
		}
		/// <summary><para>Template: Hospital Detail Page</para><para>Field: IsSinglePage</para><para>Data type: Checkbox</para></summary>
		[IndexField("is_single_page")]
		public virtual IBooleanFieldWrapper IsSinglePage
		{
			get { return GetField<BooleanFieldWrapper>("Is Single Page", "is_single_page"); }
		}

		/// <summary><para>Template: Hospital Detail Page</para><para>Field: IsSinglePage</para><para>Data type: Checkbox</para></summary>
		[IndexField("is_single_page")]
		public bool IsSinglePageValue
		{
			get { return IsSinglePage.Value; }
		}
	
	}
	public class HospitalDetailPageTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct HospitalDetailPage
	    {
	    	public static ID ID = ID.Parse("{6B41E150-344E-4B4B-A07A-F32E51ED6D2A}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
						public static readonly ID IsSinglePage = new ID("{07ACE267-F461-4526-976E-53A6FA12DD10}");
				
	        }
	    }
	}

	public class HospitalDetailPageRenderingModel : RenderingModelBase
	{
				
				/// <summary>
			/// <para>Template: Hospital Detail Page</para><para>Field: IsSinglePage</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("is_single_page")]
				 public virtual bool IsSinglePage { get; set; }
		
	}

}


#endregion
#region Location Detail Page (Custom)
namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Location Detail Page</para>
	/// <para>ID: {8D05F640-B1AA-4D30-9488-39AEE0D72E49}</para>
	/// <para>/sitecore/templates/Project/Baptist/Baptist/Locations/Location Detail Page</para>
	/// </summary>
	[TemplateMapping("{8D05F640-B1AA-4D30-9488-39AEE0D72E49}", "InterfaceMap")]
	public partial interface ILocationDetailPageItem : IItemWrapper 
	{		
		/// <summary>
		/// <para>Template: Location Detail Page</para><para>Field: IsHospital</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("is_hospital")]
		IBooleanFieldWrapper IsHospital { get; }

		/// <summary>
		/// <para>Template: Location Detail Page</para><para>Field: IsHospital</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("is_hospital")]
		bool IsHospitalValue { get; }
		/// <summary>
		/// <para>Template: Location Detail Page</para><para>Field: Location</para><para>Data type: Droptree</para>
		/// </summary>
		[IndexField("location")]
		ILinkFieldWrapper Location { get; }

		/// <summary>
		/// <para>Template: Location Detail Page</para><para>Field: Location</para><para>Data type: Droptree</para>
		/// </summary>
		[IndexField("location")]
		Guid LocationValue { get; }
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Project/Baptist/Baptist/Locations/Location Detail Page</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{8D05F640-B1AA-4D30-9488-39AEE0D72E49}", typeof(Guid))]
	[TemplateMapping("{8D05F640-B1AA-4D30-9488-39AEE0D72E49}", "")]
	public partial class LocationDetailPageItem : ItemWrapper, ILocationDetailPageItem
	{
		private Item _item = null;

		public LocationDetailPageItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public LocationDetailPageItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public LocationDetailPageItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public LocationDetailPageItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

		/// <summary><para>Template: Location Detail Page</para><para>Field: IsHospital</para><para>Data type: Checkbox</para></summary>
		[IndexField("is_hospital")]
		public virtual IBooleanFieldWrapper IsHospital
		{
			get { return GetField<BooleanFieldWrapper>("Is Hospital", "is_hospital"); }
		}

		/// <summary><para>Template: Location Detail Page</para><para>Field: IsHospital</para><para>Data type: Checkbox</para></summary>
		[IndexField("is_hospital")]
		public bool IsHospitalValue
		{
			get { return IsHospital.Value; }
		}
		/// <summary><para>Template: Location Detail Page</para><para>Field: Location</para><para>Data type: Droptree</para></summary>
		[IndexField("location")]
		public virtual ILinkFieldWrapper Location
		{
			get { return GetField<LinkFieldWrapper>("Location", "location"); }
		}

		/// <summary><para>Template: Location Detail Page</para><para>Field: Location</para><para>Data type: Droptree</para></summary>
		[IndexField("location")]
		public Guid LocationValue
		{
			get { return Location.Value; }
		}
	
	}
	public class LocationDetailPageTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct LocationDetailPage
	    {
	    	public static ID ID = ID.Parse("{8D05F640-B1AA-4D30-9488-39AEE0D72E49}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
						public static readonly ID IsHospital = new ID("{B9370E55-E16E-4923-837B-A666A4106BDE}");
						public static readonly ID Location = new ID("{577A3BEE-82F6-4C5F-9F3B-6F6635A9C8FC}");
				
	        }
	    }
	}

	public class LocationDetailPageRenderingModel : RenderingModelBase
	{
				
				/// <summary>
			/// <para>Template: Location Detail Page</para><para>Field: IsHospital</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("is_hospital")]
				 public virtual bool IsHospital { get; set; }
			
				/// <summary>
			/// <para>Template: Location Detail Page</para><para>Field: Location</para><para>Data type: Droptree</para>
			/// </summary>
			[IndexField("location")]
				 public virtual Guid Location { get; set; }
		
	}

}


#endregion
#region Location Listing Rendering Parameteres (Custom)
namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Location Listing Rendering Parameteres</para>
	/// <para>ID: {945EB22F-1286-4E96-AABF-39C3BF9391C7}</para>
	/// <para>/sitecore/templates/Feature/Experience Accelerator/Medical Center Locations/Composites/Rendering Parameters/Location Listing Rendering Parameteres</para>
	/// </summary>
	[TemplateMapping("{945EB22F-1286-4E96-AABF-39C3BF9391C7}", "InterfaceMap")]
	public partial interface ILocationListingRenderingParameteresItem : IItemWrapper 
	{		
		/// <summary>
		/// <para>Template: Location Listing Rendering Parameteres</para><para>Field: BMGOnly</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("bmg_only")]
		IBooleanFieldWrapper BMGOnly { get; }

		/// <summary>
		/// <para>Template: Location Listing Rendering Parameteres</para><para>Field: BMGOnly</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("bmg_only")]
		bool BMGOnlyValue { get; }
		/// <summary>
		/// <para>Template: Location Listing Rendering Parameteres</para><para>Field: OMWHMPSchedulingOnly</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("omw_hmp_scheduling_only")]
		IBooleanFieldWrapper OMWHMPSchedulingOnly { get; }

		/// <summary>
		/// <para>Template: Location Listing Rendering Parameteres</para><para>Field: OMWHMPSchedulingOnly</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("omw_hmp_scheduling_only")]
		bool OMWHMPSchedulingOnlyValue { get; }
		/// <summary>
		/// <para>Template: Location Listing Rendering Parameteres</para><para>Field: SearchRadius</para><para>Data type: Number</para>
		/// </summary>
		[IndexField("search_radius")]
		INumberFieldWrapper SearchRadius { get; }

		/// <summary>
		/// <para>Template: Location Listing Rendering Parameteres</para><para>Field: SearchRadius</para><para>Data type: Number</para>
		/// </summary>
		[IndexField("search_radius")]
		float SearchRadiusValue { get; }
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Feature/Experience Accelerator/Medical Center Locations/Composites/Rendering Parameters/Location Listing Rendering Parameteres</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{945EB22F-1286-4E96-AABF-39C3BF9391C7}", typeof(Guid))]
	[TemplateMapping("{945EB22F-1286-4E96-AABF-39C3BF9391C7}", "")]
	public partial class LocationListingRenderingParameteresItem : ItemWrapper, ILocationListingRenderingParameteresItem
	{
		private Item _item = null;

		public LocationListingRenderingParameteresItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public LocationListingRenderingParameteresItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public LocationListingRenderingParameteresItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public LocationListingRenderingParameteresItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

		/// <summary><para>Template: Location Listing Rendering Parameteres</para><para>Field: BMGOnly</para><para>Data type: Checkbox</para></summary>
		[IndexField("bmg_only")]
		public virtual IBooleanFieldWrapper BMGOnly
		{
			get { return GetField<BooleanFieldWrapper>("BMG Only", "bmg_only"); }
		}

		/// <summary><para>Template: Location Listing Rendering Parameteres</para><para>Field: BMGOnly</para><para>Data type: Checkbox</para></summary>
		[IndexField("bmg_only")]
		public bool BMGOnlyValue
		{
			get { return BMGOnly.Value; }
		}
		/// <summary><para>Template: Location Listing Rendering Parameteres</para><para>Field: OMWHMPSchedulingOnly</para><para>Data type: Checkbox</para></summary>
		[IndexField("omw_hmp_scheduling_only")]
		public virtual IBooleanFieldWrapper OMWHMPSchedulingOnly
		{
			get { return GetField<BooleanFieldWrapper>("OMW HMP Scheduling Only", "omw_hmp_scheduling_only"); }
		}

		/// <summary><para>Template: Location Listing Rendering Parameteres</para><para>Field: OMWHMPSchedulingOnly</para><para>Data type: Checkbox</para></summary>
		[IndexField("omw_hmp_scheduling_only")]
		public bool OMWHMPSchedulingOnlyValue
		{
			get { return OMWHMPSchedulingOnly.Value; }
		}
		/// <summary><para>Template: Location Listing Rendering Parameteres</para><para>Field: SearchRadius</para><para>Data type: Number</para></summary>
		[IndexField("search_radius")]
		public virtual INumberFieldWrapper SearchRadius
		{
			get { return GetField<NumberFieldWrapper>("Search radius", "search_radius"); }
		}

		/// <summary><para>Template: Location Listing Rendering Parameteres</para><para>Field: SearchRadius</para><para>Data type: Number</para></summary>
		[IndexField("search_radius")]
		public float SearchRadiusValue
		{
			get { return SearchRadius.Value; }
		}
	
	}
	public class LocationListingRenderingParameteresTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct LocationListingRenderingParameteres
	    {
	    	public static ID ID = ID.Parse("{945EB22F-1286-4E96-AABF-39C3BF9391C7}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
						public static readonly ID BMGOnly = new ID("{DE9BF4F0-96DC-4D58-B6ED-B3AF3567336C}");
						public static readonly ID OMWHMPSchedulingOnly = new ID("{8BCA2E62-E60E-406D-B36E-46604B5FEF0D}");
						public static readonly ID Searchradius = new ID("{F3724F68-7065-419B-8E38-0161ECF2816C}");
				
	        }
	    }
	}

	public class LocationListingRenderingParameteresRenderingModel : RenderingModelBase
	{
				
				/// <summary>
			/// <para>Template: Location Listing Rendering Parameteres</para><para>Field: BMGOnly</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("bmg_only")]
				 public virtual bool BMGOnly { get; set; }
			
				/// <summary>
			/// <para>Template: Location Listing Rendering Parameteres</para><para>Field: OMWHMPSchedulingOnly</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("omw_hmp_scheduling_only")]
				 public virtual bool OMWHMPSchedulingOnly { get; set; }
			
				/// <summary>
			/// <para>Template: Location Listing Rendering Parameteres</para><para>Field: SearchRadius</para><para>Data type: Number</para>
			/// </summary>
			[IndexField("search_radius")]
				 public virtual float SearchRadius { get; set; }
		
	}

}


#endregion
#region Services Provided Content (Custom)
namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Services Provided Content</para>
	/// <para>ID: {A5E72329-2C30-4877-AF82-4B0C7C22EA55}</para>
	/// <para>/sitecore/templates/Feature/Experience Accelerator/Medical Center Locations/Composites/Datasource/Services Provided Content</para>
	/// </summary>
	[TemplateMapping("{A5E72329-2C30-4877-AF82-4B0C7C22EA55}", "InterfaceMap")]
	public partial interface IServicesProvidedContentItem : IItemWrapper 
	{		
		/// <summary>
		/// <para>Template: Services Provided Content</para><para>Field: ContentAfter</para><para>Data type: Rich Text</para>
		/// </summary>
		[IndexField("content_after")]
		IRichTextFieldWrapper ContentAfter { get; }

		/// <summary>
		/// <para>Template: Services Provided Content</para><para>Field: ContentAfter</para><para>Data type: Rich Text</para>
		/// </summary>
		[IndexField("content_after")]
		string ContentAfterValue { get; }
		/// <summary>
		/// <para>Template: Services Provided Content</para><para>Field: ContentBefore</para><para>Data type: Rich Text</para>
		/// </summary>
		[IndexField("content_before")]
		IRichTextFieldWrapper ContentBefore { get; }

		/// <summary>
		/// <para>Template: Services Provided Content</para><para>Field: ContentBefore</para><para>Data type: Rich Text</para>
		/// </summary>
		[IndexField("content_before")]
		string ContentBeforeValue { get; }
		/// <summary>
		/// <para>Template: Services Provided Content</para><para>Field: ListingType</para><para>Data type: Droplist</para>
		/// </summary>
		ITextFieldWrapper ListingType { get; }
		string ListingTypeValue { get; }
		/// <summary>
		/// <para>Template: Services Provided Content</para><para>Field: ServicesTitle</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("servicestitle")]
		ITextFieldWrapper ServicesTitle { get; }

		/// <summary>
		/// <para>Template: Services Provided Content</para><para>Field: ServicesTitle</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("servicestitle")]
		string ServicesTitleValue { get; }
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Feature/Experience Accelerator/Medical Center Locations/Composites/Datasource/Services Provided Content</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{A5E72329-2C30-4877-AF82-4B0C7C22EA55}", typeof(Guid))]
	[TemplateMapping("{A5E72329-2C30-4877-AF82-4B0C7C22EA55}", "")]
	public partial class ServicesProvidedContentItem : ItemWrapper, IServicesProvidedContentItem
	{
		private Item _item = null;

		public ServicesProvidedContentItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public ServicesProvidedContentItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public ServicesProvidedContentItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public ServicesProvidedContentItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

		/// <summary><para>Template: Services Provided Content</para><para>Field: ContentAfter</para><para>Data type: Rich Text</para></summary>
		[IndexField("content_after")]
		public virtual IRichTextFieldWrapper ContentAfter
		{
			get { return GetField<RichTextFieldWrapper>("Content After", "content_after"); }
		}

		/// <summary><para>Template: Services Provided Content</para><para>Field: ContentAfter</para><para>Data type: Rich Text</para></summary>
		[IndexField("content_after")]
		public string ContentAfterValue
		{
			get { return ContentAfter.Value; }
		}
		/// <summary><para>Template: Services Provided Content</para><para>Field: ContentBefore</para><para>Data type: Rich Text</para></summary>
		[IndexField("content_before")]
		public virtual IRichTextFieldWrapper ContentBefore
		{
			get { return GetField<RichTextFieldWrapper>("Content Before", "content_before"); }
		}

		/// <summary><para>Template: Services Provided Content</para><para>Field: ContentBefore</para><para>Data type: Rich Text</para></summary>
		[IndexField("content_before")]
		public string ContentBeforeValue
		{
			get { return ContentBefore.Value; }
		}
		/// <summary><para>Template: Services Provided Content</para><para>Field: ListingType</para><para>Data type: Droplist</para></summary>
		public virtual ITextFieldWrapper ListingType
		{
			get { return GetField<TextFieldWrapper>("Listing Type"); }
		}

		/// <summary><para>Template: Services Provided Content</para><para>Field: ListingType</para><para>Data type: Droplist</para></summary>
		public string ListingTypeValue
		{
			get { return ListingType.Value; }
		}
		/// <summary><para>Template: Services Provided Content</para><para>Field: ServicesTitle</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("servicestitle")]
		public virtual ITextFieldWrapper ServicesTitle
		{
			get { return GetField<TextFieldWrapper>("ServicesTitle", "servicestitle"); }
		}

		/// <summary><para>Template: Services Provided Content</para><para>Field: ServicesTitle</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("servicestitle")]
		public string ServicesTitleValue
		{
			get { return ServicesTitle.Value; }
		}
	
	}
	public class ServicesProvidedContentTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct ServicesProvidedContent
	    {
	    	public static ID ID = ID.Parse("{A5E72329-2C30-4877-AF82-4B0C7C22EA55}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
						public static readonly ID ContentAfter = new ID("{51E9B3DD-23B2-4D67-AA36-C2D2949CAB1C}");
						public static readonly ID ContentBefore = new ID("{7CA009EA-56C4-443B-86B5-EA8C97C6985A}");
						public static readonly ID ListingType = new ID("{3F1704C7-4705-41D0-9948-C002CE949F9E}");
						public static readonly ID ServicesTitle = new ID("{DDAAFD69-55BC-48EA-8573-DC5755EB6CD9}");
				
	        }
	    }
	}

	public class ServicesProvidedContentRenderingModel : RenderingModelBase
	{
				
				/// <summary>
			/// <para>Template: Services Provided Content</para><para>Field: ContentAfter</para><para>Data type: Rich Text</para>
			/// </summary>
			[IndexField("content_after")]
				 public virtual string ContentAfter { get; set; }
			
				/// <summary>
			/// <para>Template: Services Provided Content</para><para>Field: ContentBefore</para><para>Data type: Rich Text</para>
			/// </summary>
			[IndexField("content_before")]
				 public virtual string ContentBefore { get; set; }
			
				 public virtual string ListingType { get; set; }
			
				/// <summary>
			/// <para>Template: Services Provided Content</para><para>Field: ServicesTitle</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("servicestitle")]
				 public virtual string ServicesTitle { get; set; }
		
	}

}


#endregion
#region Location Item (Custom)
namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Location Item</para>
	/// <para>ID: {B0006FC0-D788-44E5-9E28-698AC9AF1A72}</para>
	/// <para>/sitecore/templates/Feature/Experience Accelerator/Medical Center Locations/Composites/Datasource/Location Item</para>
	/// </summary>
	[TemplateMapping("{B0006FC0-D788-44E5-9E28-698AC9AF1A72}", "InterfaceMap")]
	public partial interface ILocationItemItem : IItemWrapper 
	{		
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: CancerTypesJson</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("cancer_types_json")]
		ITextFieldWrapper CancerTypesJson { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: CancerTypesJson</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("cancer_types_json")]
		string CancerTypesJsonValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: ServicesProvidedJson</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("services_provided_json")]
		ITextFieldWrapper ServicesProvidedJson { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: ServicesProvidedJson</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("services_provided_json")]
		string ServicesProvidedJsonValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: AddressLine1</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("address_line_1")]
		ITextFieldWrapper AddressLine1 { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: AddressLine1</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("address_line_1")]
		string AddressLine1Value { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: AddressLine2</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("address_line_2")]
		ITextFieldWrapper AddressLine2 { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: AddressLine2</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("address_line_2")]
		string AddressLine2Value { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: City</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("city")]
		ITextFieldWrapper City { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: City</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("city")]
		string CityValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: Coordinates</para><para>Data type: LatLon</para>
		/// </summary>
		ITextFieldWrapper Coordinates { get; }
		string CoordinatesValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: Latitude</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("latitude")]
		ITextFieldWrapper Latitude { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: Latitude</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("latitude")]
		string LatitudeValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: LocationName</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("location_name")]
		ITextFieldWrapper LocationName { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: LocationName</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("location_name")]
		string LocationNameValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: Longitude</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("longitude")]
		ITextFieldWrapper Longitude { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: Longitude</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("longitude")]
		string LongitudeValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: PostalCode</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("postal_code")]
		ITextFieldWrapper PostalCode { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: PostalCode</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("postal_code")]
		string PostalCodeValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: ShortName</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("short_name")]
		ITextFieldWrapper ShortName { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: ShortName</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("short_name")]
		string ShortNameValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: State</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("state")]
		ITextFieldWrapper State { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: State</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("state")]
		string StateValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: TermName</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("term_name")]
		ITextFieldWrapper TermName { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: TermName</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("term_name")]
		string TermNameValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: ClientID</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("clientid")]
		ITextFieldWrapper ClientID { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: ClientID</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("clientid")]
		string ClientIDValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: ImportProperties</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("import_properties")]
		ITextFieldWrapper ImportProperties { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: ImportProperties</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("import_properties")]
		string ImportPropertiesValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: ImportTaxonomy</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("import_taxonomy")]
		ITextFieldWrapper ImportTaxonomy { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: ImportTaxonomy</para><para>Data type: Multi-Line Text</para>
		/// </summary>
		[IndexField("import_taxonomy")]
		string ImportTaxonomyValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: IsActive</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("is_active")]
		IBooleanFieldWrapper IsActive { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: IsActive</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("is_active")]
		bool IsActiveValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: IsBMG</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("is_bmg")]
		IBooleanFieldWrapper IsBMG { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: IsBMG</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("is_bmg")]
		bool IsBMGValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: IsPubliclySearchable</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("is_publicly_searchable")]
		IBooleanFieldWrapper IsPubliclySearchable { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: IsPubliclySearchable</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("is_publicly_searchable")]
		bool IsPubliclySearchableValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: LocationType</para><para>Data type: Droplist</para>
		/// </summary>
		ITextFieldWrapper LocationType { get; }
		string LocationTypeValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: AdditionalContactInformation</para><para>Data type: Rich Text</para>
		/// </summary>
		[IndexField("additional_contact_information")]
		IRichTextFieldWrapper AdditionalContactInformation { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: AdditionalContactInformation</para><para>Data type: Rich Text</para>
		/// </summary>
		[IndexField("additional_contact_information")]
		string AdditionalContactInformationValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: EmergencyRoomPhoneNumber</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("emergency_room_phone_number")]
		ITextFieldWrapper EmergencyRoomPhoneNumber { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: EmergencyRoomPhoneNumber</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("emergency_room_phone_number")]
		string EmergencyRoomPhoneNumberValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: MainEmailAddress</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("main_email_address")]
		ITextFieldWrapper MainEmailAddress { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: MainEmailAddress</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("main_email_address")]
		string MainEmailAddressValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: MainPhoneNumber</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("main_phone_number")]
		ITextFieldWrapper MainPhoneNumber { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: MainPhoneNumber</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("main_phone_number")]
		string MainPhoneNumberValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: Title</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("title")]
		ITextFieldWrapper Title { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: Title</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("title")]
		string TitleValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: MainDescription</para><para>Data type: Rich Text</para>
		/// </summary>
		[IndexField("main_description")]
		IRichTextFieldWrapper MainDescription { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: MainDescription</para><para>Data type: Rich Text</para>
		/// </summary>
		[IndexField("main_description")]
		string MainDescriptionValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: HoursInfoMessage</para><para>Data type: Rich Text</para>
		/// </summary>
		[IndexField("hours_info_message")]
		IRichTextFieldWrapper HoursInfoMessage { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: HoursInfoMessage</para><para>Data type: Rich Text</para>
		/// </summary>
		[IndexField("hours_info_message")]
		string HoursInfoMessageValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: HoursOfOperation</para><para>Data type: Name Value List</para>
		/// </summary>
		INameValueListFieldWrapper HoursOfOperation { get; }
		System.Collections.Specialized.NameValueCollection HoursOfOperationValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: HoursSectionTitle</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("hours_section_title")]
		ITextFieldWrapper HoursSectionTitle { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: HoursSectionTitle</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("hours_section_title")]
		string HoursSectionTitleValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: ShowLocationHours</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("show_location_hours")]
		IBooleanFieldWrapper ShowLocationHours { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: ShowLocationHours</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("show_location_hours")]
		bool ShowLocationHoursValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: ERWaitURLKey</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("er_wait_url_key")]
		ITextFieldWrapper ERWaitURLKey { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: ERWaitURLKey</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("er_wait_url_key")]
		string ERWaitURLKeyValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: AlternateLocationListingPage</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("alternate_location_listing_page")]
		ITextFieldWrapper AlternateLocationListingPage { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: AlternateLocationListingPage</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("alternate_location_listing_page")]
		string AlternateLocationListingPageValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: EnableAlternateLink</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("enable_alternate_link")]
		IBooleanFieldWrapper EnableAlternateLink { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: EnableAlternateLink</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("enable_alternate_link")]
		bool EnableAlternateLinkValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: OverrideWithMapsLink</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("override_with_maps_link")]
		IBooleanFieldWrapper OverrideWithMapsLink { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: OverrideWithMapsLink</para><para>Data type: Checkbox</para>
		/// </summary>
		[IndexField("override_with_maps_link")]
		bool OverrideWithMapsLinkValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: SchedulingCode</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("scheduling_code")]
		ITextFieldWrapper SchedulingCode { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: SchedulingCode</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("scheduling_code")]
		string SchedulingCodeValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: SchedulingLabel</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("scheduling_label")]
		ITextFieldWrapper SchedulingLabel { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: SchedulingLabel</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("scheduling_label")]
		string SchedulingLabelValue { get; }
		/// <summary>
		/// <para>Template: Location Item</para><para>Field: SchedulingType</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("scheduling_type")]
		ITextFieldWrapper SchedulingType { get; }

		/// <summary>
		/// <para>Template: Location Item</para><para>Field: SchedulingType</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("scheduling_type")]
		string SchedulingTypeValue { get; }
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Feature/Experience Accelerator/Medical Center Locations/Composites/Datasource/Location Item</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{B0006FC0-D788-44E5-9E28-698AC9AF1A72}", typeof(Guid))]
	[TemplateMapping("{B0006FC0-D788-44E5-9E28-698AC9AF1A72}", "")]
	public partial class LocationItemItem : ItemWrapper, ILocationItemItem
	{
		private Item _item = null;

		public LocationItemItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public LocationItemItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public LocationItemItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public LocationItemItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

		/// <summary><para>Template: Location Item</para><para>Field: CancerTypesJson</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("cancer_types_json")]
		public virtual ITextFieldWrapper CancerTypesJson
		{
			get { return GetField<TextFieldWrapper>("Cancer Types Json", "cancer_types_json"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: CancerTypesJson</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("cancer_types_json")]
		public string CancerTypesJsonValue
		{
			get { return CancerTypesJson.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: ServicesProvidedJson</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("services_provided_json")]
		public virtual ITextFieldWrapper ServicesProvidedJson
		{
			get { return GetField<TextFieldWrapper>("Services Provided Json", "services_provided_json"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: ServicesProvidedJson</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("services_provided_json")]
		public string ServicesProvidedJsonValue
		{
			get { return ServicesProvidedJson.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: AddressLine1</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("address_line_1")]
		public virtual ITextFieldWrapper AddressLine1
		{
			get { return GetField<TextFieldWrapper>("Address Line 1", "address_line_1"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: AddressLine1</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("address_line_1")]
		public string AddressLine1Value
		{
			get { return AddressLine1.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: AddressLine2</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("address_line_2")]
		public virtual ITextFieldWrapper AddressLine2
		{
			get { return GetField<TextFieldWrapper>("Address Line 2", "address_line_2"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: AddressLine2</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("address_line_2")]
		public string AddressLine2Value
		{
			get { return AddressLine2.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: City</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("city")]
		public virtual ITextFieldWrapper City
		{
			get { return GetField<TextFieldWrapper>("City", "city"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: City</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("city")]
		public string CityValue
		{
			get { return City.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: Coordinates</para><para>Data type: LatLon</para></summary>
		public virtual ITextFieldWrapper Coordinates
		{
			get { return GetField<TextFieldWrapper>("Coordinates"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: Coordinates</para><para>Data type: LatLon</para></summary>
		public string CoordinatesValue
		{
			get { return Coordinates.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: Latitude</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("latitude")]
		public virtual ITextFieldWrapper Latitude
		{
			get { return GetField<TextFieldWrapper>("Latitude", "latitude"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: Latitude</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("latitude")]
		public string LatitudeValue
		{
			get { return Latitude.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: LocationName</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("location_name")]
		public virtual ITextFieldWrapper LocationName
		{
			get { return GetField<TextFieldWrapper>("Location Name", "location_name"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: LocationName</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("location_name")]
		public string LocationNameValue
		{
			get { return LocationName.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: Longitude</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("longitude")]
		public virtual ITextFieldWrapper Longitude
		{
			get { return GetField<TextFieldWrapper>("Longitude", "longitude"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: Longitude</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("longitude")]
		public string LongitudeValue
		{
			get { return Longitude.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: PostalCode</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("postal_code")]
		public virtual ITextFieldWrapper PostalCode
		{
			get { return GetField<TextFieldWrapper>("Postal Code", "postal_code"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: PostalCode</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("postal_code")]
		public string PostalCodeValue
		{
			get { return PostalCode.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: ShortName</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("short_name")]
		public virtual ITextFieldWrapper ShortName
		{
			get { return GetField<TextFieldWrapper>("Short Name", "short_name"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: ShortName</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("short_name")]
		public string ShortNameValue
		{
			get { return ShortName.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: State</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("state")]
		public virtual ITextFieldWrapper State
		{
			get { return GetField<TextFieldWrapper>("State", "state"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: State</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("state")]
		public string StateValue
		{
			get { return State.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: TermName</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("term_name")]
		public virtual ITextFieldWrapper TermName
		{
			get { return GetField<TextFieldWrapper>("Term Name", "term_name"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: TermName</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("term_name")]
		public string TermNameValue
		{
			get { return TermName.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: ClientID</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("clientid")]
		public virtual ITextFieldWrapper ClientID
		{
			get { return GetField<TextFieldWrapper>("ClientID", "clientid"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: ClientID</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("clientid")]
		public string ClientIDValue
		{
			get { return ClientID.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: ImportProperties</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("import_properties")]
		public virtual ITextFieldWrapper ImportProperties
		{
			get { return GetField<TextFieldWrapper>("Import Properties", "import_properties"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: ImportProperties</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("import_properties")]
		public string ImportPropertiesValue
		{
			get { return ImportProperties.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: ImportTaxonomy</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("import_taxonomy")]
		public virtual ITextFieldWrapper ImportTaxonomy
		{
			get { return GetField<TextFieldWrapper>("Import Taxonomy", "import_taxonomy"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: ImportTaxonomy</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("import_taxonomy")]
		public string ImportTaxonomyValue
		{
			get { return ImportTaxonomy.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: IsActive</para><para>Data type: Checkbox</para></summary>
		[IndexField("is_active")]
		public virtual IBooleanFieldWrapper IsActive
		{
			get { return GetField<BooleanFieldWrapper>("Is Active", "is_active"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: IsActive</para><para>Data type: Checkbox</para></summary>
		[IndexField("is_active")]
		public bool IsActiveValue
		{
			get { return IsActive.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: IsBMG</para><para>Data type: Checkbox</para></summary>
		[IndexField("is_bmg")]
		public virtual IBooleanFieldWrapper IsBMG
		{
			get { return GetField<BooleanFieldWrapper>("Is BMG", "is_bmg"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: IsBMG</para><para>Data type: Checkbox</para></summary>
		[IndexField("is_bmg")]
		public bool IsBMGValue
		{
			get { return IsBMG.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: IsPubliclySearchable</para><para>Data type: Checkbox</para></summary>
		[IndexField("is_publicly_searchable")]
		public virtual IBooleanFieldWrapper IsPubliclySearchable
		{
			get { return GetField<BooleanFieldWrapper>("Is Publicly Searchable", "is_publicly_searchable"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: IsPubliclySearchable</para><para>Data type: Checkbox</para></summary>
		[IndexField("is_publicly_searchable")]
		public bool IsPubliclySearchableValue
		{
			get { return IsPubliclySearchable.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: LocationType</para><para>Data type: Droplist</para></summary>
		public virtual ITextFieldWrapper LocationType
		{
			get { return GetField<TextFieldWrapper>("Location Type"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: LocationType</para><para>Data type: Droplist</para></summary>
		public string LocationTypeValue
		{
			get { return LocationType.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: AdditionalContactInformation</para><para>Data type: Rich Text</para></summary>
		[IndexField("additional_contact_information")]
		public virtual IRichTextFieldWrapper AdditionalContactInformation
		{
			get { return GetField<RichTextFieldWrapper>("Additional Contact Information", "additional_contact_information"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: AdditionalContactInformation</para><para>Data type: Rich Text</para></summary>
		[IndexField("additional_contact_information")]
		public string AdditionalContactInformationValue
		{
			get { return AdditionalContactInformation.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: EmergencyRoomPhoneNumber</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("emergency_room_phone_number")]
		public virtual ITextFieldWrapper EmergencyRoomPhoneNumber
		{
			get { return GetField<TextFieldWrapper>("Emergency Room Phone Number", "emergency_room_phone_number"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: EmergencyRoomPhoneNumber</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("emergency_room_phone_number")]
		public string EmergencyRoomPhoneNumberValue
		{
			get { return EmergencyRoomPhoneNumber.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: MainEmailAddress</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("main_email_address")]
		public virtual ITextFieldWrapper MainEmailAddress
		{
			get { return GetField<TextFieldWrapper>("Main Email Address", "main_email_address"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: MainEmailAddress</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("main_email_address")]
		public string MainEmailAddressValue
		{
			get { return MainEmailAddress.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: MainPhoneNumber</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("main_phone_number")]
		public virtual ITextFieldWrapper MainPhoneNumber
		{
			get { return GetField<TextFieldWrapper>("Main Phone Number", "main_phone_number"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: MainPhoneNumber</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("main_phone_number")]
		public string MainPhoneNumberValue
		{
			get { return MainPhoneNumber.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: Title</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("title")]
		public virtual ITextFieldWrapper Title
		{
			get { return GetField<TextFieldWrapper>("Title", "title"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: Title</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("title")]
		public string TitleValue
		{
			get { return Title.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: MainDescription</para><para>Data type: Rich Text</para></summary>
		[IndexField("main_description")]
		public virtual IRichTextFieldWrapper MainDescription
		{
			get { return GetField<RichTextFieldWrapper>("Main Description", "main_description"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: MainDescription</para><para>Data type: Rich Text</para></summary>
		[IndexField("main_description")]
		public string MainDescriptionValue
		{
			get { return MainDescription.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: HoursInfoMessage</para><para>Data type: Rich Text</para></summary>
		[IndexField("hours_info_message")]
		public virtual IRichTextFieldWrapper HoursInfoMessage
		{
			get { return GetField<RichTextFieldWrapper>("Hours Info Message", "hours_info_message"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: HoursInfoMessage</para><para>Data type: Rich Text</para></summary>
		[IndexField("hours_info_message")]
		public string HoursInfoMessageValue
		{
			get { return HoursInfoMessage.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: HoursOfOperation</para><para>Data type: Name Value List</para></summary>
		public virtual INameValueListFieldWrapper HoursOfOperation
		{
			get { return GetField<NameValueListFieldWrapper>("Hours of Operation"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: HoursOfOperation</para><para>Data type: Name Value List</para></summary>
		public System.Collections.Specialized.NameValueCollection HoursOfOperationValue
		{
			get { return HoursOfOperation.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: HoursSectionTitle</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("hours_section_title")]
		public virtual ITextFieldWrapper HoursSectionTitle
		{
			get { return GetField<TextFieldWrapper>("Hours Section Title", "hours_section_title"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: HoursSectionTitle</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("hours_section_title")]
		public string HoursSectionTitleValue
		{
			get { return HoursSectionTitle.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: ShowLocationHours</para><para>Data type: Checkbox</para></summary>
		[IndexField("show_location_hours")]
		public virtual IBooleanFieldWrapper ShowLocationHours
		{
			get { return GetField<BooleanFieldWrapper>("Show Location Hours", "show_location_hours"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: ShowLocationHours</para><para>Data type: Checkbox</para></summary>
		[IndexField("show_location_hours")]
		public bool ShowLocationHoursValue
		{
			get { return ShowLocationHours.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: ERWaitURLKey</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("er_wait_url_key")]
		public virtual ITextFieldWrapper ERWaitURLKey
		{
			get { return GetField<TextFieldWrapper>("ER Wait URL Key", "er_wait_url_key"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: ERWaitURLKey</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("er_wait_url_key")]
		public string ERWaitURLKeyValue
		{
			get { return ERWaitURLKey.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: AlternateLocationListingPage</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("alternate_location_listing_page")]
		public virtual ITextFieldWrapper AlternateLocationListingPage
		{
			get { return GetField<TextFieldWrapper>("Alternate Location Listing Page", "alternate_location_listing_page"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: AlternateLocationListingPage</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("alternate_location_listing_page")]
		public string AlternateLocationListingPageValue
		{
			get { return AlternateLocationListingPage.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: EnableAlternateLink</para><para>Data type: Checkbox</para></summary>
		[IndexField("enable_alternate_link")]
		public virtual IBooleanFieldWrapper EnableAlternateLink
		{
			get { return GetField<BooleanFieldWrapper>("Enable Alternate Link", "enable_alternate_link"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: EnableAlternateLink</para><para>Data type: Checkbox</para></summary>
		[IndexField("enable_alternate_link")]
		public bool EnableAlternateLinkValue
		{
			get { return EnableAlternateLink.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: OverrideWithMapsLink</para><para>Data type: Checkbox</para></summary>
		[IndexField("override_with_maps_link")]
		public virtual IBooleanFieldWrapper OverrideWithMapsLink
		{
			get { return GetField<BooleanFieldWrapper>("Override With Maps Link", "override_with_maps_link"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: OverrideWithMapsLink</para><para>Data type: Checkbox</para></summary>
		[IndexField("override_with_maps_link")]
		public bool OverrideWithMapsLinkValue
		{
			get { return OverrideWithMapsLink.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: SchedulingCode</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("scheduling_code")]
		public virtual ITextFieldWrapper SchedulingCode
		{
			get { return GetField<TextFieldWrapper>("Scheduling Code", "scheduling_code"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: SchedulingCode</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("scheduling_code")]
		public string SchedulingCodeValue
		{
			get { return SchedulingCode.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: SchedulingLabel</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("scheduling_label")]
		public virtual ITextFieldWrapper SchedulingLabel
		{
			get { return GetField<TextFieldWrapper>("Scheduling Label", "scheduling_label"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: SchedulingLabel</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("scheduling_label")]
		public string SchedulingLabelValue
		{
			get { return SchedulingLabel.Value; }
		}
		/// <summary><para>Template: Location Item</para><para>Field: SchedulingType</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("scheduling_type")]
		public virtual ITextFieldWrapper SchedulingType
		{
			get { return GetField<TextFieldWrapper>("Scheduling Type", "scheduling_type"); }
		}

		/// <summary><para>Template: Location Item</para><para>Field: SchedulingType</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("scheduling_type")]
		public string SchedulingTypeValue
		{
			get { return SchedulingType.Value; }
		}
	
	}
	public class LocationItemTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct LocationItem
	    {
	    	public static ID ID = ID.Parse("{B0006FC0-D788-44E5-9E28-698AC9AF1A72}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
						public static readonly ID CancerTypesJson = new ID("{8A76DC86-0436-4E6C-9E0C-939575B1491B}");
						public static readonly ID ServicesProvidedJson = new ID("{35F5D718-2796-4509-81C4-1DDDA913155B}");
						public static readonly ID AddressLine1 = new ID("{65E4A4B0-F3C9-4165-898A-47C0E2447E89}");
						public static readonly ID AddressLine2 = new ID("{442D1707-B401-4025-88BB-743E4F20EAA2}");
						public static readonly ID City = new ID("{898890D8-9D1B-49DC-A1E9-1B10BD2B106D}");
						public static readonly ID Coordinates = new ID("{0B26459A-0FB8-446D-98B9-32D05114BA61}");
						public static readonly ID Latitude = new ID("{D7BBC520-58BF-43D4-BD59-9B3598A73C10}");
						public static readonly ID LocationName = new ID("{3D452702-F00C-465C-8A51-AE2A1568C700}");
						public static readonly ID Longitude = new ID("{5AE39BA1-76EA-440B-B105-4577847BCFF4}");
						public static readonly ID PostalCode = new ID("{07EB1B84-51BA-4906-BD67-7DDBCFC5F75F}");
						public static readonly ID ShortName = new ID("{581E3789-5C6B-48B4-9BED-98E19614F377}");
						public static readonly ID State = new ID("{63C9665A-2DE5-4C82-B1CE-EE9BBC5D5081}");
						public static readonly ID TermName = new ID("{3868F97A-6944-45A0-9CA5-F53C0849108E}");
						public static readonly ID ClientID = new ID("{CD06DF25-D099-4A6D-A877-D6FF3353F502}");
						public static readonly ID ImportProperties = new ID("{7B3A8B13-C3A6-4322-A160-5049646A6962}");
						public static readonly ID ImportTaxonomy = new ID("{12419BB4-971D-4282-87FD-3954E326CA04}");
						public static readonly ID IsActive = new ID("{AE1F4E11-7103-457F-B8E4-5191EBF4F256}");
						public static readonly ID IsBMG = new ID("{D5AC0D3C-267A-4B0E-91D8-12E02F60E903}");
						public static readonly ID IsPubliclySearchable = new ID("{8E4C8001-BC00-4770-B05E-2A8BA44FD2E3}");
						public static readonly ID LocationType = new ID("{B4AE7EA9-D220-4C28-BA5B-3B9FA648931B}");
						public static readonly ID AdditionalContactInformation = new ID("{80895AF3-4B4D-4CAA-B1A0-0A6E03EF97D6}");
						public static readonly ID EmergencyRoomPhoneNumber = new ID("{DA6AA1EF-ABC7-42F7-9C57-2B207A93F1BA}");
						public static readonly ID MainEmailAddress = new ID("{B542FB6F-18AD-4C9B-B1B7-530E4D3CA774}");
						public static readonly ID MainPhoneNumber = new ID("{6B9FF352-B715-4EA9-B02F-CE4A8E02B23D}");
						public static readonly ID Title = new ID("{E16F9295-5F4F-42EB-926B-1A983DE2D46F}");
						public static readonly ID MainDescription = new ID("{BC6001DD-5F66-4E14-9117-5784D85FD8CA}");
						public static readonly ID HoursInfoMessage = new ID("{A670A6E2-09E3-4408-B95D-54A51BDE8879}");
						public static readonly ID HoursofOperation = new ID("{9C4F23A9-622D-4B82-B087-D0FFD67955B7}");
						public static readonly ID HoursSectionTitle = new ID("{88AD1738-A4D9-4F05-A7AF-3A78C25BFAE0}");
						public static readonly ID ShowLocationHours = new ID("{DC42CEE4-32CC-403C-BD44-5E5BB5AACEEB}");
						public static readonly ID ERWaitURLKey = new ID("{A7C99197-EF36-47C6-8BE3-2C399A88BA57}");
						public static readonly ID AlternateLocationListingPage = new ID("{EFDA30B1-BF61-41C0-9233-421458BEC5F0}");
						public static readonly ID EnableAlternateLink = new ID("{18A033F7-5202-420A-BB5E-BE2B18D65671}");
						public static readonly ID OverrideWithMapsLink = new ID("{669FDF0E-2911-4E38-B662-B4BC3FAFE51D}");
						public static readonly ID SchedulingCode = new ID("{97B69736-924C-424D-85B4-BD87D8ADC6B6}");
						public static readonly ID SchedulingLabel = new ID("{85176018-B57A-42C6-BD27-1DA1D171522F}");
						public static readonly ID SchedulingType = new ID("{2E22E4CA-12DF-471E-A3D0-CA8415C484D9}");
				
	        }
	    }
	}

	public class LocationItemRenderingModel : RenderingModelBase
	{
				
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: CancerTypesJson</para><para>Data type: Multi-Line Text</para>
			/// </summary>
			[IndexField("cancer_types_json")]
				 public virtual string CancerTypesJson { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: ServicesProvidedJson</para><para>Data type: Multi-Line Text</para>
			/// </summary>
			[IndexField("services_provided_json")]
				 public virtual string ServicesProvidedJson { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: AddressLine1</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("address_line_1")]
				 public virtual string AddressLine1 { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: AddressLine2</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("address_line_2")]
				 public virtual string AddressLine2 { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: City</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("city")]
				 public virtual string City { get; set; }
			
				 public virtual string Coordinates { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: Latitude</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("latitude")]
				 public virtual string Latitude { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: LocationName</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("location_name")]
				 public virtual string LocationName { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: Longitude</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("longitude")]
				 public virtual string Longitude { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: PostalCode</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("postal_code")]
				 public virtual string PostalCode { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: ShortName</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("short_name")]
				 public virtual string ShortName { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: State</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("state")]
				 public virtual string State { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: TermName</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("term_name")]
				 public virtual string TermName { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: ClientID</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("clientid")]
				 public virtual string ClientID { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: ImportProperties</para><para>Data type: Multi-Line Text</para>
			/// </summary>
			[IndexField("import_properties")]
				 public virtual string ImportProperties { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: ImportTaxonomy</para><para>Data type: Multi-Line Text</para>
			/// </summary>
			[IndexField("import_taxonomy")]
				 public virtual string ImportTaxonomy { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: IsActive</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("is_active")]
				 public virtual bool IsActive { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: IsBMG</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("is_bmg")]
				 public virtual bool IsBMG { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: IsPubliclySearchable</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("is_publicly_searchable")]
				 public virtual bool IsPubliclySearchable { get; set; }
			
				 public virtual string LocationType { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: AdditionalContactInformation</para><para>Data type: Rich Text</para>
			/// </summary>
			[IndexField("additional_contact_information")]
				 public virtual string AdditionalContactInformation { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: EmergencyRoomPhoneNumber</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("emergency_room_phone_number")]
				 public virtual string EmergencyRoomPhoneNumber { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: MainEmailAddress</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("main_email_address")]
				 public virtual string MainEmailAddress { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: MainPhoneNumber</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("main_phone_number")]
				 public virtual string MainPhoneNumber { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: Title</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("title")]
				 public virtual string Title { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: MainDescription</para><para>Data type: Rich Text</para>
			/// </summary>
			[IndexField("main_description")]
				 public virtual string MainDescription { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: HoursInfoMessage</para><para>Data type: Rich Text</para>
			/// </summary>
			[IndexField("hours_info_message")]
				 public virtual string HoursInfoMessage { get; set; }
			
				 public virtual System.Collections.Specialized.NameValueCollection HoursOfOperation { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: HoursSectionTitle</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("hours_section_title")]
				 public virtual string HoursSectionTitle { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: ShowLocationHours</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("show_location_hours")]
				 public virtual bool ShowLocationHours { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: ERWaitURLKey</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("er_wait_url_key")]
				 public virtual string ERWaitURLKey { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: AlternateLocationListingPage</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("alternate_location_listing_page")]
				 public virtual string AlternateLocationListingPage { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: EnableAlternateLink</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("enable_alternate_link")]
				 public virtual bool EnableAlternateLink { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: OverrideWithMapsLink</para><para>Data type: Checkbox</para>
			/// </summary>
			[IndexField("override_with_maps_link")]
				 public virtual bool OverrideWithMapsLink { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: SchedulingCode</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("scheduling_code")]
				 public virtual string SchedulingCode { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: SchedulingLabel</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("scheduling_label")]
				 public virtual string SchedulingLabel { get; set; }
			
				/// <summary>
			/// <para>Template: Location Item</para><para>Field: SchedulingType</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("scheduling_type")]
				 public virtual string SchedulingType { get; set; }
		
	}

}


#endregion
#region Specialties Folder (Custom)
namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Specialties Folder</para>
	/// <para>ID: {B9E6D995-7F54-4030-9647-2199B83C8C73}</para>
	/// <para>/sitecore/templates/Feature/Experience Accelerator/Medical Center Locations/Specialties Folder</para>
	/// </summary>
	[TemplateMapping("{B9E6D995-7F54-4030-9647-2199B83C8C73}", "InterfaceMap")]
	public partial interface ISpecialtiesFolderItem : IItemWrapper 
	{		
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Feature/Experience Accelerator/Medical Center Locations/Specialties Folder</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{B9E6D995-7F54-4030-9647-2199B83C8C73}", typeof(Guid))]
	[TemplateMapping("{B9E6D995-7F54-4030-9647-2199B83C8C73}", "")]
	public partial class SpecialtiesFolderItem : ItemWrapper, ISpecialtiesFolderItem
	{
		private Item _item = null;

		public SpecialtiesFolderItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public SpecialtiesFolderItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public SpecialtiesFolderItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public SpecialtiesFolderItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

	
	}
	public class SpecialtiesFolderTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct SpecialtiesFolder
	    {
	    	public static ID ID = ID.Parse("{B9E6D995-7F54-4030-9647-2199B83C8C73}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
				
	        }
	    }
	}

	public class SpecialtiesFolderRenderingModel : RenderingModelBase
	{
			
	}

}


#endregion
#region Specialty Facilities Folder (Custom)
namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Specialty Facilities Folder</para>
	/// <para>ID: {CE983DC1-D131-4E57-8FD3-3574CB542E79}</para>
	/// <para>/sitecore/templates/Feature/Experience Accelerator/Medical Center Locations/Specialty Facilities Folder</para>
	/// </summary>
	[TemplateMapping("{CE983DC1-D131-4E57-8FD3-3574CB542E79}", "InterfaceMap")]
	public partial interface ISpecialtyFacilitiesFolderItem : IItemWrapper 
	{		
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Feature/Experience Accelerator/Medical Center Locations/Specialty Facilities Folder</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{CE983DC1-D131-4E57-8FD3-3574CB542E79}", typeof(Guid))]
	[TemplateMapping("{CE983DC1-D131-4E57-8FD3-3574CB542E79}", "")]
	public partial class SpecialtyFacilitiesFolderItem : ItemWrapper, ISpecialtyFacilitiesFolderItem
	{
		private Item _item = null;

		public SpecialtyFacilitiesFolderItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public SpecialtyFacilitiesFolderItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public SpecialtyFacilitiesFolderItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public SpecialtyFacilitiesFolderItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

	
	}
	public class SpecialtyFacilitiesFolderTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct SpecialtyFacilitiesFolder
	    {
	    	public static ID ID = ID.Parse("{CE983DC1-D131-4E57-8FD3-3574CB542E79}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
				
	        }
	    }
	}

	public class SpecialtyFacilitiesFolderRenderingModel : RenderingModelBase
	{
			
	}

}


#endregion
#region Hospital Item (Custom)
namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Hospital Item</para>
	/// <para>ID: {F59C4777-FDFE-4C3F-937C-A2F40497B249}</para>
	/// <para>/sitecore/templates/Feature/Experience Accelerator/Medical Center Locations/Composites/Datasource/Hospital Item</para>
	/// </summary>
	[TemplateMapping("{F59C4777-FDFE-4C3F-937C-A2F40497B249}", "InterfaceMap")]
	public partial interface IHospitalItemItem : IItemWrapper , Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom.ILocationItemItem
	{		
		/// <summary>
		/// <para>Template: Hospital Item</para><para>Field: CheerCardEmailAddress</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("cheer_card_email_address")]
		ITextFieldWrapper CheerCardEmailAddress { get; }

		/// <summary>
		/// <para>Template: Hospital Item</para><para>Field: CheerCardEmailAddress</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("cheer_card_email_address")]
		string CheerCardEmailAddressValue { get; }
		/// <summary>
		/// <para>Template: Hospital Item</para><para>Field: PreRegistrationURL</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("pre_registration_url")]
		ITextFieldWrapper PreRegistrationURL { get; }

		/// <summary>
		/// <para>Template: Hospital Item</para><para>Field: PreRegistrationURL</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("pre_registration_url")]
		string PreRegistrationURLValue { get; }
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Feature/Experience Accelerator/Medical Center Locations/Composites/Datasource/Hospital Item</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{F59C4777-FDFE-4C3F-937C-A2F40497B249}", typeof(Guid))]
	[TemplateMapping("{F59C4777-FDFE-4C3F-937C-A2F40497B249}", "")]
	public partial class HospitalItemItem : ItemWrapper, IHospitalItemItem
	{
		private Item _item = null;

		public HospitalItemItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public HospitalItemItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public HospitalItemItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public HospitalItemItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: CancerTypesJson</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("cancer_types_json")]
		public virtual ITextFieldWrapper CancerTypesJson
		{
			get { return GetField<TextFieldWrapper>("Cancer Types Json", "cancer_types_json"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: CancerTypesJson</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("cancer_types_json")]
		public string CancerTypesJsonValue
		{
			get { return CancerTypesJson.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: ServicesProvidedJson</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("services_provided_json")]
		public virtual ITextFieldWrapper ServicesProvidedJson
		{
			get { return GetField<TextFieldWrapper>("Services Provided Json", "services_provided_json"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: ServicesProvidedJson</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("services_provided_json")]
		public string ServicesProvidedJsonValue
		{
			get { return ServicesProvidedJson.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: AddressLine1</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("address_line_1")]
		public virtual ITextFieldWrapper AddressLine1
		{
			get { return GetField<TextFieldWrapper>("Address Line 1", "address_line_1"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: AddressLine1</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("address_line_1")]
		public string AddressLine1Value
		{
			get { return AddressLine1.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: AddressLine2</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("address_line_2")]
		public virtual ITextFieldWrapper AddressLine2
		{
			get { return GetField<TextFieldWrapper>("Address Line 2", "address_line_2"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: AddressLine2</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("address_line_2")]
		public string AddressLine2Value
		{
			get { return AddressLine2.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: City</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("city")]
		public virtual ITextFieldWrapper City
		{
			get { return GetField<TextFieldWrapper>("City", "city"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: City</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("city")]
		public string CityValue
		{
			get { return City.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: Coordinates</para><para>Data type: LatLon</para></summary>
		public virtual ITextFieldWrapper Coordinates
		{
			get { return GetField<TextFieldWrapper>("Coordinates"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: Coordinates</para><para>Data type: LatLon</para></summary>
		public string CoordinatesValue
		{
			get { return Coordinates.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: Latitude</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("latitude")]
		public virtual ITextFieldWrapper Latitude
		{
			get { return GetField<TextFieldWrapper>("Latitude", "latitude"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: Latitude</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("latitude")]
		public string LatitudeValue
		{
			get { return Latitude.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: LocationName</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("location_name")]
		public virtual ITextFieldWrapper LocationName
		{
			get { return GetField<TextFieldWrapper>("Location Name", "location_name"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: LocationName</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("location_name")]
		public string LocationNameValue
		{
			get { return LocationName.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: Longitude</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("longitude")]
		public virtual ITextFieldWrapper Longitude
		{
			get { return GetField<TextFieldWrapper>("Longitude", "longitude"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: Longitude</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("longitude")]
		public string LongitudeValue
		{
			get { return Longitude.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: PostalCode</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("postal_code")]
		public virtual ITextFieldWrapper PostalCode
		{
			get { return GetField<TextFieldWrapper>("Postal Code", "postal_code"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: PostalCode</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("postal_code")]
		public string PostalCodeValue
		{
			get { return PostalCode.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: ShortName</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("short_name")]
		public virtual ITextFieldWrapper ShortName
		{
			get { return GetField<TextFieldWrapper>("Short Name", "short_name"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: ShortName</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("short_name")]
		public string ShortNameValue
		{
			get { return ShortName.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: State</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("state")]
		public virtual ITextFieldWrapper State
		{
			get { return GetField<TextFieldWrapper>("State", "state"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: State</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("state")]
		public string StateValue
		{
			get { return State.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: TermName</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("term_name")]
		public virtual ITextFieldWrapper TermName
		{
			get { return GetField<TextFieldWrapper>("Term Name", "term_name"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: TermName</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("term_name")]
		public string TermNameValue
		{
			get { return TermName.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: ClientID</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("clientid")]
		public virtual ITextFieldWrapper ClientID
		{
			get { return GetField<TextFieldWrapper>("ClientID", "clientid"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: ClientID</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("clientid")]
		public string ClientIDValue
		{
			get { return ClientID.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: ImportProperties</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("import_properties")]
		public virtual ITextFieldWrapper ImportProperties
		{
			get { return GetField<TextFieldWrapper>("Import Properties", "import_properties"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: ImportProperties</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("import_properties")]
		public string ImportPropertiesValue
		{
			get { return ImportProperties.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: ImportTaxonomy</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("import_taxonomy")]
		public virtual ITextFieldWrapper ImportTaxonomy
		{
			get { return GetField<TextFieldWrapper>("Import Taxonomy", "import_taxonomy"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: ImportTaxonomy</para><para>Data type: Multi-Line Text</para></summary>
		[IndexField("import_taxonomy")]
		public string ImportTaxonomyValue
		{
			get { return ImportTaxonomy.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: IsActive</para><para>Data type: Checkbox</para></summary>
		[IndexField("is_active")]
		public virtual IBooleanFieldWrapper IsActive
		{
			get { return GetField<BooleanFieldWrapper>("Is Active", "is_active"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: IsActive</para><para>Data type: Checkbox</para></summary>
		[IndexField("is_active")]
		public bool IsActiveValue
		{
			get { return IsActive.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: IsBMG</para><para>Data type: Checkbox</para></summary>
		[IndexField("is_bmg")]
		public virtual IBooleanFieldWrapper IsBMG
		{
			get { return GetField<BooleanFieldWrapper>("Is BMG", "is_bmg"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: IsBMG</para><para>Data type: Checkbox</para></summary>
		[IndexField("is_bmg")]
		public bool IsBMGValue
		{
			get { return IsBMG.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: IsPubliclySearchable</para><para>Data type: Checkbox</para></summary>
		[IndexField("is_publicly_searchable")]
		public virtual IBooleanFieldWrapper IsPubliclySearchable
		{
			get { return GetField<BooleanFieldWrapper>("Is Publicly Searchable", "is_publicly_searchable"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: IsPubliclySearchable</para><para>Data type: Checkbox</para></summary>
		[IndexField("is_publicly_searchable")]
		public bool IsPubliclySearchableValue
		{
			get { return IsPubliclySearchable.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: LocationType</para><para>Data type: Droplist</para></summary>
		public virtual ITextFieldWrapper LocationType
		{
			get { return GetField<TextFieldWrapper>("Location Type"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: LocationType</para><para>Data type: Droplist</para></summary>
		public string LocationTypeValue
		{
			get { return LocationType.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: AdditionalContactInformation</para><para>Data type: Rich Text</para></summary>
		[IndexField("additional_contact_information")]
		public virtual IRichTextFieldWrapper AdditionalContactInformation
		{
			get { return GetField<RichTextFieldWrapper>("Additional Contact Information", "additional_contact_information"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: AdditionalContactInformation</para><para>Data type: Rich Text</para></summary>
		[IndexField("additional_contact_information")]
		public string AdditionalContactInformationValue
		{
			get { return AdditionalContactInformation.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: EmergencyRoomPhoneNumber</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("emergency_room_phone_number")]
		public virtual ITextFieldWrapper EmergencyRoomPhoneNumber
		{
			get { return GetField<TextFieldWrapper>("Emergency Room Phone Number", "emergency_room_phone_number"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: EmergencyRoomPhoneNumber</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("emergency_room_phone_number")]
		public string EmergencyRoomPhoneNumberValue
		{
			get { return EmergencyRoomPhoneNumber.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: MainEmailAddress</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("main_email_address")]
		public virtual ITextFieldWrapper MainEmailAddress
		{
			get { return GetField<TextFieldWrapper>("Main Email Address", "main_email_address"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: MainEmailAddress</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("main_email_address")]
		public string MainEmailAddressValue
		{
			get { return MainEmailAddress.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: MainPhoneNumber</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("main_phone_number")]
		public virtual ITextFieldWrapper MainPhoneNumber
		{
			get { return GetField<TextFieldWrapper>("Main Phone Number", "main_phone_number"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: MainPhoneNumber</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("main_phone_number")]
		public string MainPhoneNumberValue
		{
			get { return MainPhoneNumber.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: Title</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("title")]
		public virtual ITextFieldWrapper Title
		{
			get { return GetField<TextFieldWrapper>("Title", "title"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: Title</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("title")]
		public string TitleValue
		{
			get { return Title.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: MainDescription</para><para>Data type: Rich Text</para></summary>
		[IndexField("main_description")]
		public virtual IRichTextFieldWrapper MainDescription
		{
			get { return GetField<RichTextFieldWrapper>("Main Description", "main_description"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: MainDescription</para><para>Data type: Rich Text</para></summary>
		[IndexField("main_description")]
		public string MainDescriptionValue
		{
			get { return MainDescription.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: HoursInfoMessage</para><para>Data type: Rich Text</para></summary>
		[IndexField("hours_info_message")]
		public virtual IRichTextFieldWrapper HoursInfoMessage
		{
			get { return GetField<RichTextFieldWrapper>("Hours Info Message", "hours_info_message"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: HoursInfoMessage</para><para>Data type: Rich Text</para></summary>
		[IndexField("hours_info_message")]
		public string HoursInfoMessageValue
		{
			get { return HoursInfoMessage.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: HoursOfOperation</para><para>Data type: Name Value List</para></summary>
		public virtual INameValueListFieldWrapper HoursOfOperation
		{
			get { return GetField<NameValueListFieldWrapper>("Hours of Operation"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: HoursOfOperation</para><para>Data type: Name Value List</para></summary>
		public System.Collections.Specialized.NameValueCollection HoursOfOperationValue
		{
			get { return HoursOfOperation.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: HoursSectionTitle</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("hours_section_title")]
		public virtual ITextFieldWrapper HoursSectionTitle
		{
			get { return GetField<TextFieldWrapper>("Hours Section Title", "hours_section_title"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: HoursSectionTitle</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("hours_section_title")]
		public string HoursSectionTitleValue
		{
			get { return HoursSectionTitle.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: ShowLocationHours</para><para>Data type: Checkbox</para></summary>
		[IndexField("show_location_hours")]
		public virtual IBooleanFieldWrapper ShowLocationHours
		{
			get { return GetField<BooleanFieldWrapper>("Show Location Hours", "show_location_hours"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: ShowLocationHours</para><para>Data type: Checkbox</para></summary>
		[IndexField("show_location_hours")]
		public bool ShowLocationHoursValue
		{
			get { return ShowLocationHours.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: ERWaitURLKey</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("er_wait_url_key")]
		public virtual ITextFieldWrapper ERWaitURLKey
		{
			get { return GetField<TextFieldWrapper>("ER Wait URL Key", "er_wait_url_key"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: ERWaitURLKey</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("er_wait_url_key")]
		public string ERWaitURLKeyValue
		{
			get { return ERWaitURLKey.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: AlternateLocationListingPage</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("alternate_location_listing_page")]
		public virtual ITextFieldWrapper AlternateLocationListingPage
		{
			get { return GetField<TextFieldWrapper>("Alternate Location Listing Page", "alternate_location_listing_page"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: AlternateLocationListingPage</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("alternate_location_listing_page")]
		public string AlternateLocationListingPageValue
		{
			get { return AlternateLocationListingPage.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: EnableAlternateLink</para><para>Data type: Checkbox</para></summary>
		[IndexField("enable_alternate_link")]
		public virtual IBooleanFieldWrapper EnableAlternateLink
		{
			get { return GetField<BooleanFieldWrapper>("Enable Alternate Link", "enable_alternate_link"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: EnableAlternateLink</para><para>Data type: Checkbox</para></summary>
		[IndexField("enable_alternate_link")]
		public bool EnableAlternateLinkValue
		{
			get { return EnableAlternateLink.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: OverrideWithMapsLink</para><para>Data type: Checkbox</para></summary>
		[IndexField("override_with_maps_link")]
		public virtual IBooleanFieldWrapper OverrideWithMapsLink
		{
			get { return GetField<BooleanFieldWrapper>("Override With Maps Link", "override_with_maps_link"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: OverrideWithMapsLink</para><para>Data type: Checkbox</para></summary>
		[IndexField("override_with_maps_link")]
		public bool OverrideWithMapsLinkValue
		{
			get { return OverrideWithMapsLink.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: SchedulingCode</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("scheduling_code")]
		public virtual ITextFieldWrapper SchedulingCode
		{
			get { return GetField<TextFieldWrapper>("Scheduling Code", "scheduling_code"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: SchedulingCode</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("scheduling_code")]
		public string SchedulingCodeValue
		{
			get { return SchedulingCode.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: SchedulingLabel</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("scheduling_label")]
		public virtual ITextFieldWrapper SchedulingLabel
		{
			get { return GetField<TextFieldWrapper>("Scheduling Label", "scheduling_label"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: SchedulingLabel</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("scheduling_label")]
		public string SchedulingLabelValue
		{
			get { return SchedulingLabel.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: SchedulingType</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("scheduling_type")]
		public virtual ITextFieldWrapper SchedulingType
		{
			get { return GetField<TextFieldWrapper>("Scheduling Type", "scheduling_type"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: SchedulingType</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("scheduling_type")]
		public string SchedulingTypeValue
		{
			get { return SchedulingType.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: CheerCardEmailAddress</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("cheer_card_email_address")]
		public virtual ITextFieldWrapper CheerCardEmailAddress
		{
			get { return GetField<TextFieldWrapper>("Cheer Card Email Address", "cheer_card_email_address"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: CheerCardEmailAddress</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("cheer_card_email_address")]
		public string CheerCardEmailAddressValue
		{
			get { return CheerCardEmailAddress.Value; }
		}
		/// <summary><para>Template: Hospital Item</para><para>Field: PreRegistrationURL</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("pre_registration_url")]
		public virtual ITextFieldWrapper PreRegistrationURL
		{
			get { return GetField<TextFieldWrapper>("Pre Registration URL", "pre_registration_url"); }
		}

		/// <summary><para>Template: Hospital Item</para><para>Field: PreRegistrationURL</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("pre_registration_url")]
		public string PreRegistrationURLValue
		{
			get { return PreRegistrationURL.Value; }
		}
	
	}
	public class HospitalItemTemplate {

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct HospitalItem
	    {
	    	public static ID ID = ID.Parse("{F59C4777-FDFE-4C3F-937C-A2F40497B249}");

	    	[StructLayout(LayoutKind.Sequential, Size = 1)]    
	        public struct Fields
	        {
						public static readonly ID CheerCardEmailAddress = new ID("{37774F63-B60C-4522-A7ED-5ECA820F12EC}");
						public static readonly ID PreRegistrationURL = new ID("{E152DC04-07E0-40F8-9CE7-93A031391EEE}");
				
	        }
	    }
	}

	public class HospitalItemRenderingModel : RenderingModelBase
	{
				
				/// <summary>
			/// <para>Template: Hospital Item</para><para>Field: CheerCardEmailAddress</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("cheer_card_email_address")]
				 public virtual string CheerCardEmailAddress { get; set; }
			
				/// <summary>
			/// <para>Template: Hospital Item</para><para>Field: PreRegistrationURL</para><para>Data type: Single-Line Text</para>
			/// </summary>
			[IndexField("pre_registration_url")]
				 public virtual string PreRegistrationURL { get; set; }
		
	}

}


#endregion
