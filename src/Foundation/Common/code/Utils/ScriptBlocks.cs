﻿using Sitecore.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Sitecore.Mvc;
using Sitecore.Layouts;
using System.Web.WebPages;
using System.Collections;

namespace Sitecore.Baptist.Foundation.Common.Utils
{
    public class ScriptBlock : IDisposable
    {
        private readonly WebViewPage _webViewPage;
        private readonly ID _renderingUniqueId;

        public static string GetCacheKey( ID renderingUniqueId )
        {
            return String.Concat("SCRIPT_BLOCKS_", renderingUniqueId);
        }

        public ScriptBlock( WebViewPage webPageBase, ID renderingUniqueId )
        {
            _webViewPage = webPageBase;
            _webViewPage.OutputStack.Push(new StringWriter());
            _renderingUniqueId = renderingUniqueId;
        }

        public void Dispose()
        {
            string markup = _webViewPage.OutputStack.Pop().ToString();
            string cacheKey = GetCacheKey(_renderingUniqueId);
            string cacheMarkup = _webViewPage.Context.Items[cacheKey] != null ?
                _webViewPage.Context.Items[cacheKey].ToString() :
                string.Empty;

            cacheMarkup = !string.IsNullOrEmpty(cacheMarkup) ?
                string.Concat(cacheMarkup, Environment.NewLine, markup) :
                markup;

            _webViewPage.Context.Items[cacheKey] = cacheMarkup;
            Sitecore.Context.Site.Caches.HtmlCache.SetHtml(cacheKey, cacheMarkup);
        }
    }

    public static class HtmlHelperExtensions
    {
        public static IDisposable BeginScriptBlock( this HtmlHelper helper )
        {
            return new ScriptBlock((WebViewPage)helper.ViewDataContainer,
                new ID(helper.Sitecore().CurrentRendering.UniqueId));
        }

        public static MvcHtmlString RenderScriptBlocks( this HtmlHelper helper )
        {
            RenderingReference[] renderings =
                helper.Sitecore().CurrentItem.Visualization.GetRenderings(Sitecore.Context.Device, true);

            List<string> scriptBlocks = renderings
                .Select(rendering => ScriptBlock.GetCacheKey(new ID(rendering.UniqueId)))
                .Select(cacheKey => Sitecore.Context.Site.Caches.HtmlCache.GetHtml(cacheKey))
                .Where(markup => !String.IsNullOrEmpty(markup))
                .ToList();

            return MvcHtmlString.Create(String.Join(Environment.NewLine, scriptBlocks));
        }

    } 

    public static class HtmlExtensions
    {
       /* public static MvcHtmlString HeadTag( this HtmlHelper htmlHelper, Func<object, HelperResult> template )
        {
            htmlHelper.ViewContext.HttpContext.Items["_headtag_" + Guid.NewGuid()] = template;
            return MvcHtmlString.Empty;
        }

        public static IHtmlString RenderHeadTags( this HtmlHelper htmlHelper )
        {
            foreach(object key in htmlHelper.ViewContext.HttpContext.Items.Keys)
            {
                if(key.ToString().StartsWith("_headtag_"))
                {
                    var template = htmlHelper.ViewContext.HttpContext.Items[key] as Func<object, HelperResult>;
                    if(template != null)
                    {
                        htmlHelper.ViewContext.Writer.Write(template(null));
                    }
                }
            }
            return MvcHtmlString.Empty;
        }*/

        public static MvcHtmlString Script( this HtmlHelper htmlHelper, Func<object, HelperResult> template )
        {
        
            htmlHelper.ViewContext.HttpContext.Items["_script_" + Guid.NewGuid()] = template;
            return MvcHtmlString.Empty;
        }

        public static IHtmlString RenderScripts( this HtmlHelper htmlHelper )
        {

            HttpContextBase contexts = htmlHelper.ViewContext.HttpContext;
            List<Func<object, HelperResult>> templates = new List<Func<object, HelperResult>>();
            Func<object, HelperResult> template = null;
            foreach(object key in contexts.Items.Keys) {
                if(key.ToString().StartsWith("_script_"))
                {
                    template = contexts.Items[key] as Func<object, HelperResult>;
                    templates.Add(template);
                }
            }

            foreach(Func<object, HelperResult> templt in templates)
            {
                if(templt != null)
                {
                    htmlHelper.ViewContext.Writer.Write(templt(null));
                }
            }
            return MvcHtmlString.Empty;
        }

        
    }
}