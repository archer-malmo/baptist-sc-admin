﻿using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Models;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Models;

namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Repositories
{
    public class HospitalItemRepository : LocationItemRepository, IHospitalItemRepository, IModelRepository, IAbstractRepository<IRenderingModelBase>
    {
        public override IRenderingModelBase GetModel()
        {
            HospitalItemRenderingModel hirm = new HospitalItemRenderingModel();
            this.FillBaseProperties((object)hirm);

            //set location section fields            
            hirm.LocationName = this.GetLocationName();
            hirm.TermName = this.GetTermName();
            hirm.ShortName = this.GetShortName();
            hirm.AddressLine1 = this.GetAddressLine1();
            hirm.AddressLine2 = this.GetAddressLine2();
            hirm.City = this.GetCity();
            hirm.State = this.GetState();
            hirm.PostalCode = this.GetPostalCode();
            hirm.Latitude = this.GetLatitude();
            hirm.Longitude = this.GetLongitude();
            hirm.Coordinates = this.GetCoordinates();

            //set location meta section
            hirm.ClientID = this.GetClientID();
            hirm.IsPubliclySearchable = this.GetIsPubliclySearchable();
            hirm.IsActive = this.GetIsActive();
            hirm.IsBMG = this.GetIsBMG();
            hirm.ImportTaxonomy = this.GetImportTaxonomy();
            hirm.ImportProperties = this.GetImportProperties();
            hirm.LocationType = this.GetLocationType();

            //set location contact section
            hirm.MainPhoneNumber = this.GetMainPhoneNumber();
            hirm.MainEmailAddress = this.GetMainEmailAddress();
            hirm.AdditionalContactInformation = this.GetAdditionalContactInformation();
            hirm.EmergencyRoomPhoneNumber = this.GetEmergencyRoomPhoneNumber();

            //set location content section
            hirm.Title = this.GetTitle();
            hirm.MainDescription = this.GetMainDescription();

            //set location hours 
            hirm.HoursSectionTitle = this.GetHoursSectionTitle();
            hirm.HoursOfOperation = this.GetHoursOfOperation();
            hirm.HoursInfoMessage = this.GetHoursInfoMessage();
            hirm.ShowLocationHours = this.GetShowLocationHours();


            //set location list overrides
            hirm.AlternateLocationListingPage = this.GetAlternateLocationListingPage();
            hirm.EnableAlternateLink = this.GetEnableAlternateLink();
            hirm.OverrideWithMapsLink = this.GetOverrideWithMapsLink();

            //set hospital specific items
            hirm.CheerCardEmailAddress = this.GetCheerCardEmailAddress();
            hirm.PreRegistrationURL = this.GetPreRegistraionUrl();

            //ER Settings
            hirm.ERWaitURLKey = this.GetERWaitURLKey();

            //Baptist Cancer Centers
            hirm.ServicesProvidedJson = this.GetServicesProvidedJson();
            hirm.CancerTypesJson = this.GetCancerTypesJson();

            //Scheduling
            hirm.SchedulingType = this.GetSchedulingType();
            hirm.SchedulingLabel = this.GetSchedulingLabel();
            hirm.SchedulingCode = this.GetSchedulingCode();

            return hirm;
        }

        protected virtual string GetCheerCardEmailAddress()
        {
            if(this.LocationItem != null)
                return this.LocationItem[MedicalCenterLocations.Templates.Custom.HospitalItemTemplate.HospitalItem.Fields.CheerCardEmailAddress] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[MedicalCenterLocations.Templates.Custom.HospitalItemTemplate.HospitalItem.Fields.CheerCardEmailAddress] ?? string.Empty;
            return string.Empty;
        }

        protected virtual string GetPreRegistraionUrl()
        {
            if(this.LocationItem != null)
                return this.LocationItem[MedicalCenterLocations.Templates.Custom.HospitalItemTemplate.HospitalItem.Fields.PreRegistrationURL] ?? string.Empty;
            else if(this.Rendering.DataSourceItem != null)
                return this.Rendering.DataSourceItem[MedicalCenterLocations.Templates.Custom.HospitalItemTemplate.HospitalItem.Fields.PreRegistrationURL] ?? string.Empty;
            return string.Empty;
        }    
    }
}