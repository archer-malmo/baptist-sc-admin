﻿using System.Web.Mvc;

namespace Sitecore.Baptist.Feature.DemoRendering.Areas.Feature_DemoRendering
{
    public class Feature_DemoRenderingAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Feature_DemoRendering";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Feature_DemoRendering_default",
                "Feature_DemoRendering/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}