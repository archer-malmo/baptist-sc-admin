using Sitecore.Data.Items;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq.Common;
#region Demo Rendering (Custom)
namespace Sitecore.Baptist.Feature.DemoRendering.Templates.Custom
{
	using System;
	using System.Collections.Generic;
	using Fortis.Model;
	using Fortis.Model.Fields;
	using Fortis.Providers;


	/// <summary>
	/// <para>Template interface</para>
	/// <para>Template: Demo Rendering</para>
	/// <para>ID: {787F280D-1049-4AA3-BC98-E2603C44AAD9}</para>
	/// <para>/sitecore/templates/Feature/Experience Accelerator/DemoRendering/Demo Rendering</para>
	/// </summary>
	[TemplateMapping("{787F280D-1049-4AA3-BC98-E2603C44AAD9}", "InterfaceMap")]
	public partial interface IDemoRenderingItem : IItemWrapper 
	{		
		/// <summary>
		/// <para>Template: Demo Rendering</para><para>Field: TestField</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("test_field")]
		ITextFieldWrapper TestField { get; }

		/// <summary>
		/// <para>Template: Demo Rendering</para><para>Field: TestField</para><para>Data type: Single-Line Text</para>
		/// </summary>
		[IndexField("test_field")]
		string TestFieldValue { get; }
	}

	/// <summary>
	/// <para>Template class</para><para>/sitecore/templates/Feature/Experience Accelerator/DemoRendering/Demo Rendering</para>
	/// </summary>
	[PredefinedQuery("TemplateId", ComparisonType.Equal, "{787F280D-1049-4AA3-BC98-E2603C44AAD9}", typeof(Guid))]
	[TemplateMapping("{787F280D-1049-4AA3-BC98-E2603C44AAD9}", "")]
	public partial class DemoRenderingItem : ItemWrapper, IDemoRenderingItem
	{
		private Item _item = null;

		public DemoRenderingItem(ISpawnProvider spawnProvider) : base(null, spawnProvider) { }

		public DemoRenderingItem(Guid id, ISpawnProvider spawnProvider) : base(id, spawnProvider) { }

		public DemoRenderingItem(Guid id, Dictionary<string, object> lazyFields, ISpawnProvider spawnProvider) : base(id, lazyFields, spawnProvider) { }

		public DemoRenderingItem(Item item, ISpawnProvider spawnProvider) : base(item, spawnProvider)
		{
			_item = item;
		}

		/// <summary><para>Template: Demo Rendering</para><para>Field: TestField</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("test_field")]
		public virtual ITextFieldWrapper TestField
		{
			get { return GetField<TextFieldWrapper>("Test field", "test_field"); }
		}

		/// <summary><para>Template: Demo Rendering</para><para>Field: TestField</para><para>Data type: Single-Line Text</para></summary>
		[IndexField("test_field")]
		public string TestFieldValue
		{
			get { return TestField.Value; }
		}
	
	}
}
#endregion
