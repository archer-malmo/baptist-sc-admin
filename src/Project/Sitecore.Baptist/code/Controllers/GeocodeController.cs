﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sitecore.Baptist.Foundation.Common.Utils;
using Newtonsoft.Json;
//using Google.Maps.Geocoding;

namespace Sitecore.Baptist.Website.Controllers
{
    public class GeocodeController : Controller
    {
        // GET: Geocode
        public ActionResult Index(string query)
        {
            var response = Geo.geocodeSearchCommon(query);

            //return Content(response.results.First().geometry.location.lat.ToString());

            return Content(JsonConvert.SerializeObject(response));

            //return View();
        }
    }
}