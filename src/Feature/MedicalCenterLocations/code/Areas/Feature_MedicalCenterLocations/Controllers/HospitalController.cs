﻿using Fortis.Model;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Models;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Repositories;
using Sitecore.Baptist.Feature.MedicalCenterLocations.Templates.Custom;
using Sitecore.XA.Foundation.Mvc.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Controllers
{
    public class HospitalController : StandardController
    {
        protected readonly ILocationItemRepository LocationRepository;

        public HospitalController( ILocationItemRepository locationRepository )
        {
            this.LocationRepository = locationRepository;
        }

        public ActionResult HospitalIndex()
        {
            return (ActionResult)this.PartialView("HospitalIndex", this.GetModel());
        }

        public ActionResult HospitalDetail()
        {
            return (ActionResult)this.PartialView("HospitalDetail", this.GetModel());
        }

        protected override object GetModel()
        {
            return (object)this.LocationRepository.GetModel();
        }
        
    }
}