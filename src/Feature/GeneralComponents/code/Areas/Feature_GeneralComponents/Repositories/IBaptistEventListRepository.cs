﻿using Sitecore.XA.Feature.Events.Repositories;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using Sitecore.XA.Foundation.RenderingVariants.Lists.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sitecore.Baptist.Feature.GeneralComponents.Areas.Feature_GeneralComponents.Repositories
{
    public interface IBaptistEventListRepository : IEventListRepository
    {        
    }
}