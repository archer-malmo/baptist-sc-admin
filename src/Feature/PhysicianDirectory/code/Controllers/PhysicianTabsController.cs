﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sitecore.XA.Feature.Composites.Repositories;
using Sitecore.XA.Foundation.Mvc.Controllers;

namespace Sitecore.Baptist.Feature.PhysicianDirectory.Controllers
{
    public class PhysicianTabsController : StandardController
    {
        public ITabsRepository TabsRepository { get; set; }

        public PhysicianTabsController( ITabsRepository tabsRepository )
        {
            this.TabsRepository = tabsRepository;
        }

        protected override object GetModel()
        {
            return (object)this.TabsRepository.GetModel();
        }

        public override ActionResult Index()
        {
            if(!Sitecore.SecurityModel.License.License.HasModule("Sitecore.SXA"))
                return (ActionResult)this.Redirect(string.Format("{0}?license=Sitecore.SXA", (object)Sitecore.Configuration.Settings.NoLicenseUrl));
            return (ActionResult)this.PartialView("Tabs", this.GetModel());
        }

    }
}