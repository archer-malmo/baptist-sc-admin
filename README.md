#Sitecore Habitat

Habitat is a Sitecore solution example built on a modular architecture.
The architecture and methodology focuses on:

* Simplicity - *A consistent and discoverable architecture*
* Flexibility - *Change and add quickly and without worry*
* Extensibility - *Simply add new features without steep learning curve*

For more information, please check out the [Habitat Wiki](../../wiki)

=================
Archer > Baptist
=================
-----------------
This project is an adaptation of [Sitecore Helix](http://helix.sitecore.net/) design principles loosely related [Sitecore Habitat](http://habitat.demo.sitecore.net/).  

Ultimately these are extendsion projects of the Sitecore Experience Accelerator as they were needed based around the Baptist Memorial Hospital integration.  For more information about Sitecore Experience Accelerator, please refer to the following links:

* [Documentation](https://doc.sitecore.net/sitecore_experience_accelerator)
* [Cognifide](http://www.cognifide.com/sxa-landing-page)

Additionally, to create the overall structure for the project and modules, this Yeoman generator is used extensively:

* [Prodigious Helix](https://www.npmjs.com/package/generator-prodigious-helix)
