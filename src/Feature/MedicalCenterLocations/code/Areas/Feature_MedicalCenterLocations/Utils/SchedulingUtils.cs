﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Baptist.Foundation.Common.Utils;
using System.Text.RegularExpressions;

namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Areas.Feature_MedicalCenterLocations.Utils
{
    public class SchedulingUtils
    {
        private string scheduling_url { get; }

        public static string getButtonText( string locationType, string schedulingLabel = null )
        {
            string omw = "On My Way";
            string hmp = "Hold My Place";

            string[] omtypes = { "Hospital" };
            string[] hmptypes = { "Minor Medical", "Minor Medical Center", "Clinic", "Specialty", "Cancer Center", "Outpatient Care" };

            if(schedulingLabel.HasValue() && schedulingLabel == "ONMYWAY") {
                return omw;
            }else if( schedulingLabel.HasValue() && schedulingLabel == "HOLDMYPLACE") {
                return hmp;
            }else if(omtypes.Contains(locationType)) {
                return omw;
            } else if(hmptypes.Contains(locationType)) {
                return hmp;
            }else {
                return "false";
            }
        }

        public static string getSchedulingUrl(string hostname, string schedulingCode)
        {

            string mychartenv = "tst";

            if(Regex.IsMatch(hostname,@"^www\.(baptistonline\.org|baptistcancercenter\.com|baptistdoctors\.org)"))
            {
                mychartenv = "prd";
            }

            return string.Format("https://mychart.bmhcc.org/{0}/Scheduling/OnMyWay/Widget?&vt=2205&public=1&dispgroups={1}", mychartenv, schedulingCode);

        }
    }
}