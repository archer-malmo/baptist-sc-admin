﻿using Sitecore.Baptist.Feature.MedicalCenterLocations.Models;
using Sitecore.XA.Foundation.Multisite;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using Sitecore.XA.Foundation.RenderingVariants.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sitecore.Baptist.Feature.MedicalCenterLocations.Repositories
{
  public class MedicalCenterLocationsRepository : VariantsRepository, IMedicalCenterLocationsRepository
  {
    public MedicalCenterLocationsRepository(IVariantsRepository variantsrepo, ISiteInfoResolver siteInfoResolver)
    {
      this.VarientsRepsoitory = variantsrepo;
      this.SiteInfoResolver = siteInfoResolver;
    }

    public ISiteInfoResolver SiteInfoResolver { get; set; }

    public IVariantsRepository VarientsRepsoitory { get; set; }


    public override IRenderingModelBase GetModel()
    {
      var medicalLocationsModel = new MedicalCenterLocationsModel();

      this.FillBaseProperties(medicalLocationsModel);

      medicalLocationsModel.StyleDisplay = !this.IsEdit ? "none" : "block";

      return medicalLocationsModel;
    }
  }  
  
}